---
title: ""
format: docx
editor: visual
bibliography: references.bib
---

```{r, echo=FALSE, out.width="50%",fig.align='center',fig.cap="Figure 1. 8ab Anchovy mean weight and length-at-age, PELGAS survey."}

knitr::opts_chunk$set(echo = FALSE,message=FALSE,warning = FALSE)
prefix='/media/mathieu/EchoSonde/Campagnes/'
#prefix='/home/mathieu/Documents/Campagnes/'
#prefix='D:/Campagnes/'
prefix='C:/Users/mdoray/Documents/Campagnes/'
cyear=2024
path.export.biomass.series=paste(prefix,"PELGAS/Data/Biomasse/Plots/",sep='')
path.export.mWLage.series=paste(prefix,"PELGAS/Data/Biotique/meanLength&Weights/",sep='')
path.map=paste0(prefix,"WGACEGG/Data/grids/AC_SPRING_IBBBN/",cyear,"/AC-SPRING-IBBBN_ESUpositions-",cyear,".png")
```

## 4.1. Indices for Stock Assessments of target species in ICES Divisions 6, 7, 8, 9 derived from Acoustic and DEPM Surveys

```{r, echo=FALSE, out.width="100%",fig.align='center', fig.cap="Figure 4.1 Sampling scheme of the spring acoustic surveys in ICES areas 6, 7, 8 and 9. PELAGO, PELACUS, PELGAS and WESPAS."}

knitr::include_graphics(path.map)

```

