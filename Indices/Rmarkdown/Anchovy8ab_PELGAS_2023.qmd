---
title: ""
format: docx
editor: visual
---

```{r, echo=FALSE, out.width="50%",fig.align='center',fig.cap="Figure 1. 8ab Anchovy mean weight and length-at-age, PELGAS survey."}

knitr::opts_chunk$set(echo = FALSE,message=FALSE,warning = FALSE)
prefix='/media/mathieu/EchoSonde/Campagnes/'
#prefix='/home/mathieu/Documents/Campagnes/'
#
prefix='D:/Campagnes/'
path.export.biomass.series=paste(prefix,"PELGAS/Data/Biomasse/Plots/",sep='')
path.export.mWLage.series=paste(prefix,"PELGAS/Data/Biotique/meanLength&Weights/",sep='')
```

### 4.1.2. Anchovy in Division 8abcd

#### 4.1.2.1. Spring acoustic survey (PELGAS): 8ab

##### Anchovy biomass and abundance estimates

Anchovy biomass and abundance in ICES area 8ab have been calculated since 2000, based on acoustic and trawl data from PELGAS survey (cf. details in [Doray et al. 2021](https://ices-library.figshare.com/articles/report/ICES_Survey_Protocols_Manual_for_acoustic_surveys_coordinated_under_ICES_Working_Group_on_Acoustic_and_Egg_Surveys_for_Small_Pelagic_Fish_WGACEGG_/19050638)) (Figure XX & YY).

```{r, echo=FALSE, out.width="100%",fig.align='center', fig.cap="Figure XX. Time series of biomass estimates and CVs for spring anchovy in ICES area 8ab, derived from PELGAS survey data."}

knitr::include_graphics(c(
  paste(path.export.biomass.series,'PELGASbiomassCV-ANE.png',sep='')))

```

```{r, echo=FALSE, out.width="100%",fig.align='center', fig.cap="Figure YY. Time series of abundance estimates and CVs for spring anchovy in ICES area 8ab, derived from PELGAS survey data."}

knitr::include_graphics(c(
  paste(path.export.biomass.series,'PELGASabundanceCV-ANE.png',sep='')))

```

Anchovy acoustic abundance and biomass estimates in areas 8ab were high from 2000 to 2003. They have collapsed in 2004 due to low recruitments and excessive fishing pressure. Following a fishery closure from 2005 to 2010, biomass and abundance have recovered and remained at high or very high levels since 2010, with peaks in 2015 and 2021.

##### Anchovy mean weight and length-at-age

Anchovy mean weights and lengths-at-age in ICES area 8ab were calculated based on abundance-at-length, length-age and length-weight keys from PELGAS survey (cf. details in [Doray et al. 2021](https://ices-library.figshare.com/articles/report/ICES_Survey_Protocols_Manual_for_acoustic_surveys_coordinated_under_ICES_Working_Group_on_Acoustic_and_Egg_Surveys_for_Small_Pelagic_Fish_WGACEGG_/19050638)). Time series of anchovy mean weights and lengths-at-age are presented in Figure ZZ1 and ZZ2.

```{r, echo=FALSE, out.width="75%",fig.align='center',fig.cap="Figure ZZ1. 8ab Anchovy mean weight at-age, PELGAS survey."}

knitr::include_graphics(c(paste(path.export.mWLage.series,'PELGAS-wmWatAge-ANE.png',sep='')))

```

```{r, echo=FALSE, out.width="75%",fig.align='center',fig.cap="Figure ZZ2. 8ab Anchovy mean length at-age, PELGAS survey."}

knitr::include_graphics(c(paste(path.export.mWLage.series,'PELGAS-wmLatAge-ANE.png',sep='')))

```

Anchovy mean weights and lengths-at-age in ICES area 8ab have decreased since 2000, with a sharp drop around 2010 (Figure ZZ1&2). Anchovy mean weights-at-age have decreased by 5 (age 1) to 10 g (age 2+) within 15 years, while mean lengths-at-age have decreased by about 2 cm.
