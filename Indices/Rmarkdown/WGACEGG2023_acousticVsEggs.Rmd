---
title: "Comparison between acoustic and egg-based fish biomass indices"
subtitle: "WGACEGG 2023"
author: "Mathieu Doray, Erwan Duhamel, Martin Huret / DECOD Ifremer"
date: "15/11/2023"
output:
  beamer_presentation: default
  ioslides_presentation: default
  slidy_presentation: default
editor_options:
  markdown:
    wrap: 100
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
pref='/media/mathieu/IfremerMData/Campagnes/'
pref='C:/Users/mdoray/Documents/Campagnes/'
pref='C:/Users/mdoray/ownCloud - Mathieu.Doray@ifremer.fr@cloud.ifremer.fr/documents/Campagnes/'
# Path to results
path.PELGAS.acEgg=paste(pref,'WGACEGG/Results/AcouVsEggs/',sep='')
```

## Introduction

-   Understanding discrepancies in particular years between acoustic and Daily Egg Production Method
    (DEPM) estimates essential to:

    -   better understand potential bias impacting both estimates,

    -   develop a weighting per year assessing the coherence between survey estimates, to input in
        stock assessment models (Petitgas et al., 2009; ICES, 2017).

## Acoustic biomass index

-   Bacou-i = Area x NASC / tsi (Simmonds and MacLennan, 2005)

    -   Area (survey area) and NASC (areal fish acoustic density) are the [observation
        terms]{.underline},

    -   tsi is Target Strength [scaling factor]{.underline} used to convert acoustic into fish
        density, derived from fish size distribution.

## Daily Egg Production biomass index

-   Begg-i = Ptot-i / DFi = P0-i.Area.Wi / (R.F.S) (Lasker, 1985)

    -   P0-i : mean daily egg production per area unit, linked to Ni,j, the number of eggs in egg
        cohort j:

        -   Ni,j = P 0-i,j exp(-Zi . ai,j), with ai,j mean age of each cohort and Z the daily egg
            mortality.

    -   Ni,j are the [observation terms]{.underline}

    -   DFi: daily fecundity [scaling factor]{.underline} to convert egg into spawning fish density,
        derived from biometry data.

        -   DFi derived from mean fish weight Wi, sex ratio R, batch fecundity F and spawning
            fraction S, Zi is another scaling factor.

## Acoustic:egg fecundity index (1)

-   For a survey where acoustic and egg data are collected simultenaeously, assuming that: Bacou-i
    \~ Begg-i:

    -   Bacou-i \~ Ptot-i / DFi = P0-i.Area.Wi / (R.F.S)

    -   DFi \~ Ptot-i / Bacou-i

<!-- -->

-   Assuming that Wi = Bacou-i / Aacou-i (Aacou-i: acoustic abundance estimate), one can estimate
    R.F.S as:

    -   R.F.S = DFi . Wi

        -   R: sex ratio, F: batch fecundity, S: spawning fraction

## Methods (1)

-   Acoustic and egg indices can be compared (Doray & Huret, 2020):

    -   within surveys, if both information collected on same platform (e.g. PELGAS survey),

    -   during surveys occurring in the same area, over the same time period platform (e.g. PELGAS
        and BIOMAN surveys).

-   Linear regression between (unbiased) indices

    -   **Residuals check: normality, time trend, autocorrelation**

-   Difference between indices significant for a given year, if point confidence interval outside of
    95% confidence intervals of linear regression

## Methods (2)

-   If significant difference found, it was advised (Doray & Huret, 2020):

    -   **first check potential data collection issues/bias for each indicator**,

    -   then compare observations maps / indices,

    -   then scaling factors,

    -   then final indices, in an attempt to disentangle the bias

## Biomass estimates time series: anchovy

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGAS-BiomassCufes-BIOMANdepmPtot-ANE-TimeSeries.png',sep=''))

```

## Anchovy, PELGAS CUFES Ptot \~ acoustic biomass (1)

```{r, echo=FALSE, out.width="60%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGASbiomassVsPELGAScufesPtot-ANE.png',sep=''))

```

\tiny

+----------------------------------------------------+---------------------+---------------------+
| ANE, CUFES Ptot\~acoustic                          | Acoustic\>egg       | Egg\>acoustic       |
+====================================================+=====================+=====================+
| Positive correlation, R2 = 0.48; LM dragged by     | 2000, 2002, 2015    | 2007, 2013, 2016,   |
| 2022 (secondary by 2015 and 2021)                  |                     | 2022, 2023          |
+----------------------------------------------------+---------------------+---------------------+

## Anchovy, PELGAS CUFES Ptot \~ 2023 acoustic biomass corrected

```{r, echo=FALSE, out.width="60%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGASbiomassVsPELGAScufesPtotCor-ANE.png',sep=''))

```

## Anchovy, PELGAS CUFES Ptot \~ acoustic biomass (2)

::: columns-2
```{r, echo=FALSE, out.width="30%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'AcPeleggPelLM1ResidualsCheck_ANE.png',sep=''))

```

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'AcPeleggPelResidualsTimeSeries_ANE.png',sep=''))

```

-   Significant time trend in residuals: CUFES Ptot \> acoustic biomass since 2022
:::

## Anchovy, DEPM \~ PELGAS CUFES Ptot (1)

```{r, echo=FALSE, out.width="60%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGAScufesVsBIOMANdepmPtot-ANE.png',sep=''))

```

\tiny

+-------------------------------------------+-----------------------+----------------------------+
| ANE, CUFES \~ DEPM Ptot                   | CUFES\>DEPM           | DEPM\>CUFES                |
+===========================================+=======================+============================+
| Strong positive correlation, R2 = 0.69,   | 2022                  | 2000, 2001, 2015, 2016,    |
| DEPM = CUFES -45%                         |                       | 2018, 2019, 2023           |
+-------------------------------------------+-----------------------+----------------------------+

## Anchovy, DEPM \~ PELGAS CUFES Ptot (2)

::: columns-2
```{r, echo=FALSE, out.width="30%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'EggPeleggBioLM1ResidualsCheck_ANE.png',sep=''))

```

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'EggPeleggBioResidualsTimeSeries_ANE.png',sep=''))

```

-   No time trend in residuals
:::

## Anchovy, DEPM \~ acoustic biomass

```{r, echo=FALSE, out.width="60%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGASacouVsBIOMANdepmBiomass-ANE.png',sep=''))

```

\tiny

+------------------------------------------------+-----------------------+-----------------------+
| ANE, DEPM \~ acoustic                          | Acoustic\>egg         | Egg\>acoustic         |
+================================================+=======================+=======================+
| Strong positive correlation, R2 = 0.76, DEPM = | 2000, 2002, 2012      | 2011, 2016, 2018,     |
| acoustic -40%                                  |                       | 2019, 2022, 2023      |
+------------------------------------------------+-----------------------+-----------------------+

## Anchovy, DEPM \~ 2023 acoustic biomass corrected

```{r, echo=FALSE, out.width="60%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGASacouVsBIOMANdepmBiomassCor-ANE.png',sep=''))

```

## Anchovy, PELGAS DEPM \~ acoustic biomass (2)

::: columns-2
```{r, echo=FALSE, out.width="30%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'AcPeleggBioLM1ResidualsCheck_ANE.png',sep=''))

```

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'AcPeleggBioLM1ResidualsTimeSeries_ANE.png',sep=''))

```

-   Significant time trend in residuals: DEPM biomass \> acoustic biomass since 2018
:::

## Potential bias in anchovy indices (1)

\tiny

+------------------+------------------+------------------+------------------+------------------+
| Year             | DEPM \~ acoustic | CUFES Ptot \~    | CUFES Ptot \~    | Conclusion       |
|                  | biomass          | acoustic         | DEPM             |                  |
+==================+==================+==================+==================+==================+
| 2019             | DEPM biomass \>  | CUFES Ptot \>    | CUFES Ptot =     | No obvious bias  |
|                  | acoustic biomass | acoustic biomass | DEPM Ptot        | in acoustic      |
|                  |                  |                  |                  | (surface schools |
|                  |                  |                  |                  | checked)         |
|                  |                  |                  |                  |                  |
|                  |                  |                  |                  | Fecundity under- |
|                  |                  |                  |                  | estimated in     |
|                  |                  |                  |                  | some areas?      |
+------------------+------------------+------------------+------------------+------------------+
| 2023             | DEPM biomass \>  | CUFES Ptot \>    | CUFES Ptot \<    | Adult vertical   |
|                  | acoustic biomass | acoustic biomass | DEPM Ptot        | distribution     |
|                  |                  |                  |                  | very shallow,    |
|                  |                  |                  |                  | acoustic         |
|                  |                  |                  |                  | missed\~20%      |
|                  |                  |                  |                  | biomass          |
+------------------+------------------+------------------+------------------+------------------+

## Potential bias in anchovy indices (2)

\tiny

+------------------+------------------+------------------+------------------+------------------+
| Year             | DEPM \~ acoustic | CUFES Ptot \~    | CUFES Ptot \~    | Conclusion       |
|                  | biomass          | acoustic         | DEPM             |                  |
+==================+==================+==================+==================+==================+
| 2015             | DEPM biomass \<= | CUFES Ptot =\<   | CUFES Ptot \<    | Very high        |
|                  | acoustic biomass | acoustic biomass | DEPM Ptot        | anchovy NASC in  |
|                  |                  |                  |                  | Gironde mouth    |
|                  |                  |                  |                  |                  |
|                  |                  |                  |                  | Immature fish?   |
+------------------+------------------+------------------+------------------+------------------+
| 2021             | DEPM biomass \<= | CUFES Ptot =\<   | CUFES Ptot =     | No obvious bias  |
|                  | acoustic biomass | acoustic biomass | DEPM Ptot        | in acoustic      |
|                  |                  |                  |                  |                  |
|                  |                  |                  |                  | Immature fish?   |
+------------------+------------------+------------------+------------------+------------------+

## Potential bias in anchovy indices (3)

\tiny

+-----------------+-----------------+-----------------+-----------------+----------------------+
| Year            | DEPM \~         | CUFES Ptot \~   | CUFES Ptot \~   | Conclusion           |
|                 | acoustic        | acoustic        | DEPM            |                      |
|                 | biomass         |                 |                 |                      |
+=================+=================+=================+=================+======================+
| 2022            | DEPM biomass \> | CUFES Ptot \>   | CUFES Ptot \>   | Eggs accumulation at |
|                 | acoustic        | acoustic        | DEPM Ptot       | CUFES depth due to   |
|                 | biomass         | biomass         |                 | stratification not   |
|                 |                 |                 |                 | compensated by       |
|                 |                 |                 |                 | vertical model?      |
+-----------------+-----------------+-----------------+-----------------+----------------------+

## Biomass estimates time series: sardine

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGAS-BiomassCufes-BIOMANdepmPtot-PIL-TimeSeries.png',sep=''))
```

## Sardine, PELGAS CUFES Ptot vs. acoustic biomass (1)

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGASbiomassVsPELGAScufesPtot-PIL.png',sep=''))

```

\tiny

+------------------------------------+---------------------------+--------------------------------+
| PIL, CUFES Ptot\~biomass           | Acoustic\>CUFES           | CUFES\>acoustic                |
+====================================+===========================+================================+
| Strong positive correlation, R2 =  | 2001, 2005, 2015, 2017,   | 2000, 2006, 2010, 2012, 2013,  |
| 0.68                               | 2021                      | 2016                           |
+------------------------------------+---------------------------+--------------------------------+

## Sardine, PELGAS CUFES Ptot vs. acoustic biomass (2)

::: columns-2
```{r, echo=FALSE, out.width="30%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'AcPeleggBioLM1ResidualsCheck_PIL.png',sep=''))

```

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'AcPeleggBioResidualsTimeSeries_PIL.png',sep=''))

```

-   Non-significant decreasing time trend in residuals: CUFES Ptot \> acoustic biomass since 2015?
:::

## Sardine, DEPM eggs \~ CUFES Ptot (1)

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGAScufesVsBIOMANdepmPtot-PIL.png',sep=''))

```

\tiny

+------------------------------------+-----------------------+------------------------------------+
| PIL, DEPM \~ CUFES Ptot            | CUFES\>DEPM           | DEPM\>CUFES                        |
+====================================+=======================+====================================+
| Strong positive correlation, R2 =  | 2000, 2006, 2021      | 2001, 2002, 2003, 2005, 2007,      |
| 0.67, DEPM = CUFES - 40%           |                       | 2014, 2015, 2016, 2017, 2018       |
+------------------------------------+-----------------------+------------------------------------+

## Sardine, DEPM eggs \~ CUFES Ptot (2)

::: columns-2
```{r, echo=FALSE, out.width="30%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'EggPeleggBioLM1ResidualsCheck_PIL.png',sep=''))

```

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'EggPeleggBioResidualsTimeSeries_PIL.png',sep=''))

```

-   No time trend in residuals: DEPM eggs \> CUFES Ptot
:::

## Sardine, DEPM eggs \~ acoustic biomass (1)

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PELGASbiomassVsBIOMANdepmPtot-PIL.png',sep=''))

```

\tiny

+----------------------------------+-----------------------+-------------------------------------+
| PIL, DEPM eggs\~biomass          | Acoustic\>egg         | Egg\>acoustic                       |
+==================================+=======================+=====================================+
| Very strong positive             | 2001, 2011, 2019,     | 2003, 2005, 2008, 2010, 2012, 2014, |
| correlation, R2 = 0.86           | 2021                  | 2016, 2018                          |
+----------------------------------+-----------------------+-------------------------------------+

## Sardine, DEPM eggs \~ acoustic biomass (2)

::: columns-2
```{r, echo=FALSE, out.width="30%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'BiomPeleggBioLM1ResidualsCheck_PIL.png',sep=''))

```

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'BiomPeleggBioResidualsTimeSeries_PIL.png',sep=''))

```

-   No significant time trend in residuals but sign of DEPM eggs \< acoustic biomass since 2019?
:::

## Potential bias in sardine indices

\tiny

+------------------+------------------+------------------+------------------+------------------+
| Year             | DEPM \~ acoustic | CUFES Ptot \~    | CUFES Ptot \~    | Conclusion       |
|                  | biomass          | acoustic         | DEPM             |                  |
+==================+==================+==================+==================+==================+
| 2021             | DEPM biomass \<  | CUFES Ptot \<    | CUFES Ptot \>    | Immature fish    |
|                  | acoustic biomass | acoustic biomass | DEPM Ptot        | (PELGAS S: 38%)  |
+------------------+------------------+------------------+------------------+------------------+

## PELGAS acoustic:egg fecundity index

```{r, echo=FALSE, out.width="50%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'clupFecundityComponentsTimeSeries.png',sep=''))

```

## BIOMAN anchovy fecundity estimates

```{r, echo=FALSE, out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'BIOMAN_adultParameters2023.png',sep=''))

```

## Comparison of fecundity estimates

-   Biometry-based fecundity estimates (BIOMAN)

    -   Anchovy: global decrease in DF, caused by synchronous decrease in F and W

-   Acoustic:egg fecundity estimate (PELGAS)

    -   Anchovy: global DF increase, caused by increase in F, which compensates decrease in mean
        weight

    -   Sardine: global DF decrease, caused by decrease in mean weight, not compensated by slight
        increase of F

## PELGAS daily fecundity maps, anchovy

```{r, echo=FALSE,caption='PELGAS daily fecundity proxy maps',out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'ANE-DF_PELGAS.jpg',sep=''))

```

## PELGAS daily fecundity maps, sardine

```{r, echo=FALSE,caption='PELGAS daily fecundity proxy maps',out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'PIL-DF_PELGAS.jpg',sep=''))

```

## PELGAS maturity estimates

::: columns-2
```{r, echo=FALSE, out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.PELGAS.acEgg,'clupSpawningFractionTimeSeries.png',sep=''))

```

-   PELGAS 2023 macroscopic mature fish proportions:
    -   Anchovy: 90%

    -   Sardine: 69%
:::

## Discussion (1)

-   Time trends in residuals of acoustic vs. eggs models + outliers

    -   Time trends in residuals: in(de)creasing systematic bias?

    -   Outliers: year-specific bias

-   Systematic bias related to fish fecundity = eggs / acoustic, and/or maturity?

    -   Increasing PELGAS mean anchovy DF proxy vs. decreasing BIOMAN DF estimates:

        -   fish mean weight: decreasing in PELGAS and BIOMAN

        -   increasing PELGAS F proxy vs. BIOMAN stable R, decreasing F and slightly decreasing S

    -   "Mediterranean-like scenario" in the case of PELGAS DF: over-investment in spawning
        detrimental to anchovy growth?

    -   Decrease in DF caused by synchronous decrease in F and W in the case of BIOMAN

## Discussion (2)

-   Discrepancies between DF estimates?

    -   Which trends are real? Impact on assessment?

    -   Spatial heterogeneity in DF ?

        -   BIOMAN biometry samples, and DF estimate, representative of all population / areas?
            Effect of spatial heterogeneity?

        -   Way forward: spatially stratified sampling for F estimation in DEPM method & more gonad
            samples collected during PELGAS

    -   Effect of bias in CUFES and acoustic estimates on PELGAS DF proxy?

## Discussion (3)

-   Acoustic and egg indices comparison

    -   More or less elaborated indices: which to choose / use?

    -   Model choice: linear model, with(out) biased indices, robust regression...

    -   Objective: analysis bias for all years outside of model confidence interval and for
        systematic bias

## Conclusions (1)

-   Overall strong correlation (R2\>0.6) between acoustic and egg indices

-   **But** systematic bias in anchovy acoustic vs. eggs models and outliers to be studied

    -   data collection issues:
        -   Anchovy 2023 acoustic biomass negative bias in blind zone,
        -   Anchovy and sardine 2022 CUFES positive bias with stratification,
        -   Anchovy and sardine DEPM negative bias in case of immature fish (2015, 2021) ?
    -   gridmaps / global indices comparison to identify bias in observation and / or scaling
        components
        -   Anchovy DEPM negative bias in fecundity (2019) ?
    -   up-to-date eggs and NASC gridmaps and indices needed before the group to identify potential
        bias and inform stock assessment groups

## Conclusions (2)

-   Egg:acoustic hybrid fecundity index

    -   Can be calculated whenever Ptot and acoustic biomass estimates available, can be mapped
    -   Useful to benchmark DEPM DF estimates and ecological studies
    -   Discrepancies between DF indices in the BoB to be investigated
