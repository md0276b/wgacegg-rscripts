# Script to compare acoustic and egg indices from PELGAS and BIOMAN surveys
#******************************************************
# Author: mathieu.doray[at]ifremer.fr
# Created: 20/11/2020
#******************************************************
library(ggplot2)
library(EchoR)
library(FactoMineR)
library(factoextra)

# 0. Define paths --------
# ***************************
#pref='D:/Campagnes/'
#pref='/media/mathieu/EchoSonde/Campagnes/'
#
pref='C:/Users/mdoray/Documents/Campagnes/'
prefc='C:/Users/mdoray/ownCloud - Mathieu.Doray@ifremer.fr@cloud.ifremer.fr/documents/Campagnes/'
#pref='/home/mathieu/Documents/Campagnes/'
# Path to results
ayear=2024
path.export.AcEgg=paste(pref,'WGACEGG/Results/AcouVsEggs/',ayear,'/',sep='')
dir.create(path.export.AcEgg,recursive = TRUE)
path6=paste(
  pref,'PELGAS/Data/Biomasse/biomAbunCVallSpPELGASseriesLong.csv',sep='')
path7=paste(
  pref,'WGACEGG/Data/Indices/AcousticEggComparisonData.csv',sep='')
#path7=paste(
#  prefc,'WGACEGG/WGACEGG2024/Data/AcousticEggComparisonData.csv',sep='')
path.export.results.fecundity.res1.0=paste(
  pref,'PELGAS/Resultats/gridMaps/Poissons/Fecundity/2000-2021/res1.0/',
  sep='')

# 1. Load data --------------
# 1.0. Load PELGAS and BIOMAN results from single file -----------
# ****************************
BACVtotPtot.PELGASs.BIOMAN=read.table(path7,sep=';',header=TRUE)
head(BACVtotPtot.PELGASs.BIOMAN)
sort(unique(BACVtotPtot.PELGASs.BIOMAN$year))
# NB: BIOMAN.Ptot = 8abd, BIOMAN.biom = 8abcd ...
names(BACVtotPtot.PELGASs.BIOMAN)[
  names(BACVtotPtot.PELGASs.BIOMAN)=='BIOMAN.biom8abcd']='BIOMAN.biom'
names(BACVtotPtot.PELGASs.BIOMAN)[
  names(BACVtotPtot.PELGASs.BIOMAN)=='BIOMAN.biom.8abcdCV']='BIOMAN.biom.CV'
names(BACVtotPtot.PELGASs.BIOMAN)[
  names(BACVtotPtot.PELGASs.BIOMAN)=='BIOMAN.Ptot8abd']='BIOMAN.Ptot'
names(BACVtotPtot.PELGASs.BIOMAN)[
  names(BACVtotPtot.PELGASs.BIOMAN)=='BIOMAN.Ptot.8abdcv']='BIOMAN.Ptot.sd'

BACVtotPtot.PELGASs.BIOMAN[BACVtotPtot.PELGASs.BIOMAN$year==2024,]

summary(BACVtotPtot.PELGASs.BIOMAN[
  BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC','BIOMAN.Ptot.sd'])
summary(BACVtotPtot.PELGASs.BIOMAN[
  BACVtotPtot.PELGASs.BIOMAN$sp=='SARD-PIL','BIOMAN.Ptot.sd'])

table(BACVtotPtot.PELGASs.BIOMAN$year,BACVtotPtot.PELGASs.BIOMAN$sp)

#***************************
# 2. Prepare PELGAS and BIOMAN data --------
#***************************
#BACVtotPtot.PELGASs.BIOMAN=read.table(path7,sep=';',header=TRUE)
#head(BACVtotPtot.PELGASs.BIOMAN)
#sort(unique(BACVtotPtot.PELGASs.BIOMAN$year))

BACVtotPtot.PELGASs.BIOMAN=BACVtotPtot.PELGASs.BIOMAN[
  ,names(BACVtotPtot.PELGASs.BIOMAN)!='X']
head(BACVtotPtot.PELGASs.BIOMAN)
names(BACVtotPtot.PELGASs.BIOMAN)

# If needed, change names
if (sum(!'PELGAS.biom'%in%names(BACVtotPtot.PELGASs.BIOMAN))>0){
  names.table=data.frame(EchoR.names=c("year","sp","cruise","wbiom","wabun",
                                       "wCV","spname","mw","ptot","sigma",
                                       "BIOMAN.biom",
                                       "BIOMAN.biom.CV","BIOMAN.Ptot","BIOMAN.Ptot.sd"),
                         acegg.names=c('year','sp','cruise','PELGAS.biom','PELGAS.abun',
                                       'PELGAS.biom.CV','species','PELGAS.meanWeight',
                                       'PELGAS.Ptot','PELGAS.Ptot.sd',
                                       "BIOMAN.biom","BIOMAN.biom.CV","BIOMAN.Ptot","BIOMAN.Ptot.sd"))
  
  names(BACVtotPtot.PELGASs.BIOMAN)=names.table[
    match(names(BACVtotPtot.PELGASs.BIOMAN),names.table$EchoR.names),
    'acegg.names']
}

## 2.1. Add corrected PELGAS biomass estimates ------
BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom.cor=
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom

BACVtotPtot.PELGASs.BIOMAN[BACVtotPtot.PELGASs.BIOMAN$year=='2023'&
                             BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC',
                           'PELGAS.biom.cor']=
  1.2*
  BACVtotPtot.PELGASs.BIOMAN[BACVtotPtot.PELGASs.BIOMAN$year=='2023'&
                               BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC',
                             'PELGAS.biom']

BACVtotPtot.PELGASs.BIOMAN$PELGAS.abun.cor=
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.abun

BACVtotPtot.PELGASs.BIOMAN[BACVtotPtot.PELGASs.BIOMAN$year=='2023'&
                             BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC',
                           'PELGAS.abun.cor']=
  1.2*
  BACVtotPtot.PELGASs.BIOMAN[BACVtotPtot.PELGASs.BIOMAN$year=='2023'&
                               BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC',
                             'PELGAS.abun']

BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom.CV.cor=
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom.CV
BACVtotPtot.PELGASs.BIOMAN[BACVtotPtot.PELGASs.BIOMAN$year=='2023'&
                             BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC',
                           'PELGAS.biom.CV.cor']=
  BACVtotPtot.PELGASs.BIOMAN[BACVtotPtot.PELGASs.BIOMAN$year=='2023'&
                               BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC',
                             'PELGAS.biom.CV']

## 2.2 PELGAS DF estimates ----
# with raw indices
BACVtotPtot.PELGASs.BIOMAN$PELGAS.DF=     # in eggs/g.day
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.Ptot/
  (BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom*1e3)
BACVtotPtot.PELGASs.BIOMAN$PELGAS.meanWeight=  # in gram
  1e3*BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom/
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.abun
BACVtotPtot.PELGASs.BIOMAN$PELGAS.RFS=
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.DF*
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.meanWeight

# with corrected indices
BACVtotPtot.PELGASs.BIOMAN$PELGAS.DF.cor=
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.Ptot/
  (BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom.cor*1e3)
BACVtotPtot.PELGASs.BIOMAN$PELGAS.meanWeight.cor=
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.biom.cor/
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.abun.cor
BACVtotPtot.PELGASs.BIOMAN$PELGAS.RFS.cor=BACVtotPtot.PELGASs.BIOMAN$PELGAS.DF.cor*
  BACVtotPtot.PELGASs.BIOMAN$PELGAS.meanWeight.cor

# R estimated by BIOMAN
# R.BIOMAN=.5330
# BACVtotPtot.PELGASs.BIOMAN$PELGAS.F=BACVtotPtot.PELGASs.BIOMAN$PELGAS.RFS/
#   (R.BIOMAN*BACVtotPtot.PELGASs.BIOMAN$PELGAS.S)

## 2.3. Species selection -------------
BACVtotPtot.PELGASs.BIOMAN.ANE=BACVtotPtot.PELGASs.BIOMAN[
  BACVtotPtot.PELGASs.BIOMAN$sp=='ENGR-ENC',]
#BACVtotPtot.PELGASs.BIOMAN.ANE[,c("year",'PELGAS.S')]
BACVtotPtot.PELGASs.BIOMAN.PIL=BACVtotPtot.PELGASs.BIOMAN[
  BACVtotPtot.PELGASs.BIOMAN$sp=='SARD-PIL',]
#BACVtotPtot.PELGASs.BIOMAN.PIL[,c("year",'PELGAS.S')]
BACVtotPtot.PELGASs.BIOMAN.ANEPIL=BACVtotPtot.PELGASs.BIOMAN[
  BACVtotPtot.PELGASs.BIOMAN$sp%in%c('ENGR-ENC','SARD-PIL'),]

x11()
p1 <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL,
  aes(year,PELGAS.DF)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "loess")+
  ggtitle("Daily fecundity")+theme(axis.title.x = element_blank())

p1.1=ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
  BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,],
       aes(year,PELGAS.DF)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "loess")+
  ggtitle("Daily fecundity")+theme(axis.title.x = element_blank())


p2 <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL, aes(year,PELGAS.meanWeight)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "lm")+
  ggtitle("Mean weight")+theme(axis.title.x = element_blank())

p2.1 <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
  BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,], 
  aes(year,PELGAS.meanWeight)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "lm")+
  ggtitle("Mean weight")+theme(axis.title.x = element_blank())

#x11()
p3 <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL, aes(year,PELGAS.RFS)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "loess")+
  ggtitle("RFS")+theme(axis.title.x = element_blank())

p3.1 <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
  BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,], aes(year,PELGAS.RFS)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "loess")+
  ggtitle("RFS")+theme(axis.title.x = element_blank())

x11()
multiplot(p1, p2,p3)
dev.print(png, file=paste(path.export.AcEgg,
                           "clupFecundityComponentsTimeSeries.png",sep=""), 
           units='cm',width=20, height=30,res=300)

x11()
multiplot(p1.1, p2.1,p3.1)
dev.print(png, file=paste(path.export.AcEgg,
                          "clupFecundityComponentsTimeSeriesWithout2022.png",sep=""), 
          units='cm',width=20, height=30,res=300)

x11()
p1cor <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL, aes(year,PELGAS.DF.cor)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "loess")+
  ggtitle("Daily fecundity")+theme(axis.title.x = element_blank())

p2cor <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL, aes(year,PELGAS.meanWeight.cor)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "lm")+
  ggtitle("Mean weight")+theme(axis.title.x = element_blank())

p3cor <- ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEPIL, aes(year,PELGAS.RFS.cor)) +
  geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "loess")+
  ggtitle("RFS")+theme(axis.title.x = element_blank())

multiplot(p1cor, p2cor,p3cor)
dev.print(png, file=paste(path.export.AcEgg,
                          "clupFecundityComponentsTimeSeriesCor.png",sep=""), 
          units='cm',width=20, height=30,res=300)

x11()
p1
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                           "clupDailyFecundityTimeSeries.png",sep=""), 
           units='cm',width=20, height=12,res=300)

x11()
p1.1
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "clupDailyFecundityTimeSeriesWithout2022.png",sep=""), 
          units='cm',width=20, height=12,res=300)

x11()
p2
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                            "clupMeanWeightTimeSeries.png",sep=""), 
            units='cm',width=20, height=12,res=300)
x11()
p3
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                            "clupRFStimeSeries.png",sep=""), 
            units='cm',width=20, height=12,res=300)

# p3 <- ggplot(BACVtotPtot.PELGASs.BIOMAN, aes(year,PELGAS.S)) +
#   geom_line() + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "lm")+
#   ggtitle("Spawning fraction")+theme(axis.title.x = element_blank())
# p4 <- ggplot(BACVtotPtot.PELGASs.BIOMAN, aes(year,PELGAS.F)) +
#   geom_line()+ facet_wrap(vars(sp),scales="free")+geom_smooth(method = "lm")+
#   ggtitle("Batch fecundity")+theme(axis.title.x = element_blank())
# multiplot(p1, p2, p3, p4, cols=2)
# dev.print(png, file=paste(path.export.AcEgg,
#                           "clupFecundityComponentsTimeSeries.png",sep=""), 
#           units='cm',width=20, height=20,res=300)

x11()
p <- ggplot(BACVtotPtot.PELGASs.BIOMAN, aes(year,PELGAS.RFS)) +
  geom_line()
p + facet_wrap(vars(sp),scales="free")+geom_smooth(method = "loess")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "clupRFSTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)

graphics.off()

head(BACVtotPtot.PELGASs.BIOMAN.ANEPIL)

## 2.4. Time trends in DF ---------

library(segmented)
ANE.DF.ts.lm=lm(PELGAS.DF~year,data=BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
    BACVtotPtot.PELGASs.BIOMAN.ANEPIL$sp=='ENGR-ENC',
  ])
summary(ANE.DF.ts.lm)
par(mfrow=c(2,2))
plot(ANE.DF.ts.lm)
par(mfrow=c(1,1))

ANE.DF.ts.lm.segmented=segmented(ANE.DF.ts.lm)
summary(ANE.DF.ts.lm.segmented)
davies.test(ANE.DF.ts.lm.segmented)
pscore.test(ANE.DF.ts.lm.segmented)

ANE.DF.ts.lm2=lm(PELGAS.DF~year,data=BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
  BACVtotPtot.PELGASs.BIOMAN.ANEPIL$sp=='ENGR-ENC'&
    BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,])
summary(ANE.DF.ts.lm2)
par(mfrow=c(2,2))
plot(ANE.DF.ts.lm2)
par(mfrow=c(1,1))

ANE.DF.ts.lm2.segmented=segmented(ANE.DF.ts.lm2)
summary(ANE.DF.ts.lm2.segmented)
davies.test(ANE.DF.ts.lm2.segmented)
pscore.test(ANE.DF.ts.lm2.segmented)

hist(BACVtotPtot.PELGASs.BIOMAN.ANEPIL$PELGAS.DF)

ANE.DF.ts.glm2=glm(PELGAS.DF~year,data=BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
  BACVtotPtot.PELGASs.BIOMAN.ANEPIL$sp=='ENGR-ENC'&
    BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,],family=Gamma(link="log"))
summary(ANE.DF.ts.glm2)
par(mfrow=c(2,2))
plot(ANE.DF.ts.glm2)
par(mfrow=c(1,1))

ANE.DF.ts.glm2.segmented=segmented(ANE.DF.ts.glm2)
plot(ANE.DF.ts.glm2.segmented)
summary(ANE.DF.ts.glm2.segmented)
davies.test(ANE.DF.ts.glm2.segmented)
pscore.test(ANE.DF.ts.glm2.segmented)
ANE.DF.ts.glm2.segmented.pred=data.frame(predict(
  ANE.DF.ts.glm2.segmented,
  newdata=data.frame(
    year=BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
      BACVtotPtot.PELGASs.BIOMAN.ANEPIL$sp=='ENGR-ENC'&
                       BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,'year']),
  type="response", interval="confidence"))
ANE.DF.ts.glm2.segmented.pred$year=BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
  BACVtotPtot.PELGASs.BIOMAN.ANEPIL$sp=='ENGR-ENC'&
    BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,'year']

dfs=BACVtotPtot.PELGASs.BIOMAN.ANEPIL[
  BACVtotPtot.PELGASs.BIOMAN.ANEPIL$sp=='ENGR-ENC'&
    BACVtotPtot.PELGASs.BIOMAN.ANEPIL$year!=2022,]

plot(dfs$year,dfs$PELGAS.DF,xlab='',ylab='Acoustic-egg DF')
lines(ANE.DF.ts.glm2.segmented.pred$year,ANE.DF.ts.glm2.segmented.pred$fit)
lines(ANE.DF.ts.glm2.segmented.pred$year,ANE.DF.ts.glm2.segmented.pred$lwr,
      lty=2)
lines(ANE.DF.ts.glm2.segmented.pred$year,ANE.DF.ts.glm2.segmented.pred$upr,
      lty=2)

hist(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.DF)
PIL.DF.ts.glm1=glm(PELGAS.DF~year,data=BACVtotPtot.PELGASs.BIOMAN.PIL,
                   family=Gamma(link="log"))
summary(PIL.DF.ts.glm1)
par(mfrow=c(2,2))
plot(PIL.DF.ts.glm1)
par(mfrow=c(1,1))

PIL.DF.ts.glm1.segmented=segmented(PIL.DF.ts.glm1)
plot(PIL.DF.ts.glm1.segmented)
summary(PIL.DF.ts.glm1.segmented)
davies.test(PIL.DF.ts.glm1.segmented)
pscore.test(PIL.DF.ts.glm1.segmented)
PIL.DF.ts.glm1.segmented.pred=data.frame(predict(
  PIL.DF.ts.glm1.segmented,
  newdata=data.frame(
    year=BACVtotPtot.PELGASs.BIOMAN.PIL$year),
  type="response", interval="confidence"))
PIL.DF.ts.glm1.segmented.pred$year=BACVtotPtot.PELGASs.BIOMAN.PIL$year

plot(BACVtotPtot.PELGASs.BIOMAN.PIL$year,
     BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.DF,xlab='',ylab='Acoustic-egg DF')
lines(PIL.DF.ts.glm1.segmented.pred$year,PIL.DF.ts.glm1.segmented.pred$fit)
lines(PIL.DF.ts.glm1.segmented.pred$year,PIL.DF.ts.glm1.segmented.pred$lwr,
      lty=2)
lines(PIL.DF.ts.glm1.segmented.pred$year,PIL.DF.ts.glm1.segmented.pred$upr,
      lty=2)

# 3. Comparison with BIOMAN results -----------
#******************************* -----------
## 3.1. ANE indices time series -----------
#************************ -----------
x11()
par(bg='white')
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$year,
     BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,type='b',pch=16,
     xlab='',ylab='Biomass (MT)',
     main='PELGAS acoustic biomass, anchovy, 8abd')
lines(BACVtotPtot.PELGASs.BIOMAN.ANE$year,
     BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,type='b',pch=17,
     col=2)
legend('topleft',legend=c('Raw','Corrected'),pch=c(16,17),col=seq(2))
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGAS-BiomassRawCor-ANE-TimeSeries.png',
                         sep=''),
          res=300,units='cm',width=20,height=15,bg='white')

x11()
par(mar=c(3,4,4,1),mfrow=c(4,1),bg='white')
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$year,
     BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,type='b',pch=16,
     xlab='',ylab='Biomass (MT)',
     main='PELGAS acoustic biomass, anchovy, 8abd')
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$year,
     BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom,type='b',pch=16,
     xlab='',ylab='Biomass (MT)',
     main='BIOMAN DEPM biomass, anchovy, 8abcd')
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$year,
     BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,type='b',pch=16,
     xlab='',ylab='CUFES Ptot (eggs/(gram.day))',
     main='PELGAS total daily egg production, anchovy, 8abd')
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$year,
     BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot,type='b',pch=16,
     xlab='',ylab='DEPM Ptot (eggs/(gram.day)',
     main='BIOMAN DEPM Ptot, anchovy, 8abd')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGAS-BiomassCufes-BIOMANdepmPtot-ANE-TimeSeries.png',
                         sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

## 3.2. PCA and clustering on indices -------------
head(BACVtotPtot.PELGASs.BIOMAN.ANE)
names(BACVtotPtot.PELGASs.BIOMAN.ANE)
row.names(BACVtotPtot.PELGASs.BIOMAN.ANE)=BACVtotPtot.PELGASs.BIOMAN.ANE$year
X.ANE=apply(BACVtotPtot.PELGASs.BIOMAN.ANE[
  complete.cases(BACVtotPtot.PELGASs.BIOMAN.ANE)
  ,c("PELGAS.biom","PELGAS.abun","PELGAS.biom.CV","PELGAS.meanWeight",
     "PELGAS.Ptot","PELGAS.Ptot.sd","BIOMAN.biom","BIOMAN.biom.CV","BIOMAN.Ptot",
     "PELGAS.DF","PELGAS.RFS")],2,as.numeric)
X.ANE=apply(X.ANE,2,scale)
dimnames(X.ANE)[[1]]=BACVtotPtot.PELGASs.BIOMAN.ANE$year
# PCA
PELGASs.BIOMAN.ANE.pca <- PCA(X.ANE,scale.unit = FALSE)
barplot(PELGASs.BIOMAN.ANE.pca$eig[,1],main="Eigenvalues",
        names.arg=1:nrow(PELGASs.BIOMAN.ANE.pca$eig))
summary(PELGASs.BIOMAN.ANE.pca)
plot(PELGASs.BIOMAN.ANE.pca,choix="ind")
plot(PELGASs.BIOMAN.ANE.pca,choix="var")
dimdesc(PELGASs.BIOMAN.ANE.pca, axes = 1:2)
x11()
fviz_pca_biplot(PELGASs.BIOMAN.ANE.pca)
dev.print(png, file=paste(path.export.AcEgg,
                          "PCAacEgg_ANE.png",sep=""), 
          units='cm',width=20, height=20,res=300)
# Clustering
PELGASs.BIOMAN.ANE.pca.hclust4=HCPC(PELGASs.BIOMAN.ANE.pca,nb.clust=4)
BACVtotPtot.PELGASs.BIOMAN.ANE$hclust1.4=NA
BACVtotPtot.PELGASs.BIOMAN.ANE$hclust1.4=
  PELGASs.BIOMAN.ANE.pca.hclust4$data.clust$clust
PELGASs.BIOMAN.ANE.pca.hclust3=HCPC(PELGASs.BIOMAN.ANE.pca,nb.clust=3)
BACVtotPtot.PELGASs.BIOMAN.ANE$hclust1.3=NA
BACVtotPtot.PELGASs.BIOMAN.ANE$hclust1.3=
  PELGASs.BIOMAN.ANE.pca.hclust3$data.clust$clust
x11()
plot(PELGASs.BIOMAN.ANE.pca.hclust3,choice='tree')
dev.print(png, file=paste(path.export.AcEgg,
                          "HCPCtree-PCAacEgg_ANE.png",sep=""), 
          units='cm',width=20, height=20,res=300)

x11()
fviz_pca_biplot(PELGASs.BIOMAN.ANE.pca,
                habillage=factor(
                  BACVtotPtot.PELGASs.BIOMAN.ANE$hclust1.3))
dev.print(png, file=paste(path.export.AcEgg,
                          "PCA-HCPC_acEgg_ANE.png",sep=""), 
          units='cm',width=20, height=20,res=300)

# without CVs
X.ANE2=X.ANE[,c("PELGAS.biom","PELGAS.abun","PELGAS.meanWeight",
                "PELGAS.Ptot","BIOMAN.biom","BIOMAN.Ptot",
                "PELGAS.DF","PELGAS.RFS")]
dimnames(X.ANE2)[[1]]=BACVtotPtot.PELGASs.BIOMAN.ANE$year
# PCA
PELGASs.BIOMAN.ANE.pca2 <- PCA(X.ANE2,scale.unit = FALSE)
barplot(PELGASs.BIOMAN.ANE.pca2$eig[,1],main="Eigenvalues",
        names.arg=1:nrow(PELGASs.BIOMAN.ANE.pca2$eig))
summary(PELGASs.BIOMAN.ANE.pca2)
plot(PELGASs.BIOMAN.ANE.pca2,choix="ind")
dimdesc(PELGASs.BIOMAN.ANE.pca, axes = 1:2)
names(PELGASs.BIOMAN.ANE.pca)
# Clustering
PELGASs.BIOMAN.ANE.pca2.hclust4=HCPC(PELGASs.BIOMAN.ANE.pca2,nb.clust=4)
PELGASs.BIOMAN.ANE.pca2.hclust4=HCPC(PELGASs.BIOMAN.ANE.pca2,nb.clust=3)

BACVtotPtot.PELGASs.BIOMAN.ANE$hclust2.4=NA
BACVtotPtot.PELGASs.BIOMAN.ANE$hclust2.4=
  PELGASs.BIOMAN.ANE.pca2.hclust4$data.clust$clust

## 3.3. ANE PELGAS biomass vs. BIOMAN biomass -----------
### 3.3.1. Linear models ----
hist(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)

B.ANE.PELGAS.BIOMAN.lmi=lm(BIOMAN.biom~PELGAS.biom,
                          BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.lmi)

B.ANE.PELGAS.BIOMAN.lm=lm(BIOMAN.biom~PELGAS.biom-1,
                              BACVtotPtot.PELGASs.BIOMAN.ANE)
AIC(B.ANE.PELGAS.BIOMAN.lmi,B.ANE.PELGAS.BIOMAN.lm)
summary(B.ANE.PELGAS.BIOMAN.lm)
x11()
par(mfrow=c(2,2),bg='white')
plot(B.ANE.PELGAS.BIOMAN.lm)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsBIOMANdepmBiomass_LM1-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
hist(B.ANE.PELGAS.BIOMAN.lm$residuals)
#ane.biom.newd=c(0,sort(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom))
ane.biom.newd=c(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom)
B.ANE.PELGAS.BIOMAN.lm.preds <- data.frame(predict(
  B.ANE.PELGAS.BIOMAN.lm,
  newdata = data.frame(PELGAS.biom=ane.biom.newd), 
                                            interval = 'confidence'))
B.ANE.PELGAS.BIOMAN.lm.preds$year=BACVtotPtot.PELGASs.BIOMAN.ANE$year
B.ANE.PELGAS.BIOMAN.lm.preds=merge(
  B.ANE.PELGAS.BIOMAN.lm.preds,
  BACVtotPtot.PELGASs.BIOMAN.ANE[
    ,c('year','PELGAS.biom','PELGAS.biom.CV','BIOMAN.biom','BIOMAN.biom.CV')],
  by.x='year',by.y='year')
B.ANE.PELGAS.BIOMAN.lm.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.ANE$year,
  resid=B.ANE.PELGAS.BIOMAN.lm$residuals)
# Check time trend in residuals
x11()
ggplot(data=B.ANE.PELGAS.BIOMAN.lm.residuals,
       aes(x=year,y=resid,label=year))+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")+
  geom_text()
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsBIOMANdepmBiomass_LM1-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=B.ANE.PELGAS.BIOMAN.lm.residuals$resid,
             tx=B.ANE.PELGAS.BIOMAN.lm.residuals$year,
             x0=2000,dx=1)

# LM without outliers
BACVtotPtot.PELGASs.BIOMAN.ANEs=BACVtotPtot.PELGASs.BIOMAN.ANE[
  !BACVtotPtot.PELGASs.BIOMAN.ANE$year%in%c('2015','2021'),]

B.ANE.PELGAS.BIOMAN.lm2=lm(
  BIOMAN.biom~PELGAS.biom-1,data=BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMAN.lm2)
x11()
par(mfrow=c(2,2),bg='white')
plot(B.ANE.PELGAS.BIOMAN.lm2)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsBIOMANdepmBiomass_LMwoOutliers-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
hist(B.ANE.PELGAS.BIOMAN.lm2$residuals)

ane.biom.newd2=c(BACVtotPtot.PELGASs.BIOMAN.ANEs$PELGAS.biom)
B.ANE.PELGAS.BIOMAN.lm2.preds <- data.frame(predict(
  B.ANE.PELGAS.BIOMAN.lm2,
  newdata = data.frame(PELGAS.biom=ane.biom.newd2), 
  interval = 'confidence'))
B.ANE.PELGAS.BIOMAN.lm2.preds$year=BACVtotPtot.PELGASs.BIOMAN.ANEs$year
B.ANE.PELGAS.BIOMAN.lm2.preds=merge(
  B.ANE.PELGAS.BIOMAN.lm2.preds,
  BACVtotPtot.PELGASs.BIOMAN.ANEs[
    ,c('year','PELGAS.biom','PELGAS.biom.CV','BIOMAN.biom','BIOMAN.biom.CV')],
  by.x='year',by.y='year')

B.ANE.PELGAS.BIOMAN.lm2.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.ANE[
    !BACVtotPtot.PELGASs.BIOMAN.ANE$year%in%c('2015','2021'),'year'],
  resid=B.ANE.PELGAS.BIOMAN.lm2$residuals)
# Check time trend in residuals
x11()
ggplot(data=B.ANE.PELGAS.BIOMAN.lm2.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsBIOMANdepmBiomass_LMwoOutliers-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=B.ANE.PELGAS.BIOMAN.lm2.residuals$resid,
             tx=B.ANE.PELGAS.BIOMAN.lm2.residuals$year,
             x0=2000,dx=1)

BPtot.ANE.PELGAS.BIOMAN.lm.cor=lm(BIOMAN.biom~PELGAS.biom.cor-1,
                              BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(BPtot.ANE.PELGAS.BIOMAN.lm.cor)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGAS.BIOMAN.lm.cor)
par(mfrow=c(1,1))
#ane.biom.newd.cor=c(
#  0,sort(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor))
ane.biom.newd.cor=c(
  sort(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor))
BPtot.ANE.PELGAS.BIOMAN.lm.cor.preds <- predict(
  BPtot.ANE.PELGAS.BIOMAN.lm.cor,
  newdata = data.frame(PELGAS.biom.cor=ane.biom.newd.cor), 
                                            interval = 'confidence')

B.ANE.PELGAS.BIOMAN.lm2.cor=lm(
  BIOMAN.biom~PELGAS.biom.cor-1,
  data=BACVtotPtot.PELGASs.BIOMAN.ANE[
    !BACVtotPtot.PELGASs.BIOMAN.ANE$year%in%c('2015','2021'),])
summary(B.ANE.PELGAS.BIOMAN.lm2.cor)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMAN.lm2.cor)
par(mfrow=c(1,1))

BPtot.ANE.PELGAS.BIOMAN.glm=glm(
  BIOMAN.biom~PELGAS.biom-1,family=Gamma,BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(BPtot.ANE.PELGAS.BIOMAN.glm)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGAS.BIOMAN.glm)
par(mfrow=c(1,1))
graphics.off()

# Best model
B.ANE.PELGAS.BIOMAN.lm3=lm(
  BIOMAN.biom~PELGAS.biom*hclust1.3-1,
  data=BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMAN.lm3)
x11()
par(mfrow=c(2,2),bg='white')
plot(B.ANE.PELGAS.BIOMAN.lm3)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsBIOMANdepmBiomass_LMwoOutliers-2periods-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
hist(B.ANE.PELGAS.BIOMAN.lm3$residuals)

B.ANE.PELGAS.BIOMAN.lm3.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.ANEs$year,
  resid=B.ANE.PELGAS.BIOMAN.lm3$residuals)
# Check time trend in residuals
x11()
ggplot(data=B.ANE.PELGAS.BIOMAN.lm3.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsBIOMANdepmBiomass_LMwoOutliers-2periods-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=B.ANE.PELGAS.BIOMAN.lm2.residuals$resid,
             tx=B.ANE.PELGAS.BIOMAN.lm2.residuals$year,
             x0=2000,dx=1)

ane.biom.newd3=BACVtotPtot.PELGASs.BIOMAN.ANEs[
  ,c("PELGAS.biom","hclust1.3")]
B.ANE.PELGAS.BIOMAN.lm3.preds <- data.frame(predict(
  B.ANE.PELGAS.BIOMAN.lm3,newdata = ane.biom.newd3, 
  interval = 'confidence'))
B.ANE.PELGAS.BIOMAN.lm3.preds$year=BACVtotPtot.PELGASs.BIOMAN.ANEs$year
B.ANE.PELGAS.BIOMAN.lm3.preds=merge(
  B.ANE.PELGAS.BIOMAN.lm3.preds,
  BACVtotPtot.PELGASs.BIOMAN.ANEs[
    ,c('year','PELGAS.biom','PELGAS.biom.CV','BIOMAN.biom','BIOMAN.biom.CV')],
  by.x='year',by.y='year')

### 3.3.2. Other regression models ---------
#*************************
hist(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom)
hist(BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom)

#### 3.3.2.1. With outliers ---------
B.ANE.PELGAS.BIOMAN.lm=lm(BIOMAN.biom~PELGAS.biom-1,
                          BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.lm)
B.ANE.PELGAS.BIOMAN.lm.pred=
  data.frame(predict(B.ANE.PELGAS.BIOMAN.lm,se.fit = TRUE,
                     interval = "confidence"))
B.ANE.PELGAS.BIOMAN.lm.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom

qqnorm(residuals(B.ANE.PELGAS.BIOMAN.lm))
qqline(residuals(B.ANE.PELGAS.BIOMAN.lm))
hist(residuals(B.ANE.PELGAS.BIOMAN.lm))

ggplot(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
       aes(x = PELGAS.biom, y = BIOMAN.biom))+
  geom_text(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
            aes(x = PELGAS.biom, y = BIOMAN.biom,label=year,
                colour=factor(hclust1.3))) + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV),
                    colour=factor(hclust1.3)),
                data = BACVtotPtot.PELGASs.BIOMAN.ANE) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV),
                     colour=factor(hclust1.3)),
                 data = BACVtotPtot.PELGASs.BIOMAN.ANE)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.pred, 
            aes(x = PELGAS.biom, y = fit.fit),colour='orange')

B.ANE.PELGAS.BIOMAN.lm2=lm(BIOMAN.biom~PELGAS.biom-1+factor(hclust1.3),
                           BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.lm2)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMAN.lm2)
par(mfrow=c(1,1))
B.ANE.PELGAS.BIOMAN.lm2.pred=
  data.frame(predict(B.ANE.PELGAS.BIOMAN.lm3.step,se.fit = TRUE,
                     interval = "confidence"))
B.ANE.PELGAS.BIOMAN.lm2.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom

B.ANE.PELGAS.BIOMAN.lm3=lm(BIOMAN.biom~PELGAS.biom*factor(hclust1.3),
                           BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.lm3)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMAN.lm3)
par(mfrow=c(1,1))

B.ANE.PELGAS.BIOMAN.lm3.step=step(B.ANE.PELGAS.BIOMAN.lm3)
summary(B.ANE.PELGAS.BIOMAN.lm3.step)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMAN.lm3.step)
par(mfrow=c(1,1))
B.ANE.PELGAS.BIOMAN.lm3.step.pred=
  data.frame(predict(B.ANE.PELGAS.BIOMAN.lm3.step,se.fit = TRUE,
                     interval = "confidence"))
B.ANE.PELGAS.BIOMAN.lm3.step.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom

library(lme4)
B.ANE.PELGAS.BIOMAN.lme=lmer(BIOMAN.biom~PELGAS.biom+(1|hclust1.3),
                             BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.lme)
plot(B.ANE.PELGAS.BIOMAN.lme)
qqnorm(residuals(B.ANE.PELGAS.BIOMAN.lme))
qqline(residuals(B.ANE.PELGAS.BIOMAN.lme))
hist(residuals(B.ANE.PELGAS.BIOMAN.lme))
B.ANE.PELGAS.BIOMAN.lme.pred=
  data.frame(predict(B.ANE.PELGAS.BIOMAN.lme,se.fit = TRUE,
                     interval = "confidence"))
B.ANE.PELGAS.BIOMAN.lme.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom

library(mgcv)

B.ANE.PELGAS.BIOMAN.gam1=gam(BIOMAN.biom~s(PELGAS.biom)+s(year),
                           data=BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.gam1)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMAN.gam1)
par(mfrow=c(1,1))
qqnorm(B.ANE.PELGAS.BIOMAN.gam1$residuals)
qqline(B.ANE.PELGAS.BIOMAN.gam1$residuals)
hist(B.ANE.PELGAS.BIOMAN.gam1$residuals)

ggplot(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
       aes(x = PELGAS.biom, y = BIOMAN.biom))+
  geom_text(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
            aes(x = PELGAS.biom, y = BIOMAN.biom,label=year,
                colour=factor(hclust1.3))) + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV),
                    colour=factor(hclust1.3)),
                data = BACVtotPtot.PELGASs.BIOMAN.ANE) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV),
                     colour=factor(hclust1.3)),
                 data = BACVtotPtot.PELGASs.BIOMAN.ANE)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.pred, 
            aes(x = PELGAS.biom, y = fit.fit),colour='orange')+
  geom_point(data = B.ANE.PELGAS.BIOMAN.lm3.step.pred, 
             aes(x = PELGAS.biom, y = fit.fit),colour='blue')+
  geom_point(data = B.ANE.PELGAS.BIOMAN.lme.pred, 
             aes(x = PELGAS.biom, y = fit),colour='red')

#### 3.3.2.2. Without outliers ---------
BACVtotPtot.PELGASs.BIOMAN.ANEs=BACVtotPtot.PELGASs.BIOMAN.ANE[
  !BACVtotPtot.PELGASs.BIOMAN.ANE$year%in%c(2015,2021),]

hist(BACVtotPtot.PELGASs.BIOMAN.ANEs$PELGAS.biom)
hist(BACVtotPtot.PELGASs.BIOMAN.ANEs$BIOMAN.biom)
B.ANE.PELGAS.BIOMANs.lm=lm(BIOMAN.biom~PELGAS.biom-1,
                           BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.lm)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMANs.lm)
par(mfrow=c(1,1))
B.ANE.PELGAS.BIOMANs.lm.pred=
  data.frame(predict(B.ANE.PELGAS.BIOMANs.lm,se.fit = TRUE,
                     interval = "confidence"))
B.ANE.PELGAS.BIOMANs.lm.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANEs$PELGAS.biom

B.ANE.PELGAS.BIOMANs.lm2=lm(BIOMAN.biom~PELGAS.biom+factor(hclust1.3)-1,
                            BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.lm2)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMANs.lm2)
par(mfrow=c(1,1))
B.ANE.PELGAS.BIOMANs.lm2.pred=
  data.frame(predict(B.ANE.PELGAS.BIOMANs.lm2,se.fit = TRUE,
                     interval = "confidence"))
B.ANE.PELGAS.BIOMANs.lm2.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANEs$PELGAS.biom

B.ANE.PELGAS.BIOMANs.lm3=lm(BIOMAN.biom~PELGAS.biom*factor(hclust1.3)-1,
                            BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.lm3)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMANs.lm3)
par(mfrow=c(1,1))
B.ANE.PELGAS.BIOMANs.lm3.residuals=
  data.frame(resid=B.ANE.PELGAS.BIOMANs.lm3$residuals)
B.ANE.PELGAS.BIOMANs.lm3.residuals$year=
  row.names(B.ANE.PELGAS.BIOMANs.lm3.residuals)
# Check time trend in residuals
x11()
ggplot(data=B.ANE.PELGAS.BIOMANs.lm3.residuals,
       aes(x=year,y=resid,label=year))+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")+
  geom_text()
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "AcPeleggBioLM1ResidualsTimeSeries_ANE.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=B.ANE.PELGAS.BIOMANs.lm3.residuals$resid,
             tx=B.ANE.PELGAS.BIOMANs.lm3.residuals$year,
             x0=2000,dx=1)

B.ANE.PELGAS.BIOMANs.lm3.step=step(B.ANE.PELGAS.BIOMANs.lm3)
summary(B.ANE.PELGAS.BIOMANs.lm3.step)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMANs.lm3.step)
par(mfrow=c(1,1))

AIC(B.ANE.PELGAS.BIOMANs.lm,B.ANE.PELGAS.BIOMANs.lm2,
    B.ANE.PELGAS.BIOMANs.lm3,B.ANE.PELGAS.BIOMANs.lm3.step)

BACVtotPtot.PELGASs.BIOMAN.ANEs$vi=
  (BACVtotPtot.PELGASs.BIOMAN.ANEs$BIOMAN.biom.CV*
  BACVtotPtot.PELGASs.BIOMAN.ANEs$BIOMAN.biom)^2

B.ANE.PELGAS.BIOMANs.wlm3=lm(BIOMAN.biom~PELGAS.biom*factor(hclust1.3)-1,
                            BACVtotPtot.PELGASs.BIOMAN.ANEs,
                            weights=1/BACVtotPtot.PELGASs.BIOMAN.ANEs$vi)
summary(B.ANE.PELGAS.BIOMANs.wlm3)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMANs.wlm3)
par(mfrow=c(1,1))

x11()
ggplot(data=BACVtotPtot.PELGASs.BIOMAN.ANEs, 
       aes(x = PELGAS.biom, y = BIOMAN.biom,
           label=year) ) +
  geom_text() +
  geom_smooth(method = "lm", se = TRUE,alpha = .15, 
              aes(fill = factor(hclust1.3)),
              method.args=list(
                weights=
                  1/BACVtotPtot.PELGASs.BIOMAN.ANEs$vi))+
  labs(fill = "Cluster")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "AcPeleggBioLM2-2periods_ANE.png",sep=""), 
          units='cm',width=20, height=12,res=300)

AIC(B.ANE.PELGAS.BIOMANs.lm,B.ANE.PELGAS.BIOMANs.lm2)

B.ANE.PELGAS.BIOMANs.glm3=lm(BIOMAN.biom~PELGAS.biom*factor(hclust1.3)-1,
                             family=Gamma(link = log),
                             BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.glm3)
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMANs.glm3)
par(mfrow=c(1,1))

B.ANE.PELGAS.BIOMANs.gam1=gam(BIOMAN.biom~s(PELGAS.biom),
                             family=Gamma(link = log),
                             data=BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.gam1)
plot(B.ANE.PELGAS.BIOMANs.gam1)

B.ANE.PELGAS.BIOMANs.gam2=gam(BIOMAN.biom~s(PELGAS.biom)+s(year),
                              family=Gamma(link = log),
                              data=BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.gam2)
plot(B.ANE.PELGAS.BIOMANs.gam2,pages=1)

B.ANE.PELGAS.BIOMANs.gam3=gam(BIOMAN.biom~te(PELGAS.biom,year),
                              family=Gamma(link = log),
                              data=BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.gam3)
plot(B.ANE.PELGAS.BIOMANs.gam3,pages=1)
vis.gam(B.ANE.PELGAS.BIOMANs.gam3,theta= -135)
vis.gam(B.ANE.PELGAS.BIOMANs.gam3,plot.type="contour",color="heat")

ggplot(data=BACVtotPtot.PELGASs.BIOMAN.ANEs, 
       aes(x = PELGAS.biom, y = BIOMAN.biom,label=year)) +
  geom_text() +
  geom_smooth(formula = y ~ s(x, bs = "cs"),
              method = "gam", size = 0.7,
              method.args = list(family=Gamma(link = log)),
              aes(color = "gam", fill = "confidence interval")) +
  geom_smooth(method = "lm", se = TRUE,alpha = .15, 
              aes(color = "lm",fill = factor(hclust1.3)))

ggplot(data=BACVtotPtot.PELGASs.BIOMAN.ANEs, 
       aes(x = PELGAS.biom, y = BIOMAN.biom,label=year,
           colour=factor(hclust1.3))) +
  geom_text() +
  geom_smooth(method = "glm", size = 0.7,
              method.args = list(family=Gamma(link = log)),
              aes(color='glm',fill = factor(hclust1.3)))+
  geom_smooth(method = "lm", se = TRUE,alpha = .05, 
            aes(color='lm',fill = factor(hclust1.3)))

B.ANE.PELGAS.BIOMANs.wlm=lm(BIOMAN.biom~PELGAS.biom-1,
                            BACVtotPtot.PELGASs.BIOMAN.ANEs,
                            weights=1/BACVtotPtot.PELGASs.BIOMAN.ANEs$vi)
summary(B.ANE.PELGAS.BIOMANs.wlm)
B.ANE.PELGAS.BIOMANs.wlm.pred=
  data.frame(predict(B.ANE.PELGAS.BIOMANs.wlm,se.fit = TRUE,
                     interval = "confidence"))
B.ANE.PELGAS.BIOMANs.wlm.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANEs$PELGAS.biom[-21]

B.ANE.PELGAS.BIOMANs.glm=glm(BIOMAN.biom~PELGAS.biom-1,
                             BACVtotPtot.PELGASs.BIOMAN.ANEs,
                             family=Gamma(link = log))
par(mfrow=c(2,2))
plot(B.ANE.PELGAS.BIOMANs.glm)
par(mfrow=c(1,1))
hist(residuals(B.ANE.PELGAS.BIOMANs.glm))
B.ANE.PELGAS.BIOMANs.glm.pred=
  data.frame(fit=predict(B.ANE.PELGAS.BIOMANs.glm,type = "response"))
B.ANE.PELGAS.BIOMANs.glm.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANEs$PELGAS.biom[-21]

library(mgcv)
B.ANE.PELGAS.BIOMANs.gam1=gam(BIOMAN.biom~s(PELGAS.biom),
                              data=BACVtotPtot.PELGASs.BIOMAN.ANEs,
                              family=Gamma(link = log))
summary(B.ANE.PELGAS.BIOMANs.gam1)
plot(B.ANE.PELGAS.BIOMANs.gam1,residuals=TRUE)


B.ANE.PELGAS.BIOMANs.gam2=gam(BIOMAN.biom~s(PELGAS.biom),
                              data=BACVtotPtot.PELGASs.BIOMAN.ANEs)
summary(B.ANE.PELGAS.BIOMANs.gam2)
plot(B.ANE.PELGAS.BIOMANs.gam2)

AIC(B.ANE.PELGAS.BIOMANs.lm,B.ANE.PELGAS.BIOMANs.lm2,
    B.ANE.PELGAS.BIOMANs.gam1,B.ANE.PELGAS.BIOMANs.gam2)

ggplot(data = BACVtotPtot.PELGASs.BIOMAN.ANEs, 
       aes(x = PELGAS.biom, y = BIOMAN.biom))+
  geom_text(data = BACVtotPtot.PELGASs.BIOMAN.ANEs, 
            aes(x = PELGAS.biom, y = BIOMAN.biom,label=year,col=hclust)) + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV)),
                data = BACVtotPtot.PELGASs.BIOMAN.ANEs) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BACVtotPtot.PELGASs.BIOMAN.ANEs)+
  geom_line(data = B.ANE.PELGAS.BIOMANs.lm.pred, 
            aes(x = PELGAS.biom, y = fit.fit),colour='orange')+
  geom_line(data = B.ANE.PELGAS.BIOMANs.wlm.pred, 
            aes(x = PELGAS.biom, y = fit.fit),colour='blue')

# lm with error propagation
library(metafor)
B.ANE.PELGAS.BIOMAN.lm.rma <- rma(BIOMAN.biom, 
                                  vi=BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV,
                                  mods = ~PELGAS.biom-1, 
                                  data=BACVtotPtot.PELGASs.BIOMAN.ANE)

head(BACVtotPtot.PELGASs.BIOMAN.ANE)

BACVtotPtot.PELGASs.BIOMAN.ANE$vi=
  (BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV*
     BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom)^2
BACVtotPtot.PELGASs.BIOMAN.ANE$varmodi=
  (BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV*
     BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom)^2

B.ANE.PELGAS.BIOMAN.lm.rma1 <- rma(BIOMAN.biom, 
                                   vi=BACVtotPtot.PELGASs.BIOMAN.ANE$vi,
                                   mods = ~PELGAS.biom-1, 
                                   data=BACVtotPtot.PELGASs.BIOMAN.ANE)
B.ANE.PELGAS.BIOMAN.lm.rma1.pred=data.frame(
  predict(B.ANE.PELGAS.BIOMAN.lm.rma1))
B.ANE.PELGAS.BIOMAN.lm.rma1.pred$PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom[-24]
head(B.ANE.PELGAS.BIOMAN.lm.rma1.pred)

qqnorm(residuals(B.ANE.PELGAS.BIOMAN.lm.rma1))
qqline(residuals(B.ANE.PELGAS.BIOMAN.lm.rma1))
hist(residuals(B.ANE.PELGAS.BIOMAN.lm.rma1))

ggplot(data = B.ANE.PELGAS.BIOMAN.lm.rma1.pred, 
       aes(x = PELGAS.biom, y = pred))+
  geom_ribbon(aes(ymin = ci.lb, ymax = ci.ub),fill='grey',
              data = B.ANE.PELGAS.BIOMAN.lm.rma1.pred)+geom_line()+
  geom_point(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
             aes(x = PELGAS.biom, y = BIOMAN.biom))

B.ANE.PELGAS.BIOMAN.lm.rma2 <- rma(BIOMAN.biom, 
                                   vi=BACVtotPtot.PELGASs.BIOMAN.ANE$vi,
                                   mods = ~PELGAS.biom-1,
                                   scale = ~ varmodi,
                                   data=BACVtotPtot.PELGASs.BIOMAN.ANE)
B.ANE.PELGAS.BIOMAN.lm.rma2.pred=data.frame(
  predict(B.ANE.PELGAS.BIOMAN.lm.rma2))
B.ANE.PELGAS.BIOMAN.lm.rma2.pred$PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom[-24]
head(B.ANE.PELGAS.BIOMAN.lm.rma2.pred)

qqnorm(residuals(B.ANE.PELGAS.BIOMAN.lm.rma2))
qqline(residuals(B.ANE.PELGAS.BIOMAN.lm.rma2))
hist(residuals(B.ANE.PELGAS.BIOMAN.lm.rma2))

ggplot(data = B.ANE.PELGAS.BIOMAN.lm.rma2.pred, 
       aes(x = PELGAS.biom, y = pred))+
  geom_ribbon(aes(ymin = ci.lb, ymax = ci.ub),fill='grey',
              data = B.ANE.PELGAS.BIOMAN.lm.rma2.pred)+geom_line()+
  geom_point(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
             aes(x = PELGAS.biom, y = BIOMAN.biom))

ggplot(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
       aes(x = PELGAS.biom, y = BIOMAN.biom))+
  geom_text(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
            aes(x = PELGAS.biom, y = BIOMAN.biom,label=year)) + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV)),
                data = BACVtotPtot.PELGASs.BIOMAN.ANE) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BACVtotPtot.PELGASs.BIOMAN.ANE)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma1.pred, 
            aes(x = PELGAS.biom, y = pred),colour='red')+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma1.pred, 
            aes(x = PELGAS.biom, y = ci.lb),colour='red',linetype = "dotdash")+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma1.pred, 
            aes(x = PELGAS.biom, y = ci.ub),colour='red',linetype = "dotdash")+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma2.pred, 
            aes(x = PELGAS.biom, y = pred),colour='blue')+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma2.pred, 
            aes(x = PELGAS.biom, y = ci.lb),colour='blue',linetype = "dotdash")+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma2.pred, 
            aes(x = PELGAS.biom, y = ci.ub),colour='blue',linetype = "dotdash")+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.pred, 
            aes(x = PELGAS.biom, y = fit.fit),colour='orange')+  
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.pred,
            aes(x = PELGAS.biom, y = fit.lwr),colour='orange',
            linetype = "dotdash")+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.pred,
            aes(x = PELGAS.biom, y = fit.upr),colour='orange',
            linetype = "dotdash")

# Robust regression
library(MASS)
B.ANE.PELGAS.BIOMAN.rlm <- rlm(BIOMAN.biom~PELGAS.biom-1,
                               BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.rlm)
B.ANE.PELGAS.BIOMAN.rlm.pred=data.frame(
  pred=predict(B.ANE.PELGAS.BIOMAN.rlm))
B.ANE.PELGAS.BIOMAN.rlm.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom[-24]

qqnorm(residuals(B.ANE.PELGAS.BIOMAN.rlm))
qqline(residuals(B.ANE.PELGAS.BIOMAN.rlm))
hist(residuals(B.ANE.PELGAS.BIOMAN.rlm))

# Quantile regression
library(quantreg)

B.ANE.PELGAS.BIOMAN.rq <- rq(BIOMAN.biom~PELGAS.biom-1,
                             data=BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(B.ANE.PELGAS.BIOMAN.rq)
B.ANE.PELGAS.BIOMAN.rq.pred=data.frame(
  pred=predict(B.ANE.PELGAS.BIOMAN.rq))
B.ANE.PELGAS.BIOMAN.rq.pred$PELGAS.biom=
  BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom[-24]

qqnorm(residuals(B.ANE.PELGAS.BIOMAN.rq))
qqline(residuals(B.ANE.PELGAS.BIOMAN.rq))
hist(residuals(B.ANE.PELGAS.BIOMAN.rq))

ggplot(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
       aes(x = PELGAS.biom, y = BIOMAN.biom))+
  geom_text(data = BACVtotPtot.PELGASs.BIOMAN.ANE, 
            aes(x = PELGAS.biom, y = BIOMAN.biom,label=year)) + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV)),
                data = BACVtotPtot.PELGASs.BIOMAN.ANE) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BACVtotPtot.PELGASs.BIOMAN.ANE)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma1.pred, 
            aes(x = PELGAS.biom, y = pred),colour='red')+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.rma2.pred, 
            aes(x = PELGAS.biom, y = pred),colour='blue')+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.pred, 
            aes(x = PELGAS.biom, y = fit.fit),colour='orange')+  
  geom_line(data = B.ANE.PELGAS.BIOMAN.rlm.pred, 
            aes(x = PELGAS.biom, y = pred),colour='black')+
  geom_line(data = B.ANE.PELGAS.BIOMAN.rlm.pred, 
            aes(x = PELGAS.biom, y = pred),colour='black',linetype = "dotdash")

### 3.3.3. Plots -----
#### 3.3.3.1. All years ----
x11()
ggplot(data = B.ANE.PELGAS.BIOMAN.lm.preds, 
       aes(x = PELGAS.biom, y = BIOMAN.biom,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV)),
                data = B.ANE.PELGAS.BIOMAN.lm.preds) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = B.ANE.PELGAS.BIOMAN.lm.preds)+
  geom_ribbon(alpha = .15)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.preds, 
            aes(x = PELGAS.biom, y = fit),colour='red') +
  geom_ribbon(alpha = .15)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm.preds, 
            aes(x = PELGAS.biom, y = fit,colour='Linear model'),
            size=1.5)+
   labs(
     title = paste(
       "Anchovy, ",
       get.lm.res(model=BPtot.ANE.PELGAS.BIOMAN.lm)$model_eqn,'\n',
       "Adj R2 = ",get.lm.res(BPtot.ANE.PELGAS.BIOMAN.lm)$AdjR2i,
       " P =",get.lm.res(BPtot.ANE.PELGAS.BIOMAN.lm)$Pi))+
  xlab('PELGAS acoustic biomass (tons), 8abd')+
  ylab('BIOMAN DEPM biomass (tons), 8abcd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASacouVsBIOMANdepmBiomass_LM-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

#### 3.3.3.2. Without outliers ----
x11()
ggplot(data = B.ANE.PELGAS.BIOMAN.lm2.preds, 
       aes(x = PELGAS.biom, y = BIOMAN.biom,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV))) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)))+
  geom_ribbon(alpha = .15)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm2.preds, 
            aes(x = PELGAS.biom, y = fit),colour='red') +
  geom_ribbon(alpha = .15)+
  geom_line(data = B.ANE.PELGAS.BIOMAN.lm2.preds, 
            aes(x = PELGAS.biom, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=BPtot.ANE.PELGAS.BIOMAN.lm2)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.ANE.PELGAS.BIOMAN.lm2)$AdjR2i,
      " P =",get.lm.res(BPtot.ANE.PELGAS.BIOMAN.lm2)$Pi))+
  xlab('PELGAS acoustic biomass (tons), 8abd')+
  ylab('BIOMAN DEPM biomass (tons), 8abcd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASacouVsBIOMANdepmBiomass_LMwoOutliers-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

#### 3.3.3.3. Without outliers, piecewise model ----
x11()
ggplot(data = BACVtotPtot.PELGASs.BIOMAN.ANEs, 
       aes(x = PELGAS.biom, y = BIOMAN.biom,label=year,
           colour = factor(hclust1.3)))+
  geom_text() + 
  geom_errorbar(aes(ymin = BIOMAN.biom*(1-BIOMAN.biom.CV),
                    ymax = BIOMAN.biom*(1+BIOMAN.biom.CV))) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)))+
  geom_smooth(method = "lm", se = TRUE,alpha = .15, 
              aes(fill = factor(hclust1.3)))+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=B.ANE.PELGAS.BIOMAN.lm3)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(B.ANE.PELGAS.BIOMAN.lm3)$AdjR2i,
      " P =",get.lm.res(B.ANE.PELGAS.BIOMAN.lm3)$Pi),
       fill = "Model",colour="PCA cluster")+
  xlab('PELGAS acoustic biomass (tons), 8abd')+
  ylab('BIOMAN DEPM biomass (tons), 8abcd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASacouVsBIOMANdepmBiomass_LMwoOutliers-2periods-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

## 3.4. ANE PELGAS Ptot vs. BIOMAN Ptot -----------
### 3.4.1. LM all years ------
hist(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)
Ptot.ANE.PELGAS.BIOMAN.lm=lm(BIOMAN.Ptot~PELGAS.Ptot-1,
                              BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(Ptot.ANE.PELGAS.BIOMAN.lm)
head(BACVtotPtot.PELGASs.BIOMAN.ANE)
ANE.PELGAS.Ptot.newd=PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot
Ptot.ANE.PELGAS.BIOMAN.lm.preds <- data.frame(predict(
  Ptot.ANE.PELGAS.BIOMAN.lm,newdata = data.frame(PELGAS.Ptot=ANE.PELGAS.Ptot.newd), 
  interval = 'confidence'))
Ptot.ANE.PELGAS.BIOMAN.lm.preds$year=BACVtotPtot.PELGASs.BIOMAN.ANE$year
Ptot.ANE.PELGAS.BIOMAN.lm.preds=merge(
  Ptot.ANE.PELGAS.BIOMAN.lm.preds,
  BACVtotPtot.PELGASs.BIOMAN.ANE[
    ,c('year','PELGAS.Ptot','PELGAS.Ptot.sd','BIOMAN.Ptot','BIOMAN.Ptot.sd')],
  by.x='year',by.y='year')
head(Ptot.ANE.PELGAS.BIOMAN.lm.preds)
x11()
par(mfrow=c(2,2))
plot(Ptot.ANE.PELGAS.BIOMAN.lm)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtot-LM1-ANE_residualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
Ptot.ANE.PELGAS.BIOMAN.lm.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.ANE$year,
  resid=Ptot.ANE.PELGAS.BIOMAN.lm$residuals)
# Check time trend in residuals
x11()
ggplot(data=Ptot.ANE.PELGAS.BIOMAN.lm.residuals,
       aes(x=year,y=resid,label=year))+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")+
  geom_text()
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtot-LM1-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=Ptot.ANE.PELGAS.BIOMAN.lm.residuals$resid,
             tx=Ptot.ANE.PELGAS.BIOMAN.lm.residuals$year,
             x0=2000,dx=1)

### 3.4.2. LM without outliers ------
BACVtotPtot.PELGASs.BIOMAN.ANEs2=BACVtotPtot.PELGASs.BIOMAN.ANE[
  BACVtotPtot.PELGASs.BIOMAN.ANE$year!=2022,]
Ptot.ANE.PELGAS.BIOMAN.lm2=lm(BIOMAN.Ptot~PELGAS.Ptot-1,
                             BACVtotPtot.PELGASs.BIOMAN.ANEs2)
summary(Ptot.ANE.PELGAS.BIOMAN.lm2)
ANE.PELGAS.Ptot.newd2=BACVtotPtot.PELGASs.BIOMAN.ANEs2$PELGAS.Ptot
Ptot.ANE.PELGAS.BIOMAN.lm.preds2 <- data.frame(predict(
  Ptot.ANE.PELGAS.BIOMAN.lm2,
  newdata = data.frame(PELGAS.Ptot=ANE.PELGAS.Ptot.newd2), 
  interval = 'confidence'))
Ptot.ANE.PELGAS.BIOMAN.lm.preds2$year=BACVtotPtot.PELGASs.BIOMAN.ANEs2$year
Ptot.ANE.PELGAS.BIOMAN.lm.preds2=merge(
  Ptot.ANE.PELGAS.BIOMAN.lm.preds2,
  BACVtotPtot.PELGASs.BIOMAN.ANEs2[
    ,c('year','PELGAS.Ptot','PELGAS.Ptot.sd','BIOMAN.Ptot','BIOMAN.Ptot.sd')],
  by.x='year',by.y='year')
head(Ptot.ANE.PELGAS.BIOMAN.lm.preds2)
x11()
par(mfrow=c(2,2),bg='white')
plot(Ptot.ANE.PELGAS.BIOMAN.lm2)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtotwoOutliers-LM-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
Ptot.ANE.PELGAS.BIOMAN.lm2.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.ANEs2$year,
  resid=Ptot.ANE.PELGAS.BIOMAN.lm2$residuals)
# Check time trend in residuals
x11()
ggplot(data=Ptot.ANE.PELGAS.BIOMAN.lm2.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtotwoOutliers-LM-ANE_ResidualsLM2TimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=Ptot.ANE.PELGAS.BIOMAN.lm2.residuals$resid,
             tx=Ptot.ANE.PELGAS.BIOMAN.lm2.residuals$year,
             x0=2000,dx=1)

BPtot.ANE.PELGAS.BIOMAN.glm=glm(BIOMAN.Ptot~PELGAS.Ptot-1,family=Gamma,
                                BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(BPtot.ANE.PELGAS.BIOMAN.glm)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGAS.BIOMAN.glm)
par(mfrow=c(1,1))
graphics.off()

BPtot.ANE.PELGAS.BIOMAN.glm2=glm(BIOMAN.Ptot~PELGAS.Ptot*hclust1.3-1,
                                 family=Gamma,
                                BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(BPtot.ANE.PELGAS.BIOMAN.glm2)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGAS.BIOMAN.glm2)
par(mfrow=c(1,1))
graphics.off()

### 3.4.3. Plots -----
#### 3.4.3.1. All years ----
x11()
ggplot(data = Ptot.ANE.PELGAS.BIOMAN.lm.preds, 
       aes(x = PELGAS.Ptot, y = BIOMAN.Ptot,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = BIOMAN.Ptot*(1-BIOMAN.Ptot.sd),
                    ymax = BIOMAN.Ptot*(1+BIOMAN.Ptot.sd)),
                data = Ptot.ANE.PELGAS.BIOMAN.lm.preds) + 
  geom_errorbarh(aes(xmin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                     xmax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                 data = Ptot.ANE.PELGAS.BIOMAN.lm.preds)+
  geom_ribbon(alpha = .15)+
  geom_line(data = Ptot.ANE.PELGAS.BIOMAN.lm.preds, 
            aes(x = PELGAS.Ptot, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=Ptot.ANE.PELGAS.BIOMAN.lm)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(Ptot.ANE.PELGAS.BIOMAN.lm)$AdjR2i,
      " P =",get.lm.res(Ptot.ANE.PELGAS.BIOMAN.lm)$Pi))+
  xlab('PELGAS CUFES Ptot, 8abd')+
  ylab('BIOMAN DEPM Ptot, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGAScufesVsBIOMANdepmPtot-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

#### 3.4.2.2. Without outliers ----
x11()
ggplot(data = Ptot.ANE.PELGAS.BIOMAN.lm.preds2, 
       aes(x = PELGAS.Ptot, y = BIOMAN.Ptot,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = BIOMAN.Ptot*(1-BIOMAN.Ptot.sd),
                    ymax = BIOMAN.Ptot*(1+BIOMAN.Ptot.sd)),
                data = Ptot.ANE.PELGAS.BIOMAN.lm.preds2) + 
  geom_errorbarh(aes(xmin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                     xmax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                 data = Ptot.ANE.PELGAS.BIOMAN.lm.preds2)+
  geom_ribbon(alpha = .15)+
  geom_line(data = Ptot.ANE.PELGAS.BIOMAN.lm.preds2, 
            aes(x = PELGAS.Ptot, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=Ptot.ANE.PELGAS.BIOMAN.lm2)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(Ptot.ANE.PELGAS.BIOMAN.lm2)$AdjR2i,
      " P =",get.lm.res(Ptot.ANE.PELGAS.BIOMAN.lm2)$Pi))+
  xlab('PELGAS CUFES Ptot, 8abd')+
  ylab('BIOMAN DEPM Ptot, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGAScufesVsBIOMANdepmPtotwoOutliers-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

## 3.5. ANE PELGAS biomass vs. PELGAS Ptot -----------
### 3.5.1. LM all years ------
BPtot.ANE.PELGASbiomass.PELGAS.lm=lm(PELGAS.Ptot~PELGAS.biom-1,
                                     BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(BPtot.ANE.PELGASbiomass.PELGAS.lm)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm)
par(mfrow=c(1,1))
ane.PELGAS.Ptotb.newd=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds <- data.frame(predict(
  BPtot.ANE.PELGASbiomass.PELGAS.lm,
  newdata = data.frame(PELGAS.biom=ane.PELGAS.Ptotb.newd), 
  interval = 'confidence'))
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds$year=BACVtotPtot.PELGASs.BIOMAN.ANE$year
head(BACVtotPtot.PELGASs.BIOMAN.ANE)
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds=merge(
  BPtotBiom.ANE.PELGAS.PELGAS.lm.preds,
  BACVtotPtot.PELGASs.BIOMAN.ANE[
    ,c('year','PELGAS.biom','PELGAS.biom.CV',
       'PELGAS.Ptot','PELGAS.Ptot.sd')],
  by.x='year',by.y='year')
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtot-LM1-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
BPtot.ANE.PELGASbiomass.PELGAS.lm.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.ANE$year,
  resid=BPtot.ANE.PELGASbiomass.PELGAS.lm$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.ANE.PELGASbiomass.PELGAS.lm.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtot-LM1-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.ANE.PELGASbiomass.PELGAS.lm.residuals$resid,
             tx=BPtot.ANE.PELGASbiomass.PELGAS.lm.residuals$year,
             x0=2000,dx=1)

### 3.5.1. LM without outliers ------
BACVtotPtot.PELGASs.BIOMAN.ANEs3=BACVtotPtot.PELGASs.BIOMAN.ANE[
  !BACVtotPtot.PELGASs.BIOMAN.ANE$year%in%c(2015,2021,2022),]

BPtot.ANE.PELGASbiomass.PELGAS.lm2=lm(PELGAS.Ptot~PELGAS.biom-1,
                                     BACVtotPtot.PELGASs.BIOMAN.ANEs3)
summary(BPtot.ANE.PELGASbiomass.PELGAS.lm2)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm2)
par(mfrow=c(1,1))
ane.PELGAS.Ptotb.newd2=BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.biom
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2 <- data.frame(predict(
  BPtot.ANE.PELGASbiomass.PELGAS.lm2,
  newdata = data.frame(PELGAS.biom=ane.PELGAS.Ptotb.newd2), 
  interval = 'confidence'))
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2$year=BACVtotPtot.PELGASs.BIOMAN.ANEs3$year
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2=merge(
  BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2,
  BACVtotPtot.PELGASs.BIOMAN.ANEs3[
    ,c('year','PELGAS.biom','PELGAS.biom.CV',
       'PELGAS.Ptot','PELGAS.Ptot.sd')],
  by.x='year',by.y='year')
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm2)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtotWoOutliers-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.ANEs3$year,
  resid=BPtot.ANE.PELGASbiomass.PELGAS.lm2$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtotWoOutliers-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals$resid,
             tx=BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals$year,
             x0=2000,dx=1)

# 2 periods without outliers
BPtot.ANE.PELGASbiomass.PELGAS.lm3=lm(
  PELGAS.Ptot~PELGAS.biom*factor(hclust1.3)-1,
                                      data=BACVtotPtot.PELGASs.BIOMAN.ANEs3)
summary(BPtot.ANE.PELGASbiomass.PELGAS.lm3)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm3)
par(mfrow=c(1,1))
dev.print(png, file=paste(
  path.export.AcEgg,
  "PELGASbiomassVsPELGAScufesPtotWoOutliers-2periods-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)

BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3 <- data.frame(predict(
  BPtot.ANE.PELGASbiomass.PELGAS.lm3,
  newdata = data.frame(PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.biom,
                       hclust1.3=BACVtotPtot.PELGASs.BIOMAN.ANEs3$hclust1.3), 
  interval = 'confidence'))
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$year=BACVtotPtot.PELGASs.BIOMAN.ANEs3$year
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3=merge(
  BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3,
  BACVtotPtot.PELGASs.BIOMAN.ANEs3[
    ,c('year','PELGAS.biom','PELGAS.biom.CV',
       'PELGAS.Ptot','PELGAS.Ptot.sd','hclust1.3')],
  by.x='year',by.y='year')

BPtot.ANE.PELGASbiomass.PELGAS.lm3.residuals=data.frame(
  year=as.numeric(names(BPtot.ANE.PELGASbiomass.PELGAS.lm3$residuals)),
  resid=BPtot.ANE.PELGASbiomass.PELGAS.lm3$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.ANE.PELGASbiomass.PELGAS.lm3.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(
  path.export.AcEgg,
  "PELGASbiomassVsPELGAScufesPtotWoOutliers-2periods-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals$resid,
             tx=BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals$year,
             x0=2000,dx=1)

table(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$year,
      BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$hclust1.3)

plot(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$PELGAS.biom,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$PELGAS.Ptot,type='n')
text(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$PELGAS.biom,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$PELGAS.Ptot,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$year)
abline(b=BPtot.ANE.PELGASbiomass.PELGAS.lm3$coefficients[1],
       a=BPtot.ANE.PELGASbiomass.PELGAS.lm3$coefficients[2])
abline(b=BPtot.ANE.PELGASbiomass.PELGAS.lm3$coefficients[1]+
         BPtot.ANE.PELGASbiomass.PELGAS.lm3$coefficients[4],
       a=BPtot.ANE.PELGASbiomass.PELGAS.lm3$coefficients[3])
points(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$PELGAS.biom,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds3$fit,pch=16,col=2)

BPtot.ANE.PELGASbiomass.PELGAS.lm4=lm(
  PELGAS.Ptot~PELGAS.biom:factor(hclust1.3)-1,
  data=BACVtotPtot.PELGASs.BIOMAN.ANEs3)
summary(BPtot.ANE.PELGASbiomass.PELGAS.lm4)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm4)
par(mfrow=c(1,1))
dev.print(png, file=paste(
  path.export.AcEgg,
  "PELGASbiomassVsPELGAScufesPtotWoOutliers-2periodsNointer-ANE_ResidualsCheck.png",sep=""), 
  units='cm',width=20, height=20,res=300)

BPtot.ANE.PELGASbiomass.PELGAS.lm4.residuals=data.frame(
  year=as.numeric(names(BPtot.ANE.PELGASbiomass.PELGAS.lm4$residuals)),
  resid=BPtot.ANE.PELGASbiomass.PELGAS.lm4$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.ANE.PELGASbiomass.PELGAS.lm4.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(
  path.export.AcEgg,
  "PELGASbiomassVsPELGAScufesPtotWoOutliers-2periods-ANE_ResidualsTimeSeries.png",sep=""), 
  units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals$resid,
             tx=BPtot.ANE.PELGASbiomass.PELGAS.lm2.residuals$year,
             x0=2000,dx=1)

BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4 <- data.frame(predict(
  BPtot.ANE.PELGASbiomass.PELGAS.lm4,
  newdata = data.frame(PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.biom,
                       hclust1.3=BACVtotPtot.PELGASs.BIOMAN.ANEs3$hclust1.3), 
  interval = 'confidence'))
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$year=BACVtotPtot.PELGASs.BIOMAN.ANEs3$year
BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4=merge(
  BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4,
  BACVtotPtot.PELGASs.BIOMAN.ANEs3[
    ,c('year','PELGAS.biom','PELGAS.biom.CV',
       'PELGAS.Ptot','PELGAS.Ptot.sd','hclust1.3')],
  by.x='year',by.y='year')

plot(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$PELGAS.biom,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$PELGAS.Ptot,type='n')
text(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$PELGAS.biom,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$PELGAS.Ptot,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$year)
abline(b=BPtot.ANE.PELGASbiomass.PELGAS.lm4$coefficients[1],
       a=0)
abline(b=BPtot.ANE.PELGASbiomass.PELGAS.lm4$coefficients[2],
       a=0)
points(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$PELGAS.biom,
       BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4$fit,pch=16,col=2)

BPtot.ANE.PELGASbiomass.PELGAS.lm4.residuals=data.frame(
  year=as.numeric(names(BPtot.ANE.PELGASbiomass.PELGAS.lm4$residuals)),
  resid=BPtot.ANE.PELGASbiomass.PELGAS.lm4$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.ANE.PELGASbiomass.PELGAS.lm4.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(
  path.export.AcEgg,
  "PELGASbiomassVsPELGAScufesPtotWoOutliers-2periodsNointer-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.ANE.PELGASbiomass.PELGAS.lm4.residuals$resid,
             tx=BPtot.ANE.PELGASbiomass.PELGAS.lm4.residuals$year,
             x0=2000,dx=1)

AIC(BPtot.ANE.PELGASbiomass.PELGAS.lm3,BPtot.ANE.PELGASbiomass.PELGAS.lm4)

plot(BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.Ptot,
     BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.biom)
plot(BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.biom,
     BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.Ptot)
hist(BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.Ptot)
hist(BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.biom)

BPtot.ANE.PELGASbiomass.PELGAS.lm3.1=lm(
  PELGAS.Ptot~PELGAS.biom*factor(hclust1.3),
  BACVtotPtot.PELGASs.BIOMAN.ANEs3)
summary(BPtot.ANE.PELGASbiomass.PELGAS.lm3.1)

BPtot.ANE.PELGASbiomass.PELGAS.lm3=lm(
  PELGAS.Ptot~PELGAS.biom*factor(hclust1.3)-1,
  data=BACVtotPtot.PELGASs.BIOMAN.ANEs3)
summary(BPtot.ANE.PELGASbiomass.PELGAS.lm3)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm3)
par(mfrow=c(1,1))

BPtot.ANE.PELGASbiomass.PELGAS.lm.cor=lm(
  PELGAS.Ptot~PELGAS.biom.cor-1,BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(BPtot.ANE.PELGASbiomass.PELGAS.lm.cor)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm.cor)
par(mfrow=c(1,1))
ane.PELGAS.Ptotb.newd.cor=c(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor)
BPtotBiom.ANE.PELGAS.PELGAS.lm.cor.preds <- predict(
  BPtot.ANE.PELGASbiomass.PELGAS.lm.cor,
  newdata = data.frame(PELGAS.biom.cor=ane.PELGAS.Ptotb.newd.cor), 
  interval = 'confidence')

# BPtot.ANE.PELGASbiomass.BIOMAN.glm=glm(
#   BIOMAN.Ptot~PELGAS.biom-1,family=Gamma(link="log"),
#   data=BACVtotPtot.PELGASs.BIOMAN.ANE)
# summary(BPtot.ANE.PELGASbiomass.BIOMAN.glm)
# par(mfrow=c(2,2))
# plot(BPtot.ANE.PELGASbiomass.BIOMAN.glm)
# par(mfrow=c(1,1))
# graphics.off()

ane.PELGAS.Ptotb.newd=c(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom)
BPtotBiom.ANE.PELGAS.PELGAS.glm.preds <- data.frame(predict(
  BPtot.ANE.PELGASbiomass.BIOMAN.glm,
  newdata = data.frame(PELGAS.biom=ane.PELGAS.Ptotb.newd), 
  se.fit = TRUE,type = c("response")))

plot(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
     BPtotBiom.ANE.PELGAS.PELGAS.lm.preds$fit)

library(mgcv)
BPtot.ANE.PELGASbiomass.BIOMAN.gam=gam(
  BIOMAN.Ptot~s(PELGAS.biom)-1,family=Gamma(link = "log"),
  data=BACVtotPtot.PELGASs.BIOMAN.ANE)
summary(BPtot.ANE.PELGASbiomass.BIOMAN.gam)
plot(BPtot.ANE.PELGASbiomass.BIOMAN.gam)
par(mfrow=c(2,2))
plot(BPtot.ANE.PELGASbiomass.BIOMAN.glm)
par(mfrow=c(1,1))
graphics.off()

library(segmented)
BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented=
  segmented(BPtot.ANE.PELGASbiomass.PELGAS.lm2)
slope(BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented)
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented)

BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented2=
  segmented(BPtot.ANE.PELGASbiomass.PELGAS.lm2,seg.Z = ~year)
slope(BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented2)
plot(BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented2)

BACVtotPtot.PELGASs.BIOMAN.ANEs3$pwLM.clust1 = 
  factor(ifelse(BACVtotPtot.PELGASs.BIOMAN.ANEs3$PELGAS.biom > 
                  BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented$psi[,2],1,0))

x11()
ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEs3,
       aes(x=PELGAS.biom,y=PELGAS.Ptot,group=pwLM.clust1,label=year)) + 
  geom_text() + geom_smooth(method="lm",formula=y~x,col="red")
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtotWoOutliers-LMsegmented-ANE.png",sep=""), 
          units='cm',width=20, height=12,res=300)

ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEs3,
       aes(x=PELGAS.biom,y=PELGAS.Ptot,group=hclust1.3,label=year)) + 
  geom_text() + geom_smooth(method="lm",formula=y~x,col="red")

ggplot(BACVtotPtot.PELGASs.BIOMAN.ANEs3,
       aes(x=PELGAS.biom,y=PELGAS.Ptot,group=hclust1.3,label=year)) + 
  geom_text() + geom_smooth(method="loess",formula=y~x,col="red")

x11()
qqnorm(BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented$residuals)
qqline(BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented$residuals)
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtotWoOutliers-segmented-ANE_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=12,res=300)

BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented.residuals=data.frame(
  year=as.numeric(dimnames(BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented$residuals)[[1]]),
  resid=BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.ANE.PELGASbiomass.PELGAS.lm2.segmented.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtotWoOutliers-segmented-ANE_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)

### 3.5.2. Plots ------
x11()
par(bg='white')
ggplot(data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds, 
       aes(x = PELGAS.biom, y = PELGAS.Ptot,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                     ymax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                 data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds)+
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds)+
  geom_ribbon(alpha = .15)+
  geom_line(data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds, 
            aes(x = PELGAS.biom, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=BPtot.ANE.PELGASbiomass.PELGAS.lm)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm)$AdjR2i,
      " P =",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm)$Pi))+
  xlab('PELGAS acoustic biomass, 8abd')+
  ylab('PELGAS CUFES Ptot, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASbiomassVsPELGAScufesPtot-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

x11()
par(bg='white')
ggplot(data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2, 
       aes(x = PELGAS.biom, y = PELGAS.Ptot,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                    ymax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2)+
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2)+
  geom_ribbon(alpha = .15)+
  geom_line(data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds2, 
            aes(x = PELGAS.biom, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=BPtot.ANE.PELGASbiomass.PELGAS.lm2)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm2)$AdjR2i,
      " P =",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm2)$Pi))+
  xlab('PELGAS CUFES Ptot, 8abd')+
  ylab('PELGAS acoustic biomass, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASbiomassVsPELGAScufesPtotWoOutliers-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

x11()
par(bg='white')
ggplot(data = BACVtotPtot.PELGASs.BIOMAN.ANEs3, 
       aes(x = PELGAS.biom, y = PELGAS.Ptot,label=year,
           colour = factor(hclust1.3)))+
  geom_text() + 
  geom_errorbar(aes(ymin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                    ymax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot))) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)))+
  geom_smooth(method = "lm", se = TRUE,alpha = .15, 
              aes(fill = factor(hclust1.3)))+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=BPtot.ANE.PELGASbiomass.PELGAS.lm3)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm3)$AdjR2i,
      " P =",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm3)$Pi),
        fill = "Model",colour="Cluster")+
  xlab('PELGAS acoustic biomass (tons), 8abd')+
  ylab('PELGAS CUFES Ptot, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASbiomassVsPELGAScufesPtotWoOutliers-2periods-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

x11()
par(bg='white')
ggplot(data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4, 
       aes(x = PELGAS.biom, y = PELGAS.Ptot,label=year,
           ymin = lwr, ymax = upr,group=hclust1.3,colour=hclust1.3,
           fill=hclust1.3))+
  geom_text() + 
  geom_errorbar(aes(ymin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                    ymax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4)+
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4)+
  geom_ribbon(alpha = .15)+
  geom_line(data = BPtotBiom.ANE.PELGAS.PELGAS.lm.preds4, 
            aes(x = PELGAS.biom, y = fit),
            size=1.5)+
  labs(
    title = paste(
      "Anchovy, ",'\n',
      get.lm.res(model=BPtot.ANE.PELGASbiomass.PELGAS.lm4)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm4)$AdjR2i,
      " P =",get.lm.res(BPtot.ANE.PELGASbiomass.PELGAS.lm4)$Pi))+
  ylab('PELGAS CUFES Ptot, 8abd')+
  xlab('PELGAS acoustic biomass, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='PCA cluster')+guides(fill = FALSE)
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASbiomassVsPELGAScufesPtotWoOutliers-2periodsNointer-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')


#******************************* 
## 3.6. PIL indices time series -----------
#************************* -----------
BACVtotPtot.PELGASs.BIOMAN.PIL=BACVtotPtot.PELGASs.BIOMAN[
  BACVtotPtot.PELGASs.BIOMAN$sp=='SARD-PIL',]
hist(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot)
BACVtotPtot.PELGASs.BIOMAN.PIL=BACVtotPtot.PELGASs.BIOMAN.PIL[
  order(BACVtotPtot.PELGASs.BIOMAN.PIL$year),]
row.names(BACVtotPtot.PELGASs.BIOMAN.PIL)=
  BACVtotPtot.PELGASs.BIOMAN.PIL$year

x11()
par(mfrow=c(3,1),bg='white')
plot(BACVtotPtot.PELGASs.BIOMAN.PIL$year,
     BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom,type='b',pch=16,
     xlab='',ylab='Biomass (MT)',
     main='PELGAS acoustic biomass, sardine, 8abd')
plot(BACVtotPtot.PELGASs.BIOMAN.PIL$year,
     BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,type='b',pch=16,
     xlab='',ylab='CUFES Ptot',
     main='PELGAS total daily egg production, sardine, 8abd')
plot(BACVtotPtot.PELGASs.BIOMAN.PIL$year,
     BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot,type='b',pch=16,
     xlab='',ylab='DEPM Ptot',
     main='BIOMAN DEPM total daily egg production, sardine, 8abd without NW area before 2012')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGAS-BiomassCufes-BIOMANdepmPtot-PIL-TimeSeries.png',
                         sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

# Time trends in indices
PIL.biom.ts.lm=lm(PELGAS.biom~year,
  data=BACVtotPtot.PELGASs.BIOMAN.PIL)
summary(PIL.biom.ts.lm)

## 3.7. PCA and clustering on indices -------------
head(BACVtotPtot.PELGASs.BIOMAN.PIL)
names(BACVtotPtot.PELGASs.BIOMAN.PIL)
row.names(BACVtotPtot.PELGASs.BIOMAN.PIL)=BACVtotPtot.PELGASs.BIOMAN.PIL$year
BACVtotPtot.PELGASs.BIOMAN.PILs=BACVtotPtot.PELGASs.BIOMAN.PIL[
  !is.na(BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot),]
X.PIL=apply(BACVtotPtot.PELGASs.BIOMAN.PILs[
  ,c("PELGAS.biom","PELGAS.abun","PELGAS.biom.CV","PELGAS.meanWeight",
     "PELGAS.Ptot","PELGAS.Ptot.sd","BIOMAN.Ptot",
     "PELGAS.DF","PELGAS.RFS")],2,as.numeric)
X.PIL=apply(X.PIL,2,scale)
dimnames(X.PIL)[[1]]=BACVtotPtot.PELGASs.BIOMAN.PILs$year
# PCA
PELGASs.BIOMAN.PIL.pca <- PCA(X.PIL,scale.unit = FALSE)
barplot(PELGASs.BIOMAN.PIL.pca$eig[,1],main="Eigenvalues",
        names.arg=1:nrow(PELGASs.BIOMAN.PIL.pca$eig))
summary(PELGASs.BIOMAN.PIL.pca)
plot(PELGASs.BIOMAN.PIL.pca,choix="ind")
plot(PELGASs.BIOMAN.PIL.pca,choix="var")
dimdesc(PELGASs.BIOMAN.PIL.pca, axes = 1:2)
x11()
fviz_pca_biplot(PELGASs.BIOMAN.PIL.pca)
dev.print(png, file=paste(path.export.AcEgg,
                          "PCAacEgg_PIL.png",sep=""), 
          units='cm',width=20, height=20,res=300)
# Clustering
PELGASs.BIOMAN.PIL.pca.hclust4=HCPC(PELGASs.BIOMAN.PIL.pca,nb.clust=4)
BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.4=NA
BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.4=
  PELGASs.BIOMAN.PIL.pca.hclust4$data.clust$clust
x11()
plot(PELGASs.BIOMAN.PIL.pca.hclust4,choice='tree')
dev.print(png, file=paste(path.export.AcEgg,
                          "HCPCtree4-PCAacEgg_PIL.png",sep=""), 
          units='cm',width=20, height=20,res=300)

PELGASs.BIOMAN.PIL.pca.hclust3=HCPC(PELGASs.BIOMAN.PIL.pca,nb.clust=3)
BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.3=NA
BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.3=
  PELGASs.BIOMAN.PIL.pca.hclust3$data.clust$clust
x11()
plot(PELGASs.BIOMAN.PIL.pca.hclust4,choice='tree')
dev.print(png, file=paste(path.export.AcEgg,
                          "HCPCtree3-PCAacEgg_PIL.png",sep=""), 
          units='cm',width=20, height=20,res=300)

PELGASs.BIOMAN.PIL.pca.hclust2=HCPC(PELGASs.BIOMAN.PIL.pca,nb.clust=2)
BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.2=NA
BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.2=
  PELGASs.BIOMAN.PIL.pca.hclust2$data.clust$clust
x11()
plot(PELGASs.BIOMAN.PIL.pca.hclust2,choice='tree')
dev.print(png, file=paste(path.export.AcEgg,
                          "HCPCtree2-PCAacEgg_PIL.png",sep=""), 
          units='cm',width=20, height=20,res=300)

x11()
fviz_pca_biplot(PELGASs.BIOMAN.PIL.pca,
                habillage=factor(
                  BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.2))
dev.print(png, file=paste(path.export.AcEgg,
                          "PCA-HCPC_acEgg_PIL.png",sep=""), 
          units='cm',width=20, height=20,res=300)

## 3.8. PIL PELGAS Ptot vs. BIOMAN Ptot -----------
### 3.8.1. LM ---------
BPtot.PIL.PELGAS.BIOMAN.lm=lm(BIOMAN.Ptot~PELGAS.Ptot-1,
                              BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGAS.BIOMAN.lm)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.PIL.PELGAS.BIOMAN.lm)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtot-LM-PIL_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
pil.PELGAS.Ptot.newd=c(BACVtotPtot.PELGASs.BIOMAN.PILs$PELGAS.Ptot)
BPtot.PIL.PELGAS.BIOMAN.lm.preds <- data.frame(predict(
  BPtot.PIL.PELGAS.BIOMAN.lm,
  newdata = data.frame(PELGAS.Ptot=pil.PELGAS.Ptot.newd), 
  interval = 'confidence'))
BPtot.PIL.PELGAS.BIOMAN.lm.preds$year=BACVtotPtot.PELGASs.BIOMAN.PILs$year
BPtot.PIL.PELGAS.BIOMAN.lm.preds=merge(
  BPtot.PIL.PELGAS.BIOMAN.lm.preds,
  BACVtotPtot.PELGASs.BIOMAN.PILs[
    ,c('year','PELGAS.Ptot','PELGAS.Ptot.sd','BIOMAN.Ptot','BIOMAN.Ptot.sd')],
  by.x='year',by.y='year')
plot(BPtot.PIL.PELGAS.BIOMAN.lm.preds$PELGAS.Ptot,
     BPtot.PIL.PELGAS.BIOMAN.lm.preds$fit)

BPtot.PIL.PELGAS.BIOMAN.lm.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.PILs$year,
  resid=BPtot.PIL.PELGAS.BIOMAN.lm$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.PIL.PELGAS.BIOMAN.lm.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtot-LM-PIL_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.PIL.PELGAS.BIOMAN.lm.residuals$resid,
             tx=BPtot.PIL.PELGAS.BIOMAN.lm.residuals$year,
             x0=2000,dx=1)

BPtot.PIL.PELGAS.BIOMAN.lm2=lm(BIOMAN.Ptot~PELGAS.Ptot,
                              BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGAS.BIOMAN.lm2)

BPtot.PIL.PELGAS.BIOMAN.lm3=lm(BIOMAN.Ptot~PELGAS.Ptot*factor(hclust1.2),
                               BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGAS.BIOMAN.lm3)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.PIL.PELGAS.BIOMAN.lm3)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtot-LM-PIL-2periods_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
dfs=BACVtotPtot.PELGASs.BIOMAN.PILs[
  BACVtotPtot.PELGASs.BIOMAN.PILs$year%in%c(2003,2007),]

plot(BACVtotPtot.PELGASs.BIOMAN.PILs$PELGAS.Ptot, 
     BACVtotPtot.PELGASs.BIOMAN.PILs$BIOMAN.Ptot,type='n')
text(BACVtotPtot.PELGASs.BIOMAN.PILs$PELGAS.Ptot, 
     BACVtotPtot.PELGASs.BIOMAN.PILs$BIOMAN.Ptot,
     BACVtotPtot.PELGASs.BIOMAN.PILs$year)
text(dfs$PELGAS.Ptot,dfs$BIOMAN.Ptot,dfs$year,col=2)

BPtot.PIL.PELGAS.BIOMAN.glm=glm(BIOMAN.Ptot~PELGAS.Ptot-1,family=Gamma,
                                BACVtotPtot.PELGASs.BIOMAN.PIL)
summary(BPtot.PIL.PELGAS.BIOMAN.glm)
par(mfrow=c(2,2))
plot(BPtot.PIL.PELGAS.BIOMAN.glm)
par(mfrow=c(1,1))
graphics.off()

### 3.8.2. Plots ---------
x11()
ggplot(data=BACVtotPtot.PELGASs.BIOMAN.PILs, 
       aes(x = PELGAS.Ptot, y = BIOMAN.Ptot,
           label=year)) +
  geom_text() +
  geom_smooth(method = "lm", se = TRUE,alpha = .15, 
              aes(fill = factor(hclust1.2)))+
  labs(
    title = paste(
      "Sardine, ",'\n',
      get.lm.res(model=BPtot.PIL.PELGAS.BIOMAN.lm3)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.PIL.PELGAS.BIOMAN.lm3)$AdjR2i,
      " P =",get.lm.res(BPtot.PIL.PELGAS.BIOMAN.lm3)$Pi))+
   geom_abline(slope=1,intercept = 0)+
  xlab('PELGAS Ptot, 8abd')+
  ylab('BIOMAN Ptot, 8abd')
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGAScufesVsBIOMANdepmPtot-PIL-2periods.png",sep=""), 
          units='cm',width=20, height=12,res=300)

plot(BPtot.PIL.PELGAS.BIOMAN.lm.preds$PELGAS.Ptot,
     BPtot.PIL.PELGAS.BIOMAN.lm.preds$fit)

x11()
par(bg='white')
ggplot(data = BPtot.PIL.PELGAS.BIOMAN.lm.preds, 
       aes(x = PELGAS.Ptot, y = BIOMAN.Ptot,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = BIOMAN.Ptot*(1-BIOMAN.Ptot.sd),
                    ymax = BIOMAN.Ptot*(1+BIOMAN.Ptot.sd)),
                data = BPtot.PIL.PELGAS.BIOMAN.lm.preds) + 
  geom_errorbarh(aes(xmin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                     xmax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                 data = BPtot.PIL.PELGAS.BIOMAN.lm.preds)+
  geom_ribbon(alpha = .15)+
  geom_line(data = BPtot.PIL.PELGAS.BIOMAN.lm.preds, 
            aes(x = PELGAS.Ptot, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Sardine, ",'\n',
      get.lm.res(model=BPtot.PIL.PELGAS.BIOMAN.lm)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.PIL.PELGAS.BIOMAN.lm)$AdjR2i,
      " P =",get.lm.res(BPtot.PIL.PELGAS.BIOMAN.lm)$Pi))+
  xlab('PELGAS CUFES Ptot, 8abd')+
  ylab('BIOMAN DEPM Ptot, 8abd without North West')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGAScufesVsBIOMANdepmPtot-PIL.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

## 3.9. PIL PELGAS biomass vs. BIOMAN Ptot -----------
### 3.9.1. LM -------
hist(BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot)
BPtot.PIL.PELGASbiomass.BIOMAN.lm=lm(BIOMAN.Ptot~PELGAS.biom-1,
                                     BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGASbiomass.BIOMAN.lm)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.PIL.PELGASbiomass.BIOMAN.lm)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsBIOMANdepmPtot-LM1-PIL_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)
pil.PELGAS.Ptotb.newd=BACVtotPtot.PELGASs.BIOMAN.PILs$PELGAS.biom
BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds <- data.frame(predict(
  BPtot.PIL.PELGASbiomass.BIOMAN.lm,
  newdata = data.frame(PELGAS.biom=pil.PELGAS.Ptotb.newd), 
  interval = 'confidence'))
BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds$year=BACVtotPtot.PELGASs.BIOMAN.PILs$year
BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds=merge(
  BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds,
  BACVtotPtot.PELGASs.BIOMAN.PILs[
    ,c('year','PELGAS.biom','PELGAS.biom.CV','BIOMAN.Ptot','BIOMAN.Ptot.sd')],
  by.x='year',by.y='year')

BPtot.PIL.PELGASbiomass.BIOMAN.lm.residuals=data.frame(
  year=BACVtotPtot.PELGASs.BIOMAN.PILs$year,
  resid=BPtot.PIL.PELGASbiomass.BIOMAN.lm$residuals)
# Check time trend in residuals
x11()
ggplot(data=BPtot.PIL.PELGASbiomass.BIOMAN.lm.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsBIOMANdepmPtot-LM1-PIL_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.PIL.PELGASbiomass.BIOMAN.lm.residuals$resid,
             tx=BPtot.PIL.PELGASbiomass.BIOMAN.lm.residuals$year,
             x0=2000,dx=1)

BPtot.PIL.PELGASbiomass.BIOMAN.lm2=lm(BIOMAN.Ptot~PELGAS.biom*factor(hclust1.2)-1,
                                     BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGASbiomass.BIOMAN.lm2)
AIC(BPtot.PIL.PELGASbiomass.BIOMAN.lm,BPtot.PIL.PELGASbiomass.BIOMAN.lm2)

ggplot(data=BACVtotPtot.PELGASs.BIOMAN.PILs,
       aes(x=PELGAS.biom,y=BIOMAN.Ptot,group=hclust1.2,label=year))+
  geom_text()+geom_smooth(method="lm",formula=y~x-1,col="red")

BPtot.PIL.PELGASbiomass.BIOMAN.glm=glm(BIOMAN.Ptot~PELGAS.biom-1,
                                       family=Gamma,BACVtotPtot.PELGASs.BIOMAN.PIL)
summary(BPtot.PIL.PELGASbiomass.BIOMAN.glm)
x11()
par(mfrow=c(2,2))
plot(BPtot.PIL.PELGASbiomass.BIOMAN.glm)
par(mfrow=c(1,1))
graphics.off()

### 3.9.2. Plots -----------

x11()
par(bg='white')
ggplot(data = BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds, 
       aes(x = PELGAS.biom, y = BIOMAN.Ptot,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbar(aes(ymin = BIOMAN.Ptot*(1-BIOMAN.Ptot.sd),
                    ymax = BIOMAN.Ptot*(1+BIOMAN.Ptot.sd)),
                data = BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds) + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds)+
  geom_ribbon(alpha = .15)+
  geom_line(data = BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds, 
            aes(x = PELGAS.biom, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Sardine, ",'\n',
      get.lm.res(model=BPtot.PIL.PELGASbiomass.BIOMAN.lm)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.PIL.PELGASbiomass.BIOMAN.lm)$AdjR2i,
      " P =",get.lm.res(BPtot.PIL.PELGASbiomass.BIOMAN.lm)$Pi))+
   xlab('PELGAS biomass, 8abd')+
  ylab('BIOMAN DEPM Ptot, 8abd without North West')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASbiomassVsBIOMANdepmPtot-PIL.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

## 3.10. PIL PELGAS biomass vs. PELGAS Ptot -----------
### 3.10.1. LM -----
hist(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot)
hist(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom)
BPtot.PIL.PELGASbiomass.PELGAS.lm=lm(PELGAS.Ptot~PELGAS.biom-1,
                                     BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGASbiomass.PELGAS.lm)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.PIL.PELGASbiomass.PELGAS.lm)
par(mfrow=c(1,1))
dev.print(png, file=paste(
  path.export.AcEgg,
  "PELGASbiomassVsPELGAScufesPtot-LM1-PIL_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)

BPtotBiom.PIL.PELGAS.PELGAS.lm.preds <- data.frame(predict(
  BPtot.PIL.PELGASbiomass.PELGAS.lm,
  newdata = data.frame(PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PILs$PELGAS.biom), 
  interval = 'confidence'))
BPtotBiom.PIL.PELGAS.PELGAS.lm.preds$year=BACVtotPtot.PELGASs.BIOMAN.PILs$year
BPtotBiom.PIL.PELGAS.PELGAS.lm.preds=merge(
  BPtotBiom.PIL.PELGAS.PELGAS.lm.preds,
  BACVtotPtot.PELGASs.BIOMAN.PILs[
    ,c('year','PELGAS.biom','PELGAS.biom.CV','PELGAS.Ptot','PELGAS.Ptot.sd')],
  by.x='year',by.y='year')

BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals=
  data.frame(year=BACVtotPtot.PELGASs.BIOMAN.PILs$year,
             resid=BPtot.PIL.PELGASbiomass.PELGAS.lm$residuals)
BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals$nresid=
  BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals$resid-
  min(BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals$resid)+1

plot(BACVtotPtot.PELGASs.BIOMAN.PILs$year,
       BPtot.PIL.PELGASbiomass.PELGAS.lm$residuals,ylab='Residuals',
     xlab='')
abline(h=0)

x11()
ggplot(data=BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtot-LM1-PIL_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)
# Check autocorrelation in residuals
library(RGeostats)
vario1D.plot(x=BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals$resid,
             tx=BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals$year,x0=2000,dx=1)

hist(BPtot.PIL.PELGASbiomass.PELGAS.lm.residuals$resid)

BPtot.PIL.PELGASbiomass.PELGAS.lm2=lm(PELGAS.Ptot~PELGAS.biom,
                                      BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGASbiomass.PELGAS.lm2)

BPtot.PIL.PELGASbiomass.PELGAS.lm3=lm(
  PELGAS.Ptot~PELGAS.biom*factor(hclust1.2),BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGASbiomass.PELGAS.lm3)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.PIL.PELGASbiomass.PELGAS.lm3)
par(mfrow=c(1,1))
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsCUFESPtot_LMwoOutliers-2periods-PIL_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)

BPtot.PIL.PELGASbiomass.PELGAS.lm3.residuals=
  data.frame(year=BACVtotPtot.PELGASs.BIOMAN.PILs$year,
             resid=BPtot.PIL.PELGASbiomass.PELGAS.lm3$residuals)
BPtot.PIL.PELGASbiomass.PELGAS.lm3.residuals$nresid=
  BPtot.PIL.PELGASbiomass.PELGAS.lm3.residuals$resid-
  min(BPtot.PIL.PELGASbiomass.PELGAS.lm3.residuals$resid)+1

x11()
ggplot(data=BPtot.PIL.PELGASbiomass.PELGAS.lm3.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsCUFESPtot_LMwoOutliers-2periods-PIL_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)

AIC(BPtot.PIL.PELGASbiomass.PELGAS.lm,BPtot.PIL.PELGASbiomass.PELGAS.lm3)

BPtot.PIL.PELGASbiomass.PELGAS.lm4=lm(
  PELGAS.Ptot~PELGAS.biom:factor(hclust1.2)-1,BACVtotPtot.PELGASs.BIOMAN.PILs)
summary(BPtot.PIL.PELGASbiomass.PELGAS.lm4)
x11()
par(mfrow=c(2,2),bg='white')
plot(BPtot.PIL.PELGASbiomass.PELGAS.lm4)
par(mfrow=c(1,1))
dev.print(png, file=paste(
  path.export.AcEgg,
  "PELGASacouVsCUFESPtot_LMwoOutliers-2periodsNointer-PIL_ResidualsCheck.png",sep=""), 
          units='cm',width=20, height=20,res=300)

BPtot.PIL.PELGASbiomass.PELGAS.lm4.residuals=
  data.frame(year=BACVtotPtot.PELGASs.BIOMAN.PILs$year,
             resid=BPtot.PIL.PELGASbiomass.PELGAS.lm4$residuals)
BPtot.PIL.PELGASbiomass.PELGAS.lm4.residuals$nresid=
  BPtot.PIL.PELGASbiomass.PELGAS.lm4.residuals$resid-
  min(BPtot.PIL.PELGASbiomass.PELGAS.lm4.residuals$resid)+1

x11()
ggplot(data=BPtot.PIL.PELGASbiomass.PELGAS.lm4.residuals,
       aes(x=year,y=resid,label=year))+geom_text()+
  geom_smooth(method = "loess")+
  geom_hline(yintercept=0, linetype="dashed", color = "red")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASacouVsCUFESPtot_LMwoOutliers-2periodsNointer-PIL_ResidualsTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)

BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4 <- data.frame(predict(
  BPtot.PIL.PELGASbiomass.PELGAS.lm4,
  newdata = data.frame(PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PILs$PELGAS.biom,
                       hclust1.2=BACVtotPtot.PELGASs.BIOMAN.PILs$hclust1.2), 
  interval = 'confidence'))
BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4$year=BACVtotPtot.PELGASs.BIOMAN.PILs$year
BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4=merge(
  BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4,
  BACVtotPtot.PELGASs.BIOMAN.PILs[
    ,c('year','PELGAS.biom','PELGAS.biom.CV','PELGAS.Ptot','PELGAS.Ptot.sd',
       'hclust1.2')],
  by.x='year',by.y='year')

AIC(BPtot.PIL.PELGASbiomass.PELGAS.lm3,BPtot.PIL.PELGASbiomass.PELGAS.lm4)

### 3.10.2. Plots -----
x11()
ggplot(data=BACVtotPtot.PELGASs.BIOMAN.PILs, 
       aes(x = PELGAS.Ptot, y = PELGAS.biom,
           label=year,colour=hclust1.2)) +
  geom_text() +
  geom_smooth(method = "lm", se = TRUE,alpha = .15, 
              aes(fill = factor(hclust1.2)))+
  labs(fill = "Cluster")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "PELGASbiomassVsPELGAScufesPtot-PIL-2periods.png",sep=""), 
          units='cm',width=20, height=12,res=300)

x11()
par(bg='white')
ggplot(data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds, 
       aes(x = PELGAS.biom, y = PELGAS.Ptot,label=year,
           ymin = lwr, ymax = upr))+
  geom_text() + 
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                    xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds) + 
  geom_errorbar(aes(ymin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                 ymax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                 data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds)+
  geom_ribbon(alpha = .15)+
  geom_line(data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds, 
            aes(x = PELGAS.biom, y = fit,colour='Linear model'),
            size=1.5)+
  labs(
    title = paste(
      "Sardine, ",'\n',
      get.lm.res(model=BPtot.PIL.PELGASbiomass.PELGAS.lm)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.PIL.PELGASbiomass.PELGAS.lm)$AdjR2i,
      " P =",get.lm.res(BPtot.PIL.PELGASbiomass.PELGAS.lm)$Pi))+
   xlab('PELGAS biomass, 8abd')+
  ylab('PELGAS CUFES Ptot, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASbiomassVsPELGAScufesPtot-PIL.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

#### 3.3.3.3. Without outliers, piecewise model 
x11()
ggplot(data = BACVtotPtot.PELGASs.BIOMAN.PILs, 
       aes(y = PELGAS.biom, x = PELGAS.Ptot,label=year,
           colour = factor(hclust1.2)))+
  geom_text() + 
  geom_errorbar(aes(ymin = PELGAS.biom*(1-PELGAS.biom.CV),
                    ymax = PELGAS.biom*(1+PELGAS.biom.CV))) + 
  geom_errorbarh(aes(xmin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                     xmax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)))+
  geom_smooth(method = "lm", se = TRUE,alpha = .15, 
              aes(fill = factor(hclust1.2)))+
  labs(
    title = paste(
      "Sardine, ",'\n',
      get.lm.res(model=BPtot.PIL.PELGASbiomass.PELGAS.lm3)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.PIL.PELGASbiomass.PELGAS.lm3)$AdjR2i,
      " P =",get.lm.res(BPtot.PIL.PELGASbiomass.PELGAS.lm3)$Pi),
    fill = "Model",colour="Cluster")+
  ylab('PELGAS acoustic biomass (tons), 8abd')+
  xlab('PELGAS CUFES Ptot, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+labs(colour='')
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASacouVsCUFESPtot_LMwoOutliers-2periods-PIL.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

#### 3.3.3.4. Without outliers, piecewise model, no intercept 
x11()
par(bg='white')
ggplot(data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4, 
       aes(x = PELGAS.biom, y = PELGAS.Ptot,label=year,
           ymin = lwr, ymax = upr,group=hclust1.2,colour=hclust1.2,
           fill=hclust1.2))+
  geom_text() + 
  geom_errorbar(aes(ymin = PELGAS.Ptot*(1-PELGAS.Ptot.sd/PELGAS.Ptot),
                    ymax = PELGAS.Ptot*(1+PELGAS.Ptot.sd/PELGAS.Ptot)),
                data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4)+
  geom_errorbarh(aes(xmin = PELGAS.biom*(1-PELGAS.biom.CV),
                     xmax = PELGAS.biom*(1+PELGAS.biom.CV)),
                 data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4)+
  geom_ribbon(alpha = .15)+
  geom_line(data = BPtotBiom.PIL.PELGAS.PELGAS.lm.preds4, 
            aes(x = PELGAS.biom, y = fit),
            size=1.5)+
  labs(
    title = paste(
      "Sardine, ",'\n',
      get.lm.res(model=BPtot.PIL.PELGASbiomass.PELGAS.lm4)$model_eqn,'\n',
      "Adj R2 = ",get.lm.res(BPtot.PIL.PELGASbiomass.PELGAS.lm4)$AdjR2i,
      " P =",get.lm.res(BPtot.PIL.PELGASbiomass.PELGAS.lm4)$Pi))+
  ylab('PELGAS CUFES Ptot, 8abd')+
  xlab('PELGAS acoustic biomass, 8abd')+
  geom_abline(intercept = 0, slope = 1, color="grey", 
              linetype="dashed", size=1)+
  labs(colour='PCA cluster')+guides(fill = FALSE)
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASbiomassVsPELGAScufesPtot_LMwoOutliers-2periodsNointer-PIL.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')

# 4. Maps comparison ---------
#******************************* -----------

load(file=paste0(path.export.results.fecundity.res1.0,
                 "PELGASfecundityGridMapsRes1p0.RData"))
FecunditySpBNres1.0

# Select cells by species
# 1.1.1. ANE ----
FecunditySpBNres1.0.ane=FecunditySpBNres1.0[
  !is.na(FecunditySpBNres1.0$fecundity)&FecunditySpBNres1.0$sp=='ENGR-ENC',]
# Select extra cells
FecunditySpBNres1.0.ane[
  FecunditySpBNres1.0.ane$cell%in%c('5-4','5-1'),'sel']=TRUE
FecunditySpBNres1.0s.ane=FecunditySpBNres1.0.ane[FecunditySpBNres1.0.ane$sel,]
# Check mosaic maps
dfi=FecunditySpBNres1.0s.ane
dfi$DFs=dfi$fecundity/max(dfi$fecundity)
dfi$logDFs=log(dfi$fecundity+1)/max(log(dfi$fecundity+1))
dfi$Zvalue=log(FecunditySpBNres1.0s.ane$fecundity+1)
head(dfi)

mat=grid.plot(
  pat=dfi,input='gridDataframe',ux11=TRUE,pat2=NULL,
  plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=FALSE,mosaic=TRUE),
  namz=paste(unique(dfi$sp),"fecundity"))
# Save figure
dev.print(png, file=paste(path.export.results.fecundity.res1.0,
                          "fecundityMapsRes1p0-ANE2",".png",sep=""), 
          units='cm',width=20, height=12,res=300)

# Import DF per haul from AZTI
library(readxl)
path.DF.hauls.2021=paste0(pref,'WGACEGG/Data/Indices/DF by haul_2021.xlsx')
DF.hauls.2021.BIOMAN=data.frame(read_excel(path.DF.hauls.2021))
DF.hauls.2021.BIOMAN$DFs=DF.hauls.2021.BIOMAN$DF.con.S.del.estrato/
  max(DF.hauls.2021.BIOMAN$DF.con.S.del.estrato)
DF.hauls.2021.BIOMAN$logDFs=log(DF.hauls.2021.BIOMAN$DF.con.S.del.estrato+1)/
  max(log(DF.hauls.2021.BIOMAN$DF.con.S.del.estrato+1))

hist(dfi[dfi$Year==2021,'DFs'])
hist(DF.hauls.2021.BIOMAN[,'DFs'])

summary(dfi[dfi$Year==2021,'fecundity'])
summary(DF.hauls.2021.BIOMAN$DF.con.S.del.estrato)
summary(dfi[dfi$Year==2021,'DFs'])
summary(DF.hauls.2021.BIOMAN[,'DFs'])
summary(dfi[dfi$Year==2021,'logDFs'])
summary(DF.hauls.2021.BIOMAN[,'logDFs'])

limit.ane.DFs.PELGAS <- range(dfi[dfi$Year==2021,'logDFs'])
limit.ane.DFs.BIOMAN <- range(DF.hauls.2021.BIOMAN[,'logDFs'])
limit.ane.DFs=range(limit.ane.DFs.PELGAS,limit.ane.DFs.BIOMAN)

x11()
ggplot() +
  geom_raster(data = dfi[dfi$Year==2021,], 
              aes(x = Xgd, y = Ygd, fill = logDFs))+
  labs(title = 'Anchovy daily fecundity estimates, 2021',size='DF BIOMAN',
       fill='PELGAS DF',colour='Logged 
       standardised DF') +
  theme(axis.title.x = element_blank(),
        axis.title.y = element_blank())+
  annotation_map(map_data("world"))+ #Add the map as a base layer before the points
  coord_quickmap()+  #Sets aspect ratio
  geom_point(data=DF.hauls.2021.BIOMAN,aes(x=Lon,y=Lat,
                                           size=logDFs,
                                           colour=logDFs))+
  scale_fill_viridis(limits=limit.ane.DFs)+
  scale_color_viridis(limits=limit.ane.DFs)
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "DailyFecundityMapsRes1p0&Haul-ANE2021",".png",sep=""), 
          units='cm',width=20, height=12,res=300)  


# 5. Export ---------
#***************************
head(BACVtotPtot.PELGASs.BIOMAN)
#write.csv2(BACVtotPtot.PELGASs.BIOMAN,path7)
write.table(BACVtotPtot.PELGASs.BIOMAN,path7,sep=';',dec = ".",
            row.names = FALSE)
head(BACVtotPtot.PELGASs.BIOMAN)
#write.csv2(BACVtotPtot.PELGASs.BIOMAN,path7)
write.table(BACVtotPtot.PELGASs.BIOMAN.ANEPIL,path7,sep=';',dec = ".",
            row.names = FALSE)
write.table(BACVtotPtot.PELGASs.BIOMAN.ANEPIL,path8,sep=';',dec = ".",
            row.names = FALSE)


#******************************* -----------
# 6. Alternative way to load data -----------
# 6.1. OR load PELGAS acoustic results separately -------
# ****************************
BACVtot.PELGAS=read.table(path6,sep=';',header = TRUE)
BACVtot.PELGASs=BACVtot.PELGAS[
  BACVtot.PELGAS$sp%in%
    c('CAPR-APE','ENGR-ENC','MICR-POU','SARD-PIL','SCOM-COL',
      'SCOM-SCO','SPRA-SPR','TRAC-MED','TRAC-TRU'),]
table(BACVtot.PELGASs$year,BACVtot.PELGASs$sp)
sort(unique(BACVtot.PELGASs$year))
head(BACVtot.PELGASs)

# 5.2. AND load PELGAS CUFES Ptot ----------
#***************************
# Path to data
path.PELGAS.Ptot=paste(pref,'PELGAS/Data/HydroBio/CUFES/Ptot/',sep='')
path.PELGAS.Ptot.ANE=paste(path.PELGAS.Ptot,'Ptot.anc.tab.txt',sep='')
path.PELGAS.Ptot.PIL=paste(path.PELGAS.Ptot,'Ptot.sar.tab.txt',sep='')

PELGAS.Ptot.ANE=read.table(path.PELGAS.Ptot.ANE,header=TRUE)
PELGAS.Ptot.ANE$sp='ENGR-ENC'
PELGAS.Ptot.PIL=read.table(path.PELGAS.Ptot.PIL,header=TRUE)
PELGAS.Ptot.PIL$sp='SARD-PIL'
PELGAS.Ptot=rbind(PELGAS.Ptot.ANE,PELGAS.Ptot.PIL)
head(PELGAS.Ptot)
table(PELGAS.Ptot$year)
head(BACVtot.PELGASs)

BACVtotPtot.PELGASs=merge(
  BACVtot.PELGASs,PELGAS.Ptot,by.x=c('sp','year'),
  by.y=c('sp','year'),all.x=TRUE)
head(BACVtotPtot.PELGASs)
sort(unique(BACVtotPtot.PELGASs$year))
BACVtotPtot.PELGASs$mw=BACVtotPtot.PELGASs$wbiom/BACVtotPtot.PELGASs$wabun

path.indices.BIOMAN=paste(
  pref,'WGACEGG/Data/WGHANSA/BIOMAN_allIndices.csv',sep='')
path.indices.BIOMAN=paste(
  prefc,'WGACEGG/WGACEGG2023/Data/BIOMAN_allIndices.csv',sep='')
BIOMAN.indices=read.table(path.indices.BIOMAN,sep=',',header=TRUE)
head(BIOMAN.indices)
names(BIOMAN.indices)
sort(unique(BIOMAN.indices$year))
BIOMAN.ANE=data.frame(
  BIOMAN.indices[,c('year','BIOMAN.Btot.depm','BIOMAN.cv.ssbdepm',
                    'BIOMAN.Ptot.ANE','BIOMAN.Ptot.ANE.sd')],
  sp='ENGR-ENC')
names(BIOMAN.ANE)=c('year','BIOMAN.biom','BIOMAN.biom.CV',
                    'BIOMAN.Ptot','BIOMAN.Ptot.sd','sp')
BIOMAN.PIL=data.frame(year=BIOMAN.indices$year,
                      BIOMAN.biom=NA,BIOMAN.biom.CV=NA,
                      BIOMAN.indices[,c('BIOMAN.Ptot.PIL','BIOMAN.Ptot.PIL.sd')],
                      sp='SARD-PIL')
names(BIOMAN.PIL)=c('year','BIOMAN.biom','BIOMAN.biom.CV',
                    'BIOMAN.Ptot','BIOMAN.Ptot.sd','sp')
BIOMAN=rbind(BIOMAN.ANE,BIOMAN.PIL)
head(BIOMAN)
table(BIOMAN$year,BIOMAN$sp)

BACVtotPtot.PELGASs.BIOMAN=merge(BACVtotPtot.PELGASs,BIOMAN,
                                 by.x=c('year','sp'),by.y=c('year','sp'),
                                 all.x=TRUE)
head(BACVtotPtot.PELGASs.BIOMAN)

# 6. Load PELGAS biometries ----------
#***************************
biometries=read.table(paste(
  pref,'PELGAS/Data/Biotique/Biometries/BiometriesOpeRaised.csv',sep=''),
  sep=';',header = TRUE)
head(biometries)
PELGAS.maturity=biometries[biometries$name=='Maturity',]
head(PELGAS.maturity)
sort(unique(PELGAS.maturity$voyage))
summary(PELGAS.maturity$dataValue)
table(is.na(PELGAS.maturity$dataValue))
PELGAS.maturitys=PELGAS.maturity[!is.na(PELGAS.maturity$dataValue),]
table(PELGAS.maturitys$voyage)
PELGAS.maturity$mature=factor(PELGAS.maturity$dataValue>=3)

PELGAS.maturitya=aggregate(
  PELGAS.maturity$numberAtLength,list(sp=PELGAS.maturity$baracoudaCode,
                                      survey=PELGAS.maturity$voyage,
                                      mature=PELGAS.maturity$mature),
  sum,na.rm=TRUE)
head(PELGAS.maturitya)
names(PELGAS.maturitya)[4]='NtotMat'
PELGAS.maturitya$NtotMat=na.null(PELGAS.maturitya$NtotMat)
PELGAS.maturityatot=aggregate(
  PELGAS.maturity$numberAtLength,
  list(sp=PELGAS.maturity$baracoudaCode,
       survey=PELGAS.maturity$voyage),
  sum,na.rm=TRUE)
names(PELGAS.maturityatot)[3]='Ntot'
head(PELGAS.maturitya)
PELGAS.maturitya=merge(PELGAS.maturitya,PELGAS.maturityatot)
head(PELGAS.maturitya)
PELGAS.maturitya$Ntot=na.null(PELGAS.maturitya$Ntot)
PELGAS.maturitya$pmature=PELGAS.maturitya$NtotMat/
  PELGAS.maturitya$Ntot
PELGAS.maturitya$year=as.numeric(substr(PELGAS.maturitya$survey,7,10))

# Correct 2001
# PELGAS.maturitya[PELGAS.maturitya$year==2002&
#                    PELGAS.maturitya$mature==TRUE&
#                    PELGAS.maturitya$sp=='ENGR-ENC',]
# PELGAS.maturitya[PELGAS.maturitya$year==2001&
#                    PELGAS.maturitya$sp=='ENGR-ENC','pmature']=
#   (PELGAS.maturitya[PELGAS.maturitya$year==2000&
#                       PELGAS.maturitya$mature==TRUE&
#                      PELGAS.maturitya$sp=='ENGR-ENC','pmature']+
#      PELGAS.maturitya[PELGAS.maturitya$year==2002&
#                         PELGAS.maturitya$sp=='ENGR-ENC','pmature'])/2

PELGAS.maturityas=PELGAS.maturitya[
  PELGAS.maturitya$mature=='TRUE',]
head(PELGAS.maturityas)
table(PELGAS.maturityas$year)

x11()
p <- ggplot(PELGAS.maturityas, aes(year,pmature)) +
  geom_point()+ylab('Proportion of mature fish')
p + facet_grid(.~sp)+geom_smooth(method = "lm")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "clupSpawningFractionTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)

PELGAS.maturityas[PELGAS.maturityas$year=='2023',]

PELGAS.S=biometries[biometries$name=='Sex',]
head(PELGAS.S)
unique(PELGAS.S$dataValue)
PELGAS.S[is.na(PELGAS.S$dataValue),'dataValue']='-1'
table(PELGAS.S$dataValue,useNA = c("ifany"))
PELGAS.S$sex=factor(PELGAS.S$dataValue)
PELGAS.sexa=aggregate(
  PELGAS.S$sex,list(sp=PELGAS.S$baracoudaCode,
                    survey=PELGAS.S$voyage),table)
PELGAS.sexa$Nmale=unlist(PELGAS.sexa$x)[,1]
PELGAS.sexa$Nfemale=unlist(PELGAS.sexa$x)[,2]
PELGAS.sexa$Undet=unlist(PELGAS.sexa$x)[,3]
PELGAS.sexa$pfemale=PELGAS.sexa$Nfemale/
  (PELGAS.sexa$Nmale+PELGAS.sexa$Nfemale+PELGAS.sexa$Undet)
PELGAS.sexa$year=as.numeric(substr(PELGAS.sexa$survey,7,10))
aggregate(PELGAS.sexa$pfemale,list(PELGAS.sexa$year),summary)
summary(PELGAS.sexa$pfemale)

x11()
p <- ggplot(PELGAS.sexa, aes(year,pfemale)) +
  geom_line()
p + facet_grid(.~sp)+geom_smooth(method = "lm")
# Save figure
dev.print(png, file=paste(path.export.AcEgg,
                          "clupSpawningFractionTimeSeries.png",sep=""), 
          units='cm',width=20, height=12,res=300)

# 6.1. Add maturity and sex ratio infos to PELGAS dataset -----
dim(BACVtotPtot.PELGASs)
BACVtotPtot.PELGASs=merge(
  BACVtotPtot.PELGASs,PELGAS.maturitya[,c('sp','year','pmature')],
  by.x=c('sp','year'),by.y=c('sp','year'))
dim(BACVtotPtot.PELGASs)
head(BACVtotPtot.PELGASs)


# 7. Deprecated

par(bg='white')
rmax.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
  (1+BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV)
rmin.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
  (1-BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV)
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
  (1+BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV)
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
  (1-BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV)
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
     BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom,type='n',
     xlab='PELGAS acoustic biomass (tons), 8abd',
     ylab='BIOMAN DEPM biomass (tons), 8abcd',main='Anchovy',
     ylim=range(c(rmin.PELGAS.Ptot,rmax.PELGAS.Ptot*1.2)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom)))
polygon(c(rev(ane.biom.newd),ane.biom.newd), 
        c(rev(B.ANE.PELGAS.BIOMAN.lm.preds[ ,3]),
          B.ANE.PELGAS.BIOMAN.lm.preds[ ,2]), 
        col = 'grey90', border = NA)
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
           (1-BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV),
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
           (1+BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV),
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom,col='grey50')
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
           (1-BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
           (1+BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV),
         col='grey50')
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom),
     BACVtotPtot.PELGASs.BIOMAN.ANE$year)
lines(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
      BPtot.ANE.PELGAS.BIOMAN.lm$fitted.values,col=2)
abline(c(0,B.ANE.PELGAS.BIOMAN.lm2$coefficients),col=3,lty=1)
abline(a=0,b=1,col='grey')


head(BACVtotPtot.PELGASs.BIOMAN.ANE)
x11()
par(bg='white')
rmax.PELGAS.Ptot.cor=BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
  (1+BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV)
rmin.PELGAS.Ptot.cor=BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
  (1-BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV)
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
  (1+BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor)
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
  (1-BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor)
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
     BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom,type='n',
     xlab='PELGAS acoustic biomass (tons)',
     ylab='BIOMAN DEPM biomass (tons)',main='Anchovy',
     ylim=range(c(rmin.PELGAS.Ptot.cor,rmax.PELGAS.Ptot.cor*1.2)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom)))
polygon(c(rev(ane.biom.newd.cor),ane.biom.newd.cor), 
        c(rev(BPtot.ANE.PELGAS.BIOMAN.lm.cor.preds[ ,3]),
          BPtot.ANE.PELGAS.BIOMAN.lm.cor.preds[ ,2]), 
        col = 'grey90', border = NA)
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
           (1-BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor),
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
           (1+BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor),
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom,col='grey50')
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
           (1-BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom*
           (1+BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV),
         col='grey50')
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom),
     BACVtotPtot.PELGASs.BIOMAN.ANE$year)
lines(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
      BPtot.ANE.PELGAS.BIOMAN.lm.cor$fitted.values,col=2)
abline(c(0,B.ANE.PELGAS.BIOMAN.lm2.cor$coefficients),col=3,lty=1)
abline(a=0,b=1,col='grey')
cf3 <- round(coef(BPtot.ANE.PELGAS.BIOMAN.lm.cor), 2) 
eq3 <- paste0("DEPM = ", cf3[1], " x acoustic ",', R2 = ',
              round(summary(BPtot.ANE.PELGAS.BIOMAN.lm.cor)$adj.r.squared,2))
text(320000,10000,eq3)
legend('topleft',legend=c('1:1 line','Linear model',
                          'Linear model without 2015 and 2021'),
       lty=rep(1,3),col=c('grey','red','green'))
dev.print(device=png,
          filename=paste(path.export.AcEgg,
                         'PELGASacouVsBIOMANdepmBiomassCor-ANE.png',sep=''),
          res=300,units='cm',width=20,height=20,bg='white')



x11()
par(bg='white')
rmax.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot
rmin.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
  (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/
        BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot))
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
  (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/
        BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot))
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,
     BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot,type='n',
     xlab='PELGAS CUFES Ptot',ylab='BIOMAN DEPM Ptot',
     main='Anchovy',
     ylim=range(c(rmin.PELGAS.Ptot,rmax.PELGAS.Ptot)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom)))
polygon(c(rev(ANE.PELGAS.Ptot.newd),ANE.PELGAS.Ptot.newd), 
        c(rev(BPtot.ANE.PELGAS.BIOMAN.lm.preds[,3]),
          BPtot.ANE.PELGAS.BIOMAN.lm.preds[,2]), 
        col = 'grey90', border = NA)
#plot(ANE.PELGAS.Ptot.newd,BPtot.ANE.PELGAS.BIOMAN.lm.preds[,1])
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
           (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
           (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot,col='grey50')
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot*
           (1-BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot*(1+BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.biom.CV),
         col='grey50')
#points(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot)
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot),
     BACVtotPtot.PELGASs.BIOMAN.ANE$year)
lines(Ptot.ANE.PELGAS.BIOMAN.lm$model$PELGAS.Ptot,
      Ptot.ANE.PELGAS.BIOMAN.lm$fitted.values,col=2)
abline(a=0,b=1,col='grey')
cf7 <- round(coef(Ptot.ANE.PELGAS.BIOMAN.lm), 2) 
eq7 <- paste(
  paste0("DEPM Ptot = ", cf7[1], " x CUFES Ptot "),
  paste0('R2 = ',round(
    summary(Ptot.ANE.PELGAS.BIOMAN.lm)$adj.r.squared,2)),
  sep = '\n')
text(2.5e13,1e12,eq7)


rmax.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot
rmin.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
  (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV))
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
  (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV))
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
     BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,type='n',
     xlab='PELGAS acoustic biomass (tons)',
     ylab='PELGAS CUFES Ptot',main='Anchovy',
     ylim=range(c(rmin.PELGAS.Ptot,rmax.PELGAS.Ptot)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom*1.2)))
polygon(c(rev(ane.PELGAS.Ptotb.newd),ane.PELGAS.Ptotb.newd), 
        c(rev(BPtotBiom.ANE.PELGAS.PELGAS.lm.preds[ ,3]),
          BPtotBiom.ANE.PELGAS.PELGAS.lm.preds[ ,2]), 
        col = 'grey90', border = NA)
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
           (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom*
           (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,col='grey50')
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
           (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
           (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)),col='grey50')
#points(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot)
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot),
     BACVtotPtot.PELGASs.BIOMAN.ANE$year)
lines(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom,
      BPtot.ANE.PELGASbiomass.PELGAS.lm$fitted.values,col=2)
abline(a=0,b=1,col='grey')
cf6 <- format(coef(BPtot.ANE.PELGASbiomass.PELGAS.lm),
              scientific=TRUE,digits=2) 
eq6 <- paste(
  paste0("CUFES Ptot = ", cf6[1], " x acoustic biomass "),
  paste0('R2 = ',
         round(summary(BPtot.ANE.PELGASbiomass.PELGAS.lm)$adj.r.squared,2)),
  sep='\n')
text(4e5,3e12,eq6)


rmax.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot
rmin.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
  (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor))
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
  (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor))
plot(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
     BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,type='n',
     xlab='PELGAS acoustic biomass (tons)',
     ylab='PELGAS CUFES Ptot',main='Anchovy',
     ylim=range(c(rmin.PELGAS.Ptot,rmax.PELGAS.Ptot)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom*1.2)))
polygon(c(rev(ane.PELGAS.Ptotb.newd.cor),ane.PELGAS.Ptotb.newd.cor), 
        c(rev(BPtotBiom.ANE.PELGAS.PELGAS.lm.cor.preds[ ,3]),
          BPtotBiom.ANE.PELGAS.PELGAS.lm.cor.preds[ ,2]), 
        col = 'grey90', border = NA)
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
           (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor*
           (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.CV.cor)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,col='grey50')
segments(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
           (1-(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)),
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
         BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot*
           (1+(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot)),col='grey50')
#points(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot,BACVtotPtot.PELGASs.BIOMAN.ANE$BIOMAN.Ptot)
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.Ptot),
     BACVtotPtot.PELGASs.BIOMAN.ANE$year)
lines(BACVtotPtot.PELGASs.BIOMAN.ANE$PELGAS.biom.cor,
      BPtot.ANE.PELGASbiomass.PELGAS.lm.cor$fitted.values,col=2)
abline(a=0,b=1,col='grey')
cf6 <- format(coef(BPtot.ANE.PELGASbiomass.PELGAS.lm.cor),
              scientific=TRUE,digits=2) 
eq6 <- paste(
  paste0("CUFES Ptot = ", cf6[1], " x acoustic biomass "),
  paste0('R2 = ',
         round(summary(BPtot.ANE.PELGASbiomass.PELGAS.lm.cor)$adj.r.squared,2)),
  sep='\n')
text(4e5,3e12,eq6)

rmax.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot
rmin.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot*
  (1+(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot.sd/
        BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot))
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot*
  (1-(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot.sd/
        BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot))
plot(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,
     BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot,type='n',
     xlab='PELGAS CUFES Ptot',ylab='BIOMAN DEPM egg abundance',
     main='Sardine',
     ylim=range(c(rmin.PELGAS.Ptot,rmax.PELGAS.Ptot)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom)))
polygon(c(rev(pil.PELGAS.Ptot.newd),pil.PELGAS.Ptot.newd), 
        c(rev(BPtot.PIL.PELGAS.BIOMAN.lm.preds[,3]),
          BPtot.PIL.PELGAS.BIOMAN.lm.preds[,2]), 
        col = 'grey90', border = NA)
#plot(pil.PELGAS.Ptot.newd,BPtot.ANE.PELGAS.BIOMAN.lm.preds[,1])
segments(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot*
           (1-(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot)),
         BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot*
           (1+(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot.sd/BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot)),
         BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot,col='grey50')
segments(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot*
           (1-BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.biom.CV),
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot*(1+BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.biom.CV),
         col='grey50')
#points(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot)
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot),
     BACVtotPtot.PELGASs.BIOMAN.PIL$year)
lines(BPtot.PIL.PELGAS.BIOMAN.lm$model$PELGAS.Ptot,
      BPtot.PIL.PELGAS.BIOMAN.lm$fitted.values,col=2)
abline(a=0,b=1,col='grey')
cf4 <- round(coef(BPtot.PIL.PELGAS.BIOMAN.lm), 2) 
eq4 <- paste0("DEPM no. of eggs = ", cf4[1], " x CUFES Ptot ",', R2 = ',
              round(summary(BPtot.PIL.PELGAS.BIOMAN.lm)$adj.r.squared,2))
text(1.5e13,2.1e12,eq4)

rmax.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot
rmin.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
  (1+(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV))
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
  (1-(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV))
plot(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom,
     BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot,type='n',
     xlab='PELGAS acoustic biomass (tons)',
     ylab='BIOMAN DEPM egg abundance',main='Sardine',
     ylim=range(c(rmin.PELGAS.Ptot,rmax.PELGAS.Ptot)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom*1)))
polygon(c(rev(pil.PELGAS.Ptotb.newd),pil.PELGAS.Ptotb.newd), 
        c(rev(BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds[ ,3]),
          BPtotBiom.PIL.PELGAS.BIOMAN.lm.preds[ ,2]), 
        col = 'grey90', border = NA)
segments(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
           (1-(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV)),
         BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
           (1+(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV)),
         BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot,col='grey50')
# segments(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,
#          BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot*(1-BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.biom.CV),
#          BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,
#          BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot*(1+BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.biom.CV),
#          col='grey50')
#points(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot)
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot),BACVtotPtot.PELGASs.BIOMAN.PIL$year)
lines(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom,
      BPtot.PIL.PELGASbiomass.BIOMAN.lm$fitted.values,col=2)
abline(a=0,b=1,col='grey')
cf4 <- round(coef(BPtot.PIL.PELGASbiomass.BIOMAN.lm), 2) 
eq4 <- paste("DEPM eggs = ", cf4[1], " x acoustic biomass,",
             paste('R2 = ',
                   round(summary(BPtot.PIL.PELGASbiomass.BIOMAN.lm)$adj.r.squared,2),
                   sep=''),sep='\n')
text(5.5e5,3e12,eq4)

rmax.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot
rmin.PELGAS.Ptot=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot
rmax.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
  (1+(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV))
rmin.PELGAS.biom=BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
  (1-(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV))
plot(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom,
     BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,type='n',
     xlab='PELGAS acoustic biomass (tons)',
     ylab='PELGAS CUFES Ptot',main='Sardine',
     ylim=range(c(rmin.PELGAS.Ptot,rmax.PELGAS.Ptot)),
     xlim=range(c(rmin.PELGAS.biom,rmax.PELGAS.biom*1)))
polygon(c(rev(pil.PELGAS.Ptotb.newd),pil.PELGAS.Ptotb.newd), 
        c(rev(BPtotBiom.PIL.PELGAS.PELGAS.lm.preds[ ,3]),
          BPtotBiom.PIL.PELGAS.PELGAS.lm.preds[ ,2]), 
        col = 'grey90', border = NA)
segments(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
           (1-(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV)),
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom*
           (1+(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom.CV)),
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,col='grey50')
segments(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom,
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot*
           (1-(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot)),
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom,
         BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot*
           (1+(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot.sd/
                 BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot)),col='grey50')
#points(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot,BACVtotPtot.PELGASs.BIOMAN.PIL$BIOMAN.Ptot)
text(base::jitter(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom),
     base::jitter(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.Ptot),
     BACVtotPtot.PELGASs.BIOMAN.PIL$year)
lines(BACVtotPtot.PELGASs.BIOMAN.PIL$PELGAS.biom,
      BPtot.PIL.PELGASbiomass.PELGAS.lm$fitted.values,col=2)
abline(a=0,b=1,col='grey')
cf5 <- format(coef(BPtot.PIL.PELGASbiomass.PELGAS.lm), 
              scientific=TRUE,digits=2) 
eq5 <- paste(
  paste0("CUFES Ptot = ", cf5[1], " x acoustic biomass "),
  paste0('R2 = ',
         round(
           summary(BPtot.PIL.PELGASbiomass.PELGAS.lm)$adj.r.squared,2)),
  sep='\n')
text(4e5,1.7e13,eq5)