library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)

#***************************************************************************************************************************************
#                                               Multiple Factor Analysis with FactoMineR tutorial
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.5

# 1. Import grid map data and convert them into raster package objects --------
#********************************************************************

  # path to donnees2 drive
  #*********************************
  donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
  
  # path to grid files
  #*********************************
  prefix=paste(donnees2,'Campagnes/',sep='')
  prefix='D:/Campagnes/'
  path.gridse=paste(prefix,"WGACEGG/Data/grids/",sep='')
  path.gridse1=paste(path.gridse,"AC_CUFES_SPRING_IBBB/",sep='')
  path.export=paste(prefix,"WGACEGG/Results/MFAgrids/AC_CUFES_SPRING_IBBB/",sep='')
  ID='AC_CUFES_SPRING_IBBB'
  
  # List grid files 
  #*********************************
  lffssde=list.files(path.gridse1,pattern='*.txt')
  #lfs=strsplit(lf,split='[.]',)
  lffssdes=data.frame(t(data.frame(strsplit(lffssde,split='[.]'))))
  lp=as.character(lffssdes[,1])
  lffssdes2=data.frame(t(data.frame(strsplit(lp,split='_'))))
  row.names(lffssdes2)=seq(length(lffssdes2[,1]))
  lffssdes=cbind(lffssdes,lffssdes2)
  head(lffssdes)
  names(lffssdes)=c('pat','ext','year','survey','season','area','var','sp',
                    'filter','thr')
  lffssdes$path=lffssde
  row.names(lffssdes)=seq(length(lffssdes[,1]))

  # Import grid and raster files, bind grid maps
  #***************************************************
  for (i in 1:dim(lffssdes)[1]){
    pat=paste(path.gridse1,lffssdes$path[i],sep='')
    mati=read.table(file=pat,header=T,sep=";")
    mati$sp=lffssdes$sp[i]
    mati$Survey='SPRING.IBBB'
    head(mati)
    if (i==1){
      CUFES.ac.spring.ibbb=mati
    }else{
      CUFES.ac.spring.ibbb=rbind(CUFES.ac.spring.ibbb,mati)
    }
    head(CUFES.ac.spring.ibbb)
    head(mati)
    # graphics.off()
    #assign(paste(lffssdes$sp[i],'rasterStack',sep='.'),resras)
    graphics.off()
    # Import raster stacks format, compute mean and SD maps and plot everything:
    #pat2=paste(paste(path.gridse1,'Rasters/',sep=''),lffssderss$path[i],sep='')
    #rasti=raster(pat2)
  }
  head(CUFES.ac.spring.ibbb)
  CUFES.ac.spring.ibbb$sp=as.character(CUFES.ac.spring.ibbb$sp)
  CUFES.ac.spring.ibbb[CUFES.ac.spring.ibbb$sp%in%c('ANC','Anc'),'sp']='ENGR-ENC'
  CUFES.ac.spring.ibbb[CUFES.ac.spring.ibbb$sp%in%c('SAR','Sar'),'sp']='SARD-PIL'
  lsp=unique(CUFES.ac.spring.ibbb$sp)
  table(CUFES.ac.spring.ibbb$Year)
  CUFES.ac.spring.ibbb=unique(CUFES.ac.spring.ibbb)
  table(CUFES.ac.spring.ibbb$Year)
  lyears=unique(CUFES.ac.spring.ibbb$Year)
  
  for (i in 1:length(lsp)){
    resras=grid2rasterStack(pat=CUFES.ac.spring.ibbb[
      CUFES.ac.spring.ibbb$sp==lsp[i],],
                            path1=path.gridse1,
                            varid=paste(lsp[i],
                                        'CUFES',
                                        sep=' '),
                            anomaly=TRUE)
  }

# 2. Prepare datasets for MFA -------------------
  #********************************************************************
  # 2.1. Select common species ---------------
  #***************************
  head(CUFES.ac.spring.ibbb)
  unique(CUFES.ac.spring.ibbb$Year)
  CUFES.ac.spring.ibbb$spi=substr(CUFES.ac.spring.ibbb$sp,1,8)
  CUFES.ac.spring.ibbbp=CUFES.ac.spring.ibbb[CUFES.ac.spring.ibbb$Zvalue>0&!is.na(CUFES.ac.spring.ibbb$Zvalue),]
  table(CUFES.ac.spring.ibbbp$spi,CUFES.ac.spring.ibbbp$Year)
  OccurenceFrequencySp=apply(table(CUFES.ac.spring.ibbbp$spi,CUFES.ac.spring.ibbbp$Year)>0,1,sum)/length(unique(CUFES.ac.spring.ibbbp$Year))
  sort(OccurenceFrequencySp)
  CUFES.ac.spring.ibbbs=CUFES.ac.spring.ibbb[CUFES.ac.spring.ibbb$spi%in%
                                               c('ENGR-ENC','SARD-PIL'),]
  head(CUFES.ac.spring.ibbbs)
  
  # 2.2. Select years ---------------
  #***************************
  CUFES.ac.spring.ibbbs=CUFES.ac.spring.ibbbs[!CUFES.ac.spring.ibbbs$Year%in%
                                                c(2003,2004,2009,2012),]
  # no pelago data in 2012, 2003 pelago in winter, no data in gulf of cadiz in 2004
  unique(CUFES.ac.spring.ibbbs$Year)
  head(CUFES.ac.spring.ibbbs)
  
  # 2.3. Reshape data frame for analysis with year as grouping variable -------------
  #***************************
  a.fssd.cufes <- reshape(CUFES.ac.spring.ibbbs[,c("I","J","Zvalue","sp","Year")], timevar = "sp", idvar = c("I","J","Year"), direction = "wide")
  head(a.fssd.cufes)
  names(a.fssd.cufes)
  a.fssd.cufes=a.fssd.cufes[order(a.fssd.cufes$Year),]
  
  big.fssd.cufes <- reshape(a.fssd.cufes, timevar = "Year", idvar = c("I","J"), direction = "wide")
  head(big.fssd.cufes)
  names(big.fssd.cufes)
  dim(big.fssd.cufes)
  
  # 2.4. Select cells in survey area -------------
  #***************************
  pat=paste(path.gridse1,lffssdes$path[4],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  pipo=mati[mati$Year==2004,]
  plot(pipo$Xgd,pipo$Ygd)
  plot(pipo$Xgd,pipo$Ygd,cex=0.1+pipo$Zvalue/100)
  # Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
  #******************************************
  # Define grid limits
  x1=-10.2;x2=-1;y1=35.8;y2=49
  # Define cell dimensions
  ax=0.25;ay=0.25
  # Define smoothing radius
  u=2
  # Define iteration number
  ni=200
  define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni)
  poly=data.frame(x=xpol,y=ypol)
  lines(poly)
  coast()
  head(pipo)
  library(splancs) 
  pts=as.points(pipo$Xgd,pipo$Ygd)
  testInPoly=inout(pts,poly)
  plot(pts)
  points(pts[testInPoly,],pch=16)
  coast()
  lines(poly)
  cellsel=data.frame(x=pts[,1],y=pts[,2],sel=testInPoly,I=pipo$I,J=pipo$J)
  cellsel$cell=paste(cellsel$I,cellsel$J,sep='-')
  head(cellsel)
  plot(cellsel$x,cellsel$y)
  points(cellsel[cellsel$sel,'x'],cellsel[cellsel$sel,'y'],pch=16)
  coast()
  legend('topleft',legend=c('Cells centers','Selected cells'),pch=c(1,16))
  
  # 2.5. Prepare and standardise dataset -----------
  #**************************
  names(big.fssd.cufes)
  big.fssd.cufes.MFA=big.fssd.cufes[,-seq(2)]
  row.names(big.fssd.cufes.MFA)=paste(big.fssd.cufes$I,big.fssd.cufes$J,sep='-')
  # simplify column names
  names(big.fssd.cufes.MFA)=gsub('Zvalue.','',names(big.fssd.cufes.MFA))
  names(big.fssd.cufes.MFA)
  head(big.fssd.cufes.MFA)
  
  # 2.6. Select cells in survey area before MFA ------
  #***************************************
  dim(big.fssd.cufes.MFA)
  big.fssd.cufes.MFA=big.fssd.cufes.MFA[order(dimnames(big.fssd.cufes.MFA)[[1]]),]
  cellsel=cellsel[order(cellsel$cell),]

  # 2.6.1. Select cells with no NA over the series -----
  #****************************************
  # # NA detector
  NAdetect=function(x){
    if (sum(is.na(x))>0){
      y=FALSE
    }else{
      y=TRUE
    }
    y
  }
  cselNoNAf=apply(big.fssd.cufes.MFA,1,NAdetect)
  table(cselNoNAf)
  big.fssd.cufes.MFAs=big.fssd.cufes.MFA[cselNoNAf,]
  cellsel=cellsel[order(cellsel$cell),]
  plot(cellsel$x,cellsel$y)
  points(cellsel[cselNoNAf,'x'],cellsel[cselNoNAf,'y'],pch=16)
  coast()
  cellsel$selNoNA=cselNoNAf
  
  # # convert NA to zero
  NA2null=function(x,procInf=FALSE){
    x[is.na(x)]=0
    if (procInf){
      x[is.infinite(x)]=0
    }
    x
  }
  big.fssd.cufes.MFAs=apply(big.fssd.cufes.MFAs,2,NA2null)
  head(big.fssd.cufes.MFAs)
  rnames=dimnames(big.fssd.cufes.MFAs)[[1]]
  # log variables
  big.fssd.cufes.MFAs=apply(big.fssd.cufes.MFAs,2,function(x){x=log(x+1);x})
  head(big.fssd.cufes.MFAs)
  dim(big.fssd.cufes.MFAs)
  class(big.fssd.cufes.MFAs)
  dimnames(big.fssd.cufes.MFAs)[[1]]=rnames
  # center and scale variables
  big.fssd.cufes.MFAs=apply(big.fssd.cufes.MFAs,2,scale,center=TRUE,scale=FALSE)
  head(big.fssd.cufes.MFAs)
  class(big.fssd.cufes.MFAs)
  dimnames(big.fssd.cufes.MFAs)[[1]]=rnames

  # 2.7. Remove constant columns-----------  
  #***************************************
  # with zeroes
  scols=apply(big.fssd.cufes.MFAs,2,sum,na.rm=TRUE)
  srows=apply(big.fssd.cufes.MFAs,1,sum,na.rm=TRUE)
  big.fssd.cufes.MFAs2=big.fssd.cufes.MFAs[,scols!=0]
  sum(scols==0)
  scols2=apply(big.fssd.cufes.MFAs2,2,sum,na.rm=TRUE)
  summary(scols2)
  # other constant columns
  vcols=apply(big.fssd.cufes.MFAs2,2,var,na.rm=TRUE)
  ccols=dimnames(big.fssd.cufes.MFAs2)[[2]][vcols==0]
  big.fssd.cufes.MFAs2=big.fssd.cufes.MFAs2[,!dimnames(big.fssd.cufes.MFAs2)[[2]]%in%ccols]
  dim(big.fssd.cufes.MFAs2)
  
  # 2.8. Generate no. of data in each group -----------
  #***************************************
  cwz=dimnames(big.fssd.cufes.MFAs2)[[2]]
  nct=unlist(strsplit(cwz,split='[.]'))[seq(1,2*length(cwz),2)]
  nyears=unlist(strsplit(cwz,split='[.]'))[seq(2,2*length(cwz),2)]
  tnnc=table(nct,nyears)
  tnnc.df=data.frame(table(nct,nyears))
  stnnc.cufes=apply(tnnc,2,sum)
  head(tnnc.df)
  tnnc.dfa=aggregate(tnnc.df$Freq,list(nct=tnnc.df$nct),sum)
  summary(tnnc.dfa$x)

# 3. Run MFA ----------------
#*************************************************************
  # 3.1. MFA ---------------
  #***********************************
  dim(big.fssd.cufes.MFAs2)
  res.wgacegg.cufes.MFA<-MFA(big.fssd.cufes.MFAs2, group=stnnc.cufes, 
                        type=rep("s",length(stnnc.cufes)), 
                    ncp=dim(big.fssd.cufes.MFAs2)[2], 
                    name.group=c("05","06","07", "08", "10", "11","14","15","16","17"), 
                    num.group.sup=NULL, graph=TRUE)
  names(res.wgacegg.cufes.MFA)
  
  # 3.2. Explained variance ----------
  #***********************************  
  summary(res.wgacegg.cufes.MFA)  
  par(bg='white')
  barplot(res.wgacegg.cufes.MFA$eig[,2],main="% of variance explained",names.arg=1:nrow(res.wgacegg.cufes.MFA$eig))
  barplot(res.wgacegg.cufes.MFA$eig[,3],main="Cumulative % of variance explained",names.arg=1:nrow(res.wgacegg.cufes.MFA$eig))
  res.wgacegg.cufes.MFA$eig[round(res.wgacegg.cufes.MFA$eig[,3])==95,]
  dim(big.fssd.cufes.MFAs2)
 
  # 3.3. Plots of individuals in MFA1-2-3 planes -----------
  #***********************************
  # Plot of individuals in MFA1-2 plane 
  p1 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "contrib") #with contribution in color scale
  p2 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "cos2") #with quality of representation in color scale
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA2-3 plane
  p1 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "contrib",axes = c(2, 3))
  p2 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "cos2",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-3 plane
  p1 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "contrib",axes = c(1, 3))
  p2 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "cos2",axes = c(1, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-2-3 planes, with contribution in color scale
  x11()
  p1 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "contrib")
  p2 = fviz_mfa_ind(res.wgacegg.cufes.MFA, col.ind = "contrib",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  dev.print(device=png,
            filename=paste(path.export,'WGACEGG-',ID,'-GridsMFA123biplot.png',sep=''),
            units='cm',width=29,height=21,res=300)

# 4. Contributions of variables to MFA axes -------------
#******************************************************************
  par(bg='grey50')
  plot.MFA(res.wgacegg.cufes.MFA, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, 
           habillage="group", select = "coord 10")
  par(bg='white')
  
  # 4.1. select variables with above average contribution to MFA axes ----------------
  #************************************
  var.contrib=res.wgacegg.cufes.MFA$quanti.var$contrib
  head(var.contrib)
  summary(var.contrib[,1])
  var.contribs1=var.contrib[var.contrib[,1]>mean(var.contrib[,1]),1]
  var.contribs2=var.contrib[var.contrib[,2]>mean(var.contrib[,2]),2]
  var.contribs3=var.contrib[var.contrib[,3]>mean(var.contrib[,3]),3]
  
  # 4.2. Variables correlation with MFA axes ---------------
  #************************************
  var.cor=res.wgacegg.cufes.MFA$quanti.var$contrib
  graphics.off()
  ddres1.cufes=dimdesc(res.wgacegg.cufes.MFA, axes = 1:4, proba = 0.05)
  names(ddres1.cufes)
  
  # 4.2.1. Variables correlation with MFA axis1 ----------
  #************************************
  ddres1.cufes.1=ddres1.cufes$Dim.1$quanti
  ddres1.cufes.1=as.data.frame(ddres1.cufes.1[complete.cases(ddres1.cufes.1),])
  ddres1.cufes.1cor=ddres1.cufes.1[abs(ddres1.cufes.1$correlation)>=0.5,]
  ddres1.cufes.1cor$ct=unlist(strsplit(row.names(ddres1.cufes.1cor),split='[.]'))[seq(1,2*dim(ddres1.cufes.1cor)[1],2)]
  ddres1.cufes.1cor$year=unlist(strsplit(row.names(ddres1.cufes.1cor),split='[.]'))[seq(2,2*dim(ddres1.cufes.1cor)[1],2)]
  ddres1.cufes.1cor=ddres1.cufes.1cor[order(ddres1.cufes.1cor$ct),]
  ddres1.cufes.1cor$spid=row.names(ddres1.cufes.1cor)
  ddres1.cufes.1cor$sp=substr(as.character(ddres1.cufes.1cor$spid),1,8)
  ddres1.cufes.1cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.cufes.1cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.cufes.1cor$cs=gsub('[.]','',substr(as.character(ddres1.cufes.1cor$spid),17,24))
  ddres1.cufes.1cor$cs=gsub(' ','',ddres1.cufes.1cor$cs)
  ddres1.cufes.1cor$aCor=abs(ddres1.cufes.1cor$correlation)
  unique(ddres1.cufes.1cor$cs)
  #ddres1.cufes.1cor$signif=abs(ddres1.cufes.1cor$correlation)>=0.5
  head(ddres1.cufes.1cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.cufes.1cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  p <- ggplot(ddres1.cufes.1cor, aes(year,sp,size=abs(correlation),colour=correlation),
              shape=signif)+geom_point()+ggtitle("cor(variables,MFA1)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))
  p
  
  # 4.2.2. Variables correlation with MFA axis2 ------------
  #************************************
  ddres1.cufes.2=ddres1.cufes$Dim.2$quanti
  ddres1.cufes.2=as.data.frame(ddres1.cufes.2[complete.cases(ddres1.cufes.2),])
  ddres1.cufes.2cor=ddres1.cufes.2[abs(ddres1.cufes.2[,1])>=0.5,]
  ddres1.cufes.2cor$ct=unlist(strsplit(row.names(ddres1.cufes.2cor),split='[.]'))[seq(1,2*dim(ddres1.cufes.2cor)[1],2)]
  ddres1.cufes.2cor$year=unlist(strsplit(row.names(ddres1.cufes.2cor),split='[.]'))[seq(2,2*dim(ddres1.cufes.2cor)[1],2)]
  ddres1.cufes.2cor=ddres1.cufes.2cor[order(ddres1.cufes.2cor$correlation),]
  ddres1.cufes.2cor$spid=row.names(ddres1.cufes.2cor)
  ddres1.cufes.2cor$sp=substr(as.character(ddres1.cufes.2cor$spid),1,8)
  ddres1.cufes.2cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.cufes.2cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.cufes.2cor$cs=gsub('[.]','',substr(as.character(ddres1.cufes.2cor$spid),17,24))
  ddres1.cufes.2cor$cs=gsub(' ','',ddres1.cufes.2cor$cs)
  unique(ddres1.cufes.2cor$cs)
  ddres1.cufes.2cor$aCor=abs(ddres1.cufes.2cor$correlation)
  head(ddres1.cufes.2cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.cufes.2cor)%in%names(var.contribs2))
  # Plot variables well correlated with MFA axis2 (abs(cor)>0.5)
  p <- ggplot(ddres1.cufes.2cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA2)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p
  
  ddres1.cufes2cor=rbind(data.frame(MFA='MFA1',ddres1.cufes.1cor),data.frame(MFA='MFA2',ddres1.cufes.2cor))
  
  # Summary plot
  x11()
  p <- ggplot(ddres1.cufes2cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA12)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p + facet_grid(~MFA) + theme(axis.text.x=element_text(angle=45, hjust=1,size=rel(1)),axis.title.y = element_blank(),
                                  axis.title.x = element_blank()) + guides(size=FALSE)
  dev.print(device=png,
            filename=paste(path.export,'WGACGG-',ID,'-GridsMFA1-2corVar.png',sep=''),
            units='cm',width=29,height=21,res=300)
  
  # 4.2.3. Variables correlation with MFA axis 3 ------------
  #************************************
  ddres1.cufes.3=ddres1.cufes$Dim.3$quanti
  ddres1.cufes.3=as.data.frame(ddres1.cufes.3[complete.cases(ddres1.cufes.3),])
  ddres1.cufes.3cor=ddres1.cufes.3[abs(ddres1.cufes.3[,1])>=0.5,]
  ddres1.cufes.3cor$ct=unlist(strsplit(row.names(ddres1.cufes.3cor),split='[.]'))[seq(1,2*dim(ddres1.cufes.3cor)[1],2)]
  ddres1.cufes.3cor$year=unlist(strsplit(row.names(ddres1.cufes.3cor),split='[.]'))[seq(2,2*dim(ddres1.cufes.3cor)[1],2)]
  ddres1.cufes.3cor=ddres1.cufes.3cor[order(ddres1.cufes.3cor$correlation),]
  ddres1.cufes.3cor$spid=row.names(ddres1.cufes.3cor)
  ddres1.cufes.3cor$sp=substr(as.character(ddres1.cufes.3cor$spid),1,8)
  ddres1.cufes.3cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.cufes.3cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.cufes.3cor$cs=gsub('[.]','',substr(as.character(ddres1.cufes.3cor$spid),17,24))
  ddres1.cufes.3cor$cs=gsub(' ','',ddres1.cufes.3cor$cs)
  unique(ddres1.cufes.3cor$cs)
  ddres1.cufes.3cor$aCor=abs(ddres1.cufes.3cor$correlation)
  head(ddres1.cufes.3cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.cufes.3cor)%in%names(var.contribs3))
  # Plot variables well correlated with MFA axis3 (abs(cor)>0.5)
  p <- ggplot(ddres1.cufes.3cor, aes(year,sp,size=aCor,
                               colour=correlation))+geom_point()+ggtitle("cor(variables,MFA3)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p

  # Summary plots of variable correlations with MFA axes ---------------
  #************************************
  ddres1.cufes.1corPlusa.ct=aggregate(ddres1.cufes.1cor[ddres1.cufes.1cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.cufes.1cor[ddres1.cufes.1cor$correlation>=0.5,'ct']),length)
  ddres1.cufes.2corPlusa.ct=aggregate(ddres1.cufes.2cor[ddres1.cufes.2cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.cufes.2cor[ddres1.cufes.2cor$correlation>=0.5,'ct']),length)
  ddres1.cufes.2corMoinsa.ct=aggregate(ddres1.cufes.2cor[ddres1.cufes.2cor$correlation<=0.5,'correlation'],
                                 list(ct=ddres1.cufes.2cor[ddres1.cufes.2cor$correlation<=0.5,'ct']),length)
  ddres1.cufes.3corPlusa.ct=aggregate(ddres1.cufes.3cor[ddres1.cufes.3cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.cufes.3cor[ddres1.cufes.3cor$correlation>=0.5,'ct']),length)
  par(mfrow=c(2,2),mar=c(3,12,3,1),bg='white')
  barplot(ddres1.cufes.1corPlusa.ct$x,names.arg=ddres1.cufes.1corPlusa.ct$ct,main='cor(Variable:year,MFA1)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.cufes.1corMoins.res,1,sum),main='cor(Variable:year,MFA1)<=-0.5',horiz=TRUE,las=2)
  barplot(ddres1.cufes.2corPlusa.ct$x,names.arg=ddres1.cufes.2corPlusa.ct$ct,main='cor(Variable:year,MFA2)>=0.5',horiz=TRUE,las=2)
  barplot(ddres1.cufes.2corMoinsa.ct$x,names.arg=ddres1.cufes.2corMoinsa.ct$ct,main='cor(Variable:year,MFA2)<=0.5',horiz=TRUE,las=2)
  barplot(ddres1.cufes.3corPlusa.ct$x,names.arg=ddres1.cufes.3corPlusa.ct$ct,main='cor(Variable:year,MFA3)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.cufes.3corMoins.res,1,sum),main='cor(Variable:year,MFA3)<=-0.5',horiz=TRUE,las=2)
  par(mfrow=c(1,1))

#*******************************************************************
# 5. Maps of MFA coordinates ------------
#*******************************************************************

  # Path to folder to export maps

  path.export.median=paste(prefix,'WGACEGG/Results/MFAgrids/',ID,'/medianContribSel/',sep='')
  path.export.mean=paste(prefix,'WGACEGG/Results/MFAgrids/',ID,'/meanContribSel/',sep='')
  
  # 5.1. Plot maps of mean individuals MFA coordinates, -----------
  # filtered by contribution and/oir quality of representation and 
  # within-cell inertia on MFA1 to 3, over MFA1 to 3, and over MFA1 to 61
  # see function help page for details: ?BoB.MFA.rasterPlot
  #******************************************************************
  wgacegg.cufes.MFAres.rasters=BoB.MFA.rasterPlot(MFAres=res.wgacegg.cufes.MFA,nMFA=2,cellsel=cellsel,pipo=pipo,
                                    ptitle='',path.export=path.export.median,ux11=TRUE,
                                    plotit=list(MFA123coord=TRUE,MFA123timevar=TRUE,
                                                MFA123contrib=TRUE,MFA123cos2=TRUE,
                                                MFA123coordMcos2=TRUE,
                                                MFA123coordMcontrib=TRUE,MFA123pind=FALSE),
                                    funSel=median,layout=c(2, 1),
                                    Ndim=list(c(1),c(2),c(3),seq(3),dim(big.fssd.cufes.MFAs2)[2]))
  # quality of representation of individuals
  cos2df=res.wgacegg.cufes.MFA$ind$cos2
  # individuals contributions to MFA planes
  contrib2df=res.wgacegg.cufes.MFA$ind$contrib
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals quality of representation on MFA planes
  MFAcoordMcos2.rasterStack=wgacegg.cufes.MFAres.rasters$MFAcoordMcos2.rasterStack
  
  # 5.1.1. Plot MFA123 inertia maps ----
  #************************
  MFAinertia.raster=wgacegg.cufes.MFAres.rasters$MFAinertia.raster
  raster.levelplot.PELGAS(MFAinertia.raster[[1:2]],ux11=TRUE,lptheme=viridisTheme,
                          path1=path.export,fid1=paste(ID,'-1-2_MFAcoordInertia',sep=''),
                          margin = FALSE,xlab='',ylab='',nattri='',fwidth=30,fheight=15,fres=300)
  
  # 5.2. Define "characteristic areas", ------
  # where cell maps contribution to MFA planes is higher than the results of "funSel" applied on contribution maps
  #***************************************
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals contributions to MFA planes 
  MFAcoordMcontrib.rasterStack=wgacegg.cufes.MFAres.rasters$MFAcoordMcontrib.rasterStack
  # identify 'clumps' of cells in MFA planes to define "characteristic areas"
  MFAcoordMcontrib.rasterStackc1=clump(raster(MFAcoordMcontrib.rasterStack,1))
  plot(MFAcoordMcontrib.rasterStackc1)
  MFAcoordMcontrib.rasterStackc2=clump(raster(MFAcoordMcontrib.rasterStack,2))
  plot(MFAcoordMcontrib.rasterStackc2)
  MFAcoordMcontrib.rasterStackc3=clump(raster(MFAcoordMcontrib.rasterStack,3),directions=4)
  plot(MFAcoordMcontrib.rasterStackc3)
  # Compute mean MFA coordinates in characteristic areas
  zonal(raster(MFAcoordMcontrib.rasterStack,1), MFAcoordMcontrib.rasterStackc1, 'mean')
  zonal(raster(MFAcoordMcontrib.rasterStack,2), MFAcoordMcontrib.rasterStackc2, 'mean')
  zonal(raster(MFAcoordMcontrib.rasterStack,3), MFAcoordMcontrib.rasterStackc3, 'mean')
  # Extract cell coordinates and belongings to MFA characteristic areas and merge to data
  MFAcoordMcontrib.rasterStack1.xyz <- data.frame(rasterToPoints(MFAcoordMcontrib.rasterStackc1))
  MFAcoordMcontrib.rasterStack1.xyz$x=round(MFAcoordMcontrib.rasterStack1.xyz$x,3)
  MFAcoordMcontrib.rasterStack1.xyz$y=round(MFAcoordMcontrib.rasterStack1.xyz$y,3)
  MFAcoordMcontrib.rasterStack2.xyz <- data.frame(rasterToPoints(MFAcoordMcontrib.rasterStackc2))
  MFAcoordMcontrib.rasterStack2.xyz$x=round(MFAcoordMcontrib.rasterStack2.xyz$x,3)
  MFAcoordMcontrib.rasterStack2.xyz$y=round(MFAcoordMcontrib.rasterStack2.xyz$y,3)
  MFAcoordMcontrib.rasterStack3.xyz <- data.frame(rasterToPoints(MFAcoordMcontrib.rasterStackc3))
  
  head(MFAcoordMcontrib.rasterStack1.xyz)
  MFAcoordMcontrib.rasterStack1.xyz=merge(MFAcoordMcontrib.rasterStack1.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  plot(MFAcoordMcontrib.rasterStack1.xyz$x,MFAcoordMcontrib.rasterStack1.xyz$y)
  MFAcoordMcontrib.rasterStack2.xyz=merge(MFAcoordMcontrib.rasterStack2.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  plot(MFAcoordMcontrib.rasterStack2.xyz$x,MFAcoordMcontrib.rasterStack2.xyz$y)
  MFAcoordMcontrib.rasterStack3.xyz=merge(MFAcoordMcontrib.rasterStack3.xyz,cellsel,by.x=c('x','y'),
                                          by.y=c('x','y'))
  
  # Add belongings to characteristic areas to raw data
  clumpsdf1=MFAcoordMcontrib.rasterStack1.xyz
  names(clumpsdf1)[names(clumpsdf1)=='clumps']='clumps.MFA1f'
  plot(clumpsdf1$x,clumpsdf1$y)
  clumpsdf2=MFAcoordMcontrib.rasterStack2.xyz
  names(clumpsdf2)[names(clumpsdf2)=='clumps']='clumps.MFA2f'
  clumpsdf3=MFAcoordMcontrib.rasterStack3.xyz
  names(clumpsdf3)[names(clumpsdf3)=='clumps']='clumps.MFA3f'
  head(clumpsdf2)
  clumpsdf12=merge(clumpsdf1,clumpsdf2,all.x=TRUE,all.y=TRUE)
  head(clumpsdf12)
  
  # Gives names to characteristic areas for plots
  head(CUFES.ac.spring.ibbbs)
  CLres.wgacegg.cufes.ALL.MFA1=merge(clumpsdf1,CUFES.ac.spring.ibbbs[,c('I','J','sp','Year','Zvalue')])
  head(CLres.wgacegg.cufes.ALL.MFA1)
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==1,'clumps.MFA1f']='NWBIS'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==2,'clumps.MFA1f']='SEBIS'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==3,'clumps.MFA1f']='CANTA'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==4,'clumps.MFA1f']='OWIBE'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==5,'clumps.MFA1f']='CWIBE'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==6,'clumps.MFA1f']='CWIBE'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==7,'clumps.MFA1f']='CWIBE'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f==8,'clumps.MFA1f']='CADIZ'
  unique(CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f)
  # check cluster naming
  CLres.wgacegg.cufes.ALL.MFA1a=aggregate(CLres.wgacegg.cufes.ALL.MFA1[,c('x','y')],
                                     list(CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f),mean)
  plot(CLres.wgacegg.cufes.ALL.MFA1$x,CLres.wgacegg.cufes.ALL.MFA1$y,
       pch=as.numeric(factor(CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f)),col=('grey50'))
  text(CLres.wgacegg.cufes.ALL.MFA1a$x,CLres.wgacegg.cufes.ALL.MFA1a$y,CLres.wgacegg.cufes.ALL.MFA1a$Group.1)
  coast()
  # larger clusters
  CLres.wgacegg.cufes.ALL.MFA1$clumps2.MFA1f='SWIBE&CSBIS'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f=='CANTA','clumps2.MFA1f']='CANTA&OFFS'
  CLres.wgacegg.cufes.ALL.MFA1[CLres.wgacegg.cufes.ALL.MFA1$clumps.MFA1f=='NWBIS','clumps2.MFA1f']='CANTA&OFFS'
  plot(CLres.wgacegg.cufes.ALL.MFA1$x,CLres.wgacegg.cufes.ALL.MFA1$y)
  
  CLres.wgacegg.cufes.ALL.MFA2=merge(clumpsdf2,CUFES.ac.spring.ibbbs[,c('I','J','sp','Year','Zvalue')])
  head(CLres.wgacegg.cufes.ALL.MFA2)
  unique(CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f)
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==1,'clumps.MFA2f']='CNBIS'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==2,'clumps.MFA2f']='OBIS'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==3,'clumps.MFA2f']='CSBI'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==4,'clumps.MFA2f']='CSBI'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==5,'clumps.MFA2f']='OBIS'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==6,'clumps.MFA2f']='WCANTA'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==7,'clumps.MFA2f']='ECANTA'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==8,'clumps.MFA2f']='ECANTA'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==9,'clumps.MFA2f']='WIBE'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==10,'clumps.MFA2f']='WIBE'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f==11,'clumps.MFA2f']='SWIBE'
  unique(CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f)
  # check cluster naming
  CLres.wgacegg.cufes.ALL.MFA2a=aggregate(CLres.wgacegg.cufes.ALL.MFA2[,c('x','y')],
                                     list(CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f),mean)
  plot(CLres.wgacegg.cufes.ALL.MFA2$x,CLres.wgacegg.cufes.ALL.MFA2$y,
       pch=as.numeric(factor(CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f)),col=('grey50'))
  text(CLres.wgacegg.cufes.ALL.MFA2a$x,CLres.wgacegg.cufes.ALL.MFA2a$y,CLres.wgacegg.cufes.ALL.MFA2a$Group.1)
  coast()
  
  CLres.wgacegg.cufes.ALL.MFA2$clumps2.MFA2f='WIBE&CNBIS'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f=='ONBIS','clumps2.MFA2f']='CANTA&SBIS'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f=='CSBI','clumps2.MFA2f']='CANTA&SBIS'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f=='ECANTA','clumps2.MFA2f']='CANTA&SBIS'
  CLres.wgacegg.cufes.ALL.MFA2[CLres.wgacegg.cufes.ALL.MFA2$clumps.MFA2f=='WCANTA','clumps2.MFA2f']='CANTA&SBIS'
  plot(CLres.wgacegg.cufes.ALL.MFA2$x,CLres.wgacegg.cufes.ALL.MFA2$y)
  
  CLres.wgacegg.cufes.ALL.MFA12=merge(CLres.wgacegg.cufes.ALL.MFA1,CLres.wgacegg.cufes.ALL.MFA2,
                                 all.x=TRUE,all.y=TRUE)
  plot(CLres.wgacegg.cufes.ALL.MFA12$x,CLres.wgacegg.cufes.ALL.MFA12$y)
  head(CLres.wgacegg.cufes.ALL.MFA12)
  
  # plot characteristic areas
  wgacegg.cufes.MFAres.rasters=BoB.MFA.rasterPlot(MFAres=res.wgacegg.cufes.MFA,nMFA=2,cellsel=cellsel,pipo=pipo,
                                             ptitle='',path.export=NULL,ux11=TRUE,
                                             plotit=list(MFA123coord=FALSE,MFA123timevar=FALSE,
                                                         MFA123contrib=FALSE,MFA123cos2=FALSE,
                                                         MFA123coordMcos2=FALSE,
                                                         MFA123coordMcontrib=TRUE,MFA123pind=FALSE),
                                             funSel=median,layout=c(2, 1),
                                             Ndim=list(c(1),c(2),c(3),seq(3),dim(big.fssd.cufes.MFAs2)[2]))
  names(wgacegg.cufes.MFAres.rasters)
  
  wgacegg.cufes.MFAcoordMcontrib.rasterStack=wgacegg.cufes.MFAres.rasters$MFAcoordMcontrib.rasterStack
  plot(wgacegg.cufes.MFAcoordMcontrib.rasterStack)
  raster.levelplot.PELGAS(wgacegg.cufes.MFAcoordMcontrib.rasterStack,ux11=TRUE,lptheme=viridisTheme,
                          path1=NULL,fid1=paste('1-2_MFAcoordInertia',sep=''),
                          margin = FALSE,xlab='',ylab='',nattri='',fwidth=30,fheight=15,fres=300,bathy.BoB = FALSE)
  grid.text(x=280, y=220, label='Area 1',default.units='native')
  grid.text(x=200, y=200, label='Area 2',default.units='native')
  
  # 5.3. Compute mean clumps composition -----------
  #***************************************
  #MFA1: anchovy rich BoB+GoG vs. anchovy less rich Cantabrian and Galicia
  #****************
  CLres.wgacegg.cufes.ALL.MFA1a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=CLres.wgacegg.cufes.ALL.MFA1,
                                              cname='clumps2.MFA1f')
  # Relative biomass
  x11()
  p <- ggplot(CLres.wgacegg.cufes.ALL.MFA1a, aes(clust,pmZ,fill = factor(sp))) +
    geom_bar(stat = "identity") + coord_flip()+scale_fill_manual(values=c('green','blue'),
                      guide = guide_legend(title = "Species"))
  p
  dev.print(device=png,
            filename=paste(path.export,'WGACEGGadultFishMFA1areasSpComp.png',sep=''),
            units='cm',width=29,height=21,res=300)
  
  #MFA2: sardine poor Southern Biscay vs. sardine rich Northern Biscay and Western Iberican
  #****************
  CLres.wgacegg.cufes.ALL.MFA2a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=CLres.wgacegg.cufes.ALL.MFA2,
                                              cname='clumps2.MFA2f')
  # with mackerel and official palette
  # Relative biomass
  x11()
  p <- ggplot(CLres.wgacegg.cufes.ALL.MFA2a, aes(clust,pmZ,fill = factor(sp))) +
    geom_bar(stat = "identity") + coord_flip()+
    scale_fill_manual(values=c('green','blue'),
                      guide = guide_legend(title = "Species"))
  p
  dev.print(device=png,
            filename=paste(path.export,'WGACEGGadultFishMFA2areasSpComp.png',sep=''),
            units='cm',width=29,height=21,res=300)
  
  # # MFA3: Coastal N&S vs. Fer à Cheval
  # #***************
  # CLres.fssd.ALL.MFA3a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=CLres.fssd.ALL.MFA3)
  # # with mackerel and official palette
  # # Relative biomass
  # p <- ggplot(CLres.fssd.ALL.MFA3a, aes(cs,pmZ,fill = factor(sp))) +geom_bar(stat = "identity") + coord_flip()+
  #   scale_fill_manual(values=c('brown2','green','grey','blue','orange','red','black','magenta2','yellow'),
  #                     guide = guide_legend(title = "Species"))
  # p + facet_grid(cz~clust)+ theme(axis.title.y = element_blank(),axis.title.x = element_blank())

#*********************************************************************
# 6. Temporal analysis of MFA results --------------
#*********************************************************************

  # 6.1. Years (group) mean positions in MFA space --------------
  #***********************************************
  # From Pagès (2014):
  # Graphs like this are interpreted in a similar way as correlation circles: in
  # both cases the coordinate of a point is interpreted as a relationship measurement
  # with a maximum value of 1. But this new graph has two specificities: the
  # groups of variables are unstandardised and their coordinates are always positive.
  # They therefore appear in a square (with a side of 1 and with points [0,0]
  # and [1,1] as vertices) known as a relationship square. Pages (2014), p141
  
  # Therefore, when compared with an intuitive approach to interpretation
  # (for a PCA user), one highly practical advantage of this representation is its
  # relationship with the representations of individuals and variables already
  # provided: in MFA, data are considered from different points of view, but in
  # one single framework. Pages (2014), p141
  names(res.wgacegg.cufes.MFA)
  res.wgacegg.cufes.MFA.group=res.wgacegg.cufes.MFA$group
  gcoord=data.frame(res.wgacegg.cufes.MFA.group$coord[,1:3])
  gcontrib=res.wgacegg.cufes.MFA.group$contrib[,1:3]
  gcontrib=data.frame(gcontrib)
  names(gcontrib)=paste('contrib',names(gcontrib),sep='.')
  gcontrib$year=row.names(gcontrib)
  gcoord$year=row.names(gcoord)
  gcoord=merge(gcoord,gcontrib,by.x='year',by.y='year')
  row.names(gcoord)=gcoord$year
  gcoord$contrib.Dim.1.2=gcoord$contrib.Dim.1+gcoord$contrib.Dim.2
  gcoord$contrib.Dim.2.3=gcoord$contrib.Dim.2+gcoord$contrib.Dim.3
  head(gcoord)  
  gcoord$year=paste('20',gcoord$year,sep='')
  range(gcoord$Dim.1)
  range(gcoord$Dim.2)
  range(gcoord$Dim.3)
  
  # Plot of groups mean positions in MFA space:
  #******************************************
  x11()
  p12 <- ggplot(gcoord,aes(Dim.1,Dim.2,label=year))+geom_text(aes(colour = contrib.Dim.1.2))+
    scale_colour_continuous(name="Contribution (%)")
  p12
  dev.print(device=png,
            filename=paste(path.export,'WGACEGG-',ID,'-MFA12groupsBiplot.png',sep=''),
            units='cm',width=29,height=21,res=300)
  # p23 <- ggplot(gcoord,aes(Dim.2,Dim.3,label=year))+geom_text(aes(colour = contrib.Dim.2.3))+
  #   scale_colour_continuous(name="Contribution (%)")
  # multiplot(p12,p23)

  # 6.2. Eventually, study years partial axes correlations with MFA axes --------
  #****************************************
  # We have already insisted on the need to connect the inertias from the MFA
  # and those in the separate PCAs (see Tables 4.4 and 4.5). It is also useful to
  # connect the factors of the MFA with those of the separate PCAs (also known
  # as partial axes), both to understand better the effects of weighting and to enrich
  # interpretation of the analyses. In order to do this, the latter are projected as
  # supplementary variables (see Figure 4.5). Pages 2014, p88
  
  # Partial axes correlations
  res.wgacegg.cufes.MFA.partial.axes=res.wgacegg.cufes.MFA$partial.axes
  ddres.pyears=data.frame(res.wgacegg.cufes.MFA.partial.axes$cor)
  head(ddres.pyears)
  ddres.pyears[row.names(ddres.pyears)=='Dim1.14',]
  ddres.pyears.12=data.frame(res.wgacegg.cufes.MFA.partial.axes$cor[,1:2])
  ddres.pyears.12s=ddres.pyears.12[round(abs(ddres.pyears.12$Dim.1))>=0.6|
                                     round(abs(ddres.pyears.12$Dim.2))>=0.6,]
  ddres.pyears.12s$Dim=factor(gsub('[.]','',substr(row.names(ddres.pyears.12s),1,5)),
                              ordered=TRUE,levels=paste('Dim',seq(10),sep=''))
  ddres.pyears.12s$Year=gsub('[.]','',substr(row.names(ddres.pyears.12s),6,8))
  ddres.pyears.12s.long=reshape(ddres.pyears.12s,varying=list(1:2),
                                direction='long',timevar='MFA',v.names='correlation')
  ddres.pyears.12s.long$MFA=paste('Dim',ddres.pyears.12s.long$MFA,sep='')
  ddres.pyears.12s.long$signif=abs(round(ddres.pyears.12s.long$correlation))>=0.6
  ddres.pyears.12s.long$MFA=paste('MFA',ddres.pyears.12s.long$MFA,sep='.')
  head(ddres.pyears.12s.long)  
  # Plot partial axes correlations with MFA axes
  p <- ggplot(ddres.pyears.12s.long[ddres.pyears.12s.long$Dim%in%c('Dim1','Dim2'),], 
              aes(Dim,Year,size=abs(correlation),colour=correlation))+geom_point()
  p+ scale_colour_gradientn(colours = viridis(3)) + facet_grid(.~MFA)+
    theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(1)),axis.title.y = element_blank(),
          axis.title.x = element_blank()) + guides(size=FALSE)

  # 6.3. Maps of partial individuals (map cell:year pairs) coordinates in MFA space ---------
  #***********************************
  MFAcoord=res.wgacegg.cufes.MFA$ind$coord
  pindCoord.cufes=res.wgacegg.cufes.MFA$ind$coord.partiel
  names(res.wgacegg.cufes.MFA$ind)
  MFAyears=unlist(strsplit(row.names(pindCoord.cufes),split='[.]'))[seq(2,(2*length(row.names(pindCoord.cufes))),2)]
  lyears=unique(MFAyears)
  pindCoord.cufesan=pindCoord.cufes-MFAcoord
  
  # 6.3.1. Maps of partial individuals coordinates on MFA1,2,3, one plot per year ---------------- 
  #*****************************
  for (i in 1:length(lyears)){
    dfi=pindCoord.cufes[MFAyears==lyears[i],]-MFAcoord
    dim(MFAcoord)
    head(dfi)
    BoB.MFA.rasterPlot(MFAres=dfi,nMFA=2,cellsel,pipo,path.export=NULL,
                       plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                   MFA123contrib=FALSE,
                                   MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                   MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                       ptitle=paste('20',lyears[i],sep=''),lptheme = viridisTheme())
  }
  # 6.3.2. Maps of partial individuals coordinates anomalies, one mosaic plot per MFA1,2,3, with all years
  #*****************************
  res=BoB.MFA.rasterPlot(MFAres=res.wgacegg.cufes.MFA,nMFA=2,cellsel,pipo,
                         path.export=path.export,
                         plotit=list(MFA123coord=TRUE,MFA123contrib=FALSE,MFA123timevar=FALSE,
                                     MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                     MFA123coordMcontrib=FALSE,MFA123pind=TRUE),funSel='mean',
                         ptitle=paste(''),lptheme=BuRdTheme,anomalit=TRUE,ux11=TRUE)

 
