library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)
library(rmarkdown)

#***************************************************************************************************************************************
#                                               Multiple Factor Analysis with FactoMineR tutorial
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.5

# 1. Import grid map data and convert them into raster package objects --------
#********************************************************************

  # path to donnees2 drive
  #*********************************
  donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
  
  # path to grid files
  #*********************************
  prefix=paste(donnees2,'Campagnes/',sep='')
  prefix=paste('D:/Campagnes/',sep='')
  path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
  path.grids2=paste(path.grids,"SST_SPRING_IBBB/",sep='')
  path.grids3=paste(path.grids,"SSS_SPRING_IBBB/",sep='')
  path.grids4=paste(path.grids,"SSTS_SPRING_IBBB/",sep='')
  path.export.envt=paste(prefix,"WGACEGG/Results/MFAgrids/SSTS/",sep='')
  
  # List grid files 
  #*********************************
  # SST
  lffsst=list.files(path.grids2,pattern='*.txt')
  #lfs=strsplit(lf,split='[.]',)
  lffssts=data.frame(t(data.frame(strsplit(lffsst,split='[.]'))))
  lp=as.character(lffssts[,1])
  lffssts2=data.frame(t(data.frame(strsplit(lp,split='_'))))
  row.names(lffssts2)=seq(length(lffssts2[,1]))
  lffssts=cbind(lffssts,lffssts2)
  head(lffssts)
  names(lffssts)=c('pat','ext','year','survey','time','area','unknw','sp','filter','quantile')
  lffssts$path=lffsst
  row.names(lffssts)=seq(length(lffssts[,1]))
  
  # SSS
  lffsss=list.files(path.grids3,pattern='*.txt')
  #lfs=strsplit(lf,split='[.]',)
  lffssss=data.frame(t(data.frame(strsplit(lffsss,split='[.]'))))
  lp=as.character(lffssss[,1])
  lffssss2=data.frame(t(data.frame(strsplit(lp,split='_'))))
  row.names(lffssss2)=seq(length(lffssss2[,1]))
  lffssss=cbind(lffssss,lffssss2)
  head(lffssss)
  names(lffssss)=c('pat','ext','year','survey','time','area','unknw','sp','filter','quantile')
  lffssss$path=lffsss
  row.names(lffssss)=seq(length(lffssss[,1]))
  
  lffssse=rbind(lffssss,lffssts)
  
  # Import grid and raster files, bind grid maps
  #***************************************************
  for (i in 1:dim(lffssse)[1]){
    pat=paste(path.grids4,lffssse$path[i],sep='')
    mati=read.table(file=pat,header=T,sep=";")
    mati$sp=lffssse$sp[i]
    mati$Survey='AC_SPRING_IBBB'
    dim(mati)
    mati=unique(mati)
    head(mati)
    if (i==1){
      SSTS=mati
    }else{
      SSTS=rbind(SSTS,mati)
    }
  }
  dim(SSTS)
  SSTS=unique(SSTS)
  unique(SSTS$Year)
  
  head(SSTS)
  
  # lsp=unique(SSTS$sp)
  # for (i in 1:length(lsp)){
  #   mati=SSTS[SSTS$sp==lsp[i],]
  #   graphics.off()
  #   resras=grid2rasterStack(pat=mati,path1=path.grids4,
  #                           varid=lsp[i],anomaly=TRUE)
  #   assign(paste(lsp[i],'rasterStack',sep='.'),resras)
  #   graphics.off()
  #   # Import raster stacks format, compute mean and SD maps and plot everything:
  #   #pat2=paste(paste(path.grids1,'Rasters/',sep=''),lffsstrss$path[i],sep='')
  #   #rasti=raster(pat2)
  # }

# 2. Prepare datasets for MFA -------------------
  #********************************************************************
  # 2.1. Select common species ---------------
  #***************************
  head(SSTS)
  unique(SSTS$Year)
  SSTS$spi=substr(SSTS$sp,1,8)
  SSTSp=SSTS[SSTS$Zvalue>0&!is.na(SSTS$Zvalue),]
  table(SSTSp$spi,SSTSp$Year)
  OccurenceFrequencySp=apply(table(SSTSp$spi,SSTSp$Year)>0,1,sum)/length(unique(SSTSp$Year))
  sort(OccurenceFrequencySp)

  # 2.2. Select years ---------------
  #***************************
  SSTSs=SSTS[!SSTS$Year%in%c(2003,2004,2009,2012),]
  unique(SSTSs$Year)
  head(SSTSs)
  
  # 2.3. Reshape data frame for analysis with year as grouping variable -------------
  #***************************
  a.wgaceggenvt <- reshape(SSTSs[,c("I","J","Zvalue","sp","Year")], timevar = "sp", idvar = c("I","J","Year"), direction = "wide")
  head(a.wgaceggenvt)
  names(a.wgaceggenvt)
  a.wgaceggenvt=a.wgaceggenvt[order(a.wgaceggenvt$Year),]
  
  big.wgaceggenvt <- reshape(a.wgaceggenvt, timevar = "Year", idvar = c("I","J"), direction = "wide")
  head(big.wgaceggenvt)
  names(big.wgaceggenvt)
  dim(big.wgaceggenvt)
  
  # 2.4. Select cells in survey area -------------
  #***************************
  pat=paste(path.grids4,lffssse$path[4],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  pipo=mati[mati$Year==2015,]
  plot(pipo$Xgd,pipo$Ygd)
  plot(pipo$Xgd,pipo$Ygd,cex=0.1+pipo$Zvalue/100)
  # Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
  #******************************************
  # Define grid limits
  x1=-10.2;x2=-1;y1=35.8;y2=49
  # Define cell dimensions
  ax=0.25;ay=0.25
  # Define smoothing radius
  u=2
  # Define iteration number
  ni=200
  define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni)
  poly=data.frame(x=xpol,y=ypol)
  lines(poly)
  coast()
  head(pipo)
  library(splancs) 
  pts=as.points(pipo$Xgd,pipo$Ygd)
  testInPoly=inout(pts,poly)
  plot(pts)
  points(pts[testInPoly,],pch=16)
  coast()
  lines(poly)
  cellsel.wgaceggenvt=data.frame(x=pts[,1],y=pts[,2],sel=testInPoly,I=pipo$I,J=pipo$J)
  cellsel.wgaceggenvt$cell=paste(cellsel.wgaceggenvt$I,cellsel.wgaceggenvt$J,sep='-')
  head(cellsel.wgaceggenvt)
  plot(cellsel.wgaceggenvt$x,cellsel.wgaceggenvt$y)
  points(cellsel.wgaceggenvt[cellsel.wgaceggenvt$sel,'x'],cellsel.wgaceggenvt[cellsel.wgaceggenvt$sel,'y'],pch=16)
  coast()
  legend('topleft',legend=c('Cells centers','Selected cells'),pch=c(1,16))

  # 2.5. Prepare and standardise dataset -----------
  #**************************
  head(big.wgaceggenvt)
  big.wgaceggenvt.MFA=big.wgaceggenvt[,-seq(2)]
  row.names(big.wgaceggenvt.MFA)=paste(big.wgaceggenvt$I,big.wgaceggenvt$J,sep='-')
  # simplify column names
  names(big.wgaceggenvt.MFA)=gsub('Zvalue.','',names(big.wgaceggenvt.MFA))
  names(big.wgaceggenvt.MFA)
  head(big.wgaceggenvt.MFA)

  # 2.6. Select cells in survey area before MFA ------
  #***************************************
  dim(big.wgaceggenvt.MFA)
  big.wgaceggenvt.MFA=big.wgaceggenvt.MFA[order(dimnames(big.wgaceggenvt.MFA)[[1]]),]
  head(big.wgaceggenvt.MFA)

  #cselNoNA=apply(big.wgaceggenvt.MFA,1,complete.cases)
  # 2.6.1. Select cells with no NA over the series -----
  #****************************************
  # # NA detector
  NAdetect=function(x){
    if (sum(is.na(x))>0){
      y=FALSE
    }else{
      y=TRUE
    }
    y
  }
  cselNoNA=apply(big.wgaceggenvt.MFA,1,NAdetect)
  table(cselNoNA)
  big.wgaceggenvt.MFAs=big.wgaceggenvt.MFA[cselNoNA,]
  cellsel.wgaceggenvt=cellsel.wgaceggenvt[order(cellsel.wgaceggenvt$cell),]
  plot(cellsel.wgaceggenvt$x,cellsel.wgaceggenvt$y)
  points(cellsel.wgaceggenvt[cselNoNA,'x'],cellsel.wgaceggenvt[cselNoNA,'y'],pch=16)
  coast()
  cellsel.wgaceggenvt$selNoNA=cselNoNA
  
  cselNoNA.df=data.frame(cselNoNA)
  cselNoNA.df=merge(cselNoNA.df,data.frame(cselNoNAf),by.x='row.names',by.y='row.names',all.x=TRUE)
  
  # # 2.6.2. convert NA's to zero ---------
  NA2null=function(x,procInf=FALSE){
    x[is.na(x)]=0
    if (procInf){
      x[is.infinite(x)]=0
    }
    x
  }
  big.wgaceggenvt.MFAs=apply(big.wgaceggenvt.MFAs,2,NA2null)
  head(big.wgaceggenvt.MFAs)
  rnames=dimnames(big.wgaceggenvt.MFAs)[[1]]
  # log variables
  big.wgaceggenvt.MFAs=apply(big.wgaceggenvt.MFAs,2,function(x){x=log(x+1);x})
  head(big.wgaceggenvt.MFAs)
  dim(big.wgaceggenvt.MFAs)
  class(big.wgaceggenvt.MFAs)
  dimnames(big.wgaceggenvt.MFAs)[[1]]=rnames
  # center and scale variables
  big.wgaceggenvt.MFAs=apply(big.wgaceggenvt.MFAs,2,scale,center=TRUE,scale=FALSE)
  head(big.wgaceggenvt.MFAs)
  class(big.wgaceggenvt.MFAs)
  dimnames(big.wgaceggenvt.MFAs)[[1]]=rnames
    
  # 2.7. Remove constant columns-----------  
  #***************************************
  # with zeroes
  scols=apply(big.wgaceggenvt.MFAs,2,sum,na.rm=TRUE)
  srows=apply(big.wgaceggenvt.MFAs,1,sum,na.rm=TRUE)
  big.wgaceggenvt.MFAs2=big.wgaceggenvt.MFAs[,scols!=0]
  sum(scols==0)
  scols2=apply(big.wgaceggenvt.MFAs2,2,sum,na.rm=TRUE)
  summary(scols2)
  # other constant columns
  vcols=apply(big.wgaceggenvt.MFAs2,2,var,na.rm=TRUE)
  ccols=dimnames(big.wgaceggenvt.MFAs2)[[2]][vcols==0]
  big.wgaceggenvt.MFAs2=big.wgaceggenvt.MFAs2[,!dimnames(big.wgaceggenvt.MFAs2)[[2]]%in%ccols]
  dim(big.wgaceggenvt.MFAs2)
  
  # 2.8. Generate no. of data in each group -----------
  #***************************************
  cwz.wgaceggenvt=dimnames(big.wgaceggenvt.MFAs2)[[2]]
  nct.wgaceggenvt=unlist(strsplit(cwz.wgaceggenvt,split='[.]'))[seq(1,2*length(cwz.wgaceggenvt),2)]
  nyears.wgaceggenvt=unlist(strsplit(cwz.wgaceggenvt,split='[.]'))[seq(2,2*length(cwz.wgaceggenvt),2)]
  lnyears.wgaceggenvt=sort(unique(nyears.wgaceggenvt))
  tnnc.wgaceggenvt=table(nct.wgaceggenvt,nyears.wgaceggenvt)
  tnnc.wgaceggenvt.df=data.frame(table(nct.wgaceggenvt,nyears.wgaceggenvt))
  stnnc.wgaceggenvt=apply(tnnc.wgaceggenvt,2,sum)
  head(tnnc.wgaceggenvt.df)
  tnnc.wgaceggenvt.dfa=aggregate(tnnc.wgaceggenvt.df$Freq,list(nct=tnnc.wgaceggenvt.df$nct),
                                 sum)
  summary(tnnc.wgaceggenvt.dfa$x)

# 3. Run MFA ----------------
#*************************************************************
  # 3.1. MFA ---------------
  #***********************************
  dim(big.wgaceggenvt.MFAs2)
  res.wgaceggenvt.MFA<-MFA(big.wgaceggenvt.MFAs2, group=stnnc.wgaceggenvt, 
                           type=rep("s",length(stnnc.wgaceggenvt)), 
                    ncp=dim(big.wgaceggenvt.MFAs2)[2], 
                    name.group=substr(lnyears.wgaceggenvt,3,4), 
                    num.group.sup=NULL, graph=TRUE)
  names(res.wgaceggenvt.MFA)
  
  # 3.2. Explained variance ----------
  #***********************************  
  summary(res.wgaceggenvt.MFA)  
  par(bg='white')
  barplot(res.wgaceggenvt.MFA$eig[,2],main="% of variance explained",names.arg=1:nrow(res.wgaceggenvt.MFA$eig))
  barplot(res.wgaceggenvt.MFA$eig[,3],main="Cumulative % of variance explained",names.arg=1:nrow(res.wgaceggenvt.MFA$eig))
  res.wgaceggenvt.MFA$eig[round(res.wgaceggenvt.MFA$eig[,3])==95,]
  dim(big.wgaceggenvt.MFAs2)
 
  # 3.3. Plots of individuals in MFA1-2-3 planes -----------
  #***********************************
  # Plot of individuals in MFA1-2 plane 
  p1 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "contrib") #with contribution in color scale
  p2 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "cos2") #with quality of representation in color scale
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA2-3 plane
  p1 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "contrib",axes = c(2, 3))
  p2 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "cos2",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-3 plane
  p1 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "contrib",axes = c(1, 3))
  p2 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "cos2",axes = c(1, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-2-3 planes, with contribution in color scale
  x11()
  p1 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "contrib")
  p2 = fviz_mfa_ind(res.wgaceggenvt.MFA, col.ind = "contrib",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  dev.print(device=png,
            filename=paste(path.export.envt,'WGACEGGadultEnvtGridsMFA123biplot.png',sep=''),
            units='cm',width=29,height=21,res=300)
  
# 4. Contributions of variables to MFA axes -------------
#******************************************************************
  par(bg='grey50')
  plot.MFA(res.wgaceggenvt.MFA, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, 
           habillage="group", select = "coord 10")
  par(bg='white')
  
  # 4.1. select variables with above average contribution to MFA axes ----------------
  #************************************
  var.contrib=res.wgaceggenvt.MFA$quanti.var$contrib
  head(var.contrib)
  summary(var.contrib[,1])
  var.contribs1=var.contrib[var.contrib[,1]>mean(var.contrib[,1]),1]
  var.contribs2=var.contrib[var.contrib[,2]>mean(var.contrib[,2]),2]
  var.contribs3=var.contrib[var.contrib[,3]>mean(var.contrib[,3]),3]
  
  # 4.2. Variables correlation with MFA axes ---------------
  #************************************
  var.cor=res.wgaceggenvt.MFA$quanti.var$contrib
  graphics.off()
  ddres1.wgaceggenvt=dimdesc(res.wgaceggenvt.MFA, axes = 1:4, proba = 0.05)
  names(ddres1.wgaceggenvt)
  
  # 4.2.1. Variables correlation with MFA axis1 ----------
  #************************************
  ddres1.wgaceggenvt.1=ddres1.wgaceggenvt$Dim.1$quanti
  ddres1.wgaceggenvt.1=as.data.frame(ddres1.wgaceggenvt.1[complete.cases(ddres1.wgaceggenvt.1),])
  ddres1.wgaceggenvt.1cor=ddres1.wgaceggenvt.1[abs(ddres1.wgaceggenvt.1$correlation)>=0.5,]
  ddres1.wgaceggenvt.1cor$ct=unlist(strsplit(row.names(ddres1.wgaceggenvt.1cor),split='[.]'))[seq(1,2*dim(ddres1.wgaceggenvt.1cor)[1],2)]
  ddres1.wgaceggenvt.1cor$year=unlist(strsplit(row.names(ddres1.wgaceggenvt.1cor),split='[.]'))[seq(2,2*dim(ddres1.wgaceggenvt.1cor)[1],2)]
  ddres1.wgaceggenvt.1cor=ddres1.wgaceggenvt.1cor[order(ddres1.wgaceggenvt.1cor$ct),]
  ddres1.wgaceggenvt.1cor$spid=row.names(ddres1.wgaceggenvt.1cor)
  ddres1.wgaceggenvt.1cor$sp=substr(as.character(ddres1.wgaceggenvt.1cor$spid),1,3)
  ddres1.wgaceggenvt.1cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.wgaceggenvt.1cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.wgaceggenvt.1cor$cs=gsub('[.]','',substr(as.character(ddres1.wgaceggenvt.1cor$spid),17,24))
  ddres1.wgaceggenvt.1cor$cs=gsub(' ','',ddres1.wgaceggenvt.1cor$cs)
  ddres1.wgaceggenvt.1cor$aCor=abs(ddres1.wgaceggenvt.1cor$correlation)
  unique(ddres1.wgaceggenvt.1cor$cs)
  #ddres1.wgaceggenvt.1cor$signif=abs(ddres1.wgaceggenvt.1cor$correlation)>=0.5
  head(ddres1.wgaceggenvt.1cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.wgaceggenvt.1cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  p <- ggplot(ddres1.wgaceggenvt.1cor, aes(year,sp,size=abs(correlation),colour=correlation),
              shape=signif)+geom_point()+ggtitle("cor(variables,MFA1)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))
  p
  
  # 4.2.2. Variables correlation with MFA axis2 ------------
  #************************************
  ddres1.wgaceggenvt.2=ddres1.wgaceggenvt$Dim.2$quanti
  ddres1.wgaceggenvt.2=as.data.frame(ddres1.wgaceggenvt.2[complete.cases(ddres1.wgaceggenvt.2),])
  ddres1.wgaceggenvt.2cor=ddres1.wgaceggenvt.2[abs(ddres1.wgaceggenvt.2[,1])>=0.5,]
  ddres1.wgaceggenvt.2cor$ct=unlist(strsplit(row.names(ddres1.wgaceggenvt.2cor),split='[.]'))[seq(1,2*dim(ddres1.wgaceggenvt.2cor)[1],2)]
  ddres1.wgaceggenvt.2cor$year=unlist(strsplit(row.names(ddres1.wgaceggenvt.2cor),split='[.]'))[seq(2,2*dim(ddres1.wgaceggenvt.2cor)[1],2)]
  ddres1.wgaceggenvt.2cor=ddres1.wgaceggenvt.2cor[order(ddres1.wgaceggenvt.2cor$correlation),]
  ddres1.wgaceggenvt.2cor$spid=row.names(ddres1.wgaceggenvt.2cor)
  ddres1.wgaceggenvt.2cor$sp=substr(as.character(ddres1.wgaceggenvt.2cor$spid),1,3)
  ddres1.wgaceggenvt.2cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.wgaceggenvt.2cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.wgaceggenvt.2cor$cs=gsub('[.]','',substr(as.character(ddres1.wgaceggenvt.2cor$spid),17,24))
  ddres1.wgaceggenvt.2cor$cs=gsub(' ','',ddres1.wgaceggenvt.2cor$cs)
  unique(ddres1.wgaceggenvt.2cor$cs)
  ddres1.wgaceggenvt.2cor$aCor=abs(ddres1.wgaceggenvt.2cor$correlation)
  head(ddres1.wgaceggenvt.2cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.wgaceggenvt.2cor)%in%names(var.contribs2))
  # Plot variables well correlated with MFA axis2 (abs(cor)>0.5)
  p <- ggplot(ddres1.wgaceggenvt.2cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA2)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p
  
  ddres1.wgaceggenvt2cor=rbind(data.frame(MFA='MFA1',ddres1.wgaceggenvt.1cor),data.frame(MFA='MFA2',ddres1.wgaceggenvt.2cor))
  
  p <- ggplot(ddres1.wgaceggenvt2cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA12)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p + facet_grid(~MFA) + theme(axis.text.x=element_text(angle=45, hjust=1,size=rel(1)),axis.title.y = element_blank(),
                                   axis.title.x = element_blank()) + guides(size=FALSE)

  # Summary plots of variable correlations with MFA axes ---------------
  #************************************
  ddres1.wgaceggenvt.1corPlusa.ct=aggregate(ddres1.wgaceggenvt.1cor[ddres1.wgaceggenvt.1cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.wgaceggenvt.1cor[ddres1.wgaceggenvt.1cor$correlation>=0.5,'ct']),length)
  ddres1.wgaceggenvt.2corPlusa.ct=aggregate(ddres1.wgaceggenvt.2cor[ddres1.wgaceggenvt.2cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.wgaceggenvt.2cor[ddres1.wgaceggenvt.2cor$correlation>=0.5,'ct']),length)
  ddres1.wgaceggenvt.2corMoinsa.ct=aggregate(ddres1.wgaceggenvt.2cor[ddres1.wgaceggenvt.2cor$correlation<=0.5,'correlation'],
                                 list(ct=ddres1.wgaceggenvt.2cor[ddres1.wgaceggenvt.2cor$correlation<=0.5,'ct']),length)
  ddres1.wgaceggenvt.3corPlusa.ct=aggregate(ddres1.wgaceggenvt.3cor[ddres1.wgaceggenvt.3cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.wgaceggenvt.3cor[ddres1.wgaceggenvt.3cor$correlation>=0.5,'ct']),length)
  par(mfrow=c(2,2),mar=c(3,12,3,1),bg='white')
  barplot(ddres1.wgaceggenvt.1corPlusa.ct$x,names.arg=ddres1.wgaceggenvt.1corPlusa.ct$ct,main='cor(Variable:year,MFA1)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.wgaceggenvt.1corMoins.res,1,sum),main='cor(Variable:year,MFA1)<=-0.5',horiz=TRUE,las=2)
  barplot(ddres1.wgaceggenvt.2corPlusa.ct$x,names.arg=ddres1.wgaceggenvt.2corPlusa.ct$ct,main='cor(Variable:year,MFA2)>=0.5',horiz=TRUE,las=2)
  barplot(ddres1.wgaceggenvt.2corMoinsa.ct$x,names.arg=ddres1.wgaceggenvt.2corMoinsa.ct$ct,main='cor(Variable:year,MFA2)<=0.5',horiz=TRUE,las=2)
  barplot(ddres1.wgaceggenvt.3corPlusa.ct$x,names.arg=ddres1.wgaceggenvt.3corPlusa.ct$ct,main='cor(Variable:year,MFA3)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.wgaceggenvt.3corMoins.res,1,sum),main='cor(Variable:year,MFA3)<=-0.5',horiz=TRUE,las=2)
  par(mfrow=c(1,1))

#*******************************************************************
# 5. Maps of MFA coordinates ------------
#*******************************************************************

  # Path to folder to export maps
  path.export.envt.median1=paste(prefix,"WGACEGG/Results/MFAgrids/SSTS/medianContribSel/",sep='')
  path.export.envt.mean1=paste(prefix,"WGACEGG/Results/MFAgrids/SSTS/meanContribSel/",sep='')
  
  # 5.1. Plot maps of mean individuals MFA coordinates, -----------
  # filtered by contribution and/oir quality of representation and 
  # within-cell inertia on MFA1 to 3, over MFA1 to 3, and over MFA1 to 61
  # see function help page for details: ?BoB.MFA.rasterPlot
  #******************************************************************
  wgaceggenvt.MFAres.rasters=BoB.MFA.rasterPlot(MFAres=res.wgaceggenvt.MFA,nMFA=2,
                                                cellsel=cellsel.wgaceggenvt,pipo=pipo,
                                    ptitle='',path.export.envt=path.export.envt.median1,
                                    ux11=TRUE,
                                    plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                                MFA123contrib=FALSE,MFA123cos2=FALSE,
                                                MFA123coordMcos2=FALSE,
                                                MFA123coordMcontrib=FALSE,MFA123pind=FALSE),
                                    funSel=median,layout=c(2, 1),
                                    Ndim=list(c(1),c(2),c(3),seq(3),
                                              seq(dim(big.wgaceggenvt.MFAs2)[2])))
  # quality of representation of individuals
  wgaceggenvt.cos2df=res.wgaceggenvt.MFA$ind$cos2
  # individuals contributions to MFA planes
  wgaceggenvt.contrib2df=res.wgaceggenvt.MFA$ind$contrib
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals quality of representation on MFA planes
  wgaceggenvt.MFAcoordMcos2.rasterStack=wgaceggenvt.MFAres.rasters$MFAcoordMcos2.rasterStack
  
  # Plot MFA123 inertia maps
  #************************
  names(wgaceggenvt.MFAres.rasters)
  wgaceggenvt.MFAinertia.raster=wgaceggenvt.MFAres.rasters$MFAinertia.raster
  raster.levelplot.PELGAS(wgaceggenvt.MFAinertia.raster[[1]],ux11=TRUE,lptheme=viridisTheme,
                          path1=path.export.envt,fid1=paste('1_MFAcoordInertia',sep=''),
                          margin = FALSE,xlab='',ylab='',nattri='',fwidth=30,fheight=15,fres=300)
  
  # 5.2. Define "characteristic areas", ------
  # where cell maps contribution to MFA planes is higher than the results of "funSel" applied on contribution maps
  #***************************************
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals contributions to MFA planes 
  wgaceggenvt.MFAcoordMcontrib.rasterStack=wgaceggenvt.MFAres.rasters$MFAcoordMcontrib.rasterStack
  # identify 'clumps' of cells in MFA planes to define "characteristic areas"
  plot(wgaceggenvt.MFAcoordMcontrib.rasterStack)
  raster(wgaceggenvt.MFAcoordMcontrib.rasterStack,1)
  wgaceggenvt.MFAcoordMcontrib.rasterStackc1=clump(raster(wgaceggenvt.MFAcoordMcontrib.rasterStack,1))
  plot(wgaceggenvt.MFAcoordMcontrib.rasterStackc1)
  wgaceggenvt.MFAcoordMcontrib.rasterStackc2=clump(raster(wgaceggenvt.MFAcoordMcontrib.rasterStack,2))
  plot(wgaceggenvt.MFAcoordMcontrib.rasterStackc2)
  #MFAcoordMcontrib.rasterStackc3=clump(raster(MFAcoordMcontrib.rasterStack,3),directions=4)
  #plot(MFAcoordMcontrib.rasterStackc3)
  #Compute mean MFA coordinates in characteristic areas
  zonal(raster(wgaceggenvt.MFAcoordMcontrib.rasterStack,1), wgaceggenvt.MFAcoordMcontrib.rasterStackc1, 'mean')
  #
  zonal(raster(wgaceggenvt.MFAcoordMcontrib.rasterStack,2), wgaceggenvt.MFAcoordMcontrib.rasterStackc2, 'mean')
  #zonal(raster(MFAcoordMcontrib.rasterStack,3), MFAcoordMcontrib.rasterStackc3, 'mean')
  # Extract cell coordinates and belongings to MFA characteristic areas and merge to data
  wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz <- data.frame(rasterToPoints(wgaceggenvt.MFAcoordMcontrib.rasterStackc1))
  wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz$x=round(wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz$x,3)
  wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz$y=round(wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz$y,3)
  wgaceggenvt.MFAcoordMcontrib.rasterStack2.xyz <- data.frame(rasterToPoints(wgaceggenvt.MFAcoordMcontrib.rasterStackc2))
  wgaceggenvt.MFAcoordMcontrib.rasterStack2.xyz$x=round(wgaceggenvt.MFAcoordMcontrib.rasterStack2.xyz$x,3)
  # MFAcoordMcontrib.rasterStack2.xyz$y=round(MFAcoordMcontrib.rasterStack2.xyz$y,3)
  # MFAcoordMcontrib.rasterStack3.xyz <- data.frame(rasterToPoints(MFAcoordMcontrib.rasterStackc3))
  
  head(wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz)
  wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz=merge(wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz,
                                                      cellsel.wgaceggenvt,by.x=c('x','y'),by.y=c('x','y'))
  wgaceggenvt.MFAcoordMcontrib.rasterStack2.xyz=merge(wgaceggenvt.MFAcoordMcontrib.rasterStack2.xyz,
                                                      cellsel.wgaceggenvt,by.x=c('x','y'),
                                          by.y=c('x','y'))
  #MFAcoordMcontrib.rasterStack3.xyz=merge(MFAcoordMcontrib.rasterStack3.xyz,cellsel.wgaceggenvt,by.x=c('x','y'),
  #                                        by.y=c('x','y'))
  
  # Add belongings to characteristic areas to raw data
  wgaceggenvt.clumpsdf1=wgaceggenvt.MFAcoordMcontrib.rasterStack1.xyz
  names(wgaceggenvt.clumpsdf1)[names(wgaceggenvt.clumpsdf1)=='clumps']='clumps.MFA1e'
  wgaceggenvt.clumpsdf2=wgaceggenvt.MFAcoordMcontrib.rasterStack2.xyz
  names(wgaceggenvt.clumpsdf2)[names(wgaceggenvt.clumpsdf2)=='clumps']='clumps.MFA2e'
  #clumpsdf3=MFAcoordMcontrib.rasterStack3.xyz
  wgaceggenvt.clumpsdf12=merge(wgaceggenvt.clumpsdf1,wgaceggenvt.clumpsdf2)
  head(clumpsdf12)
  
  # Gives names to characteristic areas for plots
  head(SSTSs)
  wgaceggenvt.CLres.ALL.MFA1=merge(wgaceggenvt.clumpsdf1,SSTSs[,c('I','J','sp','Year','Zvalue')])
  head(wgaceggenvt.CLres.ALL.MFA1)
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e==1,'clumps.MFA1e']='CBIS'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e==2,'clumps.MFA1e']='OBIS'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e==3,'clumps.MFA1e']='OBIS'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e==4,'clumps.MFA1e']='CBIS'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e==5,'clumps.MFA1e']='GALI'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e==6,'clumps.MFA1e']='SPOR'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e==7,'clumps.MFA1e']='SPOR'
  unique(wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e)
  wgaceggenvt.CLres.ALL.MFA1$clumps2.MFA1e='SPOR&OBIS'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e=='CBIS','clumps2.MFA1e']='CBIS&GALI'
  wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$clumps.MFA1e=='GALI','clumps2.MFA1e']='CBIS&GALI'

  wgaceggenvt.CLres.ALL.MFA2=merge(wgaceggenvt.clumpsdf2,SSTSs[,c('I','J','sp','Year','Zvalue')])
  head(wgaceggenvt.CLres.ALL.MFA2)
  unique(wgaceggenvt.CLres.ALL.MFA2$clumps)
  wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$clumps.MFA2e==1,'clumps.MFA2e']='SCBI'
  wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$clumps.MFA2e==2,'clumps.MFA2e']='SCBI'
  wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$clumps.MFA2e==3,'clumps.MFA2e']='CANT'
  wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$clumps.MFA2e==4,'clumps.MFA2e']='SPOR'
  wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$clumps.MFA2e==5,'clumps.MFA2e']='SPOR'
  unique(wgaceggenvt.CLres.ALL.MFA2$clumps.MFA2e)
  wgaceggenvt.CLres.ALL.MFA2$clumps2.MFA2e='SCBI&SPOR'
  wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$clumps.MFA2e=='CANT','clumps2.MFA2e']='CANT'

  wgaceggenvt.CLres.ALL.MFA12=merge(wgaceggenvt.CLres.ALL.MFA1,wgaceggenvt.CLres.ALL.MFA2)
  head(wgaceggenvt.CLres.ALL.MFA12)
  
  # 5.3. Compute mean clumps composition -----------
  #***************************************
  #MFA1: higher T&S in offshore Biscay and Southern Portugal vs. lower T&S in coastal Biscay and Galicia
  #****************
  # MFA2 : higher temperature in southern Portugal and South Biscay compared to Cantabrian Sea
  wgaceggenvt.CLres.ALL.MFA1a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=wgaceggenvt.CLres.ALL.MFA1,
                                                 cname='clumps2.MFA1e')
  head(wgaceggenvt.CLres.ALL.MFA1)

  p1<-ggplot(wgaceggenvt.CLres.ALL.MFA1, 
                aes(x=clumps2, y=Zvalue)) + geom_boxplot() + labs(x='Regions',y='SST in °C, SSS in psu')
  p1 + facet_grid(~sp)

  p2<-ggplot(wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$sp=='SST',], 
            aes(x=clumps2, y=Zvalue)) + geom_boxplot() + labs(x='Regions',y='SST in °C')
  p2
  
  # test differences
  ggplot(wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SST',], aes(Zvalue, colour = clumps2)) +
    geom_density()+labs(x = 'SST')
  ggplot(wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SSS',], aes(Zvalue, colour = clumps2)) +
    geom_density()+labs(x = 'SSS')
  ggplot(wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$sp=='SST',], aes(Zvalue, colour = clumps2)) +
    geom_density()+labs(x = 'SST')
  
  wilcox.test(Zvalue~clumps2,data=wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SST',],
              alternative='less')
  wilcox.test(Zvalue~clumps2,data=wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SSS',],
              alternative='less',conf.int=TRUE)  
  wilcox.test(Zvalue~clumps2,data=wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$sp=='SST',],
              alternative='less')
  aggregate(wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SST','Zvalue'],
            list(wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SST','clumps2']),summary)
  aggregate(wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SSS','Zvalue'],
            list(wgaceggenvt.CLres.ALL.MFA1[wgaceggenvt.CLres.ALL.MFA1$sp=='SSS','clumps2']),summary)
  aggregate(wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$sp=='SST','Zvalue'],
            list(wgaceggenvt.CLres.ALL.MFA2[wgaceggenvt.CLres.ALL.MFA2$sp=='SST','clumps2']),summary)

#*********************************************************************
# 6. Temporal analysis of MFA results --------------
#*********************************************************************

  # 6.1. Years (group) mean positions in MFA space --------------
  #***********************************************
  # From Pagès (2014):
  # Graphs like this are interpreted in a similar way as correlation circles: in
  # both cases the coordinate of a point is interpreted as a relationship measurement
  # with a maximum value of 1. But this new graph has two specificities: the
  # groups of variables are unstandardised and their coordinates are always positive.
  # They therefore appear in a square (with a side of 1 and with points [0,0]
  # and [1,1] as vertices) known as a relationship square. Pages (2014), p141
  
  # Therefore, when compared with an intuitive approach to interpretation
  # (for a PCA user), one highly practical advantage of this representation is its
  # relationship with the representations of individuals and variables already
  # provided: in MFA, data are considered from different points of view, but in
  # one single framework. Pages (2014), p141
  names(res.wgaceggenvt.MFA)
  res.wgaceggenvt.MFA.group=res.wgaceggenvt.MFA$group
  wgaceggenvt.gcoord=data.frame(res.wgaceggenvt.MFA.group$coord[,1:3])
  wgaceggenvt.gcontrib=res.wgaceggenvt.MFA.group$contrib[,1:3]
  wgaceggenvt.gcontrib=data.frame(wgaceggenvt.gcontrib)
  names(wgaceggenvt.gcontrib)=paste('contrib',names(wgaceggenvt.gcontrib),sep='.')
  wgaceggenvt.gcontrib$year=row.names(wgaceggenvt.gcontrib)
  wgaceggenvt.gcoord$year=row.names(wgaceggenvt.gcoord)
  wgaceggenvt.gcoord=merge(wgaceggenvt.gcoord,wgaceggenvt.gcontrib,by.x='year',by.y='year')
  row.names(wgaceggenvt.gcoord)=wgaceggenvt.gcoord$year
  wgaceggenvt.gcoord$contrib.Dim.1.2=wgaceggenvt.gcoord$contrib.Dim.1+wgaceggenvt.gcoord$contrib.Dim.2
  wgaceggenvt.gcoord$contrib.Dim.2.3=wgaceggenvt.gcoord$contrib.Dim.2+wgaceggenvt.gcoord$contrib.Dim.3
  head(wgaceggenvt.gcoord)  
  wgaceggenvt.gcoord$year=paste('20',wgaceggenvt.gcoord$year,sep='')
  range(wgaceggenvt.gcoord$Dim.1)
  range(wgaceggenvt.gcoord$Dim.2)
  range(wgaceggenvt.gcoord$Dim.3)
  
  # Plot of groups mean positions in MFA space:
  #******************************************
  p12 <- ggplot(wgaceggenvt.gcoord,aes(Dim.1,Dim.2,label=year))+geom_text(aes(colour = contrib.Dim.1.2))+
    scale_colour_continuous(name="Contribution (%)")
  p12
  
  plot(wgaceggenvt.gcoord$year,wgaceggenvt.gcoord$Dim.1,type='b',ylim=c(0,1.2),ylab='MFA',
         xlab='')
  lines(wgaceggenvt.gcoord$year,wgaceggenvt.gcoord$Dim.2,lty=2,type="b")
  legend('topleft',c('envtMFA1','envtMFA2'),lty=seq(2))
  
  # p23 <- ggplot(gcoord,aes(Dim.2,Dim.3,label=year))+geom_text(aes(colour = contrib.Dim.2.3))+
  #   scale_colour_continuous(name="Contribution (%)")
  # multiplot(p12,p23)

  # 6.2. Eventually, study years partial axes correlations with MFA axes --------
  #****************************************
  # We have already insisted on the need to connect the inertias from the MFA
  # and those in the separate PCAs (see Tables 4.4 and 4.5). It is also useful to
  # connect the factors of the MFA with those of the separate PCAs (also known
  # as partial axes), both to understand better the effects of weighting and to enrich
  # interpretation of the analyses. In order to do this, the latter are projected as
  # supplementary variables (see Figure 4.5). Pages 2014, p88
  
  # Partial axes correlations
  res.wgaceggenvt.MFA.partial.axes=res.wgaceggenvt.MFA$partial.axes
  wgaceggenvt.ddres.pyears=data.frame(res.wgaceggenvt.MFA.partial.axes$cor)
  head(wgaceggenvt.ddres.pyears)
  wgaceggenvt.ddres.pyears[row.names(wgaceggenvt.ddres.pyears)=='Dim1.14',]
  wgaceggenvt.ddres.pyears.12=data.frame(res.wgaceggenvt.MFA.partial.axes$cor[,1:2])
  wgaceggenvt.ddres.pyears.12s=wgaceggenvt.ddres.pyears.12[round(abs(wgaceggenvt.ddres.pyears.12$Dim.1))>=0.6|
                                     round(abs(wgaceggenvt.ddres.pyears.12$Dim.2))>=0.6,]
  wgaceggenvt.ddres.pyears.12s$Dim=factor(gsub('[.]','',substr(row.names(wgaceggenvt.ddres.pyears.12s),1,5)),
                              ordered=TRUE,levels=paste('Dim',seq(10),sep=''))
  wgaceggenvt.ddres.pyears.12s$Year=gsub('[.]','',substr(row.names(wgaceggenvt.ddres.pyears.12s),6,8))
  wgaceggenvt.ddres.pyears.12s.long=reshape(wgaceggenvt.ddres.pyears.12s,varying=list(1:2),
                                direction='long',timevar='MFA',v.names='correlation')
  wgaceggenvt.ddres.pyears.12s.long$MFA=paste('Dim',wgaceggenvt.ddres.pyears.12s.long$MFA,sep='')
  wgaceggenvt.ddres.pyears.12s.long$signif=abs(round(wgaceggenvt.ddres.pyears.12s.long$correlation))>=0.6
  wgaceggenvt.ddres.pyears.12s.long$MFA=paste('MFA',wgaceggenvt.ddres.pyears.12s.long$MFA,sep='.')
  head(wgaceggenvt.ddres.pyears.12s.long)  
  # Plot partial axes correlations with MFA axes
  p <- ggplot(wgaceggenvt.ddres.pyears.12s.long[wgaceggenvt.ddres.pyears.12s.long$Dim%in%c('Dim1'),], 
              aes(Dim,Year,size=abs(correlation),colour=correlation))+geom_point()
  p+ scale_colour_gradientn(colours = viridis(3)) + facet_grid(.~MFA)+
    theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(1)),axis.title.y = element_blank(),
          axis.title.x = element_blank()) + guides(size=FALSE)

  # 6.3. Maps of partial individuals (map cell:year pairs) coordinates in MFA space ---------
  #***********************************
  wgaceggenvt.MFAcoord=res.wgaceggenvt.MFA$ind$coord
  wgaceggenvt.pindCoord=res.wgaceggenvt.MFA$ind$coord.partiel
  names(res.wgaceggenvt.MFA$ind)
  wgaceggenvt.MFAyears=unlist(strsplit(row.names(wgaceggenvt.pindCoord),split='[.]'))[seq(2,(2*length(row.names(wgaceggenvt.pindCoord))),2)]
  lyears=unique(wgaceggenvt.MFAyears)
  pindCoord.fssdan=pindCoord.fssd-MFAcoord
  head(wgaceggenvt.pindCoord)
  
  # 6.3.1. Maps of partial individuals coordinates on MFA1,2,3, one plot per year ---------------- 
  #*****************************
  for (i in 1:length(lyears)){
    dfi=wgaceggenvt.pindCoord[wgaceggenvt.MFAyears==lyears[i],]-wgaceggenvt.MFAcoord
    dim(wgaceggenvt.MFAcoord)
    head(dfi)
    BoB.MFA.rasterPlot(MFAres=dfi,nMFA=1,cellsel.wgaceggenvt,pipo,path.export.envt=NULL,
                       plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                   MFA123contrib=FALSE,
                                   MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                   MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                       ptitle=paste('20',lyears[i],sep=''),lptheme = viridisTheme())
  }
  # 6.3.2. Maps of partial individuals coordinates anomalies, one mosaic plot per MFA1,2,3, with all years
  #---------------------
  res=BoB.MFA.rasterPlot(MFAres=res.wgaceggenvt.MFA,nMFA=2,cellsel.wgaceggenvt,pipo,path.export.envt=NULL,
                         plotit=list(MFA123coord=TRUE,MFA123contrib=FALSE,MFA123timevar=FALSE,
                                     MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                     MFA123coordMcontrib=FALSE,MFA123pind=TRUE),funSel='mean',
                         ptitle=paste(''),lptheme=BuRdTheme,anomalit=TRUE)

 
