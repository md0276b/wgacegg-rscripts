---
title: "Space time analysis of WGACEGG spring hydrological parameters and satellite maps"
author: "Mathieu Doray"
date: "16/11/2021"
output: ioslides_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
pref='/media/mathieu/IfremerMData/Campagnes/'
path.export.hydrosat=paste(pref,"WGACEGG/Results/MFAgrids/hydrosat/",sep='')
```

## Introduction

-   Space time analysis of AC_SPRING_IBBB hydro parameters and satellite data

## Data

-   Bug discovered in gridmap script

    -   Map cells positions erroneously shifted towards NW

    -   2003-2019 AC_SPRING_IBBB NASC maps remade

    -   2003-2019 AC_SPRING_IBBB hydro parameters gridmaps must be remade

-   L4 SST and SChl-a satellite images averaged during AC_SPRING_IBBB surveys added to AC_SPRING_IBBB hydro parameters

    -   But, Gulf of Cadiz area missing in satellite images in 2014 and 2015

## Methods

-   MFA analysis performed on hydro+satellite variables without years 2003, 2004, 2009, 2012, 2013, 2014, 2015

## Results: MFA factor plane

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.export.hydrosat,'WGACEGGadultHydroSatGridsMFA123biplot.png',sep=''))

```

## Contribution of variables to MFA axes

```{r, echo=FALSE, out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.export.hydrosat,'PELGAS_HydroSatMFA12corVar.png',sep=''))

```

## MFA maps

```{r, echo=FALSE, out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.export.hydrosat,'rasterLevelPlot_MFAcoordGeoSpace.png',sep=''))

```

## MFA time series

```{r, echo=FALSE, out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.export.hydrosat,'HydroSatMFA12timeSeries.png',sep=''))

```

## Conclusions

-   Next steps:

    -   make corrected AC_SPRING_IBBB surveys hydro gridmaps

    -   investigate missing cells in 2014 and 2015 satellite maps

    -   perform MFA on AC_SPRING_IBBB hydro parameters and satellite maps

    -   model AC_SPRING_IBBB NASC MFA results as a function of hydro+sat MFA results

    -   write abstract and article for WGSPF symposium, with interested WGACEGG members
