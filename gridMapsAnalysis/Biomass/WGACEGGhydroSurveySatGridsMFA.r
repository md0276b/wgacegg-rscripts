library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)

#***************************************************************************************************************************************
#                                               Multiple Factor Analysis with FactoMineR tutorial
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.5

# 1.1. Import survey grid map data and convert them into raster package objects --------
#********************************************************************

  # path to donnees2 drive
  #*********************************
  donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
  
  # path to grid files
  #*********************************
  prefix=paste(donnees2,'Campagnes/',sep='')
  prefix=paste('D:/Campagnes/',sep='')
  prefix=paste('G:/Campagnes/',sep='')
  pref='G:/'
  pref='E:/'
  prefix='G:/'
  prefix='E:/'
  prefix='C:/Users/mdoray/Documents/Campagnes/'
  path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
  path.grids2=paste(path.grids,"SST_SPRING_IBBB/",sep='')
  path.grids3=paste(path.grids,"SSS_SPRING_IBBB/",sep='')
  path.grids4=paste(path.grids,"SSTS_SPRING_IBBB/",sep='')
  path.grids5=paste(path.grids,"SAT_SPRING_IBBB/",sep='')
  dir.create(path.grids5,recursive = TRUE)
  path.export.envt=paste(prefix,"WGACEGG/Results/MFAgrids/SSTS/",sep='')
  
  # List grid files 
  #*********************************
  # SST
  lffsst=list.files(path.grids2,pattern='*.txt')
  #lfs=strsplit(lf,split='[.]',)
  lffssts=data.frame(t(data.frame(strsplit(lffsst,split='[.]'))))
  lp=as.character(lffssts[,1])
  lffssts2=data.frame(t(data.frame(strsplit(lp,split='_'))))
  row.names(lffssts2)=seq(length(lffssts2[,1]))
  lffssts=cbind(lffssts,lffssts2)
  head(lffssts)
  names(lffssts)=c('pat','ext','year','survey','time','area','unknw','sp','filter','quantile')
  lffssts$path=lffsst
  row.names(lffssts)=seq(length(lffssts[,1]))
  table(lffssts$year)
  
  # SSS
  lffsss=list.files(path.grids3,pattern='*.txt')
  #lfs=strsplit(lf,split='[.]',)
  lffssss=data.frame(t(data.frame(strsplit(lffsss,split='[.]'))))
  lp=as.character(lffssss[,1])
  lffssss2=data.frame(t(data.frame(strsplit(lp,split='_'))))
  row.names(lffssss2)=seq(length(lffssss2[,1]))
  lffssss=cbind(lffssss,lffssss2)
  head(lffssss)
  names(lffssss)=c('pat','ext','year','survey','time','area','unknw','sp','filter','quantile')
  lffssss$path=lffsss
  row.names(lffssss)=seq(length(lffssss[,1]))
  table(lffssss$year)
  
  lffssse=rbind(lffssss,lffssts)

  # Check 2013  
  lffsssei=lffssse[lffssse$year==2013,]
  pat=paste(path.grids4,lffsssei$path[1],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  head(mati)
  unique(mati$Year)
  grid.plot(mati,input='gridDataframe',bathy.plot = FALSE,
            ux11=TRUE)
  
  lffssses=lffssse[lffssse$year!=2013,]
  
  # Import grid and raster files, bind grid maps
  #***************************************************
  for (i in 1:dim(lffssse)[1]){
    pat=paste(path.grids4,lffssse$path[i],sep='')
    mati=read.table(file=pat,header=T,sep=";")
    mati$sp=lffssse$sp[i]
    mati$Survey='AC_SPRING_IBBB'
    dim(mati)
    print(unique(mati$Year))
    mati=unique(mati)
    head(mati)
    if (i==1){
      SSTS=mati
    }else{
      SSTS=rbind(SSTS,mati)
    }
  }
  # 2013 file comprises all years between 2003 and 2012, but no 2013
  
  dim(SSTS)
  SSTS=unique(SSTS)
  dim(SSTS)
  unique(SSTS$Year)
  table(SSTS$Year)
  SSTS$cell=paste(SSTS$I,SSTS$J,sep='-')
  
  SSTSp=SSTS[SSTS$Zvalue>0&!is.na(SSTS$Zvalue),]
  table(SSTSp$sp,SSTSp$Year)

  head(SSTS)

  # lsp=unique(SSTS$sp)
  # for (i in 1:length(lsp)){
  #   mati=SSTS[SSTS$sp==lsp[i],]
  #   graphics.off()
  #   resras=grid2rasterStack(pat=mati,path1=path.grids4,
  #                           varid=lsp[i],anomaly=TRUE)
  #   assign(paste(lsp[i],'rasterStack',sep='.'),resras)
  #   graphics.off()
  #   # Import raster stacks format, compute mean and SD maps and plot everything:
  #   #pat2=paste(paste(path.grids1,'Rasters/',sep=''),lffsstrss$path[i],sep='')
  #   #rasti=raster(pat2)
  # }

  # 1.2. Import satellite raster maps -----------
  #***********************
  pref='/run/user/1000/gvfs/sftp:host=echosonde,user=mdoray/export/home1/partages/data/'
  pref='G:/'
  
  path.rasterSatChla=paste(
    pref,'Satellite/Data/Atlantic/Chla/raster/',sep='')
  lrc=list.files(path.rasterSatChla,pattern='*.grd')
  lrc.AC_SPRING_IBBB=lrc[grep('AC_SPRING_IBBB',lrc)]
  
  mchla.rb.AC_SPRING_IBBB=brick(
    paste(path.rasterSatChla,lrc.AC_SPRING_IBBB[1],sep=''))
  plot(mchla.rb.AC_SPRING_IBBB)
  mchla.rb.AC_SPRING_IBBB.df=as.data.frame(
    mchla.rb.AC_SPRING_IBBB, xy=TRUE,long=TRUE)
  head(mchla.rb.AC_SPRING_IBBB.df)
  names(mchla.rb.AC_SPRING_IBBB.df)=c('Xgd','Ygd','layer','Zvalue')
  mchla.rb.AC_SPRING_IBBB.df$Year=substr(
    mchla.rb.AC_SPRING_IBBB.df$layer,15,19)
  mchla.rb.AC_SPRING_IBBB.df$sp='mSChla'
  mchla.rb.AC_SPRING_IBBB.df$var='mSChla'
  mchla.rb.AC_SPRING_IBBB.df$component='Sat'
  mchla.rb.AC_SPRING_IBBB.df$Nsample=NA
  mchla.rb.AC_SPRING_IBBB.df$Zstdev=NA
  # convert raster coordinates to gridmap coordinates (temporary)
  sort(unique(mchla.rb.AC_SPRING_IBBB.df$Xgd))
  sort(unique(mchla.rb.AC_SPRING_IBBB.df$Ygd))
  
  #unique(mchla.rb.AC_SPRING_IBBB.df$Xgd)
  #mchla.rb.AC_SPRING_IBBB.df$Xgd=mchla.rb.AC_SPRING_IBBB.df$Xgd+0.05
  #mchla.rb.AC_SPRING_IBBB.df$Ygd=mchla.rb.AC_SPRING_IBBB.df$Ygd+0.05
  
  sdchla.rb.AC_SPRING_IBBB=brick(
    paste(path.rasterSatChla,lrc.AC_SPRING_IBBB[3],sep=''))
  sdchla.rb.AC_SPRING_IBBB.df=as.data.frame(
    sdchla.rb.AC_SPRING_IBBB, xy=TRUE,long=TRUE)
  head(sdchla.rb.AC_SPRING_IBBB.df)
  names(sdchla.rb.AC_SPRING_IBBB.df)=c('Xgd','Ygd','layer','Zvalue')
  sdchla.rb.AC_SPRING_IBBB.df$Year=substr(
    sdchla.rb.AC_SPRING_IBBB.df$layer,15,19)
  sdchla.rb.AC_SPRING_IBBB.df$sp='sdSChla'
  sdchla.rb.AC_SPRING_IBBB.df$var='sdSChla'
  sdchla.rb.AC_SPRING_IBBB.df$component='Sat'
  sdchla.rb.AC_SPRING_IBBB.df$Nsample=NA
  sdchla.rb.AC_SPRING_IBBB.df$Zstdev=NA
  # convert raster coordinates to gridmap coordinates (add res/2)
  #sdchla.rb.AC_SPRING_IBBB.df$Xgd=sdchla.rb.AC_SPRING_IBBB.df$Xgd+0.05
  #sdchla.rb.AC_SPRING_IBBB.df$Ygd=sdchla.rb.AC_SPRING_IBBB.df$Ygd+0.05
  
  q90chla.rb.AC_SPRING_IBBB=brick(
    paste(path.rasterSatChla,lrc.AC_SPRING_IBBB[2],sep=''))
  q90chla.rb.AC_SPRING_IBBB.df=as.data.frame(
    q90chla.rb.AC_SPRING_IBBB, xy=TRUE,long=TRUE)
  head(q90chla.rb.AC_SPRING_IBBB.df)
  names(q90chla.rb.AC_SPRING_IBBB.df)=c('Xgd','Ygd','layer','Zvalue')
  q90chla.rb.AC_SPRING_IBBB.df$Year=substr(
    q90chla.rb.AC_SPRING_IBBB.df$layer,15,19)
  q90chla.rb.AC_SPRING_IBBB.df$sp='q90SChla'
  q90chla.rb.AC_SPRING_IBBB.df$var='q90SChla'
  q90chla.rb.AC_SPRING_IBBB.df$component='Sat'
  q90chla.rb.AC_SPRING_IBBB.df$Nsample=NA
  q90chla.rb.AC_SPRING_IBBB.df$Zstdev=NA
  # convert raster coordinates to gridmap coordinates (add res/2)
  #q90chla.rb.AC_SPRING_IBBB.df$Xgd=q90chla.rb.AC_SPRING_IBBB.df$Xgd+0.05
  #q90chla.rb.AC_SPRING_IBBB.df$Ygd=q90chla.rb.AC_SPRING_IBBB.df$Ygd+0.05
  
  names(mchla.rb.AC_SPRING_IBBB.df)
  names(sdchla.rb.AC_SPRING_IBBB.df)
  names(q90chla.rb.AC_SPRING_IBBB.df)
  chla.rb.AC_SPRING_IBBB.df=rbind(
    mchla.rb.AC_SPRING_IBBB.df,sdchla.rb.AC_SPRING_IBBB.df,
                          q90chla.rb.AC_SPRING_IBBB.df)
  
  path.rasterSatSST=paste(
    pref,'Satellite/Data/Atlantic/SST/raster/',sep='')
  lrc=list.files(path.rasterSatSST,pattern='*.grd')
  lrc.AC_SPRING_IBBB=lrc[grep('AC_SPRING_IBBB',lrc)]
  mSST.rb.AC_SPRING_IBBB=brick(
    paste(path.rasterSatSST,lrc.AC_SPRING_IBBB[1],sep=''))
  mSST.rb.AC_SPRING_IBBB.df=as.data.frame(
    mSST.rb.AC_SPRING_IBBB, xy=TRUE,long=TRUE)
  head(mSST.rb.AC_SPRING_IBBB.df)
  names(mSST.rb.AC_SPRING_IBBB.df)=c('Xgd','Ygd','layer','Zvalue')
  mSST.rb.AC_SPRING_IBBB.df$Year=substr(
    mSST.rb.AC_SPRING_IBBB.df$layer,15,19)
  mSST.rb.AC_SPRING_IBBB.df$sp='mSST'
  mSST.rb.AC_SPRING_IBBB.df$var='mSST'
  mSST.rb.AC_SPRING_IBBB.df$component='Sat'
  mSST.rb.AC_SPRING_IBBB.df$Nsample=NA
  mSST.rb.AC_SPRING_IBBB.df$Zstdev=NA
  # convert raster coordinates to gridmap coordinates (temporary)
  #mSST.rb.AC_SPRING_IBBB.df$Xgd=mSST.rb.AC_SPRING_IBBB.df$Xgd+0.05
  #mSST.rb.AC_SPRING_IBBB.df$Ygd=mSST.rb.AC_SPRING_IBBB.df$Ygd+0.05
  
  sdSST.rb.AC_SPRING_IBBB=brick(
    paste(path.rasterSatSST,lrc.AC_SPRING_IBBB[2],sep=''))
  sdSST.rb.AC_SPRING_IBBB.df=as.data.frame(
    sdSST.rb.AC_SPRING_IBBB, xy=TRUE,long=TRUE)
  head(sdSST.rb.AC_SPRING_IBBB.df)
  names(sdSST.rb.AC_SPRING_IBBB.df)=c('Xgd','Ygd','layer','Zvalue')
  sdSST.rb.AC_SPRING_IBBB.df$Year=substr(
    sdSST.rb.AC_SPRING_IBBB.df$layer,15,19)
  sdSST.rb.AC_SPRING_IBBB.df$sp='sdSST'
  sdSST.rb.AC_SPRING_IBBB.df$var='sdSST'
  sdSST.rb.AC_SPRING_IBBB.df$component='Sat'
  sdSST.rb.AC_SPRING_IBBB.df$Nsample=NA
  sdSST.rb.AC_SPRING_IBBB.df$Zstdev=NA
  # convert raster coordinates to gridmap coordinates (add res/2)
  #sdSST.rb.AC_SPRING_IBBB.df$Xgd=sdSST.rb.AC_SPRING_IBBB.df$Xgd+0.05
  #sdSST.rb.AC_SPRING_IBBB.df$Ygd=sdSST.rb.AC_SPRING_IBBB.df$Ygd+0.05
  
  # bind maps
  SST.rb.AC_SPRING_IBBB.df=rbind(
    mSST.rb.AC_SPRING_IBBB.df,sdSST.rb.AC_SPRING_IBBB.df)
  head(SST.rb.AC_SPRING_IBBB.df)
  SSTchla.rb.AC_SPRING_IBBB.df=rbind(
    chla.rb.AC_SPRING_IBBB.df,SST.rb.AC_SPRING_IBBB.df)
  SSTchla.rb.AC_SPRING_IBBB.df=SSTchla.rb.AC_SPRING_IBBB.df[
    ,names(SSTchla.rb.AC_SPRING_IBBB.df)!='layer']
  dim(SSTchla.rb.AC_SPRING_IBBB.df)
  head(SSTchla.rb.AC_SPRING_IBBB.df)
  unique(SSTchla.rb.AC_SPRING_IBBB.df$var)
  
  # Plot mosaic maps -----
  dfiS=SSTchla.rb.AC_SPRING_IBBB.df[
    SSTchla.rb.AC_SPRING_IBBB.df$sp=='mSChla',]
  mat=grid.plot(
    pat=dfiS,input='gridDataframe',ux11=TRUE,pat2=NULL,
    plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=FALSE,
                mosaic=TRUE),
    namz=paste(unique(dfiS$sp)))
  graphics.off()
  
  # export sat data in grid format -----
  write.table(SSTchla.rb.AC_SPRING_IBBB.df,
              paste(path.grids5,
                    'SSTchla_AC_SPRING_IBBB_gridmaps.csv',sep=''),
              row.names = FALSE,sep=';')
  
  -9.825
  head(SSTS)
  sort(unique(SSTS$Xgd))
  sort(unique(SSTchla.rb.AC_SPRING_IBBB.df$Xgd))
  sort(unique(SSTS$Ygd))
  sort(unique(SSTchla.rb.AC_SPRING_IBBB.df$Ygd))
  # Convert survey coordinates
  SSTS$Xgd=SSTS$Xgd-0.05
  SSTS$Ygd=SSTS$Ygd-0.05

  dim(SSTchla.rb.AC_SPRING_IBBB.df)
  SSTchla.rb.AC_SPRING_IBBB.df=merge(
    SSTchla.rb.AC_SPRING_IBBB.df,
    unique(SSTS[,c('I','J','cell','Xgd','Ygd')]),
    by.x=c('Xgd','Ygd'),by.y=c('Xgd','Ygd'))
  dim(SSTchla.rb.AC_SPRING_IBBB.df)
  head(SSTchla.rb.AC_SPRING_IBBB.df)
  SSTchla.rb.AC_SPRING_IBBB.df$Survey='sat'
  table(SSTchla.rb.AC_SPRING_IBBB.df$Year)
  
  # 1.3. Add sat data to AC_SPRING_IBBB hydro parameters ---------
  head(SSTS)
  sort(names(SSTS))
  sort(names(SSTchla.rb.AC_SPRING_IBBB.df))
  dim(SSTS)
  lxgm=sort(unique(SSTS$Xgd))
  sort(unique(SSTchla.rb.AC_SPRING_IBBB.df$Xgd))-
    sort(unique(SSTS$Xgd))
  
  cnames=names(SSTS)[names(SSTS)%in%names(SSTchla.rb.AC_SPRING_IBBB.df)]
  SSTS.sat=rbind(SSTS[,cnames],
                 SSTchla.rb.AC_SPRING_IBBB.df[,cnames])
  dim(SSTS.sat)
  head(SSTS.sat)
  
# 2. Prepare datasets for MFA -------------------
  #********************************************************************
  # 2.1. Select common species ---------------
  #***************************
  head(SSTS.sat)
  unique(SSTS.sat$Year)
  SSTS.sat$spi=substr(SSTS.sat$sp,1,8)
  SSTS.satp=SSTS.sat[SSTS.sat$Zvalue>0&!is.na(SSTS.sat$Zvalue),]
  table(SSTS.satp$spi,SSTS.satp$Year)
  OccurenceFrequencySp=apply(
    table(SSTS.satp$spi,SSTS.satp$Year)>0,1,sum)/
    length(unique(SSTS.satp$Year))
  sort(OccurenceFrequencySp)

  # 2.2. Select years ---------------
  #***************************
  SSTS.sats=SSTS.sat[!SSTS.sat$Year%in%c(2003,2004,2009,2012,
                                         2013,2014),]
  unique(SSTS.sats$Year)
  head(SSTS.sats)
  
  # 2.3. Reshape data frame for analysis with year as grouping variable -------------
  #***************************
  a.AC_SPRING_IBBB.hydrosat <- reshape(
    SSTS.sats[,c("I","J","Zvalue","sp","Year")],timevar = "sp",
    idvar = c("I","J","Year"), direction = "wide")
  head(a.AC_SPRING_IBBB.hydrosat)
  names(a.AC_SPRING_IBBB.hydrosat)
  a.AC_SPRING_IBBB.hydrosat=a.AC_SPRING_IBBB.hydrosat[
    order(a.AC_SPRING_IBBB.hydrosat$Year),]
  
  big.AC_SPRING_IBBB.hydrosat <- reshape(
    a.AC_SPRING_IBBB.hydrosat, timevar = "Year", idvar = c("I","J"),
    direction = "wide")
  head(big.AC_SPRING_IBBB.hydrosat)
  names(big.AC_SPRING_IBBB.hydrosat)
  dim(big.AC_SPRING_IBBB.hydrosat)
  
  # 2.4. Select cells in survey area -------------
  #***************************
  pat=paste(path.grids4,lffssse$path[4],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  pipo=mati[mati$Year==2015,]
  plot(pipo$Xgd,pipo$Ygd)
  plot(pipo$Xgd,pipo$Ygd,cex=0.1+pipo$Zvalue/100)
  # Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
  #******************************************
  # Define grid limits
  #x1=-10.2;x2=-1;y1=35.8;y2=49
  x1=-10;x2=-1;y1=36;y2=49
  # Define cell dimensions
  ax=0.25;ay=0.25
  # Define smoothing radius
  u=2
  # Define iteration number
  ni=200
  define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni)
  xpol.SSTS.sats=xpol-0.25
  ypol.SSTS.sats=ypol-0.2
  poly=data.frame(x=xpol.SSTS.sats,y=ypol.SSTS.sats)
  lines(poly)
  coast()
  head(pipo)
  library(splancs) 
  pts=splancs::as.points(pipo[,c('Xgd','Ygd')])
  testInPoly=inout(pts,poly)
  plot(pts)
  points(pts[testInPoly,],pch=16)
  coast()
  lines(poly)
  cellsel.AC_SPRING_IBBB.hydrosat=data.frame(
    x=pts[,1],y=pts[,2],sel=testInPoly,I=pipo$I,J=pipo$J)
  cellsel.AC_SPRING_IBBB.hydrosat$cell=paste(
    cellsel.AC_SPRING_IBBB.hydrosat$I,cellsel.AC_SPRING_IBBB.hydrosat$J,sep='-')
  head(cellsel.AC_SPRING_IBBB.hydrosat)
  plot(cellsel.AC_SPRING_IBBB.hydrosat$x,
       cellsel.AC_SPRING_IBBB.hydrosat$y)
  points(cellsel.AC_SPRING_IBBB.hydrosat[
    cellsel.AC_SPRING_IBBB.hydrosat$sel,'x'],
    cellsel.AC_SPRING_IBBB.hydrosat[
      cellsel.AC_SPRING_IBBB.hydrosat$sel,'y'],pch=16)
  coast()
  legend('topleft',legend=c('Cells centers','Selected cells'),pch=c(1,16))

  # 2.5. Prepare and standardise dataset -----------
  #**************************
  head(big.AC_SPRING_IBBB.hydrosat)
  big.AC_SPRING_IBBB.hydrosat.MFA=big.AC_SPRING_IBBB.hydrosat[,-seq(2)]
  row.names(big.AC_SPRING_IBBB.hydrosat.MFA)=paste(
    big.AC_SPRING_IBBB.hydrosat$I,big.AC_SPRING_IBBB.hydrosat$J,sep='-')
  # simplify column names
  names(big.AC_SPRING_IBBB.hydrosat.MFA)=gsub(
    'Zvalue.','',names(big.AC_SPRING_IBBB.hydrosat.MFA))
  names(big.AC_SPRING_IBBB.hydrosat.MFA)
  head(big.AC_SPRING_IBBB.hydrosat.MFA)
  
  head(cellsel.AC_SPRING_IBBB.hydrosat)
  
  dim(big.AC_SPRING_IBBB.hydrosat.MFA)
  big.AC_SPRING_IBBB.hydrosat.MFAs1=big.AC_SPRING_IBBB.hydrosat.MFA[
    row.names(big.AC_SPRING_IBBB.hydrosat.MFA)%in%
      cellsel.AC_SPRING_IBBB.hydrosat[
        cellsel.AC_SPRING_IBBB.hydrosat$sel,'cell'],]
  dim(big.AC_SPRING_IBBB.hydrosat.MFAs1)
  
  # 2.6. Select cells in survey area before MFA ------
  #***************************************
  dim(big.AC_SPRING_IBBB.hydrosat.MFA)
  big.AC_SPRING_IBBB.hydrosat.MFA=big.AC_SPRING_IBBB.hydrosat.MFA[
    order(dimnames(big.AC_SPRING_IBBB.hydrosat.MFA)[[1]]),]
  head(big.AC_SPRING_IBBB.hydrosat.MFA)

  #cselNoNA=apply(big.AC_SPRING_IBBB.hydrosat.MFA,1,complete.cases)
  # 2.6.1. Select cells with no NA over the series -----
  #****************************************
  # # NA detector
  NAdetect=function(x){
    if (sum(is.na(x))>0){
      y=FALSE
    }else{
      y=TRUE
    }
    y
  }
  cselNoNA=apply(big.AC_SPRING_IBBB.hydrosat.MFA,1,NAdetect)
  table(cselNoNA)
  big.AC_SPRING_IBBB.hydrosat.MFAs=big.AC_SPRING_IBBB.hydrosat.MFA[
    cselNoNA,]
  head(big.AC_SPRING_IBBB.hydrosat.MFAs)
  lcells=row.names(big.AC_SPRING_IBBB.hydrosat.MFAs)
  cellsel.AC_SPRING_IBBB.hydrosat=cellsel.AC_SPRING_IBBB.hydrosat[
    order(cellsel.AC_SPRING_IBBB.hydrosat$cell),]
  plot(cellsel.AC_SPRING_IBBB.hydrosat$x,
       cellsel.AC_SPRING_IBBB.hydrosat$y)
  points(cellsel.AC_SPRING_IBBB.hydrosat[
    cellsel.AC_SPRING_IBBB.hydrosat$cell%in%lcells,'x'],
         cellsel.AC_SPRING_IBBB.hydrosat[
           cellsel.AC_SPRING_IBBB.hydrosat$cell%in%lcells,'y'],pch=16)
  coast()
  cellsel.AC_SPRING_IBBB.hydrosat$selNoNA=cselNoNA
  
  cselNoNA.df=data.frame(cselNoNA)
  cselNoNA.df=merge(cselNoNA.df,data.frame(cselNoNAf),
                    by.x='row.names',by.y='row.names',all.x=TRUE)
  
  # # 2.6.2. convert NA's to zero ---------
  NA2null=function(x,procInf=FALSE){
    x[is.na(x)]=0
    if (procInf){
      x[is.infinite(x)]=0
    }
    x
  }
  big.AC_SPRING_IBBB.hydrosat.MFAs=apply(big.AC_SPRING_IBBB.hydrosat.MFAs,2,NA2null)
  head(big.AC_SPRING_IBBB.hydrosat.MFAs)
  rnames=dimnames(big.AC_SPRING_IBBB.hydrosat.MFAs)[[1]]
  # log variables
  big.AC_SPRING_IBBB.hydrosat.MFAs=apply(big.AC_SPRING_IBBB.hydrosat.MFAs,2,function(x){x=log(x+1);x})
  head(big.AC_SPRING_IBBB.hydrosat.MFAs)
  dim(big.AC_SPRING_IBBB.hydrosat.MFAs)
  class(big.AC_SPRING_IBBB.hydrosat.MFAs)
  dimnames(big.AC_SPRING_IBBB.hydrosat.MFAs)[[1]]=rnames
  # center and scale variables
  big.AC_SPRING_IBBB.hydrosat.MFAs=apply(big.AC_SPRING_IBBB.hydrosat.MFAs,2,scale,center=TRUE,scale=FALSE)
  head(big.AC_SPRING_IBBB.hydrosat.MFAs)
  class(big.AC_SPRING_IBBB.hydrosat.MFAs)
  dimnames(big.AC_SPRING_IBBB.hydrosat.MFAs)[[1]]=rnames
    
  # 2.7. Remove constant columns-----------  
  #***************************************
  # with zeroes
  scols=apply(big.AC_SPRING_IBBB.hydrosat.MFAs,2,sum,na.rm=TRUE)
  srows=apply(big.AC_SPRING_IBBB.hydrosat.MFAs,1,sum,na.rm=TRUE)
  big.AC_SPRING_IBBB.hydrosat.MFAs2=big.AC_SPRING_IBBB.hydrosat.MFAs[,scols!=0]
  sum(scols==0)
  scols2=apply(big.AC_SPRING_IBBB.hydrosat.MFAs2,2,sum,na.rm=TRUE)
  summary(scols2)
  # other constant columns
  vcols=apply(big.AC_SPRING_IBBB.hydrosat.MFAs2,2,var,na.rm=TRUE)
  ccols=dimnames(big.AC_SPRING_IBBB.hydrosat.MFAs2)[[2]][vcols==0]
  big.AC_SPRING_IBBB.hydrosat.MFAs2=big.AC_SPRING_IBBB.hydrosat.MFAs2[,!dimnames(big.AC_SPRING_IBBB.hydrosat.MFAs2)[[2]]%in%ccols]
  dim(big.AC_SPRING_IBBB.hydrosat.MFAs2)
  
  # 2.8. Generate no. of data in each group -----------
  #***************************************
  cwz.AC_SPRING_IBBB.hydrosat=dimnames(big.AC_SPRING_IBBB.hydrosat.MFAs2)[[2]]
  nct.AC_SPRING_IBBB.hydrosat=unlist(strsplit(cwz.AC_SPRING_IBBB.hydrosat,split='[.]'))[seq(1,2*length(cwz.AC_SPRING_IBBB.hydrosat),2)]
  nyears.AC_SPRING_IBBB.hydrosat=unlist(strsplit(cwz.AC_SPRING_IBBB.hydrosat,split='[.]'))[seq(2,2*length(cwz.AC_SPRING_IBBB.hydrosat),2)]
  lnyears.AC_SPRING_IBBB.hydrosat=sort(unique(nyears.AC_SPRING_IBBB.hydrosat))
  tnnc.AC_SPRING_IBBB.hydrosat=table(nct.AC_SPRING_IBBB.hydrosat,nyears.AC_SPRING_IBBB.hydrosat)
  tnnc.AC_SPRING_IBBB.hydrosat.df=data.frame(table(nct.AC_SPRING_IBBB.hydrosat,nyears.AC_SPRING_IBBB.hydrosat))
  stnnc.AC_SPRING_IBBB.hydrosat=apply(tnnc.AC_SPRING_IBBB.hydrosat,2,sum)
  head(tnnc.AC_SPRING_IBBB.hydrosat.df)
  tnnc.AC_SPRING_IBBB.hydrosat.dfa=aggregate(tnnc.AC_SPRING_IBBB.hydrosat.df$Freq,list(nct=tnnc.AC_SPRING_IBBB.hydrosat.df$nct),
                                 sum)
  summary(tnnc.AC_SPRING_IBBB.hydrosat.dfa$x)

# 3. Run MFA ----------------
#*************************************************************
  # 3.1. MFA ---------------
  #***********************************
  dim(big.AC_SPRING_IBBB.hydrosat.MFAs2)
  res.AC_SPRING_IBBB.hydrosat.MFA<-MFA(big.AC_SPRING_IBBB.hydrosat.MFAs2, group=stnnc.AC_SPRING_IBBB.hydrosat, 
                           type=rep("s",length(stnnc.AC_SPRING_IBBB.hydrosat)), 
                    ncp=dim(big.AC_SPRING_IBBB.hydrosat.MFAs2)[2], 
                    name.group=substr(lnyears.AC_SPRING_IBBB.hydrosat,3,4), 
                    num.group.sup=NULL, graph=TRUE)
  names(res.AC_SPRING_IBBB.hydrosat.MFA)
  
  # 3.2. Explained variance ----------
  #***********************************  
  summary(res.AC_SPRING_IBBB.hydrosat.MFA)  
  par(bg='white')
  barplot(res.AC_SPRING_IBBB.hydrosat.MFA$eig[,2],main="% of variance explained",names.arg=1:nrow(res.AC_SPRING_IBBB.hydrosat.MFA$eig))
  barplot(res.AC_SPRING_IBBB.hydrosat.MFA$eig[,3],main="Cumulative % of variance explained",names.arg=1:nrow(res.AC_SPRING_IBBB.hydrosat.MFA$eig))
  res.AC_SPRING_IBBB.hydrosat.MFA$eig[round(res.AC_SPRING_IBBB.hydrosat.MFA$eig[,3])==95,]
  dim(big.AC_SPRING_IBBB.hydrosat.MFAs2)
 
  # 3.3. Plots of individuals in MFA1-2-3 planes -----------
  #***********************************
  # Plot of individuals in MFA1-2 plane 
  p1 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "contrib") #with contribution in color scale
  p2 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "cos2") #with quality of representation in color scale
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA2-3 plane
  p1 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "contrib",axes = c(2, 3))
  p2 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "cos2",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-3 plane
  p1 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "contrib",axes = c(1, 3))
  p2 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "cos2",axes = c(1, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-2-3 planes, with contribution in color scale
  x11()
  p1 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "contrib")
  p2 = fviz_mfa_ind(res.AC_SPRING_IBBB.hydrosat.MFA, col.ind = "contrib",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  dev.print(device=png,
            filename=paste(path.export.envt,'WGACEGGadultEnvtGridsMFA123biplot.png',sep=''),
            units='cm',width=29,height=21,res=300)
  
# 4. Contributions of variables to MFA axes -------------
#******************************************************************
  par(bg='grey50')
  plot.MFA(res.AC_SPRING_IBBB.hydrosat.MFA, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, 
           habillage="group", select = "coord 10")
  par(bg='white')
  
  # 4.1. select variables with above average contribution to MFA axes ----------------
  #************************************
  var.contrib=res.AC_SPRING_IBBB.hydrosat.MFA$quanti.var$contrib
  head(var.contrib)
  summary(var.contrib[,1])
  var.contribs1=var.contrib[var.contrib[,1]>mean(var.contrib[,1]),1]
  var.contribs2=var.contrib[var.contrib[,2]>mean(var.contrib[,2]),2]
  var.contribs3=var.contrib[var.contrib[,3]>mean(var.contrib[,3]),3]
  
  # 4.2. Variables correlation with MFA axes ---------------
  #************************************
  var.cor=res.AC_SPRING_IBBB.hydrosat.MFA$quanti.var$contrib
  graphics.off()
  ddres1.AC_SPRING_IBBB.hydrosat=dimdesc(res.AC_SPRING_IBBB.hydrosat.MFA, axes = 1:4, proba = 0.05)
  names(ddres1.AC_SPRING_IBBB.hydrosat)
  
  # 4.2.1. Variables correlation with MFA axis1 ----------
  #************************************
  ddres1.AC_SPRING_IBBB.hydrosat.1=ddres1.AC_SPRING_IBBB.hydrosat$Dim.1$quanti
  ddres1.AC_SPRING_IBBB.hydrosat.1=as.data.frame(ddres1.AC_SPRING_IBBB.hydrosat.1[complete.cases(ddres1.AC_SPRING_IBBB.hydrosat.1),])
  ddres1.AC_SPRING_IBBB.hydrosat.1cor=ddres1.AC_SPRING_IBBB.hydrosat.1[abs(ddres1.AC_SPRING_IBBB.hydrosat.1$correlation)>=0.5,]
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$ct=unlist(strsplit(row.names(ddres1.AC_SPRING_IBBB.hydrosat.1cor),split='[.]'))[seq(1,2*dim(ddres1.AC_SPRING_IBBB.hydrosat.1cor)[1],2)]
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$year=unlist(strsplit(row.names(ddres1.AC_SPRING_IBBB.hydrosat.1cor),split='[.]'))[seq(2,2*dim(ddres1.AC_SPRING_IBBB.hydrosat.1cor)[1],2)]
  ddres1.AC_SPRING_IBBB.hydrosat.1cor=ddres1.AC_SPRING_IBBB.hydrosat.1cor[order(ddres1.AC_SPRING_IBBB.hydrosat.1cor$ct),]
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$spid=row.names(ddres1.AC_SPRING_IBBB.hydrosat.1cor)
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$sp=substr(as.character(ddres1.AC_SPRING_IBBB.hydrosat.1cor$spid),1,3)
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.AC_SPRING_IBBB.hydrosat.1cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$cs=gsub('[.]','',substr(as.character(ddres1.AC_SPRING_IBBB.hydrosat.1cor$spid),17,24))
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$cs=gsub(' ','',ddres1.AC_SPRING_IBBB.hydrosat.1cor$cs)
  ddres1.AC_SPRING_IBBB.hydrosat.1cor$aCor=abs(ddres1.AC_SPRING_IBBB.hydrosat.1cor$correlation)
  unique(ddres1.AC_SPRING_IBBB.hydrosat.1cor$cs)
  #ddres1.AC_SPRING_IBBB.hydrosat.1cor$signif=abs(ddres1.AC_SPRING_IBBB.hydrosat.1cor$correlation)>=0.5
  head(ddres1.AC_SPRING_IBBB.hydrosat.1cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.AC_SPRING_IBBB.hydrosat.1cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  p <- ggplot(ddres1.AC_SPRING_IBBB.hydrosat.1cor, aes(year,sp,size=abs(correlation),colour=correlation),
              shape=signif)+geom_point()+ggtitle("cor(variables,MFA1)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))
  p
  
  # 4.2.2. Variables correlation with MFA axis2 ------------
  #************************************
  ddres1.AC_SPRING_IBBB.hydrosat.2=ddres1.AC_SPRING_IBBB.hydrosat$Dim.2$quanti
  ddres1.AC_SPRING_IBBB.hydrosat.2=as.data.frame(ddres1.AC_SPRING_IBBB.hydrosat.2[complete.cases(ddres1.AC_SPRING_IBBB.hydrosat.2),])
  ddres1.AC_SPRING_IBBB.hydrosat.2cor=ddres1.AC_SPRING_IBBB.hydrosat.2[abs(ddres1.AC_SPRING_IBBB.hydrosat.2[,1])>=0.5,]
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$ct=unlist(strsplit(row.names(ddres1.AC_SPRING_IBBB.hydrosat.2cor),split='[.]'))[seq(1,2*dim(ddres1.AC_SPRING_IBBB.hydrosat.2cor)[1],2)]
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$year=unlist(strsplit(row.names(ddres1.AC_SPRING_IBBB.hydrosat.2cor),split='[.]'))[seq(2,2*dim(ddres1.AC_SPRING_IBBB.hydrosat.2cor)[1],2)]
  ddres1.AC_SPRING_IBBB.hydrosat.2cor=ddres1.AC_SPRING_IBBB.hydrosat.2cor[order(ddres1.AC_SPRING_IBBB.hydrosat.2cor$correlation),]
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$spid=row.names(ddres1.AC_SPRING_IBBB.hydrosat.2cor)
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$sp=substr(as.character(ddres1.AC_SPRING_IBBB.hydrosat.2cor$spid),1,3)
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.AC_SPRING_IBBB.hydrosat.2cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$cs=gsub('[.]','',substr(as.character(ddres1.AC_SPRING_IBBB.hydrosat.2cor$spid),17,24))
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$cs=gsub(' ','',ddres1.AC_SPRING_IBBB.hydrosat.2cor$cs)
  unique(ddres1.AC_SPRING_IBBB.hydrosat.2cor$cs)
  ddres1.AC_SPRING_IBBB.hydrosat.2cor$aCor=abs(ddres1.AC_SPRING_IBBB.hydrosat.2cor$correlation)
  head(ddres1.AC_SPRING_IBBB.hydrosat.2cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.AC_SPRING_IBBB.hydrosat.2cor)%in%names(var.contribs2))
  # Plot variables well correlated with MFA axis2 (abs(cor)>0.5)
  p <- ggplot(ddres1.AC_SPRING_IBBB.hydrosat.2cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA2)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p
  
  ddres1.AC_SPRING_IBBB.hydrosat2cor=rbind(data.frame(MFA='MFA1',ddres1.AC_SPRING_IBBB.hydrosat.1cor),data.frame(MFA='MFA2',ddres1.AC_SPRING_IBBB.hydrosat.2cor))
  
  p <- ggplot(ddres1.AC_SPRING_IBBB.hydrosat2cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA12)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p + facet_grid(~MFA) + theme(axis.text.x=element_text(angle=45, hjust=1,size=rel(1)),axis.title.y = element_blank(),
                                   axis.title.x = element_blank()) + guides(size=FALSE)

  # Summary plots of variable correlations with MFA axes ---------------
  #************************************
  ddres1.AC_SPRING_IBBB.hydrosat.1corPlusa.ct=aggregate(ddres1.AC_SPRING_IBBB.hydrosat.1cor[ddres1.AC_SPRING_IBBB.hydrosat.1cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.AC_SPRING_IBBB.hydrosat.1cor[ddres1.AC_SPRING_IBBB.hydrosat.1cor$correlation>=0.5,'ct']),length)
  ddres1.AC_SPRING_IBBB.hydrosat.2corPlusa.ct=aggregate(ddres1.AC_SPRING_IBBB.hydrosat.2cor[ddres1.AC_SPRING_IBBB.hydrosat.2cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.AC_SPRING_IBBB.hydrosat.2cor[ddres1.AC_SPRING_IBBB.hydrosat.2cor$correlation>=0.5,'ct']),length)
  ddres1.AC_SPRING_IBBB.hydrosat.2corMoinsa.ct=aggregate(ddres1.AC_SPRING_IBBB.hydrosat.2cor[ddres1.AC_SPRING_IBBB.hydrosat.2cor$correlation<=0.5,'correlation'],
                                 list(ct=ddres1.AC_SPRING_IBBB.hydrosat.2cor[ddres1.AC_SPRING_IBBB.hydrosat.2cor$correlation<=0.5,'ct']),length)
  ddres1.AC_SPRING_IBBB.hydrosat.3corPlusa.ct=aggregate(ddres1.AC_SPRING_IBBB.hydrosat.3cor[ddres1.AC_SPRING_IBBB.hydrosat.3cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.AC_SPRING_IBBB.hydrosat.3cor[ddres1.AC_SPRING_IBBB.hydrosat.3cor$correlation>=0.5,'ct']),length)
  par(mfrow=c(2,2),mar=c(3,12,3,1),bg='white')
  barplot(ddres1.AC_SPRING_IBBB.hydrosat.1corPlusa.ct$x,names.arg=ddres1.AC_SPRING_IBBB.hydrosat.1corPlusa.ct$ct,main='cor(Variable:year,MFA1)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.AC_SPRING_IBBB.hydrosat.1corMoins.res,1,sum),main='cor(Variable:year,MFA1)<=-0.5',horiz=TRUE,las=2)
  barplot(ddres1.AC_SPRING_IBBB.hydrosat.2corPlusa.ct$x,names.arg=ddres1.AC_SPRING_IBBB.hydrosat.2corPlusa.ct$ct,main='cor(Variable:year,MFA2)>=0.5',horiz=TRUE,las=2)
  barplot(ddres1.AC_SPRING_IBBB.hydrosat.2corMoinsa.ct$x,names.arg=ddres1.AC_SPRING_IBBB.hydrosat.2corMoinsa.ct$ct,main='cor(Variable:year,MFA2)<=0.5',horiz=TRUE,las=2)
  barplot(ddres1.AC_SPRING_IBBB.hydrosat.3corPlusa.ct$x,names.arg=ddres1.AC_SPRING_IBBB.hydrosat.3corPlusa.ct$ct,main='cor(Variable:year,MFA3)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.AC_SPRING_IBBB.hydrosat.3corMoins.res,1,sum),main='cor(Variable:year,MFA3)<=-0.5',horiz=TRUE,las=2)
  par(mfrow=c(1,1))

#*******************************************************************
# 5. Maps of MFA coordinates ------------
#*******************************************************************

  # Path to folder to export maps
  path.export.envt.median1=paste(prefix,"WGACEGG/Results/MFAgrids/SSTS/medianContribSel/",sep='')
  path.export.envt.mean1=paste(prefix,"WGACEGG/Results/MFAgrids/SSTS/meanContribSel/",sep='')
  
  # 5.1. Plot maps of mean individuals MFA coordinates, -----------
  # filtered by contribution and/oir quality of representation and 
  # within-cell inertia on MFA1 to 3, over MFA1 to 3, and over MFA1 to 61
  # see function help page for details: ?BoB.MFA.rasterPlot
  #******************************************************************
  AC_SPRING_IBBB.hydrosat.MFAres.rasters=MFA.rasterPlot(
    MFAres=res.AC_SPRING_IBBB.hydrosat.MFA,
    nMFA=2,cellsel=cellsel.fwgacegg,
    xyCell=pipo,ptitle='',
    path.export=NULL,ux11=TRUE,
    plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                MFA123contrib=FALSE,MFA123cos2=FALSE,
                MFA123coordMcos2=FALSE,
                MFA123coordMcontrib=FALSE,MFA123pind=FALSE),
    funSel=median,layout=c(2, 1),
    Ndim=list(c(1),c(2),c(3),seq(3),seq(ncpmax)),
    fwidth=30,fheight=15,fres=300,xlab='',ylab='',
    main='',bathy.plot = TRUE,newbathy = FALSE)
  
  graphics.off()
  
  AC_SPRING_IBBB.hydrosat.MFAres.rasters=BoB.MFA.rasterPlot(
    MFAres=res.AC_SPRING_IBBB.hydrosat.MFA,nMFA=2,
    cellsel=cellsel.AC_SPRING_IBBB.hydrosat,pipo=pipo,
    ptitle='',path.export.envt=path.export.envt.median1,
                                    ux11=TRUE,
                                    plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                                MFA123contrib=FALSE,MFA123cos2=FALSE,
                                                MFA123coordMcos2=FALSE,
                                                MFA123coordMcontrib=FALSE,MFA123pind=FALSE),
                                    funSel=median,layout=c(2, 1),
                                    Ndim=list(c(1),c(2),c(3),seq(3),
                                              seq(dim(big.AC_SPRING_IBBB.hydrosat.MFAs2)[2])))
  # quality of representation of individuals
  AC_SPRING_IBBB.hydrosat.cos2df=res.AC_SPRING_IBBB.hydrosat.MFA$ind$cos2
  # individuals contributions to MFA planes
  AC_SPRING_IBBB.hydrosat.contrib2df=res.AC_SPRING_IBBB.hydrosat.MFA$ind$contrib
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals quality of representation on MFA planes
  AC_SPRING_IBBB.hydrosat.MFAcoordMcos2.rasterStack=AC_SPRING_IBBB.hydrosat.MFAres.rasters$MFAcoordMcos2.rasterStack
  
  # Plot MFA123 inertia maps
  #************************
  names(AC_SPRING_IBBB.hydrosat.MFAres.rasters)
  AC_SPRING_IBBB.hydrosat.MFAinertia.raster=AC_SPRING_IBBB.hydrosat.MFAres.rasters$MFAinertia.raster
  raster.levelplot.PELGAS(AC_SPRING_IBBB.hydrosat.MFAinertia.raster[[1]],ux11=TRUE,lptheme=viridisTheme,
                          path1=path.export.envt,fid1=paste('1_MFAcoordInertia',sep=''),
                          margin = FALSE,xlab='',ylab='',nattri='',fwidth=30,fheight=15,fres=300)
  
  # 5.2. Define "characteristic areas", ------
  # where cell maps contribution to MFA planes is higher than the results of "funSel" applied on contribution maps
  #***************************************
  # raster stack of maps of mean indivuals coordinates on MFA1:3, filtered by individuals contributions to MFA planes 
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack=AC_SPRING_IBBB.hydrosat.MFAres.rasters$MFAcoordMcontrib.rasterStack
  # identify 'clumps' of cells in MFA planes to define "characteristic areas"
  plot(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack)
  raster(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack,1)
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc1=clump(raster(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack,1))
  plot(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc1)
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc2=clump(raster(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack,2))
  plot(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc2)
  #MFAcoordMcontrib.rasterStackc3=clump(raster(MFAcoordMcontrib.rasterStack,3),directions=4)
  #plot(MFAcoordMcontrib.rasterStackc3)
  #Compute mean MFA coordinates in characteristic areas
  zonal(raster(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack,1), AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc1, 'mean')
  #
  zonal(raster(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack,2), AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc2, 'mean')
  #zonal(raster(MFAcoordMcontrib.rasterStack,3), MFAcoordMcontrib.rasterStackc3, 'mean')
  # Extract cell coordinates and belongings to MFA characteristic areas and merge to data
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz <- data.frame(rasterToPoints(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc1))
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz$x=round(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz$x,3)
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz$y=round(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz$y,3)
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack2.xyz <- data.frame(rasterToPoints(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStackc2))
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack2.xyz$x=round(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack2.xyz$x,3)
  # MFAcoordMcontrib.rasterStack2.xyz$y=round(MFAcoordMcontrib.rasterStack2.xyz$y,3)
  # MFAcoordMcontrib.rasterStack3.xyz <- data.frame(rasterToPoints(MFAcoordMcontrib.rasterStackc3))
  
  head(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz)
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz=merge(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz,
                                                      cellsel.AC_SPRING_IBBB.hydrosat,by.x=c('x','y'),by.y=c('x','y'))
  AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack2.xyz=merge(AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack2.xyz,
                                                      cellsel.AC_SPRING_IBBB.hydrosat,by.x=c('x','y'),
                                          by.y=c('x','y'))
  #MFAcoordMcontrib.rasterStack3.xyz=merge(MFAcoordMcontrib.rasterStack3.xyz,cellsel.AC_SPRING_IBBB.hydrosat,by.x=c('x','y'),
  #                                        by.y=c('x','y'))
  
  # Add belongings to characteristic areas to raw data
  AC_SPRING_IBBB.hydrosat.clumpsdf1=AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack1.xyz
  names(AC_SPRING_IBBB.hydrosat.clumpsdf1)[names(AC_SPRING_IBBB.hydrosat.clumpsdf1)=='clumps']='clumps.MFA1e'
  AC_SPRING_IBBB.hydrosat.clumpsdf2=AC_SPRING_IBBB.hydrosat.MFAcoordMcontrib.rasterStack2.xyz
  names(AC_SPRING_IBBB.hydrosat.clumpsdf2)[names(AC_SPRING_IBBB.hydrosat.clumpsdf2)=='clumps']='clumps.MFA2e'
  #clumpsdf3=MFAcoordMcontrib.rasterStack3.xyz
  AC_SPRING_IBBB.hydrosat.clumpsdf12=merge(AC_SPRING_IBBB.hydrosat.clumpsdf1,AC_SPRING_IBBB.hydrosat.clumpsdf2)
  head(clumpsdf12)
  
  # Gives names to characteristic areas for plots
  head(SSTSs)
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1=merge(AC_SPRING_IBBB.hydrosat.clumpsdf1,SSTSs[,c('I','J','sp','Year','Zvalue')])
  head(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1)
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e==1,'clumps.MFA1e']='CBIS'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e==2,'clumps.MFA1e']='OBIS'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e==3,'clumps.MFA1e']='OBIS'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e==4,'clumps.MFA1e']='CBIS'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e==5,'clumps.MFA1e']='GALI'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e==6,'clumps.MFA1e']='SPOR'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e==7,'clumps.MFA1e']='SPOR'
  unique(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e)
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps2.MFA1e='SPOR&OBIS'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e=='CBIS','clumps2.MFA1e']='CBIS&GALI'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$clumps.MFA1e=='GALI','clumps2.MFA1e']='CBIS&GALI'

  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2=merge(AC_SPRING_IBBB.hydrosat.clumpsdf2,SSTSs[,c('I','J','sp','Year','Zvalue')])
  head(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2)
  unique(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps)
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps.MFA2e==1,'clumps.MFA2e']='SCBI'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps.MFA2e==2,'clumps.MFA2e']='SCBI'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps.MFA2e==3,'clumps.MFA2e']='CANT'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps.MFA2e==4,'clumps.MFA2e']='SPOR'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps.MFA2e==5,'clumps.MFA2e']='SPOR'
  unique(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps.MFA2e)
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps2.MFA2e='SCBI&SPOR'
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$clumps.MFA2e=='CANT','clumps2.MFA2e']='CANT'

  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA12=merge(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1,AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2)
  head(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA12)
  
  # 5.3. Compute mean clumps composition -----------
  #***************************************
  #MFA1: higher T&S in offshore Biscay and Southern Portugal vs. lower T&S in coastal Biscay and Galicia
  #****************
  # MFA2 : higher temperature in southern Portugal and South Biscay compared to Cantabrian Sea
  AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1a=Fishcluster.arcomp(CLres.fssd.ALL.MFA=AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1,
                                                 cname='clumps2.MFA1e')
  head(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1)

  p1<-ggplot(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1, 
                aes(x=clumps2, y=Zvalue)) + geom_boxplot() + labs(x='Regions',y='SST in °C, SSS in psu')
  p1 + facet_grid(~sp)

  p2<-ggplot(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$sp=='SST',], 
            aes(x=clumps2, y=Zvalue)) + geom_boxplot() + labs(x='Regions',y='SST in °C')
  p2
  
  # test differences
  ggplot(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SST',], aes(Zvalue, colour = clumps2)) +
    geom_density()+labs(x = 'SST')
  ggplot(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SSS',], aes(Zvalue, colour = clumps2)) +
    geom_density()+labs(x = 'SSS')
  ggplot(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$sp=='SST',], aes(Zvalue, colour = clumps2)) +
    geom_density()+labs(x = 'SST')
  
  wilcox.test(Zvalue~clumps2,data=AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SST',],
              alternative='less')
  wilcox.test(Zvalue~clumps2,data=AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SSS',],
              alternative='less',conf.int=TRUE)  
  wilcox.test(Zvalue~clumps2,data=AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$sp=='SST',],
              alternative='less')
  aggregate(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SST','Zvalue'],
            list(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SST','clumps2']),summary)
  aggregate(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SSS','Zvalue'],
            list(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA1$sp=='SSS','clumps2']),summary)
  aggregate(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$sp=='SST','Zvalue'],
            list(AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2[AC_SPRING_IBBB.hydrosat.CLres.ALL.MFA2$sp=='SST','clumps2']),summary)

#*********************************************************************
# 6. Temporal analysis of MFA results --------------
#*********************************************************************

  # 6.1. Years (group) mean positions in MFA space --------------
  #***********************************************
  # From Pagès (2014):
  # Graphs like this are interpreted in a similar way as correlation circles: in
  # both cases the coordinate of a point is interpreted as a relationship measurement
  # with a maximum value of 1. But this new graph has two specificities: the
  # groups of variables are unstandardised and their coordinates are always positive.
  # They therefore appear in a square (with a side of 1 and with points [0,0]
  # and [1,1] as vertices) known as a relationship square. Pages (2014), p141
  
  # Therefore, when compared with an intuitive approach to interpretation
  # (for a PCA user), one highly practical advantage of this representation is its
  # relationship with the representations of individuals and variables already
  # provided: in MFA, data are considered from different points of view, but in
  # one single framework. Pages (2014), p141
  names(res.AC_SPRING_IBBB.hydrosat.MFA)
  res.AC_SPRING_IBBB.hydrosat.MFA.group=res.AC_SPRING_IBBB.hydrosat.MFA$group
  AC_SPRING_IBBB.hydrosat.gcoord=data.frame(res.AC_SPRING_IBBB.hydrosat.MFA.group$coord[,1:3])
  AC_SPRING_IBBB.hydrosat.gcontrib=res.AC_SPRING_IBBB.hydrosat.MFA.group$contrib[,1:3]
  AC_SPRING_IBBB.hydrosat.gcontrib=data.frame(AC_SPRING_IBBB.hydrosat.gcontrib)
  names(AC_SPRING_IBBB.hydrosat.gcontrib)=paste('contrib',names(AC_SPRING_IBBB.hydrosat.gcontrib),sep='.')
  AC_SPRING_IBBB.hydrosat.gcontrib$year=row.names(AC_SPRING_IBBB.hydrosat.gcontrib)
  AC_SPRING_IBBB.hydrosat.gcoord$year=row.names(AC_SPRING_IBBB.hydrosat.gcoord)
  AC_SPRING_IBBB.hydrosat.gcoord=merge(AC_SPRING_IBBB.hydrosat.gcoord,AC_SPRING_IBBB.hydrosat.gcontrib,by.x='year',by.y='year')
  row.names(AC_SPRING_IBBB.hydrosat.gcoord)=AC_SPRING_IBBB.hydrosat.gcoord$year
  AC_SPRING_IBBB.hydrosat.gcoord$contrib.Dim.1.2=AC_SPRING_IBBB.hydrosat.gcoord$contrib.Dim.1+AC_SPRING_IBBB.hydrosat.gcoord$contrib.Dim.2
  AC_SPRING_IBBB.hydrosat.gcoord$contrib.Dim.2.3=AC_SPRING_IBBB.hydrosat.gcoord$contrib.Dim.2+AC_SPRING_IBBB.hydrosat.gcoord$contrib.Dim.3
  head(AC_SPRING_IBBB.hydrosat.gcoord)  
  AC_SPRING_IBBB.hydrosat.gcoord$year=paste('20',AC_SPRING_IBBB.hydrosat.gcoord$year,sep='')
  range(AC_SPRING_IBBB.hydrosat.gcoord$Dim.1)
  range(AC_SPRING_IBBB.hydrosat.gcoord$Dim.2)
  range(AC_SPRING_IBBB.hydrosat.gcoord$Dim.3)
  
  # Plot of groups mean positions in MFA space:
  #******************************************
  p12 <- ggplot(AC_SPRING_IBBB.hydrosat.gcoord,aes(Dim.1,Dim.2,label=year))+geom_text(aes(colour = contrib.Dim.1.2))+
    scale_colour_continuous(name="Contribution (%)")
  p12
  
  plot(AC_SPRING_IBBB.hydrosat.gcoord$year,AC_SPRING_IBBB.hydrosat.gcoord$Dim.1,type='b',ylim=c(0,1.2),ylab='MFA',
         xlab='')
  lines(AC_SPRING_IBBB.hydrosat.gcoord$year,AC_SPRING_IBBB.hydrosat.gcoord$Dim.2,lty=2,type="b")
  legend('topleft',c('envtMFA1','envtMFA2'),lty=seq(2))
  
  # p23 <- ggplot(gcoord,aes(Dim.2,Dim.3,label=year))+geom_text(aes(colour = contrib.Dim.2.3))+
  #   scale_colour_continuous(name="Contribution (%)")
  # multiplot(p12,p23)

  # 6.2. Eventually, study years partial axes correlations with MFA axes --------
  #****************************************
  # We have already insisted on the need to connect the inertias from the MFA
  # and those in the separate PCAs (see Tables 4.4 and 4.5). It is also useful to
  # connect the factors of the MFA with those of the separate PCAs (also known
  # as partial axes), both to understand better the effects of weighting and to enrich
  # interpretation of the analyses. In order to do this, the latter are projected as
  # supplementary variables (see Figure 4.5). Pages 2014, p88
  
  # Partial axes correlations
  res.AC_SPRING_IBBB.hydrosat.MFA.partial.axes=res.AC_SPRING_IBBB.hydrosat.MFA$partial.axes
  AC_SPRING_IBBB.hydrosat.ddres.pyears=data.frame(res.AC_SPRING_IBBB.hydrosat.MFA.partial.axes$cor)
  head(AC_SPRING_IBBB.hydrosat.ddres.pyears)
  AC_SPRING_IBBB.hydrosat.ddres.pyears[row.names(AC_SPRING_IBBB.hydrosat.ddres.pyears)=='Dim1.14',]
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12=data.frame(res.AC_SPRING_IBBB.hydrosat.MFA.partial.axes$cor[,1:2])
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12s=AC_SPRING_IBBB.hydrosat.ddres.pyears.12[round(abs(AC_SPRING_IBBB.hydrosat.ddres.pyears.12$Dim.1))>=0.6|
                                     round(abs(AC_SPRING_IBBB.hydrosat.ddres.pyears.12$Dim.2))>=0.6,]
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12s$Dim=factor(gsub('[.]','',substr(row.names(AC_SPRING_IBBB.hydrosat.ddres.pyears.12s),1,5)),
                              ordered=TRUE,levels=paste('Dim',seq(10),sep=''))
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12s$Year=gsub('[.]','',substr(row.names(AC_SPRING_IBBB.hydrosat.ddres.pyears.12s),6,8))
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long=reshape(AC_SPRING_IBBB.hydrosat.ddres.pyears.12s,varying=list(1:2),
                                direction='long',timevar='MFA',v.names='correlation')
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long$MFA=paste('Dim',AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long$MFA,sep='')
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long$signif=abs(round(AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long$correlation))>=0.6
  AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long$MFA=paste('MFA',AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long$MFA,sep='.')
  head(AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long)  
  # Plot partial axes correlations with MFA axes
  p <- ggplot(AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long[AC_SPRING_IBBB.hydrosat.ddres.pyears.12s.long$Dim%in%c('Dim1'),], 
              aes(Dim,Year,size=abs(correlation),colour=correlation))+geom_point()
  p+ scale_colour_gradientn(colours = viridis(3)) + facet_grid(.~MFA)+
    theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(1)),axis.title.y = element_blank(),
          axis.title.x = element_blank()) + guides(size=FALSE)

  # 6.3. Maps of partial individuals (map cell:year pairs) coordinates in MFA space ---------
  #***********************************
  AC_SPRING_IBBB.hydrosat.MFAcoord=res.AC_SPRING_IBBB.hydrosat.MFA$ind$coord
  AC_SPRING_IBBB.hydrosat.pindCoord=res.AC_SPRING_IBBB.hydrosat.MFA$ind$coord.partiel
  names(res.AC_SPRING_IBBB.hydrosat.MFA$ind)
  AC_SPRING_IBBB.hydrosat.MFAyears=unlist(strsplit(row.names(AC_SPRING_IBBB.hydrosat.pindCoord),split='[.]'))[seq(2,(2*length(row.names(AC_SPRING_IBBB.hydrosat.pindCoord))),2)]
  lyears=unique(AC_SPRING_IBBB.hydrosat.MFAyears)
  pindCoord.fssdan=pindCoord.fssd-MFAcoord
  head(AC_SPRING_IBBB.hydrosat.pindCoord)
  
  # 6.3.1. Maps of partial individuals coordinates on MFA1,2,3, one plot per year ---------------- 
  #*****************************
  for (i in 1:length(lyears)){
    dfi=AC_SPRING_IBBB.hydrosat.pindCoord[AC_SPRING_IBBB.hydrosat.MFAyears==lyears[i],]-AC_SPRING_IBBB.hydrosat.MFAcoord
    dim(AC_SPRING_IBBB.hydrosat.MFAcoord)
    head(dfi)
    BoB.MFA.rasterPlot(MFAres=dfi,nMFA=1,cellsel.AC_SPRING_IBBB.hydrosat,pipo,path.export.envt=NULL,
                       plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                   MFA123contrib=FALSE,
                                   MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                   MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                       ptitle=paste('20',lyears[i],sep=''),lptheme = viridisTheme())
  }
  # 6.3.2. Maps of partial individuals coordinates anomalies, one mosaic plot per MFA1,2,3, with all years
  #---------------------
  res=BoB.MFA.rasterPlot(MFAres=res.AC_SPRING_IBBB.hydrosat.MFA,nMFA=2,cellsel.AC_SPRING_IBBB.hydrosat,pipo,path.export.envt=NULL,
                         plotit=list(MFA123coord=TRUE,MFA123contrib=FALSE,MFA123timevar=FALSE,
                                     MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                     MFA123coordMcontrib=FALSE,MFA123pind=TRUE),funSel='mean',
                         ptitle=paste(''),lptheme=BuRdTheme,anomalit=TRUE)

 
