library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)

#***************************************************************************************************************************************
#                                               Multiple Factor Analysis with FactoMineR tutorial
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.5

# 1. Import grid map data and convert them into raster package objects --------
#********************************************************************

  # path to donnees2 drive
  #*********************************
  donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
  
  # path to grid files
  #*********************************
  prefix=paste(donnees2,'Campagnes/',sep='')
  prefix=paste('D:/Campagnes/',sep='')
  path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
  path.grids1=paste(path.grids,"AC_SPRING_IBBB/",sep='')
  path.export.results.fwgacegg.fMFA=paste(prefix,"WGACEGG/Results/gridMaps/AdultFish/MFA/",sep='')
  dir.create(path.export.results.fwgacegg.fMFA,recursive = TRUE)
  
  # List grid files 
  #*********************************
  lffwgacegg=list.files(path.grids1,pattern='*.txt')
  #lfs=strsplit(lf,split='[.]',)
  lffwgaceggs=data.frame(t(data.frame(strsplit(lffwgacegg,split='[.]'))))
  lp=as.character(lffwgaceggs[,1])
  lffwgaceggs2=data.frame(t(data.frame(strsplit(lp,split='_'))))
  row.names(lffwgaceggs2)=seq(length(lffwgaceggs2[,1]))
  lffwgaceggs=cbind(lffwgaceggs,lffwgaceggs2)
  head(lffwgaceggs)
  names(lffwgaceggs)=c('pat','ext','method','sp','var','filter')
  lffwgaceggs$path=lffwgacegg
  row.names(lffwgaceggs)=seq(length(lffwgaceggs[,1]))

  # Import grid and raster files, bind grid maps
  #***************************************************
  for (i in 1:dim(lffwgaceggs)[1]){
    pat=paste(path.grids1,lffwgaceggs$path[i],sep='')
    mati=read.table(file=pat,header=T,sep=";")
    mati$sp=lffwgaceggs$sp[i]
    head(mati)
    if (i==1){
      FishNASCSp=mati
    }else{
      FishNASCSp=rbind(FishNASCSp,mati)
    }
    # graphics.off()
    # resras=grid2rasterStack(pat=mati,path1=NULL,
    #                         varid=paste(lffwgaceggs$sp[i],
    #                                     lffwgaceggs$var[i],
    #                                     sep=' '),
    #                         anomaly=TRUE)
    # assign(paste(lffwgaceggs$sp[i],'rasterStack',sep='.'),resras)
    # graphics.off()
    # Import raster stacks format, compute mean and SD maps and plot everything:
    #pat2=paste(paste(path.grids1,'Rasters/',sep=''),lffwgaceggrss$path[i],sep='')
    #rasti=raster(pat2)
  }

# 2. Prepare datasets for MFA -------------------
  #********************************************************************
  # 2.1. Select common species ---------------
  #***************************
  head(FishNASCSp)
  unique(FishNASCSp$Year)
  table(FishNASCSp$Year)
  unique(FishNASCSp$sp)
  FishNASCSp$spi=substr(FishNASCSp$sp,1,8)
  FishNASCSpp=FishNASCSp[FishNASCSp$Zvalue>0&!is.na(FishNASCSp$Zvalue),]
  table(FishNASCSpp$spi,FishNASCSpp$Year)
  OccurenceFrequencySp=apply(table(FishNASCSpp$spi,FishNASCSpp$Year)>0,1,sum)/length(unique(FishNASCSpp$Year))
  sort(OccurenceFrequencySp)
  FishNASCSps=FishNASCSp[FishNASCSp$spi%in%c('ENGR-ENC','SARD-PIL'),]
  head(FishNASCSps)
  
  # 2.2. Select years ---------------
  #***************************
  FishNASCSps=FishNASCSps[!FishNASCSps$Year%in%c(2003,2004,2009,2012,2020),]
  unique(FishNASCSps$Year)
  head(FishNASCSps)
  
  # -> Pelago in winter in 2003, no gulf of cadiz in 2004, no Pelago in 2012
  
  # 2.3. Reshape data frame for analysis with year as grouping variable -------------
  #***************************
  a.fwgacegg <- reshape(FishNASCSps[,c("I","J","Zvalue","sp","Year")], 
                        timevar = "sp", idvar = c("I","J","Year"), direction = "wide")
  head(a.fwgacegg)
  names(a.fwgacegg)
  a.fwgacegg=a.fwgacegg[order(a.fwgacegg$Year),]
  
  big.fwgacegg <- reshape(a.fwgacegg, timevar = "Year", idvar = c("I","J"), 
                          direction = "wide")
  head(big.fwgacegg)
  names(big.fwgacegg)
  dim(big.fwgacegg)
  
  # 2.4. Select cells in survey area -------------
  #***************************
  pat=paste(path.grids1,lffwgaceggs$path[4],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  pipo=mati[mati$Year==2006,]
  
  plot(pipo$Xgd,pipo$Ygd)
  plot(pipo$Xgd,pipo$Ygd,cex=0.1+pipo$Zvalue/100)
  coast()
  # Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
  #******************************************
  # Define grid limits
  x1=-10.2;x2=-1;y1=35.8;y2=49
  # Define cell dimensions
  ax=0.25;ay=0.25
  # Define smoothing radius
  u=2
  # Define iteration number
  ni=200
  define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni)
  poly=data.frame(x=xpol,y=ypol)
  lines(poly)
  coast()
  head(pipo)
  library(splancs) 
  pts=as.points(pipo$Xgd,pipo$Ygd)
  testInPoly=inout(pts,poly)
  plot(pts)
  points(pts[testInPoly,],pch=16)
  coast()
  lines(poly)
  cellsel.fwgacegg=data.frame(x=pts[,1],y=pts[,2],sel=testInPoly,I=pipo$I,J=pipo$J)
  cellsel.fwgacegg$cell=paste(cellsel.fwgacegg$I,cellsel.fwgacegg$J,sep='-')
  head(cellsel.fwgacegg)
  plot(cellsel.fwgacegg$x,cellsel.fwgacegg$y)
  points(cellsel.fwgacegg[cellsel.fwgacegg$sel,'x'],cellsel.fwgacegg[cellsel.fwgacegg$sel,'y'],pch=16)
  coast()
  legend('topleft',legend=c('Cells centers','Selected cells'),pch=c(1,16))
  
  # 2.5. Prepare and standardise dataset -----------
  #**************************
  names(big.fwgacegg)
  big.fwgacegg.MFA=big.fwgacegg[,-seq(2)]
  row.names(big.fwgacegg.MFA)=paste(big.fwgacegg$I,big.fwgacegg$J,sep='-')
  # simplify column names
  names(big.fwgacegg.MFA)=gsub('Zvalue.','',names(big.fwgacegg.MFA))
  names(big.fwgacegg.MFA)
  head(big.fwgacegg.MFA)
  
  # 2.6. Select cells in survey area before MFA ------
  #***************************************
  dim(big.fwgacegg.MFA)
  big.fwgacegg.MFA=big.fwgacegg.MFA[order(dimnames(big.fwgacegg.MFA)[[1]]),]
  cellsel.fwgacegg=cellsel.fwgacegg[order(cellsel.fwgacegg$cell),]
  head(big.fwgacegg.MFA)

  # 2.6.1. Select cells with no NA over the series -----
  #****************************************
  # # NA detector
  NAdetect=function(x){
    if (sum(is.na(x))>0){
      y=FALSE
    }else{
      y=TRUE
    }
    y
  }
  cselNoNAf=apply(big.fwgacegg.MFA,1,NAdetect)
  names(cselNoNAf)
  cselNoNAf.df=data.frame(cselNoNAf)
  head(cselNoNAf.df)
  row.names(cselNoNAf.df)
  big.fwgacegg.MFAs=big.fwgacegg.MFA[cselNoNAf,]
  cellsel.fwgacegg=cellsel.fwgacegg[order(cellsel.fwgacegg$cell),]
  #cellsel.fwgacegg2=merge(cellsel.fwgacegg,cselNoNAf.df,by.x="cell",by.y="row.names")
  plot(cellsel.fwgacegg$x,cellsel.fwgacegg$y)
  points(cellsel.fwgacegg[cselNoNAf,'x'],cellsel.fwgacegg[cselNoNAf,'y'],pch=16)
  coast()
  cellsel.fwgacegg$selNoNA=cselNoNAf
  
  # # convert NA to zero
  NA2null=function(x,procInf=FALSE){
    x[is.na(x)]=0
    if (procInf){
      x[is.infinite(x)]=0
    }
    x
  }
  big.fwgacegg.MFAs=apply(big.fwgacegg.MFAs,2,NA2null)
  head(big.fwgacegg.MFAs)
  rnames=dimnames(big.fwgacegg.MFAs)[[1]]
  # log variables
  big.fwgacegg.MFAs=apply(big.fwgacegg.MFAs,2,function(x){x=log(x+1);x})
  head(big.fwgacegg.MFAs)
  dim(big.fwgacegg.MFAs)
  class(big.fwgacegg.MFAs)
  dimnames(big.fwgacegg.MFAs)[[1]]=rnames
  # center and not scale variables
  big.fwgacegg.MFAs=apply(big.fwgacegg.MFAs,2,scale,center=TRUE,scale=FALSE)
  head(big.fwgacegg.MFAs)
  class(big.fwgacegg.MFAs)
  dimnames(big.fwgacegg.MFAs)[[1]]=rnames

  # 2.7. Remove constant columns-----------  
  #***************************************
  # with zeroes
  scols=apply(big.fwgacegg.MFAs,2,sum,na.rm=TRUE)
  srows=apply(big.fwgacegg.MFAs,1,sum,na.rm=TRUE)
  big.fwgacegg.MFAs2=big.fwgacegg.MFAs[,scols!=0]
  sum(scols==0)
  scols2=apply(big.fwgacegg.MFAs2,2,sum,na.rm=TRUE)
  summary(scols2)
  # other constant columns
  vcols=apply(big.fwgacegg.MFAs2,2,var,na.rm=TRUE)
  ccols=dimnames(big.fwgacegg.MFAs2)[[2]][vcols==0]
  big.fwgacegg.MFAs2=big.fwgacegg.MFAs2[,!dimnames(big.fwgacegg.MFAs2)[[2]]%in%ccols]
  dim(big.fwgacegg.MFAs2)
  
  # 2.8. Generate no. of data in each group -----------
  #***************************************
  cwz=dimnames(big.fwgacegg.MFAs2)[[2]]
  nct=unlist(strsplit(cwz,split='[.]'))[seq(1,2*length(cwz),2)]
  nyears=unlist(strsplit(cwz,split='[.]'))[seq(2,2*length(cwz),2)]
  lnyears.fwgacegg=unique(nyears)
  tnnc=table(nct,nyears)
  tnnc.df=data.frame(table(nct,nyears))
  stnnc.fwgacegg=apply(tnnc,2,sum)
  head(tnnc.df)
  tnnc.dfa=aggregate(tnnc.df$Freq,list(nct=tnnc.df$nct),sum)
  summary(tnnc.dfa$x)

# 3. Run MFA ----------------
#*************************************************************
  # 3.1. MFA ---------------
  #***********************************
  dim(big.fwgacegg.MFAs2)
  res.fwgacegg.MFA<-MFA(big.fwgacegg.MFAs2, group=stnnc.fwgacegg, 
                        type=rep("s",length(stnnc.fwgacegg)), 
                    ncp=dim(big.fwgacegg.MFAs2)[2],name.group=substr(lnyears.fwgacegg,3,4), 
                    num.group.sup=NULL, graph=TRUE)
  names(res.fwgacegg.MFA)
  
  # 3.2. Explained variance ----------
  #***********************************  
  summary(res.fwgacegg.MFA)  
  par(bg='white')
  barplot(res.fwgacegg.MFA$eig[,2],main="% of variance explained",names.arg=1:nrow(res.fwgacegg.MFA$eig))
  barplot(res.fwgacegg.MFA$eig[,3],main="Cumulative % of variance explained",names.arg=1:nrow(res.fwgacegg.MFA$eig))
  res.fwgacegg.MFA$eig[round(res.fwgacegg.MFA$eig[,3])==95,]
  dim(big.fwgacegg.MFAs2)
  
  # 5.3. Groups (years) correlations with MFA -----
  #****************************************
  # 6.2 Relationship Between a Variable and Groups of Variables
  # Pagès 2014, p 131
  # Methodological Remark. In practice, we begin to analyse MFA results by examining
  # the canonical correlation coefficients. If we conclude that there are
  # no common factors, it is logical to interrupt the analysis: 
  # in such cases, the # factors of the separate PCAs 
  # will be favoured over the factors (thus specific to one group) of the MFA.
  
  # In the FactoMineRMFAoutput (namedres, for example),Table 6.1 is found
  # in res$group$correlation. Pages, 2014 MFA in R p131
  
  # In the absence of a validation technique, we can only empirically choose a
  # threshold below which canonical correlation coefficients should be considered
  # negligible. In practice, we consult these coefficients for a large number of axes,
  # with those of the highest ranks (almost) certainly corresponding to an absence
  # of structure; we therefore try to find discontinuity in this distribution.
  # -> cor coeff should decrease with increasing rank. If it does not for a given group, this may indicate 
  # the correlation threshold to be used 
  # Table 6.1 suggests this limit should be fixed between .8030 and .9522. This
  # leads us to considering the first factor as common to two groups and the
  # second factor as specific to group 2 (sensory).
  
  gcor.fwgacegg=res.fwgacegg.MFA$group$correlation[,1:5]
  head(gcor.fwgacegg)
  (gcor.fwgacegg.max=apply(gcor.fwgacegg,2,max))
  
  apply(gcor.fwgacegg,2,summary)
  gcor.fwgacegg.q95=apply(gcor.fwgacegg,2,quantile,.95)
  gcor.fwgacegg.q75=apply(gcor.fwgacegg,2,quantile,.75)
  gcor.fwgacegg.q75=apply(gcor.fwgacegg,2,quantile,.75)
  
  # threshold below which canonical correlation coefficients 
  # should be considered negligible: 
  gcor.min.thr.fwgacegg=max(gcor.fwgacegg.max[4:5]) # threshold min
  #gcor.min.thr.fwgacegg=max(gcor.fwgacegg.q95[4:5]) # threshold min
  #gcor.min.thr.fwgacegg=max(gcor.fwgacegg.q75[4:5]) # threshold min
  gcor.max.thr.fwgacegg=max(gcor.fwgacegg.max[1:2]) # threshold max
  
  gcor.fwgaceggs=gcor.fwgacegg
  gcor.fwgaceggs[gcor.fwgacegg<gcor.min.thr.fwgacegg]=0
  x11()
  corrplot(t(gcor.fwgaceggs),is.cor=TRUE,col = viridis(10))
  #mtext('Groups (years) significant correlations with MFA dimensions',
  #      cex=1.5)
  # Save figures
  dev.print(png, file=paste(path.export.results.fwgacegg.fMFA,
                              "fishMFA_gcorYears.png",sep=""), 
              units='cm',width=15, height=15,res=300)
 
  # 3.3. Plots of individuals in MFA1-2-3 planes -----------
  #***********************************
  # Plot of individuals in MFA1-2 plane 
  p1 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "contrib") #with contribution in color scale
  p2 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "cos2") #with quality of representation in color scale
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA2-3 plane
  p1 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "contrib",axes = c(2, 3))
  p2 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "cos2",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-3 plane
  p1 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "contrib",axes = c(1, 3))
  p2 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "cos2",axes = c(1, 3))
  grid.arrange(p1, p2, ncol = 2)
  # Plot of individuals in MFA1-2-3 planes, with contribution in color scale
  x11()
  p1 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "contrib")
  p2 = fviz_mfa_ind(res.fwgacegg.MFA, col.ind = "contrib",axes = c(2, 3))
  grid.arrange(p1, p2, ncol = 2)
  dev.print(device=png,
            filename=paste(path.export,'WGACEGGadultFishGridsMFA123biplot.png',sep=''),
            units='cm',width=29,height=21,res=300)

# 4. Contributions of variables to MFA axes -------------
#******************************************************************
  par(bg='grey50')
  plot.MFA(res.fwgacegg.MFA, axes=c(1, 2), choix="var", new.plot=TRUE, lab.var=TRUE, 
           habillage="group", select = "coord 10")
  par(bg='white')
  
  # 4.1. select variables with above average contribution to MFA axes ----------------
  #************************************
  var.contrib=res.fwgacegg.MFA$quanti.var$contrib
  head(var.contrib)
  summary(var.contrib[,1])
  var.contribs1=var.contrib[var.contrib[,1]>mean(var.contrib[,1]),1]
  var.contribs2=var.contrib[var.contrib[,2]>mean(var.contrib[,2]),2]
  var.contribs3=var.contrib[var.contrib[,3]>mean(var.contrib[,3]),3]
  
  # 4.2. Variables correlation with MFA axes ---------------
  #************************************
  var.cor=res.fwgacegg.MFA$quanti.var$contrib
  graphics.off()
  ddres1=dimdesc(res.fwgacegg.MFA, axes = 1:4, proba = 0.05)
  names(ddres1)
  
  # 4.2.1. Variables correlation with MFA axis1 ----------
  #************************************
  ddres1.1=ddres1$Dim.1$quanti
  ddres1.1=as.data.frame(ddres1.1[complete.cases(ddres1.1),])
  ddres1.1cor=ddres1.1[abs(ddres1.1$correlation)>=0.5,]
  ddres1.1cor$ct=unlist(strsplit(row.names(ddres1.1cor),split='[.]'))[seq(1,2*dim(ddres1.1cor)[1],2)]
  ddres1.1cor$year=unlist(strsplit(row.names(ddres1.1cor),split='[.]'))[seq(2,2*dim(ddres1.1cor)[1],2)]
  ddres1.1cor=ddres1.1cor[order(ddres1.1cor$ct),]
  ddres1.1cor$spid=row.names(ddres1.1cor)
  ddres1.1cor$sp=substr(as.character(ddres1.1cor$spid),1,8)
  ddres1.1cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.1cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.1cor$cs=gsub('[.]','',substr(as.character(ddres1.1cor$spid),17,24))
  ddres1.1cor$cs=gsub(' ','',ddres1.1cor$cs)
  ddres1.1cor$aCor=abs(ddres1.1cor$correlation)
  unique(ddres1.1cor$cs)
  #ddres1.1cor$signif=abs(ddres1.1cor$correlation)>=0.5
  head(ddres1.1cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.1cor)%in%names(var.contribs1))
  # Plot variables well correlated with MFA axis1 (abs(cor)>0.5)
  p <- ggplot(ddres1.1cor, aes(year,sp,size=abs(correlation),colour=correlation),
              shape=signif)+geom_point()+ggtitle("cor(variables,MFA1)")+ 
    scale_colour_gradientn(colours = viridis(10)) + theme_bw()+ scale_shape_discrete(guide=guide_legend(title = "abs(cor)>=0.6?"))
  p
  
  # 4.2.2. Variables correlation with MFA axis2 ------------
  #************************************
  ddres1.2=ddres1$Dim.2$quanti
  ddres1.2=as.data.frame(ddres1.2[complete.cases(ddres1.2),])
  ddres1.2cor=ddres1.2[abs(ddres1.2[,1])>=0.5,]
  ddres1.2cor$ct=unlist(strsplit(row.names(ddres1.2cor),split='[.]'))[seq(1,2*dim(ddres1.2cor)[1],2)]
  ddres1.2cor$year=unlist(strsplit(row.names(ddres1.2cor),split='[.]'))[seq(2,2*dim(ddres1.2cor)[1],2)]
  ddres1.2cor=ddres1.2cor[order(ddres1.2cor$correlation),]
  ddres1.2cor$spid=row.names(ddres1.2cor)
  ddres1.2cor$sp=substr(as.character(ddres1.2cor$spid),1,8)
  ddres1.2cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.2cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.2cor$cs=gsub('[.]','',substr(as.character(ddres1.2cor$spid),17,24))
  ddres1.2cor$cs=gsub(' ','',ddres1.2cor$cs)
  unique(ddres1.2cor$cs)
  ddres1.2cor$aCor=abs(ddres1.2cor$correlation)
  head(ddres1.2cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.2cor)%in%names(var.contribs2))
  # Plot variables well correlated with MFA axis2 (abs(cor)>0.5)
  p <- ggplot(ddres1.2cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA2)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p
  
  ddres12cor=rbind(data.frame(MFA='MFA1',ddres1.1cor),data.frame(MFA='MFA2',ddres1.2cor))
  
  # Summary plot
  x11()
  p <- ggplot(ddres12cor, aes(year,sp,size=aCor,colour=correlation))+geom_point()+ggtitle("cor(variables,MFA12)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p + facet_grid(~MFA) + theme(axis.text.x=element_text(angle=45, hjust=1,size=rel(1)),axis.title.y = element_blank(),
                                  axis.title.x = element_blank()) + guides(size=FALSE)
  dev.print(device=png,
            filename=paste(path.export.results.fwgacegg.fMFA,'WGACGGadultFishGridsMFA1-2corVar.png',sep=''),
            units='cm',width=29,height=21,res=300)
  
  # 4.2.3. Variables correlation with MFA axis 3 ------------
  #************************************
  ddres1.3=ddres1$Dim.3$quanti
  ddres1.3=as.data.frame(ddres1.3[complete.cases(ddres1.3),])
  ddres1.3cor=ddres1.3[abs(ddres1.3[,1])>=0.5,]
  ddres1.3cor$ct=unlist(strsplit(row.names(ddres1.3cor),split='[.]'))[seq(1,2*dim(ddres1.3cor)[1],2)]
  ddres1.3cor$year=unlist(strsplit(row.names(ddres1.3cor),split='[.]'))[seq(2,2*dim(ddres1.3cor)[1],2)]
  ddres1.3cor=ddres1.3cor[order(ddres1.3cor$correlation),]
  ddres1.3cor$spid=row.names(ddres1.3cor)
  ddres1.3cor$sp=substr(as.character(ddres1.3cor$spid),1,8)
  ddres1.3cor$cz=factor(gsub(' ','',
                             substr(as.character(ddres1.3cor$spid),10,16)),
                        ordered=TRUE,levels=c('Surface','Bottom'))
  ddres1.3cor$cs=gsub('[.]','',substr(as.character(ddres1.3cor$spid),17,24))
  ddres1.3cor$cs=gsub(' ','',ddres1.3cor$cs)
  unique(ddres1.3cor$cs)
  ddres1.3cor$aCor=abs(ddres1.3cor$correlation)
  head(ddres1.3cor)
  # well correlated variable not contributive?
  sum(!rownames(ddres1.3cor)%in%names(var.contribs3))
  # Plot variables well correlated with MFA axis3 (abs(cor)>0.5)
  p <- ggplot(ddres1.3cor, aes(year,sp,size=aCor,
                               colour=correlation))+geom_point()+ggtitle("cor(variables,MFA3)")+ 
    scale_colour_gradientn(colours = viridis(5)) + theme_bw()
  p

  # Summary plots of variable correlations with MFA axes ---------------
  #************************************
  ddres1.1corPlusa.ct=aggregate(ddres1.1cor[ddres1.1cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.1cor[ddres1.1cor$correlation>=0.5,'ct']),length)
  ddres1.2corPlusa.ct=aggregate(ddres1.2cor[ddres1.2cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.2cor[ddres1.2cor$correlation>=0.5,'ct']),length)
  ddres1.2corMoinsa.ct=aggregate(ddres1.2cor[ddres1.2cor$correlation<=0.5,'correlation'],
                                 list(ct=ddres1.2cor[ddres1.2cor$correlation<=0.5,'ct']),length)
  ddres1.3corPlusa.ct=aggregate(ddres1.3cor[ddres1.3cor$correlation>=0.5,'correlation'],
                                list(ct=ddres1.3cor[ddres1.3cor$correlation>=0.5,'ct']),length)
  par(mfrow=c(2,2),mar=c(3,12,3,1),bg='white')
  barplot(ddres1.1corPlusa.ct$x,names.arg=ddres1.1corPlusa.ct$ct,main='cor(Variable:year,MFA1)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.1corMoins.res,1,sum),main='cor(Variable:year,MFA1)<=-0.5',horiz=TRUE,las=2)
  barplot(ddres1.2corPlusa.ct$x,names.arg=ddres1.2corPlusa.ct$ct,main='cor(Variable:year,MFA2)>=0.5',horiz=TRUE,las=2)
  barplot(ddres1.2corMoinsa.ct$x,names.arg=ddres1.2corMoinsa.ct$ct,main='cor(Variable:year,MFA2)<=0.5',horiz=TRUE,las=2)
  barplot(ddres1.3corPlusa.ct$x,names.arg=ddres1.3corPlusa.ct$ct,main='cor(Variable:year,MFA3)>=0.5',horiz=TRUE,las=2)
  #barplot(apply(ddres1.3corMoins.res,1,sum),main='cor(Variable:year,MFA3)<=-0.5',horiz=TRUE,las=2)
  par(mfrow=c(1,1))

#*******************************************************************
# 5. Maps of MFA coordinates ------------
#*******************************************************************

  # Path to folder to export maps
  path.export.median=paste(prefix,"WGACEGG/Results/MFAgrids/AdultFish/medianContribSel/",sep='')
  path.export.mean=paste(prefix,"WGACEGG/Results/MFAgrids/AdultFish/meanContribSel/",sep='')
  
  # 5.1. Plot maps of mean individuals MFA coordinates, -----------
  # filtered by contribution and/oir quality of representation and 
  # within-cell inertia on MFA1 to 3, over MFA1 to 3, and over MFA1 to 61
  # see function help page for details: ?BoB.MFA.rasterPlot
  #******************************************************************
  fwgacegg.MFAres.rasters=BoB.MFA.rasterPlot(
    MFAres=res.fwgacegg.MFA,nMFA=2,cellsel=cellsel.fwgacegg,pipo=pipo,
    ptitle='',path.export=path.export.results.fwgacegg.fMFA,ux11=TRUE,
                                    plotit=list(MFA123coord=TRUE,MFA123timevar=TRUE,
                                                MFA123contrib=TRUE,MFA123cos2=TRUE,
                                                MFA123coordMcos2=TRUE,
                                                MFA123coordMcontrib=TRUE,MFA123pind=FALSE),
                                    funSel=median,layout=c(2, 1),
                                    Ndim=list(c(1),c(2),c(3),seq(3),dim(big.fwgacegg.MFAs2)[2]))
  
  MFAres.rasters.fwgacegg.MFA=MFA.rasterPlot(MFAres=res.fwgacegg.MFA,
                                     nMFA=2,
                                     cellsel=cellsel.fwgacegg,xyCell=pipo,
                                     ptitle='',
                                     path.export=path.export.results.fwgacegg.fMFA,
                                     ux11=TRUE,
                                     plotit=list(MFA123coord=TRUE,
                                                 MFA123timevar=TRUE,
                                                 MFA123contrib=TRUE,
                                                 MFA123cos2=TRUE,
                                                 MFA123coordMcos2=FALSE,
                                                 MFA123coordMcontrib=FALSE,
                                                 MFA123pind=FALSE),
                                     funSel=median,layout=c(2, 1),
                                     Ndim=list(c(1),c(2),c(3),seq(3),seq(ncpmax)),
                                     fwidth=30,fheight=15,fres=300,
                                     xlab='',ylab='',main='',bathy.plot = TRUE,
                                     newbathy = FALSE)
  
  MFAres.rasters.fwgacegg.MFA.df=as.data.frame(
    MFAres.rasters.fwgacegg.MFA$MFAcoord.rasterStack,xy=TRUE)
  head(MFAres.rasters.fwgacegg.MFA.df)
  
  MFAres.rasters.fwgacegg.MFA.df.long=NULL
  for (i in 1:2){
    dfi=MFAres.rasters.fwgacegg.MFA.df[,c('x','y',paste('Dim.',i,sep=''))]
    names(dfi)[3]='value'
    dfi$MFA=paste('Dim.',i,sep='')
    MFAres.rasters.fwgacegg.MFA.df.long=rbind(
      MFAres.rasters.fwgacegg.MFA.df.long,dfi)
  }
  
  # 7.0. MFA maps ------------
  x11()
  p1=ggplot() + geom_raster(
    data = MFAres.rasters.fwgacegg.MFA.df.long, aes(x = x, y = y, 
                                                fill =value))+
    labs(title = "MFA") + 
    theme(axis.title.x = element_blank(),axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    facet_wrap(vars(MFA),ncol=3)+scale_fill_viridis_c()
  print(p1)
  dev.print(png, file=paste(path.export.results.fwgacegg.fMFA,
                            "MFA_2PCsMaps.png",sep=""), 
            units='cm',width=30, height=15,res=300)
  
  # Cell PCs maps
  x11()
  par(bg='white')
  p1<-ggplot() + geom_raster(
    data = MFAres.rasters.fwgacegg.MFA.df.long, aes(x = x, y = y, fill =Dim.1))+
    labs(title = "MFA1") + 
    theme(axis.title.x = element_blank(),axis.title.y = element_blank())+
    geom_sf(data=sfPoly.BoB, fill="gray20",color="black", size=0.25)+
    scale_fill_viridis_c()
  p2<-ggplot() + geom_raster(
    data = MFAres.rasters.clup.df.BoB, aes(x = x, y = y, fill =Dim.2))+
    labs(title = "MFA2") + 
    theme(axis.title.x = element_blank(),axis.title.y = element_blank())+
    geom_sf(data=sfPoly.BoB, fill="gray20",color="black", size=0.25)+
    scale_fill_viridis_c()
  figure <- ggarrange(p1,p2,ncol = 2)
  figure
  dev.print(png, file=paste(path.export.results.clup.BoB,
                            "MFA_3PCsMaps.png",sep=""), 
            units='cm',width=30, height=15,res=300)
  
#*********************************************************************
# 6. Temporal analysis of MFA results --------------
#*********************************************************************

  # 6.1. Years (group) mean positions in MFA space --------------
  #***********************************************
  # From Pagès (2014):
  # Graphs like this are interpreted in a similar way as correlation circles: in
  # both cases the coordinate of a point is interpreted as a relationship measurement
  # with a maximum value of 1. But this new graph has two specificities: the
  # groups of variables are unstandardised and their coordinates are always positive.
  # They therefore appear in a square (with a side of 1 and with points [0,0]
  # and [1,1] as vertices) known as a relationship square. Pages (2014), p141
  
  # Therefore, when compared with an intuitive approach to interpretation
  # (for a PCA user), one highly practical advantage of this representation is its
  # relationship with the representations of individuals and variables already
  # provided: in MFA, data are considered from different points of view, but in
  # one single framework. Pages (2014), p141
  names(res.fwgacegg.MFA)
  res.fwgacegg.MFA.group=res.fwgacegg.MFA$group
  gcoord.fwgacegg=data.frame(res.fwgacegg.MFA.group$coord[,1:3])
  gcontrib=res.fwgacegg.MFA.group$contrib[,1:3]
  gcontrib=data.frame(gcontrib)
  names(gcontrib)=paste('contrib',names(gcontrib),sep='.')
  gcontrib$year=row.names(gcontrib)
  gcoord.fwgacegg$year=row.names(gcoord.fwgacegg)
  gcoord.fwgacegg=merge(gcoord.fwgacegg,gcontrib,by.x='year',by.y='year')
  row.names(gcoord.fwgacegg)=gcoord.fwgacegg$year
  gcoord.fwgacegg$contrib.Dim.1.2=gcoord.fwgacegg$contrib.Dim.1+gcoord.fwgacegg$contrib.Dim.2
  gcoord.fwgacegg$contrib.Dim.2.3=gcoord.fwgacegg$contrib.Dim.2+gcoord.fwgacegg$contrib.Dim.3
  head(gcoord.fwgacegg)  
  gcoord.fwgacegg$year=paste('20',gcoord.fwgacegg$year,sep='')
  range(gcoord.fwgacegg$Dim.1)
  range(gcoord.fwgacegg$Dim.2)
  range(gcoord.fwgacegg$Dim.3)
  
  # Plot of groups mean positions in MFA space:
  #******************************************
  x11()
  p12 <- ggplot(gcoord.fwgacegg,aes(Dim.1,Dim.2,label=year))+geom_text(aes(colour = contrib.Dim.1.2))+
    scale_colour_continuous(name="Contribution (%)")
  p12
  dev.print(device=png,
            filename=paste(path.export,'WGACEGGadultFishMFA12groupsBiplot.png',sep=''),
            units='cm',width=29,height=21,res=300)
  
  plot(gcoord.fwgacegg$year,gcoord.fwgacegg$Dim.1,type='b',ylim=c(0,1),ylab='MFA',xlab='')
  lines(gcoord.fwgacegg$year,gcoord.fwgacegg$Dim.2,lty=2,type="b")
  legend('topleft',c('MFA1','MFA2'),lty=seq(2))
  
  # p23 <- ggplot(gcoord,aes(Dim.2,Dim.3,label=year))+geom_text(aes(colour = contrib.Dim.2.3))+
  #   scale_colour_continuous(name="Contribution (%)")
  # multiplot(p12,p23)

  # 6.2. Eventually, study years partial axes correlations with MFA axes --------
  #****************************************
  # We have already insisted on the need to connect the inertias from the MFA
  # and those in the separate PCAs (see Tables 4.4 and 4.5). It is also useful to
  # connect the factors of the MFA with those of the separate PCAs (also known
  # as partial axes), both to understand better the effects of weighting and to enrich
  # interpretation of the analyses. In order to do this, the latter are projected as
  # supplementary variables (see Figure 4.5). Pages 2014, p88
  
  # Partial axes correlations
  res.fwgacegg.MFA.partial.axes=res.fwgacegg.MFA$partial.axes
  ddres.pyears=data.frame(res.fwgacegg.MFA.partial.axes$cor)
  head(ddres.pyears)
  ddres.pyears[row.names(ddres.pyears)=='Dim1.14',]
  ddres.pyears.12=data.frame(res.fwgacegg.MFA.partial.axes$cor[,1:2])
  ddres.pyears.12s=ddres.pyears.12[round(abs(ddres.pyears.12$Dim.1))>=0.6|
                                     round(abs(ddres.pyears.12$Dim.2))>=0.6,]
  ddres.pyears.12s$Dim=factor(gsub('[.]','',substr(row.names(ddres.pyears.12s),1,5)),
                              ordered=TRUE,levels=paste('Dim',seq(10),sep=''))
  ddres.pyears.12s$Year=gsub('[.]','',substr(row.names(ddres.pyears.12s),6,8))
  ddres.pyears.12s.long=reshape(ddres.pyears.12s,varying=list(1:2),
                                direction='long',timevar='MFA',v.names='correlation')
  ddres.pyears.12s.long$MFA=paste('Dim',ddres.pyears.12s.long$MFA,sep='')
  ddres.pyears.12s.long$signif=abs(round(ddres.pyears.12s.long$correlation))>=0.6
  ddres.pyears.12s.long$MFA=paste('MFA',ddres.pyears.12s.long$MFA,sep='.')
  head(ddres.pyears.12s.long)  
  # Plot partial axes correlations with MFA axes
  p <- ggplot(ddres.pyears.12s.long[ddres.pyears.12s.long$Dim%in%c('Dim1','Dim2'),], 
              aes(Dim,Year,size=abs(correlation),colour=correlation))+geom_point()
  p+ scale_colour_gradientn(colours = viridis(3)) + facet_grid(.~MFA)+
    theme(axis.text.x=element_text(angle=90, hjust=1,size=rel(1)),axis.title.y = element_blank(),
          axis.title.x = element_blank()) + guides(size=FALSE)

  # 6.3. Maps of partial individuals (map cell:year pairs) coordinates in MFA space ---------
  #***********************************
  MFAcoord.fwgacegg=res.fwgacegg.MFA$ind$coord
  pindCoord.fwgacegg=res.fwgacegg.MFA$ind$coord.partiel
  names(res.fwgacegg.MFA$ind)
  MFAyears=unlist(strsplit(row.names(pindCoord.fwgacegg),split='[.]'))[seq(2,(2*length(row.names(pindCoord.fwgacegg))),2)]
  lyears=unique(MFAyears)
  pindCoord.fwgaceggan=pindCoord.fwgacegg-MFAcoord.fwgacegg
  
  # 6.3.1. Maps of partial individuals coordinates on MFA1,2,3, one plot per year ---------------- 
  #*****************************
  for (i in 1:length(lyears)){
    dfi=pindCoord.fwgacegg[MFAyears==lyears[i],]-MFAcoord.fwgacegg
    dim(MFAcoord.fwgacegg)
    head(dfi)
    BoB.MFA.rasterPlot(MFAres=dfi,nMFA=2,cellsel.fwgacegg,pipo,path.export=NULL,
                       plotit=list(MFA123coord=TRUE,MFA123timevar=FALSE,
                                   MFA123contrib=FALSE,
                                   MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                   MFA123coordMcontrib=FALSE,MFA123pind=FALSE),funSel='mean',
                       ptitle=paste('20',lyears[i],sep=''),lptheme = viridisTheme())
  }
  # 6.3.2. Maps of partial individuals coordinates anomalies, one mosaic plot per MFA1,2,3, with all years
  #---------------------
  res=BoB.MFA.rasterPlot(MFAres=res.fwgacegg.MFA,nMFA=2,cellsel.fwgacegg,pipo,
                         path.export=path.export,
                         plotit=list(MFA123coord=TRUE,MFA123contrib=FALSE,
                                     MFA123timevar=FALSE,
                                     MFA123cos2=FALSE,MFA123coordMcos2=FALSE,
                                     MFA123coordMcontrib=FALSE,MFA123pind=TRUE),funSel='mean',
                         ptitle=paste(''),lptheme=BuRdTheme,anomalit=TRUE,ux11=TRUE)

 
