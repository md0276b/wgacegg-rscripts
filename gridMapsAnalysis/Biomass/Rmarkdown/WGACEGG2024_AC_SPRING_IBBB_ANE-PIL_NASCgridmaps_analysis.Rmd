---
title: Space time analysis of anchovy and sardine acoustic densities from WGACEGG spring joint survey
  maps
author: "Mathieu Doray"
date: "19/11/2024"
output:
  beamer_presentation: default
  ioslides_presentation: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
pref='/media/mathieu/EchoSonde/Campagnes/'
prefix='C:/Users/mdoray/Documents/Campagnes/'
path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
path.grids1=paste(path.grids,"AC_SPRING_IBBB/",sep='')
path.export.fwgacegg.EOF=
    paste(prefix,"WGACEGG/Results/gridMaps/AdultFish/EOF/",sep='')
path.export.fwgacegg.EOF.final=
    paste(prefix,"WGACEGG/Results/gridMaps/AdultFish/EOF/Final/",sep='')
```

## Introduction

-   Space time analysis of anchovy and sardine acoustic densities (NASC) from WGACEGG spring joint survey

## Data

-   Anchovy and sardine gridmaps of acoustic densities from WGACEGG spring joint survey, 2003-2023

    -   Years removed: Pelago in winter in 2003, no gulf of cadiz in 2004, no Pelago in 2012

## Methods (1)

-   Empirical Orthogonal Function (EOF) analysis performed on NASC gridmaps for anchovy and sardine separately
-   Empirical Orthogonal Function (EOF, Petitgas et al. 2014)
    -   Linear decomposition of series of maps:
        -   Average map + residuals / anomaly maps (time)
        -   EOF characterise variability in residuals / anomaly maps, based on Principal Component Analysis

## Methods (2)

-   Empirical Orthogonal Function (EOF, Petitgas et al. 2014)
    -   Z(t,s)=Zbar(.,s) + sum( Um(t).Em\^T(s))
        -   Z(t,s) variable under study at time t and spatial coordinate s,
        -   Zbar(.,s) is the average map at each coordinate s,
        -   Em (s) the eigenvectors or EOFs (principal spatial modes) scaled to unity,
        -   Um(t) the EOF amplitudes (principal components) scaled to the square root of the non-null eigenvalues associated with the EOFs.
        -   “local” explained variance of the residuals explained by Um(t).Em\^T(s)

## Methods (3): EOF example, anchovy springtime NASC

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'EOF1diagPlotsAnchovy-logNASC.png',sep=''))

```

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'EOF2diagPlotsAnchovy-logNASC.png',sep=''))

```

## Methods (4): EOF example, anchovy NASC EOF1 maps

::: columns
::: {.column width="50%"}
```{r, echo=FALSE, out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'EOF1diagPlotsAnchovy-logNASC.png',sep=''))

```

Time series of EOF1 amplitudes * EOF1 map -> Maps of EOF1 per year

:::
::: {.column width="50%"}
```{r, echo=FALSE, out.width="100%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'AC_SPRING_IBBB.ANE-EOF1-years_Maps.png',sep=''))

```
:::
:::

## Results: anchovy NASC

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'EOF1diagPlotsAnchovy-logNASC.png',sep=''))

```

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'EOF2diagPlotsAnchovy-logNASC.png',sep=''))

```

## Results: sardine NASC

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'EOF1diagPlotsSardine-logNASC.png',sep=''))

```

```{r, echo=FALSE, out.width="75%",fig.align='center'}

knitr::include_graphics(paste(path.export.fwgacegg.EOF.final,'EOF2diagPlotsSardine-logNASC.png',sep=''))

```

## Discussion

-   EOF analysis efficiently characterise core distribution areas (average map) and significant variability around it (EOF maps and amplitudes)
-   EOF analysis could easily be applied to WGACEGG gridmaps every year to monitor changes in adult, spawning and environment
    -   Providing that time series of maps are available on the sharepoint

## Conclusions

-   Perspectives:

    -   up-to-date surface salinity (SSS) and temperature (SST) gridmaps series on the sharepoint, eventually add satellite Chl-a

    -   perform EOF on SSS, SST and Chl-a

    -   model SPF EOFs with hydrobiology EOFs, to try to explain space time changes

    -   same approach with egg gridmaps / other seasons?

    -   publish WGAECGG gridmaps datasets, with (data) paper?
