library(EchoR)
library(FactoMineR)
library(rasterVis)
library(latticeExtra)
library(vegan)
library(maps)
library(maptools)
library(sp)
library(viridisLite)
library(ggplot2)
library(scales)
library(NbClust)
library(factoextra)
library(gridExtra)
library(rmarkdown)
library(corrplot)
library(metR)
library(ggpattern)

#***************************************************************************************************************************************
#                                               Multiple Factor Analysis with FactoMineR tutorial
#***************************************************************************************************************************************
# Author: mathieu.doray@ifremer.fr
# Requires EchoR >= 1.3.5

# 1. Import grid map data and convert them into raster package objects --------
#********************************************************************

  # path to donnees2 drive
  #*********************************
  donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
  
  # path to grid files
  #*********************************
  prefix=paste(donnees2,'Campagnes/',sep='')
  prefix=paste('D:/Campagnes/',sep='')
  prefix='E:/Campagnes/'
  prefix='/media/mathieu/EchoSonde/Campagnes/'
  prefix='D:/Documents/Campagnes/'
  prefix='C:/Users/mdoray/Documents/Campagnes/'
  path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
  path.grids1=paste(path.grids,"AC_SPRING_IBBB/",sep='')
  path.export.fwgacegg.EOF=
    paste(prefix,"WGACEGG/Results/gridMaps/AdultFish/EOF/",sep='')
  dir.create(path.export.fwgacegg.EOF,recursive = TRUE)
  path.export.fwgacegg.EOF.final=
    paste(prefix,"WGACEGG/Results/gridMaps/AdultFish/EOF/Final/",sep='')
  dir.create(path.export.fwgacegg.EOF.final,recursive = TRUE)
  
  # List grid files 
  #*********************************
  lffwgacegg=list.files(path.grids1,pattern='*.txt')
  #lfs=strsplit(lf,split='[.]',)
  lffwgaceggs=data.frame(t(data.frame(strsplit(lffwgacegg,split='[.]'))))
  lp=as.character(lffwgaceggs[,1])
  lffwgaceggs2=data.frame(t(data.frame(strsplit(lp,split='_'))))
  row.names(lffwgaceggs2)=seq(length(lffwgaceggs2[,1]))
  lffwgaceggs=cbind(lffwgaceggs,lffwgaceggs2)
  head(lffwgaceggs)
  names(lffwgaceggs)=c('pat','ext','method','sp','var','filter')
  lffwgaceggs$path=lffwgacegg
  row.names(lffwgaceggs)=seq(length(lffwgaceggs[,1]))

  # Import grid and raster files, bind grid maps
  #***************************************************
  for (i in 1:dim(lffwgaceggs)[1]){
    pat=paste(path.grids1,lffwgaceggs$path[i],sep='')
    mati=read.table(file=pat,header=T,sep=";")
    mati$sp=lffwgaceggs$sp[i]
    head(mati)
    if (i==1){
      FishNASCSp=mati
    }else{
      FishNASCSp=rbind(FishNASCSp,mati)
    }
    # graphics.off()
    # resras=grid2rasterStack(pat=mati,path1=NULL,
    #                         varid=paste(lffwgaceggs$sp[i],
    #                                     lffwgaceggs$var[i],
    #                                     sep=' '),
    #                         anomaly=TRUE)
    # assign(paste(lffwgaceggs$sp[i],'rasterStack',sep='.'),resras)
    # graphics.off()
    # Import raster stacks format, compute mean and SD maps and plot everything:
    #pat2=paste(paste(path.grids1,'Rasters/',sep=''),lffwgaceggrss$path[i],sep='')
    #rasti=raster(pat2)
  }
  sort(unique(FishNASCSp$Xgd))
  sort(unique(FishNASCSp$Ygd))

  # Plot mosaic maps -----
  dfiB=FishNASCSp[FishNASCSp$sp=='ENGR-ENC',]
  mat=grid.plot(
      pat=dfiB,input='gridDataframe',ux11=TRUE,pat2=NULL,
      plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=FALSE,mosaic=TRUE),
      namz=paste(unique(dfiB$sp),"NASC"))
  graphics.off()
  
# 2. Prepare datasets for EOF -------------------
  #********************************************************************
  # 2.1. Select common species ---------------
  #***************************
  head(FishNASCSp)
  sort(unique(FishNASCSp$Year))
  unique(FishNASCSp$Xgd)
  table(FishNASCSp$Year)
  unique(FishNASCSp$sp)
  FishNASCSp$spi=substr(FishNASCSp$sp,1,8)
  FishNASCSpp=FishNASCSp[FishNASCSp$Zvalue>0&!is.na(FishNASCSp$Zvalue),]
  table(FishNASCSpp$spi,FishNASCSpp$Year)
  OccurenceFrequencySp=apply(table(FishNASCSpp$spi,FishNASCSpp$Year)>0,1,sum)/length(unique(FishNASCSpp$Year))
  sort(OccurenceFrequencySp)
  FishNASCSps=FishNASCSp[FishNASCSp$spi%in%c('ENGR-ENC','SARD-PIL'),]
  head(FishNASCSps)
  
  # 2.2. Select years ---------------
  #***************************
  FishNASCSps=FishNASCSps[!FishNASCSps$Year%in%c(2003,2004,2009,2012,2020,2021),]
  FishNASCSps=FishNASCSps[!FishNASCSps$Year%in%c(2003,2004,2012,2020),]
  sort(unique(FishNASCSps$Year))
  head(FishNASCSps)
  
  # -> Pelago in winter in 2003, no gulf of cadiz in 2004, no Pelago in 2012

  # 3. EOFs per species ---------
  #********************************
  # 3.1. Anchovy EOF ---------
  FishNASCSps.ane=FishNASCSps[
    !is.na(FishNASCSps$Zvalue)&FishNASCSps$sp=='ENGR-ENC',]
  head(FishNASCSps.ane)
  summary(FishNASCSps.ane$Zvalue)
  FishNASCSps.ane$cell=paste(FishNASCSps.ane$Xgd,FishNASCSps.ane$Ygd,sep='-')
  FishNASCSps.ane$logNASC=log(FishNASCSps.ane$Zvalue+1)
  uxy=unique(FishNASCSps.ane[,c('Xgd','Ygd','cell')])
  dim(uxy)
  plot(uxy$Xgd,uxy$Ygd)
  length(unique(FishNASCSps.ane$cell))
  length(FishNASCSps.ane$cell)
  plot(FishNASCSps.ane$Xgd,FishNASCSps.ane$Ygd)

# Check sampling schemes
  lyears.ane=unique(FishNASCSps.ane$Year)
  graphics.off()
  for (i in 1:length(lyears.ane)){
    dfi=FishNASCSps.ane[FishNASCSps.ane$Year==lyears.ane[i],]
    dfiWoNA=dfi[!is.na(dfi$logNASC),]
    dfiNA=dfi[is.na(dfi$logNASC),]
    plot(dfiWoNA$Xgd,dfiWoNA$Ygd,main=lyears.ane[i])
    points(dfiNA$Xgd,dfiNA$Ygd,pch=2)
    coast()
  }  
  
#keep cells without NA
  Nyears.cell=data.frame(Nyears=table(FishNASCSps.ane$cell))
  names(Nyears.cell)[1]='cell'
  FishNASCSps.ane=merge(FishNASCSps.ane,Nyears.cell,by.x='cell',by.y='cell')
  FishNASCSps.ane.woNA=FishNASCSps.ane[
    FishNASCSps.ane$Nyears.Freq==max(FishNASCSps.ane$Nyears.Freq),]
  FishNASCSps.ane.woNA.xy=unique(FishNASCSps.ane.woNA[,c('Xgd','Ygd','cell')])
  plot(FishNASCSps.ane.woNA.xy$Xgd,FishNASCSps.ane.woNA.xy$Ygd)
  coast()

  AC_SPRING_IBBB.ANE.EOF.res=resEOF(df=FishNASCSps.ane.woNA,xname='Xgd',yname='Ygd',
                         zname='logNASC',tname='Year',
                         path.export.results=path.export.fwgacegg.EOF,
                         nEOF=5,addinverse.eof=c(FALSE,FALSE,FALSE,FALSE,FALSE),
                         nsp="Anchovy",signif.thr=0.5,plot.all=FALSE)
  graphics.off()
  
  AC_SPRING_IBBB.ANE.EOF.res=resEOF(
    df=FishNASCSps.ane.woNA,xname='Xgd',yname='Ygd',
    zname='logNASC',tname='Year',
    path.export.results=path.export.fwgacegg.EOF.final,
    nEOF=2,addinverse.eof=c(FALSE,TRUE),
    nsp="Anchovy",signif.thr=0.5,plot.all=FALSE)
  
  # 3.2. Sardine EOF ---------

  FishNASCSps.pil=FishNASCSps[
    !is.na(FishNASCSps$Zvalue)&FishNASCSps$sp=='SARD-PIL',]
  head(FishNASCSps.pil)
  summary(FishNASCSps.pil$Zvalue)
  FishNASCSps.pil$cell=paste(FishNASCSps.pil$Xgd,FishNASCSps.pil$Ygd,sep='-')
  FishNASCSps.pil$logNASC=log(FishNASCSps.pil$Zvalue+1)
  uxy=unique(FishNASCSps.pil[,c('Xgd','Ygd','cell')])
  dim(uxy)
  plot(uxy$Xgd,uxy$Ygd)
  length(unique(FishNASCSps.pil$cell))
  length(FishNASCSps.pil$cell)
  plot(FishNASCSps.pil$Xgd,FishNASCSps.pil$Ygd)
  
  # Check sampling schemes
  lyears.pil=unique(FishNASCSps.pil$Year)
  graphics.off()
  for (i in 1:length(lyears.pil)){
    dfi=FishNASCSps.pil[FishNASCSps.pil$Year==lyears.pil[i],]
    dfiWoNA=dfi[!is.na(dfi$logNASC),]
    dfiNA=dfi[is.na(dfi$logNASC),]
    plot(dfiWoNA$Xgd,dfiWoNA$Ygd,main=lyears.pil[i])
    points(dfiNA$Xgd,dfiNA$Ygd,pch=2)
    coast()
  }  
  
  #keep cells without NA
  Nyears.cell.pil=data.frame(Nyears=table(FishNASCSps.pil$cell))
  names(Nyears.cell.pil)[1]='cell'
  FishNASCSps.pil=merge(FishNASCSps.pil,Nyears.cell.pil,by.x='cell',by.y='cell')
  FishNASCSps.pil.woNA=FishNASCSps.pil[
    FishNASCSps.pil$Nyears.Freq==max(FishNASCSps.pil$Nyears.Freq),]
  FishNASCSps.pil.woNA.xy=unique(FishNASCSps.pil.woNA[,c('Xgd','Ygd','cell')])
  plot(FishNASCSps.pil.woNA.xy$Xgd,FishNASCSps.pil.woNA.xy$Ygd)
  coast()
  
  AC_SPRING_IBBB.pil.EOF.res=resEOF(
    df=FishNASCSps.pil.woNA,xname='Xgd',yname='Ygd',
    zname='logNASC',tname='Year',
    path.export.results=path.export.fwgacegg.EOF,
    nEOF=5,addinverse.eof=c(FALSE,FALSE,FALSE,FALSE,FALSE),
    nsp="Sardine",signif.thr=0.5,plot.all=FALSE)
  graphics.off()
  
  AC_SPRING_IBBB.pil.EOF.res=resEOF(
    df=FishNASCSps.pil.woNA,xname='Xgd',yname='Ygd',
    zname='logNASC',tname='Year',
    path.export.results=path.export.fwgacegg.EOF.final,
    nEOF=2,addinverse.eof=c(FALSE,TRUE),
    nsp="Anchovy",signif.thr=0.5,plot.all=FALSE)
