library(EchoR)

#--------------------------------------------------
# Creates maps, MEDIAS surveys
#--------------------------------------------------
### load data
# Select variable of interest
vart='NASC'
varname='NASC'

# Blocking per species
#-----------------------------
# 8.3.1. Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
#-------------------------------------------------------------
# Define grid limits
x1=19;x2=27;y1=36;y2=41.5
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=2
# Define iteration number
ni=200

define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,maskit = FALSE,
                      poligonit =FALSE,shelf = FALSE)

# Grid and plot
#-----------------------------
path.grids="/home/mathieu/Documents/WGACEGG/grids/MEDIAS/"

gridNplot(df=df.MED,vart=vart,varname=varname,
          tname='year',xname='x',yname='y',sname='survey',
          path.grids=path.grids,p=0.98,spname='sp',
          zfilter="quantile",newbathy=TRUE,
          splot = c(0.05, 0.09, 0.054, 0.45),
          default.grid=FALSE,deep=-450,shallow=-50,bstep=450,
          bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3,
          lon1=x1,lon2=x2,lat1=y1,lat2=y2)
