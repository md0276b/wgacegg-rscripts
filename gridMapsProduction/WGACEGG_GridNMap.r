#*******************************************************************************************************
# STEP 8: MAPPING: BLOCK AVERAGING
#*******************************************************************************************************

# 8.1. Load packages
#****************************************************
library(EchoR)
library(raster)
library(ggplot2)
library(reshape2)
library(sf)

# 0. Set export paths ----------
#****************************************************
#prefix='C:/Users/mdoray/Documents/'
prefix='Z:/Campagnes/'
prefix='G:/Campagnes/'
prefix='D:/Campagnes/'
#prefix='~/Documents/Campagnes/'
#prefix='/media/mathieu/EchoSonde/Campagnes/'
#prefix='/home/mathieu/Documents/Campagnes/'
prefix='C:/Users/mdoray/Documents/Campagnes/'
#prefix='Z:/Campagnes/'
#prefix='/home/mathieu/Documents/Ifremer/WGACEGG2016files/WGACEGG/'
donnees2='/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/'
#prefix=paste(donnees2,'Campagnes/',sep='')
cyear=2024
path.raw=paste(prefix,"WGACEGG/Data/rawData/SA/",sep='')
path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
path.grids1=paste(path.grids,"AC_SPRING_IBBB/",cyear,'/',sep='')
dir.create(path.grids1,recursive = TRUE)
path.grids1.1=paste(path.grids1,"LastYears/",sep='')
dir.create(path.grids1.1,,recursive = TRUE)
path.grids2=paste(path.grids,"AC_SPRING_IBBBN/",cyear,'/',sep='')
dir.create(path.grids2,recursive = TRUE)
path.grids2.1=paste(path.grids2,"LastYears/",sep='')
dir.create(path.grids2.1)
#path.grids=paste(donnees2,'/Campagnes/WGACEGG/grids/',sep='')
path.echobase="/home/mathieu/Documents/WGACEGG/grids/WGACEGG4Echobase/"
path.echobase=paste(prefix,"/WGACEGG/grids/WGACEGG4Echobase/",sep='')
path.gridsTemp=paste("~/Documents/temp/grids/",sep='')
dir.create(path.gridsTemp,recursive = TRUE)

#****************************************************
# 0.1. Species table ---------------
#****************************************************
PT.sp=data.frame(country='PT',lsp=c("PIL","ANE","MAS","HOM","BOG"))
PT.sp$sp=c('SARD-PIL','ENGR-ENC','SCOM-COL','TRAC-TRU','BOOP-BOO')
FR.sp=data.frame(country='FR',lsp=c('CAPR-APE','CLUP-HAR','ENGR-ENC','MERL-MCC','MERL-MNG',
                                           'MICR-POU','SARD-PIL','SCOM-COL','SCOM-SCO','SPRA-SPR',
                                           'TRAC-MED','TRAC-TRU'))
FR.sp$sp=FR.sp$lsp
SP.sp=data.frame(country='SP',lsp=c("MICRO-POU","MICR-POU","SCOM-SCO","MERL-MER","TRAC-TRA","SARD-PIL","TRAC-PIC",
                                    "BOOP-BOO","SCOM-COL","CAPR-APE","MAUR-MUE","ENGR-ENC","MEGA-NOR",
                                    "TRAC-MED"),
                 sp=c("MICR-POU","MICR-POU","SCOM-SCO","MERL-MCC","TRAC-TRU","SARD-PIL","TRAC-PIC",
                      "BOOP-BOO","SCOM-COL","CAPR-APE","MAUR-MUE","ENGR-ENC","MEGA-NOR",
                      "TRAC-MED"))                 
sp.acegg=rbind(PT.sp,FR.sp,SP.sp)

#**********************************************************************
# 1. Spring surveys ----------------
#****************************************************
#****************************************************
## 1.1. Spanish spring fish NASC ----------
#****************************************************
path.raw=paste(prefix,'WGACEGG/Data/rawData/',sep='')
path.SP0=paste(path.raw,
               'SA/PELACUS/PELACUS2003_2012.csv',sep='')
path.SP1=paste(path.raw,
  'SA/PELACUS/PELACUSrawNASCgrid2013-19.csv',sep='')
path.SP2=paste(path.raw,
  'SA/PELACUS/PELACUSrawNASCgrid2021.csv',sep='')
path.SP3=paste(path.raw,
  'SA/PELACUS/PELACUSrawNASCgrid2022.csv',sep='')
path.SP4=paste(path.raw,
               'SA/PELACUS/PELACUSrawNASCgrid2023.csv',sep='')

# Import 2003-2012
sa.sp0=read.table(path.SP0,sep=';',header=TRUE)
head(sa.sp0)

library(reshape2)
sa.sp0.long <- melt(sa.sp0[,1:6], 
                    id.vars = c("Campaign","Year","Latitude","Longitude"))
#sa.sp0.long=reshape(
#  sa.sp0[,1:6],idvar = c("Campaign","Year","Latitude","Longitude"), 
#  varying = list(5:6),v.names = "SA", direction = "long",
#  times=names(sa.sp0)[5:6],timevar='sp')
head(sa.sp0.long)
row.names(sa.sp0.long)=seq(dim(sa.sp0.long)[1])
names(sa.sp0.long)=c("survey","year","y","x","sp","NASC")
sa.sp0.long$sp=as.character(sa.sp0.long$sp)
sa.sp0.long[sa.sp0.long$sp=='PIL','sp']="SARD-PIL"
sa.sp0.long[sa.sp0.long$sp=='ANE','sp']="ENGR-ENC"
sa.sp0.long$time=NA
unique(sa.sp0.long$sp)
  
# Import 2003-2019
sa.sp1=read.table(path.SP1,sep=',',header=TRUE)
head(sa.sp1)
unique(sa.sp1$sp)
unique(sa.sp1$year)
table(sa.sp1$year)
sa.sp1$sp=as.character(sa.sp1$sp)
#sa.sp1[sa.sp1$sp=='MICRO-POU','sp']='MICR-POU'
#sa.sp1[sa.sp1$sp=='TRAC-TRA','sp']='TRAC-TRU'
sa.sp1=sa.sp1[sa.sp1$sp!="MERL-MER_1",]
unique(sa.sp1$survey)
unique(sa.sp1$sp)
sa.sp1$time=paste(substr(sa.sp1$time,7,8),'/',substr(sa.sp1$time,5,6),'/',substr(sa.sp1$time,1,4),
                 substr(sa.sp1$time,10,18),sep='')
#sa.sp1[sa.sp0$survey=='PELACUS0315','time']
#sa.sp0[sa.sp0$survey=='PELACUS0316','time']
# sa.sp0[sa.sp0$survey=='PELACUS0315','time']=paste(substr(sa.sp0[sa.sp0$survey=='PELACUS0315','time'],7,8),
#                                                   '/',substr(sa.sp0[sa.sp0$survey=='PELACUS0315','time'],5,6),
#                                                   '/',substr(sa.sp0[sa.sp0$survey=='PELACUS0315','time'],1,4),
#                                                   substr(sa.sp0[sa.sp0$survey=='PELACUS0315','time'],10,18),sep='')

# sa.sp15=read.table(paste(prefix,'WGACEGG/grids/rawData/PELACUS0315rawNASCgrid.csv',
#                          sep=''),sep=',',header=TRUE)
# head(sa.sp15)
# sa.sp15=sa.sp15[,names(sa.sp0)]
#sa.sp=rbind(sa.sp0,sa.sp15)

# Import 2021
sa.sp2=read.table(path.SP2,sep=',',header=TRUE)
head(sa.sp2)
# invert x and y
sa.sp2$x0=sa.sp2$x
sa.sp2$x=sa.sp2$y
sa.sp2$y=sa.sp2$x0
unique(sa.sp2$survey)
unique(sa.sp2$sp)
sa.sp2$sp=as.character(sa.sp2$sp)
sa.sp2[sa.sp2$sp=='MICRO-POU','sp']='MICR-POU'
sa.sp2[sa.sp2$sp=='TRAC-TRA','sp']='TRAC-TRU'
#sa.sp2=sa.sp2[sa.sp2$sp!="MERL-MER_1",]
sa.sp2$time=paste(substr(sa.sp2$time,7,8),'/',substr(sa.sp2$time,5,6),'/',
                  substr(sa.sp2$time,1,4),substr(sa.sp2$time,10,18),sep='')

# Import 2022
sa.sp3=read.table(path.SP3,sep=',',header=TRUE)
head(sa.sp3)
# invert x and y if needed
#sa.sp3$x0=sa.sp3$x
#sa.sp3$x=sa.sp3$y
#sa.sp3$y=sa.sp3$x0
unique(sa.sp3$survey)
unique(sa.sp3$sp)
sa.sp3$sp=as.character(sa.sp3$sp)
sa.sp3[sa.sp3$sp=='MICRO-POU','sp']='MICR-POU'
sa.sp3[sa.sp3$sp=='TRAC-TRA','sp']='TRAC-TRU'
#sa.sp3=sa.sp3[sa.sp3$sp!="MERL-MER_1",]
sa.sp3$time=paste(substr(sa.sp3$time,7,8),'/',substr(sa.sp3$time,5,6),'/',
                  substr(sa.sp3$time,1,4),substr(sa.sp3$time,10,18),sep='')

# Import 2023
sa.sp4=read.table(path.SP4,sep=',',header=TRUE)
head(sa.sp4)
# invert x and y if needed
#sa.sp3$x0=sa.sp3$x
#sa.sp3$x=sa.sp3$y
#sa.sp3$y=sa.sp3$x0
unique(sa.sp4$survey)
unique(sa.sp4$sp)
sa.sp4$sp=as.character(sa.sp4$sp)
sa.sp4[sa.sp4$sp=='MICRO-POU','sp']='MICR-POU'
sa.sp4[sa.sp4$sp=='MICRO-POU_b','sp']='MICR-POU'
sa.sp4[sa.sp4$sp=='MICRO-POU_S','sp']='MICR-POU-JUV'
sa.sp4[sa.sp4$sp=='TRACH-MED','sp']='TRAC-MED'
sa.sp4[sa.sp4$sp=='TRACH-TRA','sp']='TRAC-TRU'
sa.sp4[sa.sp4$sp=='TRACH-TRA_S','sp']='TRAC-TRU-JUV'
sa.sp4[sa.sp4$sp=='SARD-PIL_b','sp']='SARD-PIL'
sa.sp4[sa.sp4$sp=='SARD-PIL_S','sp']='SARD-PIL-JUV'
sa.sp4[sa.sp4$sp=='SOMB-COL','sp']='SCOM-COL'
unique(sa.sp4$sp)
# Aggregate duplicated species
# dim(sa.sp4)
# sa.sp4=aggregate(
#   sa.sp4$NASC,
#   list(survey=sa.sp4$survey,year=sa.sp4$year,
#        time=sa.sp4$time,x=sa.sp4$x,y=sa.sp4$y,
#        sp=sa.sp4$sp),sum,na.rm=TRUE)
# dim(sa.sp4)
# names(sa.sp4)[7]='NASC'

#sa.sp4=sa.sp4[sa.sp4$sp!="MERL-MER_1",]
sa.sp4$time=paste(substr(sa.sp4$time,7,8),'/',substr(sa.sp4$time,5,6),'/',
                  substr(sa.sp4$time,1,4),substr(sa.sp4$time,10,18),sep='')

# Import 2024
path.SP5=paste(path.raw,
               'SA/PELACUS/PELACUS_2024_rawNASC.csv',sep='')
sa.sp5=read.table(path.SP5,sep=';',header=TRUE)
head(sa.sp5)
names(sa.sp4)
names(sa.sp5)=c("survey","year","date","time","x","y","sp","NASC")
# invert x and y if needed
#sa.sp3$x0=sa.sp3$x
#sa.sp3$x=sa.sp3$y
#sa.sp3$y=sa.sp3$x0
unique(sa.sp5$survey)
unique(sa.sp5$sp)
sa.sp5$sp=as.character(sa.sp5$sp)
sa.sp5[sa.sp5$sp=='MICRO-POU','sp']='MICR-POU'
sa.sp5[sa.sp5$sp=='MICRO-POU_b','sp']='MICR-POU'
sa.sp5[sa.sp5$sp=='MICRO-POU_S','sp']='MICR-POU-JUV'
sa.sp5[sa.sp5$sp=='TRACH-MED','sp']='TRAC-MED'
sa.sp5[sa.sp5$sp=='TRACH-TRA','sp']='TRAC-TRU'
sa.sp5[sa.sp5$sp=='TRACH-TRA_S','sp']='TRAC-TRU-JUV'
sa.sp5[sa.sp5$sp=='TRAC-TRA_S','sp']='TRAC-TRU-JUV'
sa.sp5[sa.sp5$sp=='SARD-PIL_b','sp']='SARD-PIL'
sa.sp5[sa.sp5$sp=='SARD-PIL_B','sp']='SARD-PIL'
sa.sp5[sa.sp5$sp=='SARD-PIL_S','sp']='SARD-PIL-JUV'
sa.sp5[sa.sp5$sp=='SOMB-COL','sp']='SCOM-COL'
unique(sa.sp5$sp)
# Aggregate duplicated species
# dim(sa.sp4)
# sa.sp4=aggregate(
#   sa.sp4$NASC,
#   list(survey=sa.sp4$survey,year=sa.sp4$year,
#        time=sa.sp4$time,x=sa.sp4$x,y=sa.sp4$y,
#        sp=sa.sp4$sp),sum,na.rm=TRUE)
# dim(sa.sp4)
# names(sa.sp4)[7]='NASC'

# Remove "UNA"
sa.sp5=sa.sp5[sa.sp5$sp!='UNA',]

#sa.sp4=sa.sp4[sa.sp4$sp!="MERL-MER_1",]
sa.sp5$time=paste(substr(sa.sp5$time,7,8),'/',substr(sa.sp5$time,5,6),'/',
                  substr(sa.sp5$time,1,4),substr(sa.sp5$time,10,18),sep='')

# Bind all years
df.SP.spring=rbind(sa.sp0.long[,names(sa.sp1)],sa.sp1,
                   sa.sp2[,names(sa.sp1)],sa.sp3[,names(sa.sp1)],
                   sa.sp4[,names(sa.sp1)],sa.sp5[,names(sa.sp1)])
head(df.SP.spring)
unique(df.SP.spring$year)
unique(df.SP.spring$sp)

# Replace local species name by ACEGG species names
unique(df.SP.spring$sp)
names(df.SP.spring)[names(df.SP.spring)=='sp']='sp0'
df.SP.spring=merge(df.SP.spring,sp.acegg[sp.acegg$country=='SP',
                           c('lsp','sp')],by.x='sp0',by.y='lsp')
unique(df.SP.spring$sp)
df.SP.spring=df.SP.spring[,-1]
head(df.SP.spring)
df.SP.spring$survey=paste('PELACUS',df.SP.spring$year,sep='')

#plot(df.SP.spring$x,df.SP.spring$y,type='n')
#text(df.SP.spring$x,df.SP.spring$y,df.SP.spring$year)
#coast()

ggplot(data=df.SP.spring,aes(x=x,y=y))+geom_point()+
  facet_wrap(vars(year))

df.SP.spring[df.SP.spring$sp=='SCOM-COL',]

#remove pelacus2017 south Biscay
df.SP.spring=df.SP.spring[!(df.SP.spring$survey=="PELACUS2017"&df.SP.spring$x>-3
                           &df.SP.spring$y>43.7),]
df.SP.springs=df.SP.spring[df.SP.spring$survey=="PELACUS2013"&df.SP.spring$sp=='ENGR-ENC',]
# add zero anchovy biomass in 2013
df.SP.springs0=df.SP.spring[df.SP.spring$survey=="PELACUS2013",]
table(df.SP.springs0$sp)
table(df.SP.springs0$year)
df.SP.springs0$NASC=0
df.SP.springs0$sp='ENGR-ENC'
df.SP.springs0=unique(df.SP.springs0)
dim(df.SP.springs0)
df.SP.spring=rbind(df.SP.spring,df.SP.springs0)
head(df.SP.spring)
table(df.SP.spring$sp)
table(df.SP.spring$year)
plot(df.SP.spring$x,df.SP.spring$y)
coast()

df.SP.spring.22=df.SP.spring[df.SP.spring$year==2022,]
df.SP.spring.23=df.SP.spring[df.SP.spring$year==2023,]
plot(df.SP.spring.22$x,df.SP.spring.22$y)
points(df.SP.spring.23$x,df.SP.spring.23$y,
       pch=16,col=2)
coast()

#****************************************************
## 1.2. Portuguese spring fish NASC ----------------------
#****************************************************
#path.PT='/home/mathieu/Documents/WGACEGG/grids/rawData/PELAGO/'
#path.PT=paste(donnees2,'Campagnes/WGACEGG/Data/grids/rawData/SA/PELAGO/',sep='')
path.PT=paste(prefix,'WGACEGG/Data/rawData/SA/PELAGO/',sep='')

fpt=list.files(path.PT)
fpt=fpt[fpt!='zold']
fpt.df=data.frame(t(data.frame(strsplit(fpt,split=' '))))
names(fpt.df)=c('label','year0')
fpt.df$year=substr(fpt.df[,2],1,4)
row.names(fpt.df)=seq(dim(fpt.df)[1])
fpt.df$fname=fpt
for (i in 1:dim(fpt.df)[1]){
  sepi=','
  #if (i==15) sepi=';'
  dfi=read.table(paste(path.PT,fpt.df$fname[i],sep=''),sep=sepi,
                 header=TRUE)
  if (dim(dfi)[2]==1){
    sepi=';'
    dfi=read.table(paste(path.PT,fpt.df$fname[i],sep=''),sep=sepi,
                   header=TRUE)
  }
  head(dfi)
  names(dfi)[names(dfi)=="LongDec"]="long"
  names(dfi)[names(dfi)=="Latdec"]="lat"
  names(dfi)[names(dfi)=="Longitude"]="long"
  names(dfi)[names(dfi)=="latitude"]="lat"
  names(dfi)[names(dfi)=="x"]="long"
  names(dfi)[names(dfi)=="y"]="lat"
  names(dfi)[names(dfi)=="nsurvey"]="survey"
  #dfi=read.csv2(paste(path.PT,fpt[i],sep=''))
  dfi$year=fpt.df$year[i]
  if (!"survey"%in%names(dfi)){
    dfi$survey=paste("PELAGO",fpt.df$year[i],sep='')
  }
  head(dfi)
  if (i==1) {
    sa.pt=dfi
    namesi=names(sa.pt)[names(sa.pt)%in%names(dfi)]
  }else{
    namesi=names(sa.pt)[names(sa.pt)%in%names(dfi)]
    #dfi=dfi[,namesi]
    sa.pt=plyr::rbind.fill(sa.pt,dfi)
  }  
}
head(sa.pt)
sa.pt[is.na(sa.pt$Day),]
unique(sa.pt$survey)
sort(unique(sa.pt$year))
unique(sa.pt$Day)
dim(sa.pt)
sa.pt=sa.pt[!is.na(sa.pt$year),]
dim(sa.pt)
table(sa.pt$year)
sa.pt$survey=as.character(sa.pt$survey)
table(sa.pt$survey)
sa.pt[!sa.pt$survey%in%c('PELAGO16','pelago17'),'survey']=paste(
  'PELAGO',sa.pt[!sa.pt$survey%in%c('PELAGO16','pelago17'),'survey'],
  sep='')
sa.pt[sa.pt$year==2024,]
sa.pt[is.na(sa.pt$Day),]
unique(sa.pt$Month)
sa.pt[!is.na(sa.pt$Day)&nchar(sa.pt$Day)==1,'Day']=paste(
  '0',sa.pt[!is.na(sa.pt$Day)&nchar(sa.pt$Day)==1,'Day'],sep='')
sa.pt[!is.na(sa.pt$Month)&nchar(sa.pt$Month)==1,'Month']=paste(
  '0',sa.pt[!is.na(sa.pt$Month)&nchar(sa.pt$Month)==1,'Month'],sep='')
sa.pt[!is.na(sa.pt$Hour)&nchar(sa.pt$Hour)==1,'Hour']=paste(
  '0',sa.pt[!is.na(sa.pt$Hour)&nchar(sa.pt$Hour)==1,'Hour'],sep='')
sa.pt[!is.na(sa.pt$Min)&nchar(sa.pt$Min)==1,'Min']=paste(
  '0',sa.pt[!is.na(sa.pt$Min)&nchar(sa.pt$Min)==1,'Min'],sep='')
hist(sa.pt$long)
sa.pt[sa.pt$year==2024&sa.pt$long>0,]
ggplot(sa.pt,aes(x=long)) +
  geom_histogram()+facet_wrap(.~year,scales = 'free')
# ESU maps per year
ggplot(sa.pt,aes(x=long,y=lat)) +
  geom_point()+facet_wrap(.~year,scales = 'free')

sa.pt08=sa.pt[sa.pt$year==2008,]
sa.pt13=sa.pt[sa.pt$year==2013,]
sa.pt14=sa.pt[sa.pt$year==2014,]
sa.pt15=sa.pt[sa.pt$year==2015,]
sa.pt18=sa.pt[sa.pt$year==2018,]
sa.pt19=sa.pt[sa.pt$year==2019,]
hist(sa.pt18$long)
hist(sa.pt18$lat)
sa.pt18[sa.pt18$long<(-10),]
sa.pt14=correct.positions(df=sa.pt14,xname='long',yname='lat',
                        asNewColumn=FALSE)
#sa.pt15=correct.positions(df=sa.pt15,xname='long',yname='lat',
#                          asNewColumn=FALSE)
plot(sa.pt15$lon,sa.pt15$lat)
points(sa.pt14$lon,sa.pt14$lat)
coast()
plot(sa.pt14$lon,sa.pt14$lat)
# Corrected dataset
sa.pt.cor=rbind(sa.pt[!sa.pt$year%in%c(2014),],sa.pt14)
sort(unique(sa.pt.cor$year))
# ESU maps per year
ggplot(sa.pt.cor,aes(x=long,y=lat)) +
  geom_point()+facet_wrap(.~year)

# # Import 2016 data
# path.PT16=paste(prefix,'WGACEGG/grids/rawData/NASC-PELAGO16-POR.csv',sep='')
# sa.pt.cor16=read.table(path.PT16,sep=';',header=TRUE)
# head(sa.pt.cor16)
#sa.pt.cor=rbind(sa.pt.cor[!sa.pt.cor$year%in%c(2014,2015),],sa.pt.cor14,sa.pt.cor15)

sa.pt.cor$x=sa.pt.cor$long;sa.pt.cor$y=sa.pt.cor$lat
min(sa.pt.cor$y);max(sa.pt.cor$y)
plot(sa.pt.cor$x,sa.pt.cor$y,asp=1/cos((mean(sa.pt.cor$x)*pi)/180))
coast()

# lsp.PT=c('PIL','ANE','MAS','HOM','BOG')
# for (i in 1:length(lsp.PT)){
#   plot(sa.pt.cor$x,sa.pt.cor$y,cex=0.1,pch=16,col='grey80',
#        asp=1/cos((mean(sa.pt.cor$x)*pi)/180),main=lsp.PT[i],xlab='',
#        ylab='')
#   points(sa.pt.cor$x,sa.pt.cor$y,cex=log(sa.pt.cor[,lsp[i]]+1)/5,pch=16)
#   coast()
# }
sa.pt.cor$time=paste(paste(sa.pt.cor$Day,sa.pt.cor$Month,sa.pt.cor$year,sep='/'),
                 paste(sa.pt.cor$Hour,sa.pt.cor$Min,'00',
                       sep=':'))
head(sa.pt.cor)
lsp.PT=as.character(sp.acegg[sp.acegg$country=='PT','lsp'])
lsp.PT=names(sa.pt.cor)[!names(sa.pt.cor)%in%
                          c('survey','Day','Month','Year','Hour','Min',
                          'lat','long',"year","x","y","time")]
sa.pt.cor=unique(sa.pt.cor)

sa.pt.cors=sa.pt.cor[,c('survey','year','time','x','y',lsp.PT)]
head(sa.pt.cors)
PT.years=sort(unique(sa.pt.cors$year))

head(sa.pt.cors)
# Reshape per year to reduce processing time
for (i in 1:length(PT.years)){
  sa.pt.cors2=sa.pt.cors[sa.pt.cors$year==PT.years[i],]
  df.PTi=melt(sa.pt.cors2,
              variable.name = "sp", 
              value.name = "NASC", 
                 id.vars = c('survey','year','time','x','y'))
  head(df.PTi)
  # df.PTi=reshape(sa.pt.cors2,varying=list(6:(6+length(lsp.PT)-1)),
  #                v.names = "NASC", 
  #               idvar = c('survey','year','time','x','y'),
  #               direction = "long",
  #               times=names(sa.pt.cors)[6:(6+length(lsp.PT)-1)],
  #               timevar='sp')
  row.names(df.PTi)=seq(dim(df.PTi)[1])
  if (i==1){
    df.PT=df.PTi
  }else{
    df.PT=rbind(df.PT,df.PTi)
  }
}
# Too long:
# df.PT=reshape(sa.pt.cors,varying=list(6:(6+length(lsp.PT)-1)),
#                v.names = "NASC", 
#                idvar = c('survey','year','time','x','y'),
#                direction = "long",
#                times=names(sa.pt.cors)[6:(6+length(lsp.PT)-1)],
#                timevar='sp')
# row.names(df.PT)=seq(dim(df.PT)[1])
head(df.PT)
unique(df.PT$sp)
sort(unique(df.PT$year))

# head(sa.pt.cor16)
# df.pt16=reshape(sa.pt.cor16,varying=list(3:5),v.names = "NASC", 
#                 idvar = c('LongDec','Latdec'),
#                 direction = "long",times=names(sa.pt16)[3:5],
#                 timevar='sp')
# head(df.pt16)
# df.pt16$time='08/02/2016 00:00:00'
# df.pt16$year=2016
# df.pt16$survey='PELAGOapr16'
# names(df.pt16)[1:2]=c('x','y')
# 
# df.PT=rbind(df.PT,df.pt16[,names(df.PT)])

# Replace local species name by ACEGG species names
head(df.PT)
names(df.PT)[names(df.PT)=='sp']='sp0'
unique(df.PT$sp)
df.PT=merge(df.PT,sp.acegg[sp.acegg$country=='PT',
                           c('lsp','sp')],by.x='sp0',by.y='lsp')
unique(df.PT$sp)
unique(df.PT$survey)
df.PT=df.PT[,-1]

# Select only spring surveys
df.PT.spring=df.PT[df.PT$survey!="PELAGOfev03",]
df.PT.spring=df.PT
head(df.PT.spring)
sort(unique(df.PT.spring$year))
sort(unique(df.PT.spring$survey))
sort(unique(df.PT.spring$sp))
table(df.PT.spring$sp,df.PT.spring$year)

# remove all species but anchovy and sardine
#df.PT.spring=df.PT.spring[
#  df.PT.spring$sp%in%c('SARD-PIL','ENGR-ENC'),]

unique(df.PT.spring[df.PT.spring$year==2018&
                      df.PT.spring$sp=='TRAC-TRU',"NASC"])
unique(df.PT.spring[df.PT.spring$year==2013&
                      df.PT.spring$sp=='TRAC-TRU','NASC'])
table(df.PT.spring$year)

#****************************************************
## 1.3. French fish NASC -------------
#****************************************************
#import PELGAS biomres
#****************************************************
path.data=paste(prefix,'PELGAS/Data/Biomasse/',sep='')
df0=read.table(paste(path.data,'biomres2_PELGAS.csv',sep=''),sep=';',
               header=TRUE)
head(df0)
df=aggregate(df0[,c('BB','BN','NASCsp')],
             list(survey=df0$CAMPAGNE,proc=df0$f,
                  time=df0$TC,x=df0$LONG,y=df0$LAT,sp=df0$GENR_ESP),sum)
names(df)=c("survey","proc","time","x","y","sp","biomass","abundance",
            "NASC" )
tf=format(strptime(df$time,format="%d/%m/%Y %H:%M:%S"))
df$year=substr(tf,1,4)
sort(unique(df$year))
head(df)
table(df$year)

# select PELGAS surveys
unique(df$survey)
df.FR.spring=df[substr(df$survey,1,6)=='PELGAS',]
dim(df.FR.spring)
unique(df.FR.spring$sp)
df.FR.spring[df.FR.spring$sp=='SCOM-JAP','sp']='SCOM-COL'

lsp.FR=c('CAPR-APE','CLUP-HAR','ENGR-ENC','MERL-MCC',
         'MICR-POU','SARD-PIL','SCOM-SCO','SPRA-SPR',
         'TRAC-MED','TRAC-TRU','SCOM-COL')

df.FR.spring=df.FR.spring[df.FR.spring$sp%in%lsp.FR,
          c("survey","year","time","x","y","sp","NASC")]

df.FR.spring[df.FR.spring$year==2011,]

table(df.FR.spring$year)
table(df.FR.spring$year,df.FR.spring$sp)

# df.FR.springs=df.FR.spring[df.FR.spring$year%in%
#                              c(2015,2016)&
#                              df.FR.spring$sp%in%
#                              c('ENGR-ENC','SARD-PIL'),]
# write.table(df.FR.springs,
#             'D:/R/echor/EchoRpackage/EchoR/data/gridMapDataTest.txt',
#             row.names=FALSE)

#****************************************************
## 1.4. Bind spring surveys -----
#****************************************************
df.SP.spring=df.SP.spring[,names(df.PT.spring)]
df.SP.spring$nsurvey='PELACUS'
df.FR.spring=df.FR.spring[,names(df.PT.spring)]
df.FR.spring$nsurvey='PELGAS'
df.PT.spring$nsurvey='PELAGO'
names(df.PT.spring)
names(df.SP.spring)
names(df.FR.spring)

AC_SPRING_IBBB=rbind(df.PT.spring,df.FR.spring,df.SP.spring)
AC_SPRING_IBBB$survey='AC_SPRING_IBBB'
head(AC_SPRING_IBBB)
unique(AC_SPRING_IBBB$sp)
table(AC_SPRING_IBBB$year)

# Unique sampling points
head(AC_SPRING_IBBB)
uxy=unique(AC_SPRING_IBBB[,c('year','x','y','nsurvey')])
plot(uxy$x,uxy$y,asp=1/cos((mean(uxy$x)*pi)/180))
coast()
lines(xpol-0.3,ypol-0.3,col=2)
# Unique sampling points per year
ggplot() +
  geom_point(data = uxy, 
              aes(x = x, y = y),col='blue')+
  theme(axis.title.x = element_blank(),
        axis.title.y = element_blank())+
  #geom_sf(data=sfPoly, fill="transparent",color="red", size=0.25)+
  #scale_fill_continuous(na.value="transparent",type = "viridis")+
  annotation_map(map_data("world"))+ #Add the map as a base layer before the points
  coord_quickmap()+  #Sets aspect ratio
  facet_wrap(.~year,nrow=4)

### 1.4.0. Map ESUs position for current year -------
uxys=uxy[uxy$year==cyear,]
dfs=uxys[uxys$nsurvey=='PELAGO',]
plot(dfs$x,dfs$y)

x11()
ggplot() +
  geom_point(data = uxys, 
             aes(x = x, y = y,colour=nsurvey),size=.1)+
  theme(axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        legend.title=element_blank())+
        ggtitle(paste('WGACEGG spring joint acoustic survey',cyear))+
  annotation_map(map_data("world"))+ #Add the map as a base layer before the points
  coord_quickmap()+ guides(colour = guide_legend(override.aes = list(size=1)))
# Save figure
dev.print(png, file=paste(path.grids1,
                          'AC-SPRING-IBBB_ESUpositions-',
                          cyear,
                          ".png",sep=""), 
          units='cm',width=15, height=20,res=300)

# Select years to define spring polygon
uxyu=unique(uxy[!uxy$year%in%c(2001,2010,2011,2014),c('x','y')])

table(AC_SPRING_IBBB$year)
head(AC_SPRING_IBBB)
AC.SPRING.IBBB.lsp0=unique(AC_SPRING_IBBB$sp)

# NASC bubble plots (takes time to display)
for (i in 1:length(AC.SPRING.IBBB.lsp0)){
  AC_SPRING_IBBBi=AC_SPRING_IBBB[
    AC_SPRING_IBBB$sp==AC.SPRING.IBBB.lsp0[i],]
  x11()
  par(bg='white')
  p1<-ggplot(data = AC_SPRING_IBBB, 
             aes(x = x, y = y, size = log(NASC+1)/10)) +
    geom_point(col=2,alpha=0.7,pch=1)+
    labs(title = 
           paste(AC.SPRING.IBBB.lsp0[i],'AC SPRING IBBB NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    facet_wrap(.~factor(year),nrow = 3)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids1,'bubbleplotsMosaic-',
                            AC.SPRING.IBBB.lsp0[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}

### 1.4.1. Data export ------------
# Export raw datan all years, all species 
write.table(AC_SPRING_IBBB,paste(path.raw,
                                  'AC-SPRING-IBBB-NASC.csv'),
            sep=';',row.names = FALSE)

# Export raw datan all years, all species, PELGAS 
write.table(AC_SPRING_IBBB[AC_SPRING_IBBB$nsurvey=='PELGAS',],
            paste(path.raw,'AC-SPRING-PELGAS2024-NASC.csv'),
            sep=';',row.names = FALSE)

head(AC_SPRING_IBBB)
AC_SPRING_IBBBxy=unique(AC_SPRING_IBBB[AC_SPRING_IBBB$year==cyear,
                                       c('survey','x','y')])

# Export last year sampling points 
head(AC_SPRING_IBBB)
AC_SPRING_IBBBxy=unique(AC_SPRING_IBBB[AC_SPRING_IBBB$year==cyear,
                                       c('survey','x','y')])

# Export selected years:
AC_SPRING_IBBBs=AC_SPRING_IBBB[
  AC_SPRING_IBBB$year%in%c(2018,2021)&
    AC_SPRING_IBBB$sp%in%c('ENGR-ENC','SARD-PIL'),]

head(AC_SPRING_IBBBs)

AC.SPRING.IBBB.lsp0s=unique(AC_SPRING_IBBBs$sp)

# NASC bubble plots (takes time to display)
for (i in 1:length(AC.SPRING.IBBB.lsp0s)){
  AC_SPRING_IBBBi=AC_SPRING_IBBBs[
    AC_SPRING_IBBBs$sp==AC.SPRING.IBBB.lsp0s[i],]
  x11()
  par(bg='white')
  p1<-ggplot(data = AC_SPRING_IBBBs, 
            aes(x = x, y = y,size=log(NASC+1))) +
            #aes(x = x, y = y)) +    
    geom_point(col=2,alpha=0.7,pch=1)+
    scale_size(range = c(.1, 1), name="NASC")+
    labs(title = 
           paste(AC.SPRING.IBBB.lsp0s[i],'AC SPRING IBBB NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    facet_wrap(.~factor(year),nrow = 3)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.gridsTemp,'bubbleplotsMosaicSel-',
                            AC.SPRING.IBBB.lsp0s[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}

write.table(AC_SPRING_IBBBs,paste(path.gridsTemp,
                                 'AC-SPRING-IBBB-NASC_ANE-PIL_2018-2021.csv'),
            sep=';',row.names = FALSE)

names(AC_SPRING_IBBBs)
uAC_SPRING_IBBBs=unique(AC_SPRING_IBBBs[,c('x','y')])
plot(uAC_SPRING_IBBBs$x,uAC_SPRING_IBBBs$y,cex=0.5)
coast()

### 1.4.2. If needed, create spring polygon ------------ 
#x11()
plot(uxyu$x,uxyu$y,asp=1/cos((mean(uxyu$y)*pi)/180))
lines(xpol-0.3,ypol-0.3,col=2)
coast()
#Draw polygon
#p=locator(type='l')
#poly.autumn=data.frame(x=p$x,y=p$y)
#plot(poly.autumn$x,poly.autumn$y,type='l')
#coast()

#OR alphahull style
mcosas=cos(uxyu$y*pi/180)
xxas=uxyu$x*mcosas
xxas=uxyu$x
yyas=uxyu$y
dfxys=data.frame(xxas,yyas)
plot(xxas,yyas)
coast()

library(alphahull)
valphas=.3
apolyas=ahull(xxas,yyas,alpha=valphas)
plot(apolyas,main=paste('alpha =',valphas))
coast()

# with ahull_track
library(spatstat.geom)
library(spatstat.core)
apolya.ggs <- ahull_track(xxas,yyas, alpha=valphas, nps = 3,sc=1)

dim(apolya.ggs[[i]]$data)
plot(apolya.ggs[[i]]$data$x,apolya.ggs[[i]]$data$y)

# get unique tracks
for(i in 1:length(apolya.ggs)) {
  apolya.ggs[[i]]$aes_params$colour <- "green3"
  tiis=apolya.ggs[[i]]$data
  tiis2=tiis[c(1,dim(tiis)[1]),]
  if (i==1){
    tiis2.db=tiis2
  }else{
    tiis2.db=rbind(tiis2.db,tiis2)
  }
}
tiis2.db=unique(tiis2.db)
plot(tiis2.db$x,tiis2.db$y)
coast()

# plot tracks
library(ggplot2)
ggplot(aes(x = xxas, y = yyas), data = dfxys)+ 
  geom_point(aes(x = xxas, y = yyas), data = dfxys, alpha = .5, 
             color = "darkred") +  apolya.ggs

# identify disjunct polygons
tiis2.db$polygon.id=NA
tiis2.db$di=NA
pid=1
dthr=100
for(i in 1:dim(tiis2.db)[1]) {
  if (i>1){
    tiis2.db[i,'di']=60*sqrt((tiis2.db[i-1,'x']-tiis2.db[i,'x'])^2+
                               (tiis2.db[i-1,'y']-tiis2.db[i,'y'])^2)
    if(tiis2.db[i,'di']>dthr){
      pid=pid+1
    }
  }else{
    tiis2.db[i,'di']=NA
  }
  tiis2.db[i,'polygon.id']=pid
}  
unique(tiis2.db$polygon.id)  
summary(tiis2.db$di)
hist(tiis2.db$di)

#tiis.db=rbind(tiis.db[1,],tiis.db,tiis.db[dim(tiis.db)[1],])
plot(tiis2.db$x,tiis2.db$y,type='l',col=tiis2.db$polygon.id,
     lty=tiis2.db$polygon.id)
text(tiis2.db$x,tiis2.db$y,tiis2.db$polygon.id)
coast()

# Create polygons and shapefile 
#***********************************
library(rgdal)
lpolyss=unique(tiis2.db$polygon.id)
for (i in 1:length(lpolyss)){
  polyixys=tiis2.db[tiis2.db$polygon.id==lpolyss[i],]
  #polyi=Polygon(polyixy[,c('x','y')])
  Srsi = Polygons(list(Polygon(polyixys[,c('x','y')])), paste("s",i,sep=''))
  #assign(paste('poly',i,sep='.'),Srsi,envir=.GlobalEnv)
  if (i==1){
    lSrsis=list(Srsi)
  }else{
    lSrsis=c(lSrsis,Srsi)
  }
}

SpPs = SpatialPolygons(lSrsis, seq(length(lSrsis)))
SpPs.df <- SpatialPolygonsDataFrame(SpPs, 
                                   data.frame(id=seq(length(lSrsis)), 
                                              row.names=row.names(SpPs)))
plot(SpPs,col=2)
plot(SpPs.df,col=2)
coast()

# union polygons
# Generate IDs for grouping
upoly.ids=rep(1,length(lSrsis))

# Merge polygons by ID
SpPs.union <- unionSpatialPolygons(SpPs, upoly.ids)

# Plotting
plot(SpPs.union, add = FALSE, border = "red", lwd = 2)

# Export file as shape file, for manual modification in GIS if needed
#***********
path.polygrid=paste(path.grids,'polygons/',sep='')
dir.create(path.polygrid)
path.polygrida=paste(path.grids,'polygons/WGACEGGspringPolygon.shp',sep='')
proj4string(SpPs.df)="+proj=longlat +datum=WGS84"
writeOGR(obj=SpPs.df, dsn=path.polygrida,layer='WGACEGGspringPolygon', 
         driver='ESRI Shapefile',overwrite_layer=TRUE)

### 1.4.3. Import corrected spring polygon ------------- 
# as shapefile and convert it to dataframe
#********************************
path.polygrida2s=paste(path.grids,'polygons/WGACEGGspringPolygonOK.shp',sep='')
WGACEGGspringPolygon.sf=st_read(
  dsn=path.polygrida2s,
  layer='WGACEGGspringPolygonOK')
plot(WGACEGGspringPolygon.sf)
WGACEGGspringPolygon = st_coordinates(WGACEGGspringPolygon.sf[,1])
plot(WGACEGGspringPolygon)
coast()

# export as text file
WGACEGGspringPolygon.df=data.frame(WGACEGGspringPolygon)
head(WGACEGGspringPolygon.df)
plot(WGACEGGspringPolygon.df$long,WGACEGGspringPolygon.df$lat,type='l')
coast()

write.table(
  WGACEGGspringPolygon.df,
  paste(path.polygrid,'WGACEGGspringPolygonOK.csv',sep=''),
            sep=';',row.names=FALSE)

#****************************************************
## 1.5. Gridmaps, spring surveys ---------
#****************************************************
### load data
# Select variable of interest
vart='NASC'
varname='NASC'

head(AC_SPRING_IBBB)
sort(unique(AC_SPRING_IBBB$year))
lyears.spring=seq(min(AC_SPRING_IBBB$year),
                  max(AC_SPRING_IBBB$year),1)
lyears.spring=lyears.spring[lyears.spring>=2003]
lsp0=unique(AC_SPRING_IBBB$sp)
lsp0s=c("ENGR-ENC","CAPR-APE","BOOP-BOO","TRAC-MED","TRAC-TRU",
        "TRAC-PIC","SCOM-SCO","SARD-PIL","MERL-MCC","MICR-POU",
        "SCOM-COL","SPRA-SPR")
AC.SPRING.IBBB.lsp=lsp0s
#dfs=AC_SPRING_IBBB[AC_SPRING_IBBB$sp%in%c('TRAC-TRU')&
#                     AC_SPRING_IBBB$year%in%lyears,]
#
dfs=AC_SPRING_IBBB[AC_SPRING_IBBB$year%in%lyears.spring&
                     AC_SPRING_IBBB$sp%in%lsp0s,]
table(dfs$year,dfs$sp)

#******************************************
### 1.5.1. Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
#******************************************
# Define grid limits
# x1=-10.2;x2=-1;y1=35.8;y2=49 old limits
x1=-10;x2=-1;y1=36;y2=49
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=1
# Define iteration number
ni=200
# Define grid
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni)
# Define polygon
xpol=WGACEGGspringPolygon.df$X
ypol=WGACEGGspringPolygon.df$Y

grid0=expand.grid(x = xg,y = yg)
plot(grid0$x,grid0$y,main=paste('Res',ax))
lines(xpol,ypol,type='l')
coast()

# 1.5.2. Grid and plot per species and year
#******************************************
head(dfs)
unique(dfs$sp)
#dfs=dfs[dfs$sp=='ENGR-ENC',]

res1=gridNplot(df=dfs,vart=vart,varname=varname,
          tname='year',xname='x',yname='y',
          sname='survey',
          path.grids=path.grids1,p=0.98,spname='sp',
          zfilter="quantile",newbathy=FALSE,
          default.grid=FALSE,deep=-450,shallow=-50,bstep=450,
          bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3,
          lon1=x1,lon2=x2,lat1=y1,lat2=y2,
          xycor=FALSE,mini=-1,maxi=1,centerit=FALSE,
          grid.plotit = FALSE)
names(res1)
# Get all gridfiles
AC.SPRING.IBBB.gridmaps=res1$gridDataframe
head(AC.SPRING.IBBB.gridmaps)
sort(unique(AC.SPRING.IBBB.gridmaps$Year))
table(AC.SPRING.IBBB.gridmaps$Year,AC.SPRING.IBBB.gridmaps$sp)

# OR import grid files
# path.grids1.list=list.files(path.grids1,pattern='*.txt')
# AC.SPRING.IBBB.gridmaps=NULL
# for (i in 1:length(path.grids1.list)){
#   dfi=read.table(paste(path.grids1,path.grids1.list[i],sep=''),sep=';',
#                  header=TRUE)
#   dfi$sp=substr(path.grids1.list[i],6,13)
#   AC.SPRING.IBBB.gridmaps=rbind(AC.SPRING.IBBB.gridmaps,dfi)
# }
# head(dfi)

# Save gridmaps data frame
write.table(
  AC.SPRING.IBBB.gridmaps,
  paste(path.grids1,'AC_SPRING_IBBB_gridmaps.csv',
        sep=''),sep=';',row.names = FALSE)

# Load gridmaps data frame
AC.SPRING.IBBB.gridmaps=read.table(
  paste(path.grids1,'AC_SPRING_IBBB_gridmaps.csv',
        sep=''),sep=';',header = TRUE)

AC.SPRING.IBBB.lsp=unique(AC.SPRING.IBBB.gridmaps$sp)
AC.SPRING.IBBB.lyears=unique(AC.SPRING.IBBB.gridmaps$Year)

### 1.5.3. Make mean and SD maps and mosaic plots ------------
#### 1.5.3.1. All years ------
path.mosaics1=paste(path.grids,
                   '/AC_SPRING_IBBB/Mosaics/',sep='')
dir.create(path.mosaics1,recursive = TRUE)
path.mosaics1m=paste(path.grids,
                    '/AC_SPRING_IBBB/AverageMaps/',sep='')
dir.create(path.mosaics1m,recursive = TRUE)
path.mosaics1sd=paste(path.grids,
                     '/AC_SPRING_IBBB/SDmaps/',sep='')
dir.create(path.mosaics1sd,recursive = TRUE)

head(AC.SPRING.IBBB.gridmaps)

for (i in 1:length(AC.SPRING.IBBB.lsp)){
  AC.SPRING.IBBB.gridmapsi=
    AC.SPRING.IBBB.gridmaps[
    AC.SPRING.IBBB.gridmaps$sp==AC.SPRING.IBBB.lsp[i],]
  print(AC.SPRING.IBBB.lsp[i])
  mat=grid.plot(pat=AC.SPRING.IBBB.gridmapsi,
                input='gridDataframe',ux11=TRUE,
                pat2=path.mosaics1,
                id=AC.SPRING.IBBB.lsp[i],
                namz='',nams='',namn='',
                deep=-450,shallow=-50,bstep=450,
                bcol='grey50',drawlabels=TRUE,
                default.grid=FALSE,bathy.plot=TRUE,
                plotit=list(zmean=TRUE,zstd=TRUE,
                            nz=TRUE,
                            plot1=FALSE,mosaic=TRUE))
  # Plot mean and SD maps
  gmdf.msd=mat$gmdf.msd.meanZ$gmdf.msd
  unique(gmdf.msd$Year)
  gmdf.msd.mean=gmdf.msd[grep('mean',gmdf.msd$Year),]
  gmdf.msd.sd=gmdf.msd[grep('SD',gmdf.msd$Year),]
  # Mean map
  resi=grid.plot(pat=gmdf.msd.mean,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='Average NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics1m,
                 id=paste(AC.SPRING.IBBB.lsp[i],'meanZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE),xlim=NULL,ylim=NULL)
  # SD map
  resi=grid.plot(pat=gmdf.msd.sd,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='SD NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics1sd,
                 id=paste(AC.SPRING.IBBB.lsp[i],'sdZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE))
  if (i==1){
    AC.SPRING.IBBB.gridmaps.zmsd=
      mat$gmdf.msd.meanZ$gmdf.msd
  }else{
    AC.SPRING.IBBB.gridmaps.zmsd=rbind(
      AC.SPRING.IBBB.gridmaps.zmsd,
      mat$gmdf.msd.meanZ$gmdf.msd)
  }
}

head(AC.SPRING.IBBB.gridmaps)
# Save gridmaps data frame
write.table(
  AC.SPRING.IBBB.gridmaps.zmsd,
  paste(path.grids1,'AC_SPRING_IBBB_gridmapsZmeanSD.csv',
        sep=''),sep=';',row.names = FALSE)

unique(AC.SPRING.IBBB.gridmaps$Year)
unique(AC.SPRING.IBBB.gridmaps.zmsd$Year)

# Last N years
AC.SPRING.IBBB.lyears=unique(AC.SPRING.IBBB.gridmaps.zmsd$Year)[
  !substr(unique(AC.SPRING.IBBB.gridmaps.zmsd$Year),1,2)%in%
  c('me','SD')]
Nlast.years=8
for (i in 1:length(AC.SPRING.IBBB.lsp)){
  AC.SPRING.IBBB.gridmapsi=AC.SPRING.IBBB.gridmaps.zmsd[
    AC.SPRING.IBBB.gridmaps.zmsd$sp==AC.SPRING.IBBB.lsp[i],]
  AC.SPRING.IBBB.gridmapsi=AC.SPRING.IBBB.gridmapsi[
    AC.SPRING.IBBB.gridmapsi$Year%in%
      c(sort(rev(AC.SPRING.IBBB.lyears)[1:Nlast.years]),
        'Zvalue','Zstdev'),]
  unique(AC.SPRING.IBBB.gridmapsi$Year)
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = AC.SPRING.IBBB.gridmapsi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(AC.SPRING.IBBB.lsp[i],'AC SPRING IBBB NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    scale_fill_gradientn(colours=gradient.colors)+
    facet_wrap(.~factor(Year),nrow = 2)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids1,'Mosaics/gridmapMosaicLastYears-',
                            AC.SPRING.IBBB.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}

#### 1.5.3.2. Anomaly maps ------------
unique(AC.SPRING.IBBB.gridmaps.zmsd$Year)
AC.SPRING.IBBB.gridmaps.ano=NULL
for (i in 1:length(AC.SPRING.IBBB.lyears)){
  for (j in 1:length(AC.SPRING.IBBB.lsp)){
    dfiz=AC.SPRING.IBBB.gridmaps.zmsd[
      AC.SPRING.IBBB.gridmaps.zmsd$sp==AC.SPRING.IBBB.lsp[j]&
        AC.SPRING.IBBB.gridmaps.zmsd$Year==AC.SPRING.IBBB.lyears[i],]  
    dfim=AC.SPRING.IBBB.gridmaps.zmsd[
      AC.SPRING.IBBB.gridmaps.zmsd$sp==AC.SPRING.IBBB.lsp[j]&
        AC.SPRING.IBBB.gridmaps.zmsd$Year%in%
        AC.SPRING.IBBB.gridmaps.zmsd$Year[grep(
        'mean',AC.SPRING.IBBB.gridmaps.zmsd$Year)],]
    names(dfim)[names(dfim)=='Zvalue']='mZvalue'
    dfizm=merge(dfiz,dfim[,c('I','J','mZvalue')],
                by.x=c('I','J'),by.y=c('I','J'))
    dfizm$Zvalue=dfizm$Zvalue-dfizm$mZvalue
    AC.SPRING.IBBB.gridmaps.ano=rbind(AC.SPRING.IBBB.gridmaps.ano,
                                      dfizm)
  }  
}
dim(AC.SPRING.IBBB.gridmaps.ano)
dim(AC.SPRING.IBBB.gridmaps)

# All years
for (i in 1:length(AC.SPRING.IBBB.lsp)){
  AC.SPRING.IBBB.gridmaps.anoi=AC.SPRING.IBBB.gridmaps.ano[
    AC.SPRING.IBBB.gridmaps.ano$sp==AC.SPRING.IBBB.lsp[i],]
  summary(AC.SPRING.IBBB.gridmaps.anoi$Zvalue)
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = AC.SPRING.IBBB.gridmaps.anoi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(AC.SPRING.IBBB.lsp[i],'AC SPRING IBBB NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    #scale_fill_distiller(palette="RdBu")+
    scale_fill_gradient2(low = "blue",
                         mid = "white",
                         high = "red",midpoint = 0)+
    facet_wrap(.~factor(Year),nrow = 3)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids1,'Mosaics/gridmapMosaicAnomaly-',
                            AC.SPRING.IBBB.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}

# Recent years
Nlast.years.ano=8
for (i in 1:length(AC.SPRING.IBBB.lsp)){
  AC.SPRING.IBBB.gridmaps.anoi=AC.SPRING.IBBB.gridmaps.ano[
    AC.SPRING.IBBB.gridmaps.ano$sp==AC.SPRING.IBBB.lsp[i],]
  AC.SPRING.IBBB.gridmaps.anoi=AC.SPRING.IBBB.gridmaps.anoi[
    AC.SPRING.IBBB.gridmaps.anoi$Year%in%
      c(sort(rev(AC.SPRING.IBBB.lyears)[1:Nlast.years.ano]),
        'Zvalue','Zstdev'),]
  summary(AC.SPRING.IBBB.gridmaps.anoi$Zvalue)
  if (dim(AC.SPRING.IBBB.gridmaps.anoi)[1]>0){
    x11()
    par(bg='white')
    p1<-ggplot() +
      geom_raster(data = AC.SPRING.IBBB.gridmaps.anoi, 
                  aes(x = Xgd, y = Ygd, fill = Zvalue))+
      labs(title = 
             paste(AC.SPRING.IBBB.lsp[i],'AC SPRING IBBB NASC')) +
      theme(axis.title.x = element_blank(),
            axis.title.y = element_blank())+
      annotation_map(map_data("world"))+ #Add the map as a base layer before the points
      coord_quickmap()+  #Sets aspect ratio
      #scale_fill_distiller(palette="RdBu")+
      scale_fill_gradient2(low = "blue",
                           mid = "white",
                           high = "red",midpoint = 0)+
      facet_wrap(.~factor(Year),nrow = 2)
    print(p1)
    # Save figure
    dev.print(png, file=paste(path.grids1,
                              'Mosaics/gridmapMosaicAnomalyLastYears-',
                              AC.SPRING.IBBB.lsp[i],
                              ".png",sep=""), 
              units='cm',width=40, height=30,res=300)
    dev.off()
  }else{
    cat('No data available for',AC.SPRING.IBBB.lsp[i],'\n')
  }
}

## 1.6. Optional, list and bind grid files and convert to rasters objects -----------
# Useful to plot in GIS for example
#*********************************************************
# List the grid files in "path.grids" folder
#************************************
lffssd=list.files(path.grids1,pattern='*.txt')
#lfs=strsplit(lf,split='[.]',)
lffssds=data.frame(t(data.frame(strsplit(lffssd,split='[.]'))))
lp=as.character(lffssds[,1])
lffssds2=data.frame(t(data.frame(strsplit(lp,split='_'))))
row.names(lffssds2)=seq(length(lffssds2[,1]))
lffssds=cbind(lffssds,lffssds2)
head(lffssds)
names(lffssds)=c('pat','ext','method','sp','var','filter')
lffssds$path=lffssd
row.names(lffssds)=seq(length(lffssds[,1]))
lyears=NULL
lffssds=lffssds[lffssds$sp!=c("CLUP-HAR"),]

# Import files and bind maps
#************************************
# Eventually define a subset of years to process
# in "lyears" vector
lyears=2003:2019
if (is.null(lyears)){
  lyears=unique(rawData$year)
}
# Define the name of the merged dataset:
nname='AC_SPRING_IBBB'

for (i in 1:dim(lffssds)[1]){
  pat=paste(path.grids1,lffssds$path[i],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  mati$sp=lffssds$sp[i]
  #select surveys in lyears
  mati=mati[mati$Year%in%lyears,]
  head(mati)
  mati$Survey=nname
  if (i==1){
    rasterDB=mati
  }else{
    rasterDB=rbind(rasterDB,mati)
  }
  # convert gridfiles to raster stack format, 
  # compute mean and SD maps and plot everything:
  # see the grid2rasterStack function help page for details
  resras.AC.SPRING.IBBB=grid2rasterStack(pat=mati,
                                         path1=path.grids1,
                          varid=paste(lffssds$sp[i],
                                      lffssds$var[i],
                                      sep=' '),
                          anomaly=TRUE)
  
  # Plot last 8 years for report 
  # **********************************
  N2=length(names(resras.AC.SPRING.IBBB))
  N1=N2-9
  if (N2<10){
    N1=1
  }
  resras.AC.SPRING.IBBBs=subset(resras.AC.SPRING.IBBB,N1:N2)
  #plot(resras.AC.SPRING.IBBBs)
  varid=paste(lffssds$sp[i],lffssds$var[i],sep=' ')
  fid1=paste(varid,'_SPRING_IBBBs',sep='')

  rasterStackGridPlot(resras.AC.SPRING.IBBBs,ux11=TRUE,
                      path1=path.grids1.1,fid1=fid1,
                      plotit=list(MosaicSameScale=TRUE,IndSameScale=FALSE,
                                  MosaicDiffScale=FALSE))
  # Compute and plot anomaly maps
  names(resras.AC.SPRING.IBBBs)
  indy=which(!substr(names(resras.AC.SPRING.IBBBs),1,1)%in%c('A','S'))
  indym=which(substr(names(resras.AC.SPRING.IBBBs),1,1)%in%c('A'))
  ar=resras.AC.SPRING.IBBBs[[indy]]-resras.AC.SPRING.IBBBs[[indym]]
  #ar=log(ar+abs(floor(min(cellStats(ar,min)))))
  names(ar)=names(resras.AC.SPRING.IBBBs)[c(indy)]
  fid2=paste('anom',fid1,sep='')
  mini=min(cellStats(ar,min))
  maxi=max(cellStats(ar,max))
  minmax=max(c(abs(mini),abs(maxi)))
  rasterStackGridPlot(ar,ux11=TRUE,path1=path.grids1.1,fid1=fid2,
                        plotit=list(MosaicSameScale=TRUE,IndSameScale=FALSE,
                                    MosaicDiffScale=FALSE),
                      bathyBoB=FALSE,palette=BuRdTheme,
                      zlimi = c(-minmax,minmax))
}  

## 1.7. Spring surveys, extended ------
#******************************
### 1.7.1. Import WESPAS data -----
#prefix='/home/mathieu/Documents/Campagnes/'

path.IRL=paste(prefix,'WGACEGG/Data/rawData/SA/WESPAS/',sep='')

lfirl=list.files(path.IRL)
firl.df=data.frame(t(data.frame(strsplit(lfirl,split='_'))))
names(firl.df)=c('survey','year','var','ext')

for (i in 1:length(lfirl)){
  df.IRL=read.table(paste(path.IRL,lfirl[i],sep=''),
                    sep=',',header=TRUE)
  head(df.IRL)
  unique(df.IRL$Species)
  df0=df.IRL[df.IRL$Species==0,]
  df0.1=df0;df0.2=df0;df0.3=df0;df0.4=df0;df0.5=df0;df0.6=df0
  df0.1$Species='CAPR-APE'
  df0.2$Species='CLUP-HAR'
  df0.3$Species='SPRA-SPR'
  df0.4$Species='MICR-POU'
  df0.5$Species='TRAC-TRA'
  df0.6$Species='SCRO-SCO'
  df.IRL.alli=rbind(df.IRL[df.IRL$Species=='CAPR-APE',],
                   df.IRL[df.IRL$Species=='CLUP-HAR',],
                   df.IRL[df.IRL$Species=='SPRA-SPR',],
                   df.IRL[df.IRL$Species=='MICR-POU',],
                   df.IRL[df.IRL$Species=='TRAC-TRA',],
                   df.IRL[df.IRL$Species=='TRAC-TRA',],
                   df0.1,df0.2,df0.3,df0.4,df0.5,df0.6)
  df.IRL.alli$Species=as.character(df.IRL.alli$Species)
  df.IRL.alli[df.IRL.alli$Species=='TRAC-TRA','Species']='TRAC-TRU'
  df.IRL.alli[df.IRL.alli$Species=='SCRO-SCO','Species']='SCOM-SCO'
  df.IRL.alli$date.time=paste(df.IRL.alli$date,df.IRL.alli$time)
  names(df.IRL.alli)[names(df.IRL.alli)=='time']='hour'
  names(df.IRL.alli)[names(df.IRL.alli)=='Species']='sp'
  names(df.IRL.alli)[names(df.IRL.alli)=='date.time']='time'
  df.IRL.alli.yneg=df.IRL.alli[df.IRL.alli$y<0,]
  df.IRL.alli.yneg$y0=df.IRL.alli.yneg$y
  df.IRL.alli.yneg$y=df.IRL.alli.yneg$x
  df.IRL.alli.yneg$x=df.IRL.alli.yneg$y0
  df.IRL.alli=rbind(df.IRL.alli[df.IRL.alli$y>0,],
                    df.IRL.alli.yneg[,names(df.IRL.alli.yneg)!='y0'])
  plot(df.IRL.alli$x,df.IRL.alli$y,main=lfirl[i])
  if (i==1){
    df.IRL.all=df.IRL.alli
  }else{
    df.IRL.all=rbind(df.IRL.all=df.IRL.all,df.IRL.alli)
  }
}  

lsp=unique(df.IRL.all$sp)
unique(df.IRL.all$year)
for (i in 1:length(lsp)){
  dfi=df.IRL.all[df.IRL.all$sp==lsp[i],]
  plot(dfi$x,dfi$y,cex=0.1+log(dfi$NASC+1)/10,main=lsp[i])
}
summary(dfi$NASC)
IRL.lsp0=unique(df.IRL.all$sp)

for (i in 1:length(IRL.lsp0)){
  df.IRL.alli=df.IRL.all[
    df.IRL.all$sp==IRL.lsp0[i],]
  x11()
  par(bg='white')
  p1<-ggplot(data = df.IRL.alli, 
             aes(x = x, y = y, size = log(NASC+1))) +
    geom_point(col=2,alpha=0.7,pch=1)+
    labs(title = 
           paste(IRL.lsp0[i],'AC SPRING IRL NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    facet_wrap(.~factor(year),nrow = 3)+
    scale_size(range = c(.1, 5), name="log(NASC+1)")
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids2,'bubbleplotsMosaic-',
                            IRL.lsp0[i],
                             ".png",sep=""), 
             units='cm',width=40, height=30,res=300)
  dev.off()
}
graphics.off()

df.IRL.all$nsurvey='WESPAS'
names(df.IRL.all)
names(df.PT.spring)
head(df.PT.spring)

### 1.7.2. Bind all spring/summer surveys -----------------
#****************************************************
AC_SPRING_IBBBN=rbind(df.PT.spring,df.FR.spring,df.SP.spring,
                      df.IRL.all[,names(df.PT.spring)])
AC_SPRING_IBBBN$surveyComponent=AC_SPRING_IBBBN$survey
AC_SPRING_IBBBN$survey='AC_SPRING_IBBBN'
lsp.wespas=c('CAPR-APE','CLUP-HAR','SPRA-SPR','MICR-POU','TRAC-TRU','SCOM-SCO')
AC_SPRING_IBBBNs=AC_SPRING_IBBBN[
  AC_SPRING_IBBBN$sp%in%lsp.wespas&
    AC_SPRING_IBBBN$year%in%unique(df.IRL.all$year),]

uxy.wespas=unique(AC_SPRING_IBBBNs[AC_SPRING_IBBBNs$year!=2022,
                                   c('x','y')])
uxy.wespas=unique(AC_SPRING_IBBBNs[,c('x','y','year','nsurvey')])

#### 1.7.2.1. Export raw data ----

# Export raw datan all years, all species 
write.table(AC_SPRING_IBBBN,paste(path.raw,
                                 'AC-SPRING-IBBBN-NASC.csv'),
            sep=';',row.names = FALSE)

### 1.7.3. Map ESUs position for current year -------

uxys.ibbbn=uxy.wespas[uxy.wespas$year==cyear,]

x11()
ggplot() +
  geom_point(data = uxys.ibbbn, 
             aes(x = x, y = y,colour=nsurvey),size=.1)+
  theme(axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        legend.title=element_blank())+
  ggtitle(paste('WGACEGG extended
  spring joint acoustic survey',cyear))+
  annotation_map(map_data("world"))+ #Add the map as a base layer before the points
  coord_quickmap()+ guides(colour = guide_legend(override.aes = list(size=1)))
# Save figure
dev.print(png, file=paste(path.grids2,
                          'AC-SPRING-IBBBN_ESUpositions-',
                          cyear,
                          ".png",sep=""), 
          units='cm',width=15, height=20,res=300)

### 1.7.4. Create ext. spring/summer polygon -----------------
#****************************************************
mcos=cos(uxy.wespas$y*pi/180)
xxas=uxy.wespas$x*mcos
xxas=uxy.wespas$x
yyas=uxy.wespas$y
plot(xxas,yyas)

dfxyas=data.frame(xxas,yyas)
plot(xxas,yyas)
coast()

library(alphahull)
valphas=.3
apolyas=ahull(xxas,yyas,alpha=valphas)
plot(apolyas,main=paste('alpha =',valphas))
coast()
# -> OK but does not provide polygon vertices coordinates

# with ahull_track to get polygon vertices coordinates
library(spatstat.geom)
library(spatstat.core)

apolya.ggs <- ahull_track(xxas,yyas, alpha=valphas, nps = 3,sc=1)

dim(apolya.ggs[[i]]$data)
plot(apolya.ggs[[i]]$data$x,apolya.ggs[[i]]$data$y)

# get unique tracks
for(i in 1:length(apolya.ggs)) {
  apolya.ggs[[i]]$aes_params$colour <- "green3"
  tiis=apolya.ggs[[i]]$data
  tiis2=tiis[c(1,dim(tiis)[1]),]
  if (i==1){
    tiis2.db=tiis2
  }else{
    tiis2.db=rbind(tiis2.db,tiis2)
  }
}
tiis2.db=unique(tiis2.db)
plot(tiis2.db$x,tiis2.db$y)
coast()

# plot tracks
library(ggplot2)
ggplot(aes(x = xxas, y = yyas), data = dfxys)+ 
  geom_point(aes(x = xxas, y = yyas), data = dfxys, alpha = .5, 
             color = "darkred") +  apolya.ggs

# identify disjunct polygons
tiis2.db$polygon.id=NA
tiis2.db$di=NA
pid=1
dthr=100
for(i in 1:dim(tiis2.db)[1]) {
  if (i>1){
    tiis2.db[i,'di']=60*sqrt((tiis2.db[i-1,'x']-tiis2.db[i,'x'])^2+
                               (tiis2.db[i-1,'y']-tiis2.db[i,'y'])^2)
    if(tiis2.db[i,'di']>dthr){
      pid=pid+1
    }
  }else{
    tiis2.db[i,'di']=NA
  }
  tiis2.db[i,'polygon.id']=pid
}  
unique(tiis2.db$polygon.id)  
summary(tiis2.db$di)
hist(tiis2.db$di)

#tiis.db=rbind(tiis.db[1,],tiis.db,tiis.db[dim(tiis.db)[1],])
plot(tiis2.db$x,tiis2.db$y,type='l',col=tiis2.db$polygon.id,
     lty=tiis2.db$polygon.id)
text(tiis2.db$x,tiis2.db$y,tiis2.db$polygon.id)
coast()

# Create polygons and shapefile 
#***********************************
library(rgdal)
lpolyss=unique(tiis2.db$polygon.id)
for (i in 1:length(lpolyss)){
  polyixys=tiis2.db[tiis2.db$polygon.id==lpolyss[i],]
  #polyi=Polygon(polyixy[,c('x','y')])
  Srsi = Polygons(list(Polygon(polyixys[,c('x','y')])), paste("s",i,sep=''))
  #assign(paste('poly',i,sep='.'),Srsi,envir=.GlobalEnv)
  if (i==1){
    lSrsis=list(Srsi)
  }else{
    lSrsis=c(lSrsis,Srsi)
  }
}

SpPs = SpatialPolygons(lSrsis, seq(length(lSrsis)))
SpPs.df <- SpatialPolygonsDataFrame(SpPs, 
                                    data.frame(id=seq(length(lSrsis)), 
                                               row.names=row.names(SpPs)))
plot(SpPs,col=2)
plot(SpPs.df,col=2)
coast()

plot(SpPs.df[SpPs.df$id==1,])
plot(SpPs.df[SpPs.df$id==4,],add=TRUE,col=2)
coast()

SpPs.dfs=SpPs.df[SpPs.df$id==1,]

# union polygons (not useful if polygon#1 is OK)
# Generate IDs for grouping
upoly.ids=rep(1,length(lSrsis))
# Merge polygons by ID
SpPs.union <- unionSpatialPolygons(SpPs, upoly.ids)
# Plotting
plot(SpPs.union, add = FALSE, border = "red", lwd = 2)

# Export file as shape file, for manual modification in GIS if needed
#***********
path.polygrid=paste(path.grids,'polygons/',sep='')
dir.create(path.polygrid)
path.polygrida=paste(
  path.grids,'polygons/WGACEGGspringExtendedPolygon.shp',sep='')
proj4string(SpPs.dfs)="+proj=longlat +datum=WGS84"
writeOGR(obj=SpPs.dfs, dsn=path.polygrida,
         layer='WGACEGGspringExtendedPolygon', 
         driver='ESRI Shapefile',overwrite_layer=TRUE)

### 1.7.5. Import extended spring polygon ------------- 
# Load IBBBN polygon
path.polygridan=paste(
  path.grids,'polygons/WGACEGGspringExtendedPolygon.shp',sep='')
WGACEGGspringExtPolygon.sf=st_read(
  dsn=path.polygridan,
  layer='WGACEGGspringExtendedPolygon')
plot(WGACEGGspringExtPolygon.sf)
# convert to dataframe
WGACEGGspringExtPolygon = st_coordinates(WGACEGGspringExtPolygon.sf[,1])
WGACEGGspringExtPolygon.df=data.frame(WGACEGGspringExtPolygon)
head(WGACEGGspringExtPolygon.df)
plot(WGACEGGspringExtPolygon.df$X,WGACEGGspringExtPolygon.df$Y,type='l')
coast()

### 1.7.6. Gridmaps, spring surveys extended ----
#******************************************
#******************************************
# Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
#******************************************
# Define grid limits
x1=-15;x2=-1;y1=35;y2=60
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=1
# Define iteration number
ni=200

define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=FALSE,
                      poligonit=FALSE)

poly.IBBBN <- WGACEGGspringExtPolygon.df
head(poly.IBBBN)
plot(poly.IBBBN$X,poly.IBBBN$Y,type='l')
coast()

xpol=poly.IBBBN$X
ypol=poly.IBBBN$Y

grid0=expand.grid(x = xg,y = yg)
plot(grid0$x,grid0$y,main=paste('Res',ax),cex=.5,col='grey50')
lines(xpol,ypol,type='l')
coast()

# Select variable of interest
vart='NASC'
varname='NASC'

path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
path.grids1e=paste(path.grids,"AC_SPRING_IBBBN/",cyear,"/",sep='')

res2=gridNplot(df=AC_SPRING_IBBBNs,vart=vart,varname=varname,
               tname='year',xname='x',yname='y',sname='survey',
               path.grids=path.grids1e,p=0.98,spname='sp',
               zfilter="quantile",newbathy=FALSE,
               default.grid=FALSE,deep=-450,shallow=-50,bstep=450,
               bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3,
               lon1=x1,lon2=x2,lat1=y1,lat2=y2,
               xycor=FALSE,mini=-1,maxi=1,centerit=FALSE,
               grid.plotit = FALSE)

names(res2)
AC.SPRING.IBBBN.gridmaps=res2$gridDataframe
head(AC.SPRING.IBBBN.gridmaps)
sort(unique(AC.SPRING.IBBBN.gridmaps$Year))
table(AC.SPRING.IBBBN.gridmaps$Year,AC.SPRING.IBBBN.gridmaps$sp)
AC.SPRING.IBBBN.lsp=unique(AC.SPRING.IBBBN.gridmaps$sp)

# Save gridmaps data frame
write.csv2(
  AC.SPRING.IBBBN.gridmaps,
  paste(path.grids1e,'AC_SPRING_IBBBN_gridmaps.csv',sep=''))

### 1.7.8. Make mean and SD maps and mosaic plots ------------
# 1.7.6.1. All years
path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
path.mosaics2=paste(path.grids1e,
                    'Mosaics/',sep='')
dir.create(path.mosaics2,recursive = TRUE)
path.mosaics2m=paste(path.grids1e,
                     'AverageMaps/',sep='')
dir.create(path.mosaics2m,recursive = TRUE)
path.mosaics2sd=paste(path.grids1e,
                      'SDmaps/',sep='')
dir.create(path.mosaics2sd,recursive = TRUE)

head(AC.SPRING.IBBBN.gridmaps)

for (i in 1:length(AC.SPRING.IBBBN.lsp)){
  AC.SPRING.IBBBN.gridmapsi=
    AC.SPRING.IBBBN.gridmaps[
      AC.SPRING.IBBBN.gridmaps$sp==AC.SPRING.IBBBN.lsp[i],]
  print(AC.SPRING.IBBBN.lsp[i])
  mat=grid.plot(pat=AC.SPRING.IBBBN.gridmapsi,
                input='gridDataframe',ux11=TRUE,
                pat2=path.mosaics2,
                id=AC.SPRING.IBBBN.lsp[i],
                namz='',nams='',namn='',
                deep=-450,shallow=-50,bstep=450,
                bcol='grey50',drawlabels=TRUE,
                default.grid=FALSE,bathy.plot=FALSE,
                plotit=list(zmean=TRUE,zstd=TRUE,
                            nz=TRUE,
                            plot1=FALSE,mosaic=TRUE),
                ggnrow = 2)
  # Plot mean and SD maps
  gmdf.msd=mat$gmdf.msd.meanZ$gmdf.msd
  unique(gmdf.msd$Year)
  gmdf.msd.mean=gmdf.msd[grep('mean',gmdf.msd$Year),]
  gmdf.msd.sd=gmdf.msd[grep('SD',gmdf.msd$Year),]
  # Mean map
  resi=grid.plot(pat=gmdf.msd.mean,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='Average NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics2m,
                 id=paste(AC.SPRING.IBBBN.lsp[i],'meanZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE),xlim=NULL,ylim=NULL)
  # SD map
  resi=grid.plot(pat=gmdf.msd.sd,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='SD NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics2sd,
                 id=paste(AC.SPRING.IBBBN.lsp[i],'sdZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE))
  if (i==1){
    AC.SPRING.IBBBN.gridmaps.zmsd=
      mat$gmdf.msd.meanZ$gmdf.msd
  }else{
    AC.SPRING.IBBBN.gridmaps.zmsd=rbind(
      AC.SPRING.IBBBN.gridmaps.zmsd,
      mat$gmdf.msd.meanZ$gmdf.msd)
  }
}

head(AC.SPRING.IBBBN.gridmaps.zmsd)
# Save gridmaps data frame
write.table(
  AC.SPRING.IBBBN.gridmaps.zmsd,
  paste(path.grids2,'AC_SPRING_IBBBN_gridmapsZmeanSD.csv',
        sep=''),sep=';',row.names = FALSE)

unique(AC.SPRING.IBBBN.gridmaps$Year)
unique(AC.SPRING.IBBBN.gridmaps.zmsd$Year)

# Plot one mean map
gmdf.msd.mean=AC.SPRING.IBBBN.gridmaps.zmsd[
  AC.SPRING.IBBBN.gridmaps.zmsd$Year=="mean.2018-2023"&
    AC.SPRING.IBBBN.gridmaps.zmsd$sp=='TRAC-TRU',]  
resi=grid.plot(pat=gmdf.msd.mean,input='gridDataframe',ux11=TRUE,
               bathy.plot=FALSE,namz='Average NASC',nams='SD NASC',
               namn='No of samples',
               plotit=list(zmean=TRUE,zstd=TRUE,nz=TRUE,plot1=TRUE,
                           mosaic=FALSE))

### 1.7.9. Mosaic plots ------------
#### 1.7.9.1. Anomaly maps ------------
AC.SPRING.IBBBN.lyears=unique(AC.SPRING.IBBBN.gridmaps$Year)
AC.SPRING.IBBBN.gridmaps.ano=NULL
for (i in 1:length(AC.SPRING.IBBBN.lyears)){
  for (j in 1:length(AC.SPRING.IBBBN.lsp)){
    dfiz=AC.SPRING.IBBBN.gridmaps.zmsd[
      AC.SPRING.IBBBN.gridmaps.zmsd$sp==AC.SPRING.IBBBN.lsp[j]&
        AC.SPRING.IBBBN.gridmaps.zmsd$Year==AC.SPRING.IBBBN.lyears[i],]  
    dfim=AC.SPRING.IBBBN.gridmaps.zmsd[
      AC.SPRING.IBBBN.gridmaps.zmsd$sp==AC.SPRING.IBBBN.lsp[j]&
        AC.SPRING.IBBBN.gridmaps.zmsd$Year%in%
        AC.SPRING.IBBBN.gridmaps.zmsd$Year[grep(
          'mean',AC.SPRING.IBBBN.gridmaps.zmsd$Year)],]
    names(dfim)[names(dfim)=='Zvalue']='mZvalue'
    dfizm=merge(dfiz,dfim[,c('I','J','mZvalue')],
                by.x=c('I','J'),by.y=c('I','J'))
    dfizm$Zvalue=dfizm$Zvalue-dfizm$mZvalue
    AC.SPRING.IBBBN.gridmaps.ano=rbind(
      AC.SPRING.IBBBN.gridmaps.ano,dfizm)
  }  
}
dim(AC.SPRING.IBBBN.gridmaps.ano)
dim(AC.SPRING.IBBBN.gridmaps)

for (i in 1:length(AC.SPRING.IBBBN.lsp)){
  AC.SPRING.IBBBN.gridmaps.anoi=AC.SPRING.IBBBN.gridmaps.ano[
    AC.SPRING.IBBBN.gridmaps.ano$sp==AC.SPRING.IBBBN.lsp[i],]
  summary(AC.SPRING.IBBBN.gridmaps.anoi$Zvalue)
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = AC.SPRING.IBBBN.gridmaps.anoi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(AC.SPRING.IBBBN.lsp[i],'AC SPRING IBBBN NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    #scale_fill_distiller(palette="RdBu")+
    scale_fill_gradient2(low = "blue",
                         mid = "white",
                         high = "red",midpoint = 0)+
    facet_wrap(.~factor(Year),ncol = 5)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids1e,'Mosaics/gridmapMosaicAnomaly-',
                            AC.SPRING.IBBBN.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}

### 1.7.10. Eventually, list and bind grid files and convert to rasters objects-----------
#*********************************************************
# List the grid files in "path.grids" folder
#************************************
lffssde=list.files(path.grids1e,pattern='*.txt')
#lfs=strsplit(lf,split='[.]',)
lffssdes=data.frame(t(data.frame(strsplit(lffssde,split='[.]'))))
lp=as.character(lffssdes[,1])
lffssdes2=data.frame(t(data.frame(strsplit(lp,split='_'))))
row.names(lffssdes2)=seq(length(lffssdes2[,1]))
lffssdes=cbind(lffssdes,lffssdes2)
head(lffssdes)
names(lffssdes)=c('pat','ext','method','sp','var','filter')
lffssdes$path=lffssde
row.names(lffssdes)=seq(length(lffssdes[,1]))
lyears=NULL
#lffssdes=lffssdes[lffssdes$sp!=c("CLUP-HAR"),]

# Import files and bind maps
#************************************
# Eventually define a subset of years to process
# in "lyears" vector
lyears=2018:2019
# Define the name of the merged dataset:
nname='AC_SPRING_IBBBN'

path.grids1e.1=paste(path.grids1e,'LastYears/',sep='')
dir.create(path.grids1e.1)

for (i in 1:dim(lffssdes)[1]){
  pat=paste(path.grids1e,lffssdes$path[i],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  mati$sp=lffssdes$sp[i]
  #select surveys in lyears
  mati=mati[mati$Year%in%lyears,]
  head(mati)
  mati$Survey=nname
  if (i==1){
    rasterDB=mati
  }else{
    rasterDB=rbind(rasterDB,mati)
  }
  # convert gridfiles to raster stack format, 
  # compute mean and SD maps and plot everything:
  # see the grid2rasterStack function help page for details
  resras.AC.SPRING.IBBBN=grid2rasterStack(pat=mati,
                                         path1=path.grids1e,
                                         varid=paste(lffssdes$sp[i],
                                                     lffssdes$var[i],
                                                     sep=' '),
                                         anomaly=TRUE)
  
  # Plot last 8 years for report 
  # **********************************
  N2=length(names(resras.AC.SPRING.IBBBN))
  N1=N2-9
  if (N2<10){
    N1=1
  }
  resras.AC.SPRING.IBBBNs=subset(resras.AC.SPRING.IBBBN,N1:N2)
  #plot(resras.AC.SPRING.IBBBs)
  varid=paste(lffssdes$sp[i],lffssdes$var[i],sep=' ')
  fid1=paste(varid,'_SPRING_IBBBNs',sep='')
  
  rasterStackGridPlot(resras.AC.SPRING.IBBBNs,ux11=TRUE,
                      path1=path.grids1e.1,fid1=fid1,
                      plotit=list(MosaicSameScale=TRUE,IndSameScale=FALSE,
                                  MosaicDiffScale=FALSE))
  # Compute and plot anomaly maps
  names(resras.AC.SPRING.IBBBNs)
  indy=which(!substr(names(resras.AC.SPRING.IBBBNs),1,1)%in%c('A','S'))
  indym=which(substr(names(resras.AC.SPRING.IBBBNs),1,1)%in%c('A'))
  ar=resras.AC.SPRING.IBBBNs[[indy]]-resras.AC.SPRING.IBBBNs[[indym]]
  #ar=log(ar+abs(floor(min(cellStats(ar,min)))))
  names(ar)=names(resras.AC.SPRING.IBBBNs)[c(indy)]
  fid2=paste('anom',fid1,sep='')
  mini=min(cellStats(ar,min))
  maxi=max(cellStats(ar,max))
  minmax=max(c(abs(mini),abs(maxi)))
  rasterStackGridPlot(ar,ux11=TRUE,path1=path.grids1e.1,fid1=fid2,
                      plotit=list(MosaicSameScale=TRUE,IndSameScale=FALSE,
                                  MosaicDiffScale=FALSE),
                      bathyBoB=FALSE,palette=BuRdTheme,
                      zlimi = c(-minmax,minmax))
}  

#**************************************************************
# 2. Autumn surveys----
#**************************************************************
# Create paths
path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
path.grids3=paste(path.grids,"AC_AUTUMN_IBCE/",sep='')
path.grids3.1=paste(path.grids,"AC_AUTUMN_IBCE/LastYears/",sep='')
dir.create(path.grids3.1)

# 2.1. Basque autumn fish NASC----
#**************************************************************
#path.EU='/home/mathieu/Documents/WGACEGG/grids/rawData/'
path.EU=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/old/',sep='')

feu=list.files(paste(path.EU,sep=''),pattern='.txt')
#feu=list.files(path.EU,pattern='*_corr.txt')
feu.df=data.frame(t(data.frame(strsplit(feu,split='_'))))
years=substr(feu.df[,2],7,10)
for (i in 1:length(feu)){
  dfi=read.table(paste(paste(path.EU,'',sep=''),feu[i],sep=''),
                 sep='\t',header=TRUE)
  dfi$year=years[i]
  if (i==1) {
    sa.eu=dfi
  }else{
    sa.eu=rbind(sa.eu,dfi)
  }  
}
names(sa.eu)
names(dfi)
unique(sa.eu$sp)
sa.eu$sp=as.character(sa.eu$sp)
unique(sa.eu$sp)
table(sa.eu$sp,sa.eu$year)
AC_JUVENA_IB=sa.eu

#sa.eu=read.table(path.EU,sep='\t',header=TRUE)
head(sa.eu)
head(dfi)
unique(sa.eu$year)

# Adds 2015, 2016
path.EU.2015.1=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2015_ane_nodup.txt',sep='')
path.EU.2015.2=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2015_ane_nodup.txt',sep='')
path.EU.2015.3=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2015_pil_nodup.txt',sep='')
df.eu15.1=read.table(path.EU.2015.1,sep='\t',header=TRUE)
head(df.eu15.1)
plot(df.eu15.1$long,df.eu15.1$lat,
     cex=0.1+log(df.eu15.1$sa.ane.juv+1)/10,
     main='JUV')
plot(df.eu15.1$long,df.eu15.1$lat,
     cex=0.1+log(df.eu15.1$sa.ane.adul+1)/10,col=2,
     main='ADUL')
coast()
df.eu15.1=df.eu15.1[order(df.eu15.1$lat,df.eu15.1$long),]
tot=df.eu15.1$sa.ane.juv+df.eu15.1$sa.ane.adul
plot(tot,df.eu15.1$sa.tot)
summary(tot-df.eu15.1$sa.tot)

df.eu15.1=df.eu15.1[,c(1:6,9),]
df.eu15.1$sp='ENGR-ENC'
names(df.eu15.1)[7]='NASC'

df.eu15.2=read.table(path.EU.2015.2,sep='\t',header=TRUE)
head(df.eu15.2)
df.eu15.2=df.eu15.2[,1:7]
df.eu15.2$sp='ENGR-JUV'
names(df.eu15.2)[7]='NASC'
df.eu15.3=read.table(path.EU.2015.3,sep='\t',header=TRUE)
head(df.eu15.3)
df.eu15.3=df.eu15.3[,1:7]
df.eu15.3$sp='SARD-PIL'
names(df.eu15.3)[7]='NASC'
df.eu15=rbind(df.eu15.1,df.eu15.2,df.eu15.3)
head(df.eu15)
boxplot(NASC~sp,df.eu15)
aggregate(df.eu15$NASC,list(df.eu15$sp),summary)
unique(df.eu15$sp)
unique(df.eu15$Survey)
unique(df.eu15$year)
unique(df.eu15$NASC)
df.eu15$date.time=paste(df.eu15$date,df.eu15$hour)
unique(df.eu15$date.time)
df.eu15$date.time=as.character(df.eu15$date.time)
names(df.eu15)[1]='survey'
names(sa.eu)
names(df.eu15)
# checks
unique(nchar(df.eu16$date.time))
df.eu15$date.time[nchar(df.eu15$date.time)<11]
df.eu15[is.na(df.eu15$date),]
df.eu15$id=paste(df.eu15$year,df.eu15$date.time,df.eu15$sp,
                 df.eu15$long,df.eu15$lat)
dupid15=df.eu15[duplicated(df.eu15$id),'id']
dup15=df.eu15[df.eu15$id%in%dupid15,]
dup15=dup15[order(dup15$id),]
dim(dup15)

path.EU.2016.1=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2016_ane_nodup.txt',sep='')
path.EU.2016.2=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2016_pil_nodup.txt',sep='')
df.eu16.1=read.table(path.EU.2016.1,sep='\t',header=TRUE)
head(df.eu16.1)
df.eu16.2=read.table(path.EU.2016.2,sep='\t',header=TRUE)
head(df.eu16.2)
df.eu16=rbind(df.eu16.1,df.eu16.2)
df.eu16$date.time=paste(df.eu16$date,df.eu16$hour)
df.eu16$date.time=as.character(df.eu16$date.time)
names(df.eu16)
# checks
unique(nchar(df.eu16$date.time))
df.eu16$date.time[nchar(df.eu16$date.time)<11]
df.eu16[is.na(df.eu16$date),]
df.eu16$id=paste(df.eu16$year,df.eu16$date.time,df.eu16$sp,
                 df.eu16$long,df.eu16$lat)
dupid17=df.eu16[duplicated(df.eu16$id),'id']
dup16=df.eu16[df.eu16$id%in%dupid17,]
dup16=dup16[order(dup16$id),]
dim(dup16)
#write.table(dup17,paste(prefix,
#                      'WGACEGG/grids/rawData/JUVENA/pb/','JUVENA17duplicated.txt',sep=''))

# add 2017
path.EU.2017.1=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2017_ANEADUL_nodup.txt',sep='')
path.EU.2017.2=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2017_ANEJUV_nodup.txt',sep='')
path.EU.2017.3=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2017_PIL_nodup.txt',sep='')
df.eu17.1=read.table(path.EU.2017.1,sep='\t',header=TRUE)
head(df.eu17.1)
df.eu17.1=df.eu17.1[,1:7]
df.eu17.1$sp='ENGR-ENC'
names(df.eu17.1)[7]='NASC'
df.eu17.2=read.table(path.EU.2017.2,sep='\t',header=TRUE)
head(df.eu17.2)
df.eu17.2=df.eu17.2[,1:7]
df.eu17.2$sp='ENGR-JUV'
names(df.eu17.2)[7]='NASC'
df.eu17.3=read.table(path.EU.2017.3,sep='\t',header=TRUE)
head(df.eu17.3)
df.eu17.3=df.eu17.3[,1:7]
df.eu17.3$sp='SARD-PIL'
names(df.eu17.3)[7]='NASC'
df.eu17=rbind(df.eu17.1,df.eu17.2,df.eu17.3)
df.eu17$date.time=paste(df.eu17$date,df.eu17$hour)
df.eu17$date.time=as.character(df.eu17$date.time)
names(df.eu17)=c("survey","year","long","lat","date","hour",
                 "NASC","sp","date.time")
# checks
unique(nchar(df.eu17$date.time))
df.eu17$date.time[nchar(df.eu17$date.time)<11]
df.eu17[is.na(df.eu17$date),]
df.eu17$id=paste(df.eu17$year,df.eu17$date.time,df.eu17$sp,
                 df.eu17$long,df.eu17$lat)
dupid17=df.eu17[duplicated(df.eu17$id),'id']
dup17=df.eu17[df.eu17$id%in%dupid17,]
dup17=dup17[order(dup17$id),]
dim(dup17)

# add 2018
path.EU.2018.1=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2018_ANE_PIL_long.txt',sep='')
df.eu18.1=read.table(path.EU.2018.1,sep='\t',header=TRUE)
head(df.eu18.1)
names(df.eu18.1)[6]='hour'
unique(df.eu18.1$sp)
df.eu18.1.juv=df.eu18.1[df.eu18.1$sp=='ENGR_ENC_JUV',]
df.eu18.1.all=df.eu18.1[df.eu18.1$sp=='ENGR_ENC',]
df.eu18.1.adu=df.eu18.1[df.eu18.1$sp=='ENGR_ENC_ADULT',]
plot(df.eu18.1.juv$long,df.eu18.1.juv$lat,cex=df.eu18.1.juv$NASC/10000)
coast()
plot(df.eu18.1.all$long,df.eu18.1.all$lat,cex=df.eu18.1.all$NASC/10000)
coast()
plot(df.eu18.1.adu$long,df.eu18.1.adu$lat,cex=df.eu18.1.adu$NASC/10000)
coast()
df.eu18.1$date=paste(substr(df.eu18.1$date,7,8),'/',
                         substr(df.eu18.1$date,5,6),'/',
                         substr(df.eu18.1$date,1,4),sep='')
df.eu18.1$date.time=paste(df.eu18.1$date,df.eu18.1$hour)
unique(df.eu18.1$sp)
df.eu18=df.eu18.1[df.eu18.1$sp%in%c('ENGR_ENC_JUV','ENGR_ENC_ADULT','SARD_PIL'),]
head(df.eu18)
df.eu18$date.time

# add 2019
path.EU.2019.1=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2019_ANE_PIL_long.txt',sep='')
df.eu19.1=read.table(path.EU.2019.1,sep='\t',header=TRUE)
head(df.eu19.1)
names(df.eu19.1)[6]='hour'
unique(df.eu19.1$sp)
df.eu19.1.juv=df.eu19.1[df.eu19.1$sp=='ENGR_ENC_JUV',]
df.eu19.1.all=df.eu19.1[df.eu19.1$sp=='ENGR_ENC',]
df.eu19.1.adu=df.eu19.1[df.eu19.1$sp=='ENGR_ENC_ADULT',]
plot(df.eu19.1.juv$long,df.eu19.1.juv$lat,cex=df.eu19.1.juv$NASC/10000)
coast()
plot(df.eu19.1.all$long,df.eu19.1.all$lat,cex=df.eu19.1.all$NASC/10000)
coast()
plot(df.eu19.1.adu$long,df.eu19.1.adu$lat,cex=df.eu19.1.adu$NASC/10000)
coast()
df.eu19.1$date=paste(substr(df.eu19.1$date,9,10),'/',
                     substr(df.eu19.1$date,6,7),'/',
                     substr(df.eu19.1$date,1,4),sep='')
df.eu19.1$date.time=paste(df.eu19.1$date,df.eu19.1$hour)
unique(df.eu19.1$sp)
df.eu19=df.eu19.1[df.eu19.1$sp%in%c('ENGR_ENC_JUV','ENGR_ENC_ADULT','SARD_PIL'),]
head(df.eu19)
df.eu19$date.time

#write.table(dup17,paste(prefix,
#                      'WGACEGG/grids/rawData/JUVENA/pb/','JUVENA17duplicated.txt',sep=''))

# binds all JUVENA data
sa.eu$date.time=as.character(sa.eu$date.time)
df.eu17$date=as.character(df.eu17$date)
sa.eu=rbind(df.eu15[,names(sa.eu)],df.eu16[,names(sa.eu)],
             df.eu17[,names(sa.eu)],df.eu18[,names(sa.eu)],df.eu19[,names(sa.eu)])
# sa.eu=rbind(df.eu16[,names(sa.eu)],
#             df.eu17[,names(sa.eu)])
unique(nchar(sa.eu$date.time))
head(sa.eu)
# correct species names
sa.eu[sa.eu$sp=='ENGR_ENC_ADULT','sp']='ENGR-ENC'
sa.eu[sa.eu$sp=='ENGR_ENC_JUV','sp']='ENGR-JUV'
sa.eu[sa.eu$sp=='SARD_PIL','sp']='SARD-PIL'

#Select Basque data 
head(sa.eu)
df.EU=sa.eu[,c('survey','year','date.time','long','lat','NASC','sp')]
head(df.EU)
df.EU[is.na(df.EU$year),]
unique(df.EU$year)
unique(df.EU$sp)
unique(df.EU$long)
unique(df.EU$lat)
AC_JUVENA_IB=df.EU

head(df.EU)
AC_JUVENA_IBs=df.EU[df.EU$year%in%c(2015:2019),]
unique(AC_JUVENA_IBs$year)
unique(AC_JUVENA_IBs$sp)
unique(AC_JUVENA_IBs$long)
unique(AC_JUVENA_IBs$lat)
dim(AC_JUVENA_IBs)
dim(unique(AC_JUVENA_IBs))
# check for duplicated rows
head(AC_JUVENA_IBs)
AC_JUVENA_IBs$id=paste(AC_JUVENA_IBs$year,AC_JUVENA_IBs$date.time,AC_JUVENA_IBs$sp,
                 AC_JUVENA_IBs$long,AC_JUVENA_IBs$lat)
dupideu=AC_JUVENA_IBs[duplicated(AC_JUVENA_IBs$id),'id']
dupeu=AC_JUVENA_IBs[AC_JUVENA_IBs$id%in%dupideu,]
dupeu=dupeu[order(dupeu$id),]
dim(dupeu)
#write.table(dup17,paste(prefix,
#                      'WGACEGG/grids/rawData/JUVENA/pb/','JUVENA17duplicated.txt',sep=''))
table(dupeu$year)
# checks
unique(nchar(AC_JUVENA_IBs$date.time))
AC_JUVENA_IBs$date.time[nchar(AC_JUVENA_IBs$date.time)<12]
AC_JUVENA_IBs[is.na(AC_JUVENA_IBs$date),]
AC_JUVENA_IBs[is.na(AC_JUVENA_IBs$date),]

names(AC_JUVENA_IBs)=c("survey","year","time","x","y","NASC","sp","id")

# check for duplicates 
# 
# sa.eus=sa.eu[,c('Survey','year','Nesdu','time','long','lat','sa.ane.juv','sa.tot','sa.ane.adul')]
# names(sa.eus)=c('survey','year','Nesdu','time','x','y','ENGR-JUV','ENGR-TOT','ENGR-ENC')
# years=unique(sa.eus$year)
# head(sa.eus)
# length(sa.eus$time);length(unique(sa.eus$time))
# #sa.eus[duplicated(sa.eus$time),'time']
# sa.eus$id=paste(sa.eus$time,sa.eus$x,sa.eus$y)
# dus=sa.eus[sa.eus$time%in%sa.eus$time[duplicated(sa.eus$time)],]
# table(dus$year)
# dus=dus[order(dus$time),]
# dus11=dus[dus$year==2011,]
# dus12=dus[dus$year==2012,]
# plot(dus$x,dus$y)
# table(sa.eus$year)
# years2=years[5:10]
# dus2=sa.eus[sa.eus$id%in%sa.eus$id[duplicated(sa.eus$id)],]
# dim(sa.eus)
# dim(dus2)
# adus2=aggregate(dus2[,c('ENGR-JUV','ENGR-TOT','ENGR-ENC')],list(dus2$id),sum)
# names(adus2)=c('id','ENGR-JUV','ENGR-TOT','ENGR-ENC')
# dim(adus2)
# sa.eus2=rbind(sa.eus[!sa.eus$id%in%dus2$id,c('id','ENGR-JUV','ENGR-TOT','ENGR-ENC')],adus2)
# dim(sa.eus2)
# dus3=sa.eus2[sa.eus2$id%in%sa.eus2$id[duplicated(sa.eus2$id)],]
# # no more duplicated ESDUs
# df.EU=reshape(sa.eus2,varying=list(2:4),v.names = "NASC",
#                idvar = c('id'),
#                direction = "long",times=names(sa.eus2)[2:4],
#                timevar='sp')
# row.names(df.EU)=seq(dim(df.EU)[1])
# head(df.EU)
# dim(df.EU)
# AC_JUVENA_IB=merge(df.EU,unique(sa.eus[,c('id','survey','year','time','x','y')]),by.x='id',by.y='id')
# dim(AC_JUVENA_IB)
# head(AC_JUVENA_IB)
# AC_JUVENA_IB$survey='AC_JUVENA_IB'

# 2.2. UK autumn fish NASC: PELTIC---------------
#***********************************************
path.UK=paste(prefix,'WGACEGG/Data/rawData/SA/PELTIC/',sep='')

fuk=list.files(path.UK,pattern='peltic*')
fuk.df=data.frame(t(data.frame(strsplit(fuk,split='_'))))
years=as.numeric(as.character(paste('20',substr(fuk.df[,1],7,10),sep='')))
for (i in 1:length(fuk)){
  dfi=read.table(paste(path.UK,fuk[i],sep=''),sep=',',header=TRUE)
  dfi$year=years[i]
  if (i==1) {
    sa.uk=dfi
  }else{
    sa.uk=rbind(sa.uk,dfi)
  }  
}
unique(sa.uk$sp)
sa.uk$sp=as.character(sa.uk$sp)
head(sa.uk)
unique(sa.uk$year)
sa.uk$date.time=paste(sa.uk$date,sa.uk$time)
names(sa.uk)[names(sa.uk)=='x']='long'
names(sa.uk)[names(sa.uk)=='y']='lat'
names(sa.uk)[names(sa.uk)=='time']='hour'
df.UK=sa.uk[,names(df.EU)]
df.UK$survey=substr(df.UK$survey,1,6)
names(df.UK)
# Correct species codes
df.UK[df.UK$sp=='ANE','sp']='ENGR-ENC'
df.UK[df.UK$sp=='PIL','sp']='SARD-PIL'
df.UK[df.UK$sp=='SPR','sp']='SPRA-SPR'
df.UK[df.UK$sp=='HER','sp']='CLUP-HAR'
df.UK[df.UK$sp=='HOM','sp']='TRAC-TRU'
df.UK[df.UK$sp=='BOF','sp']='CAPR-APE'
df.UK[df.UK$sp=='WHB','sp']='MICR-POU'
df.UK[df.UK$sp=='HOM','sp']='TRAC-TRU'
df.UK[df.UK$sp=='TRAC-TRA','sp']='TRAC-TRU'

names(df.UK)=c("survey","year","time","x","y","NASC","sp")

# 2.3. Irish herring survey data -------------
#***********************************
path.IRL2=paste(prefix,'WGACEGG/Data/rawData/SA/CSHAS/',sep='')

lfirl2=list.files(path.IRL2)
firl2.df=data.frame(t(data.frame(strsplit(lfirl2,split='_'))))
names(firl2.df)=c('survey','year','var','ext')

for (i in 1:length(lfirl2)){
  df.IRL2=read.table(paste(path.IRL2,lfirl2[i],sep=''),sep=',',header=TRUE)
  head(df.IRL2)
  unique(df.IRL2$Species)
  df0=df.IRL2[df.IRL2$Species==0,]
  df0.1=df0;df0.2=df0;df0.3=df0;df0.4=df0;df0.5=df0
  df0.1$Species='SARD-PIL'
  df0.2$Species='CLUP-HAR'
  df0.3$Species='SPRA-SPR'
  df0.4$Species='ENGR-ENC'
  df0.5$Species='TRAC-TRA'
  df.IRL2.alli=rbind(df.IRL2[df.IRL2$Species=='SARD-PIL',],
                    df.IRL2[df.IRL2$Species=='CLUP-HAR',],
                    df.IRL2[df.IRL2$Species=='SPRA-SPR',],
                    df.IRL2[df.IRL2$Species=='ENGR-ENC',],
                    df.IRL2[df.IRL2$Species=='TRAC-TRA',],
                    df0.1,df0.2,df0.3,df0.4,df0.5)
  df.IRL2.alli$Species=as.character(df.IRL2.alli$Species)
  df.IRL2.alli[df.IRL2.alli$Species=='TRAC-TRA','Species']='TRAC-TRU'
  df.IRL2.alli$date.time=paste(df.IRL2.alli$date,df.IRL2.alli$time)
  names(df.IRL2.alli)[names(df.IRL2.alli)=='time']='hour'
  names(df.IRL2.alli)[names(df.IRL2.alli)=='Species']='sp'
  names(df.IRL2.alli)[names(df.IRL2.alli)=='date.time']='time'
  if (i==1){
    df.IRL2.all=df.IRL2.alli
  }else{
    df.IRL2.all=rbind(df.IRL2.all=df.IRL2.all,df.IRL2.alli)
  }
}  

lsp=unique(df.IRL2.all$sp)
unique(df.IRL2.all$year)
for (i in 1:length(lsp)){
  dfi=df.IRL2.all[df.IRL2.all$sp==lsp[i],]
  plot(dfi$x,dfi$y,cex=0.1+log(dfi$NASC+1)/10,main=lsp[i])
  coast()
}

head(df.IRL2.all)

#****************************************************
# 2.4. Spanish autumn fish NASC ----------
#****************************************************
path.SP2=paste(prefix,'WGACEGG/Data/rawData/SA/IBERAS/IBERASrawNASCgrid2019.csv',
               sep='')

sa.sp2=read.table(path.SP2,sep=',',header=TRUE)
head(sa.sp2)
unique(sa.sp2$sp)
unique(sa.sp2$year)
table(sa.sp2$year)
sa.sp2$sp=as.character(sa.sp2$sp)
#sa.sp2[sa.sp2$sp=='MICRO-POU','sp']='MICR-POU'
#sa.sp2[sa.sp2$sp=='TRAC-TRA','sp']='TRAC-TRU'
sa.sp2=sa.sp2[sa.sp2$sp!="MERL-MER_1",]
unique(sa.sp2$survey)
unique(sa.sp2$sp)
sa.sp2$time=paste(substr(sa.sp2$time,7,8),'/',substr(sa.sp2$time,5,6),'/',substr(sa.sp2$time,1,4),
                  substr(sa.sp2$time,10,18),sep='')

df.SP.autumn=sa.sp2
head(df.SP.autumn)
unique(df.SP.autumn$year)
unique(df.SP.autumn$sp)

# Replace local species name by ACEGG species names
unique(df.SP.autumn$sp)
names(df.SP.autumn)[names(df.SP.autumn)=='sp']='sp0'
df.SP.autumn=merge(df.SP.autumn,sp.acegg[sp.acegg$country=='SP',
                                         c('lsp','sp')],by.x='sp0',by.y='lsp')
unique(df.SP.autumn$sp)
df.SP.autumn=df.SP.autumn[,-1]
head(df.SP.autumn)
df.SP.autumn$survey=paste('IBERAS',df.SP.autumn$year,sep='')
#invert x and y
df.SP.autumn$x0=df.SP.autumn$x
df.SP.autumn$x=df.SP.autumn$y
df.SP.autumn$y=df.SP.autumn$x0

plot(df.SP.autumn$x,df.SP.autumn$y)
coast()

df.SP.autumn[df.SP.autumn$sp=='SCOM-COL',]
head(df.SP.autumn)

# 2.4. Bind autumn surveys data ----
#*****************************************
AC_AUTUMN_IBCE=rbind(AC_JUVENA_IBs[,names(df.UK)],df.UK,df.IRL2.all[,names(df.UK)],
                     df.SP.autumn[,names(df.UK)])

table(AC_AUTUMN_IBCE$year,AC_AUTUMN_IBCE$sp)

AC_AUTUMN_IBCE$surveyComponent=AC_AUTUMN_IBCE$survey
AC_AUTUMN_IBCE$survey='AC_AUTUMN_IBCE'
head(AC_AUTUMN_IBCE)

# 2.4.1. Export last year sampling points -----
head(AC_AUTUMN_IBCE)
AC_AUTUMN_IBCExy=unique(AC_AUTUMN_IBCE[AC_AUTUMN_IBCE$year==2019,
                                       c('survey','surveyComponent','x','y')])

path.grids2exy=paste(path.grids,"AC_AUTUMN_IBCE/sampling/",sep='')
dir.create(path.grids2exy)
write.table(AC_AUTUMN_IBCExy,paste(path.grids2exy,'AC_AUTUMN_IBCExy2019.csv',sep=''),
            sep=';',row.names=FALSE)

plot(AC_AUTUMN_IBCExy$x,AC_AUTUMN_IBCExy$y)
coast()

uxy2a=unique(AC_AUTUMN_IBCE[,c('x','y')])
uxy2a=uxy2a[order(uxy2a$y),]
write.table(uxy2a,paste(path.grids2exy,'AUTUMN_AC_ESDUs.csv',sep=''),
            sep=';',row.names = FALSE)

# 2.4.2. Create autumn polygon ------------ 
#x11()
# plot(uxy2a$x,uxy2a$y,asp=1/cos((mean(uxy2a$y)*pi)/180))
# coast()
# #Draw polygon
# #p=locator(type='l')
# #poly.autumn=data.frame(x=p$x,y=p$y)
# #plot(poly.autumn$x,poly.autumn$y,type='l')
# #coast()
# 
# #OR alphahull style
# mcosa=cos(uxy2a$y*pi/180)
# xxa=uxy2a$x*mcosa
# xxa=uxy2a$x
# yya=uxy2a$y
# df=data.frame(xxa,yya)
# plot(xxa,yya)
# coast()
# 
# library(alphahull)
# valpha=.3
# apolya=ahull(xxa,yya,alpha=valpha)
# plot(apolya,main=paste('alpha =',valpha))
# coast()
# 
# # with ahull_track
# apolya.gg <- ahull_track(xxa,yya, alpha=valpha, nps = 3,sc=1)
# 
# dim(apolya.gg[[i]]$data)
# plot(apolya.gg[[i]]$data$x,apolya.gg[[i]]$data$y)
# 
# # get unique tracks
# for(i in 1:length(apolya.gg)) {
#   apolya.gg[[i]]$aes_params$colour <- "green3"
#   tii=apolya.gg[[i]]$data
#   tiis=tii[c(1,dim(tii)[1]),]
#   tiis
#   if (i==1){
#     tiis.db=tiis
#   }else{
#     tiis.db=rbind(tiis.db,tiis)
#   }
# }
# tiis.db=unique(tiis.db)
# plot(tiis.db$x,tiis.db$y)
# coast()
# 
# # plot tracks
# library(ggplot2)
# ggplot(aes(x = xxa, y = yya), data = df)+ 
#   geom_point(aes(x = xxa, y = yya), data = df, alpha = .5, color = "darkred") +
#   apolya.gg
# 
# # identify disjunct polygons
# tiis.db$polygon.id=NA
# tiis.db$di=NA
# pid=1
# dthr=100
# for(i in 1:dim(tiis.db)[1]) {
#   if (i>1){
#     tiis.db[i,'di']=60*sqrt((tiis.db[i-1,'x']-tiis.db[i,'x'])^2+(tiis.db[i-1,'y']-tiis.db[i,'y'])^2)
#     if(tiis.db[i,'di']>dthr){
#       pid=pid+1
#     }
#   }else{
#     tiis.db[i,'di']=NA
#   }
#   tiis.db[i,'polygon.id']=pid
# }  
# unique(tiis.db$polygon.id)  
# summary(tiis.db$di)
# hist(tiis.db$di)
# 
# #tiis.db=rbind(tiis.db[1,],tiis.db,tiis.db[dim(tiis.db)[1],])
# plot(tiis.db$x,tiis.db$y,type='l',col=tiis.db$polygon.id,
#      lty=tiis.db$polygon.id)
# text(tiis.db$x,tiis.db$y,tiis.db$polygon.id)
# coast()

# Create autumn polygons and shapefile 
#***********************************
# library(rgdal)
# lpolys=unique(tiis.db$polygon.id)
# for (i in 1:length(lpolys)){
#   polyixy=tiis.db[tiis.db$polygon.id==lpolys[i],]
#   #polyi=Polygon(polyixy[,c('x','y')])
#   Srsi = Polygons(list(Polygon(polyixy[,c('x','y')])), paste("s",i,sep=''))
#   #assign(paste('poly',i,sep='.'),Srsi,envir=.GlobalEnv)
#   if (i==1){
#     lSrsi=list(Srsi)
#   }else{
#     lSrsi=c(lSrsi,Srsi)
#   }
# }
# 
# SpP = SpatialPolygons(lSrsi, seq(length(lSrsi)))
# SpP.df <- SpatialPolygonsDataFrame(SpP, 
#                                    data.frame(id=seq(length(lSrsi)), 
#                                               row.names=row.names(SpP)))
# plot(SpP)
# plot(SpP.df)
# coast()
# 
# # union polygons
# # Generate IDs for grouping
# upoly.id=rep(1,length(lSrsi))
# 
# # Merge polygons by ID
# SpP.union <- unionSpatialPolygons(SpP, upoly.id)
# 
# # Plotting
# plot(oregon)
# plot(SpP.union, add = FALSE, border = "red", lwd = 2)
# 
# # Export file as shape file
# #***********
# path.polygrid=paste(path.grids,'polygons/',sep='')
# dir.create(path.polygrid)
# path.polygrida=paste(path.grids,'polygons/WGACEGGautumnPolygon.shp',sep='')
# proj4string(SpP.df)="+proj=longlat +datum=WGS84"
# writeOGR(obj=SpP.df, dsn=path.polygrida,layer='WGACEGGautumnPolygon', 
#          driver='ESRI Shapefile',overwrite_layer=TRUE)
# 
# # Import dissolved polygon as shapefile and convert it to dataframe
# #********************************
# path.polygrida2=paste(path.grids,'polygons/WGACEGGautumnPolygonOK.shp',sep='')
# WGACEGGautumnPolygon=readOGR(dsn=path.polygrida2,layer='WGACEGGautumnPolygonOK')
# plot(WGACEGGautumnPolygon)
# coast()
# # export as text file
# write.table(WGACEGGautumnPolygon.df,paste(path.polygrid,'WGACEGGautumnPolygonOK.csv',sep=''),
#             sep=';',row.names=FALSE)
# 
# library(ggplot2)
# WGACEGGautumnPolygon.df=fortify(WGACEGGautumnPolygon)
# head(WGACEGGautumnPolygon.df)
# 
# plot(WGACEGGautumnPolygon.df$long,WGACEGGautumnPolygon.df$lat,type='l')
# coast()

# Import new autumn polygon
#********************************
path.polygrida2.1=paste(path.grids,'polygons/WGACEGGautumnPol_CarBay.csv',
                        sep='')
WGACEGGautumnPolygon.df2=read.table(path.polygrida2.1,sep=';',
                                    header=TRUE)
head(WGACEGGautumnPolygon.df2)

#****************************************
# 2.5. Gridmaps of autumn surveys fish NASC------
#*******************************************
### load data
# Select variable of interest
vart='NASC'
varname='NASC'

# 8.3.1. Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
#*************************************
# Define grid limits
# x1=-10.2;x2=0;y1=35.8;y2=53  # old grid < 2021
x1=-10;x2=-1;y1=36;y2=53      # new grid > 2021
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
#u=2                             # old smoothing parameter < 2021
u=1                             # new smoothing parameter > 2021
# Define iteration number
ni=200
# Define grid
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=FALSE,
                      poligonit=FALSE)
# Set autumn polygon
xpol=WGACEGGautumnPolygon.df2$long
ypol=WGACEGGautumnPolygon.df2$lat
# Check grid and polygon
grid0=expand.grid(x = xg,y = yg)
plot(grid0$x,grid0$y,main=paste('Res',ax))
lines(xpol,ypol,type='l')
coast()

# Grid and plot 
#*********************************
#
AC_AUTUMN_IBCEs=AC_AUTUMN_IBCE[AC_AUTUMN_IBCE$sp%in%c('ENGR-ENC','SARD-PIL'),]

res.IBCE=gridNplot(df=AC_AUTUMN_IBCEs,vart=vart,varname=varname,
                 tname='year',xname='x',yname='y',sname='survey',
                 path.grids=path.grids3,p=0.98,spname='sp',
                 zfilter="quantile",newbathy=FALSE,
                 deep=-450,shallow=-50,bstep=450,
                 bcol='grey50',drawlabels=TRUE,
                 lon1=x1,lon2=x2,lat1=y1,lat2=y2,
                 xycor=FALSE,mini=-1,maxi=1,
                 centerit=FALSE)

names(res.IBCE)
AC.AUTUMN.IBCE.gridmaps=res.IBCE$gridDataframe
head(AC.AUTUMN.IBCE.gridmaps)
sort(unique(AC.AUTUMN.IBCE.gridmaps$Year))
table(AC.AUTUMN.IBCE.gridmaps$Year,AC.AUTUMN.IBCE.gridmaps$sp)
AC.AUTUMN.IBCE.lsp=unique(AC.AUTUMN.IBCE.gridmaps$sp)

# Save gridmaps data frame
write.csv2(
  AC.AUTUMN.IBCE.gridmaps,
  paste(path.grids3,'AC_SPRING_IBCE_gridmaps.csv',sep=''))

# 1.7.6. Make mean and SD maps and mosaic plots ------------
# 1.7.6.1. All years
path.mosaics3=paste(path.grids3,'/Mosaics/',sep='')
dir.create(path.mosaics3,recursive = TRUE)
path.mosaics3m=paste(path.grids3,
                     '/AverageMaps/',sep='')
dir.create(path.mosaics3m,recursive = TRUE)
path.mosaics3sd=paste(path.grids3,
                      '/SDmaps/',sep='')
dir.create(path.mosaics3sd,recursive = TRUE)

head(AC.AUTUMN.IBCE.gridmaps)

AC.AUTUMN.IBCE.lsp=unique(AC.AUTUMN.IBCE.gridmaps$sp)

for (i in 1:length(AC.AUTUMN.IBCE.lsp)){
  AC.AUTUMN.IBCE.gridmapsi=
    AC.AUTUMN.IBCE.gridmaps[
      AC.AUTUMN.IBCE.gridmaps$sp==AC.AUTUMN.IBCE.lsp[i],]
  print(AC.AUTUMN.IBCE.lsp[i])
  mat=grid.plot(pat=AC.AUTUMN.IBCE.gridmapsi,
                input='gridDataframe',ux11=TRUE,
                pat2=path.mosaics3,
                id=AC.AUTUMN.IBCE.lsp[i],
                namz='',nams='',namn='',
                deep=-450,shallow=-50,bstep=450,
                bcol='grey50',drawlabels=TRUE,
                default.grid=FALSE,bathy.plot=FALSE,
                plotit=list(zmean=TRUE,zstd=TRUE,
                            nz=TRUE,
                            plot1=FALSE,mosaic=TRUE),
                ggnrow = 2)
  # Plot mean and SD maps
  gmdf.msd=mat$gmdf.msd.meanZ$gmdf.msd
  unique(gmdf.msd$Year)
  gmdf.msd.mean=gmdf.msd[grep('mean',gmdf.msd$Year),]
  gmdf.msd.sd=gmdf.msd[grep('SD',gmdf.msd$Year),]
  # Mean map
  resi=grid.plot(pat=gmdf.msd.mean,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='Average NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics3m,
                 id=paste(AC.AUTUMN.IBCE.lsp[i],'meanZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE),xlim=NULL,ylim=NULL)
  # SD map
  resi=grid.plot(pat=gmdf.msd.sd,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='SD NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics3sd,
                 id=paste(AC.AUTUMN.IBCE.lsp[i],'sdZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE))
  if (i==1){
    AC.AUTUMN.IBCE.gridmaps.zmsd=
      mat$gmdf.msd.meanZ$gmdf.msd
  }else{
    AC.AUTUMN.IBCE.gridmaps.zmsd=rbind(
      AC.AUTUMN.IBCE.gridmaps.zmsd,
      mat$gmdf.msd.meanZ$gmdf.msd)
  }
}

head(AC.AUTUMN.IBCE.gridmaps.zmsd)
# Save gridmaps data frame
write.table(
  AC.AUTUMN.IBCE.gridmaps.zmsd,
  paste(path.grids2,'AC_SPRING_IBBBN_gridmapsZmeanSD.csv',
        sep=''),sep=';',row.names = FALSE)

unique(AC.AUTUMN.IBCE.gridmaps$Year)
unique(AC.AUTUMN.IBCE.gridmaps.zmsd$Year)

# 1.7.6. Mosaic plots ------------
# 1.7.6.2. Anomaly maps ------------
AC.AUTUMN.IBCE.lyears=unique(AC.AUTUMN.IBCE.gridmaps$Year)
AC.AUTUMN.IBCE.gridmaps.ano=NULL
for (i in 1:length(AC.AUTUMN.IBCE.lyears)){
  for (j in 1:length(AC.AUTUMN.IBCE.lsp)){
    dfiz=AC.AUTUMN.IBCE.gridmaps.zmsd[
      AC.AUTUMN.IBCE.gridmaps.zmsd$sp==AC.AUTUMN.IBCE.lsp[j]&
        AC.AUTUMN.IBCE.gridmaps.zmsd$Year==AC.AUTUMN.IBCE.lyears[i],]  
    dfim=AC.AUTUMN.IBCE.gridmaps.zmsd[
      AC.AUTUMN.IBCE.gridmaps.zmsd$sp==AC.AUTUMN.IBCE.lsp[j]&
        AC.AUTUMN.IBCE.gridmaps.zmsd$Year%in%
        AC.AUTUMN.IBCE.gridmaps.zmsd$Year[grep(
          'mean',AC.AUTUMN.IBCE.gridmaps.zmsd$Year)],]
    names(dfim)[names(dfim)=='Zvalue']='mZvalue'
    dfizm=merge(dfiz,dfim[,c('I','J','mZvalue')],
                by.x=c('I','J'),by.y=c('I','J'))
    dfizm$Zvalue=dfizm$Zvalue-dfizm$mZvalue
    AC.AUTUMN.IBCE.gridmaps.ano=rbind(
      AC.AUTUMN.IBCE.gridmaps.ano,dfizm)
  }  
}
dim(AC.AUTUMN.IBCE.gridmaps.ano)
dim(AC.AUTUMN.IBCE.gridmaps)

for (i in 1:length(AC.AUTUMN.IBCE.lsp)){
  AC.AUTUMN.IBCE.gridmaps.anoi=AC.AUTUMN.IBCE.gridmaps.ano[
    AC.AUTUMN.IBCE.gridmaps.ano$sp==AC.AUTUMN.IBCE.lsp[i],]
  summary(AC.AUTUMN.IBCE.gridmaps.anoi$Zvalue)
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = AC.AUTUMN.IBCE.gridmaps.anoi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(AC.AUTUMN.IBCE.lsp[i],'AC SPRING IBBBN NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    #scale_fill_distiller(palette="RdBu")+
    scale_fill_gradient2(low = "blue",
                         mid = "white",
                         high = "red",midpoint = 0)+
    facet_wrap(.~factor(Year),ncol = 5)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids2,'Mosaics/gridmapMosaicAnomaly-',
                            AC.AUTUMN.IBCE.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}




# Optionally, list and bind grid files and convert to rasters objects-----------
#*********************************************************
lffssda=list.files(path.gridsa,pattern='*.txt')
#lfs=strsplit(lf,split='[.]',)
lffssdas=data.frame(t(data.frame(strsplit(lffssda,split='[.]'))))
lp=as.character(lffssdas[,1])
lffssdas2=data.frame(t(data.frame(strsplit(lp,split='_'))))
row.names(lffssdas2)=seq(length(lffssdas2[,1]))
lffssdas=cbind(lffssdas,lffssdas2)
head(lffssdas)
names(lffssdas)=c('pat','ext','method','sp','var','filter')
lffssdas$path=lffssda
row.names(lffssdas)=seq(length(lffssdas[,1]))
#lffssdas=lffssdas[lffssdas$sp!='CLUP-HAR',]
#lffssdas=lffssdas[lffssdas$sp=='SCOM-COL',]
lffssdas=lffssdas[!lffssdas$sp%in%c('MICR-POU','Other','BOOP-BOO',
                                    'MEGA-NOR','MERL-MCC','TRAC-PIC',
                                    'SCOM-COL'),]

# Import files and bind maps
#************************************
lyears=2015:2019

for (i in 1:dim(lffssdas)[1]){
  pat=paste(path.gridsa,lffssdas$path[i],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  mati$sp=lffssdas$sp[i]
  #select surveys in lyears
  mati=mati[mati$Year%in%lyears,]
  head(mati)
  mati$Survey='AC_AUTUMN_IBCE'
  if (i==1){
    FishNASCSp.AC_AUTUMN_IBCE=mati
  }else{
    FishNASCSp.AC_AUTUMN_IBCE=rbind(FishNASCSp.AC_AUTUMN_IBCE,mati)
  }
  # convert gridfiles to raster stack format, compute mean and SD maps and plot everything:
  resras=grid2rasterStack(pat=mati,path1=path.gridsa,
                          varid=paste(lffssdas$sp[i],
                                      lffssdas$var[i],sep=' '),
                          anomaly=TRUE)
}  

# Autumn surveys CTD data ------
#***********************************
path.ctd.eu=paste(prefix,
                  '/WGACEGG/grids/rawData/JUVENA/CTD/CTD_Peltic_Juvena_2017.txt',sep='')
ctd.eu=read.table(path.ctd.eu,header=TRUE)
head(ctd.eu)
names(ctd.eu)=c('survey','lat','long','SST','SSS')
ctd.eu$year=2017
ctd.eu$type='CTD'
path.ctd.uk='Q:/Meetings/WGACEGG/WGACEGG2017/Data/Cend 19_17_SAIV.csv'  
ctd.uk=read.table(path.ctd.uk,header=TRUE,sep=',')
head(ctd.uk)
ctd.uks=ctd.uk[,c('Long_dec','Lat_dec','Surface_T','Surface_S')]
ctd.uks$Survey='PELTIC2017'
names(ctd.uks)=c('long','lat','SST','SSS','survey')
ctd.uks$year=2017
ctd.uks$type='CTD'
ctd.au=rbind(ctd.eu,ctd.uks[,names(ctd.eu)])
head(ctd.au)

plot(ctd.au$long,ctd.au$lat)
coast()

#****************************************
# Gridmaps of autumn surveys SST&SSS------
#*******************************************
# 8.3.1. Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
#*************************************
# Define grid limits
x1=-10.2;x2=-1;y1=35.8;y2=52
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=2
# Define iteration number
ni=200

define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=FALSE,
                      poligonit=FALSE)
# Set autumn polygon
xpol=poly.autumn$x
ypol=poly.autumn$y
plot(xpol,ypol,type='l')
coast()

# Grid and plot --------------
#*********************************
# Select variable of interest
vart='SST'
varname='SST'
path.gridsasst=paste(prefix,"WGACEGG/Data/grids/SST_AUTUMN_IBCE/",sep='')

res.SST.IBCE=gridNplot(df=ctd.au,vart=vart,varname=varname,
                   tname='year',xname='long',yname='lat',
                   sname='survey',path.grids=path.gridsasst,
                   p=0.98,spname='type',
                   zfilter="none",newbathy=FALSE,
                   deep=-450,shallow=-50,bstep=450,
                   bcol='grey50',drawlabels=TRUE,
                   lon1=x1,lon2=x2,lat1=42.5,lat2=52)

# Select variable of interest
vart='SSS'
varname='SSS'
path.gridsasss=paste(prefix,"WGACEGG/grids/SSS_AUTUMN_IBCE/",sep='')

res.SSS.IBCE=gridNplot(df=ctd.au,vart=vart,varname=varname,
                       tname='year',xname='long',yname='lat',
                       sname='survey',path.grids=path.gridsasss,
                       p=0.98,spname='type',
                       zfilter="none",newbathy=FALSE,
                       deep=-450,shallow=-50,bstep=450,
                       bcol='grey50',drawlabels=TRUE,
                       lon1=x1,lon2=x2,lat1=42.5,lat2=52)

