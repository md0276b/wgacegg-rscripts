# direct connection: does not work
#-------------------------------------------------------------------------
#library(RCurl)
#gf=getURLContent('https://community.ices.dk/ExpertGroups/wgacegg/2013%20Meeting%20Docs/CRR/grid%20files/AC_ECOCADIZ_GC/AC_ECOCADIZ_GC_SA_ANE.txt',
#                 userpwd = 'doray:md',ssl.verifypeer = FALSE)

# Load packages
#-------------------------------------------------------------------------
library(EchoR)
library(maps)
library(mapdata)
library(fields)
library(SpatialIndices)
library(splancs)

# source blocking functions
#source('/var/svn/trunk/Spatial/Mapping/ACEGG_CRR_script 1_BlockingFunctions+DateTime_bugfixed.r')
# and loads a nice palette
#load("/var/svn/trunk/Spatial/Mapping/gradient_colors.rda")
# load coastline
data(map)

# Define grid limits
#-------------------------------------------------------------------------
x1<-(-10.2); x2<-(-1); y1<-35.8; y2<-48
#x1=-6;x2=-1;y1=43;y2=48
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=2
# Define iteration number
ni=200
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=TRUE)

#check grid and polygon
plot(matxg,matyg,main='Defined grid and polygon',xlab='',ylab='')
lines(xpol,ypol)

# List grid files
#-------------------------------------------------------------------------
pref='R:/'
pref="/run/user/1000/gvfs/smb-share:domain=ifr,server=nantes,share=donnees2,user=mdoray/"
path=paste(pref,'Campagnes/WGACEGG/CRR/Data/gridCRR_20140131/',sep='')
path=paste(pref,'Campagnes/WGACEGG/CRR/Data/gridCRR_20150428/',sep='')
lf=list.files(path,pattern='*.txt')
#write.csv2(lf,paste(path,'lf.txt',sep=''))
#lfs=strsplit(lf,split='_',)
#lfs=strsplit(lf,split='[.]',)
lfs=data.frame(t(data.frame(strsplit(lf,split='[.]'))))
row.names(lfs)=seq(length(lfs[,1]))
names(lfs)=c('pat','ext')
lfs$path=lf

# remove years before 2003 in spring surveys
for (i in 1:dim(lfs)[1]){
  if (substr(lfs$pat[i],1,14)=='AC_SPRING_IBBB'){
    pat=paste(path,lfs$path[i],sep='')
    mat=read.table(file=pat,header=T,sep=";")
    head(mat)
    mats=mat[mat$Year>=2003,]
    write.table(mats,pat,sep=';',row.names=FALSE)
  }
}

# Import files and create maps
#-------------------------------------------------------------------------
for (i in 1:dim(lfs)[1]){
  pat=paste(path,lfs$path[i],sep='')
  pat2=path
  if (substr(lfs$pat[i],4,9)=='JUVENA'){
    shelf=FALSE
  }else{
    shelf=TRUE
  }
  if (substr(lfs$pat[i],1,13)=='DEPM_SAR_IBBB'){
    #lyears=c(1988,1988+3,1988+3*2,1988+3*3,1988+3*4,1988+3*5,1988+3*6,1988+3*7,1988+3*8)
    lyears=NA
  }else{
    lyears=seq(2003,2012,1)
  }  
  # Create annual maps
  #-----------------------------
  resgp=grid.plot(pat=pat,id=as.character(lfs$pat[i]),pat2=pat2,a=0.5,ux11=TRUE,splot=c(.15,.19, .3,.7),
                  namz=NULL,namn=NULL,nams=NULL,
                  newbathy=FALSE,default.grid=TRUE,shelf=shelf,lyears=lyears,
                  deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3)
  
  # Compute mean and stdev maps per file over the series
  #-----------------------------
  mat=read.table(file=pat,header=T,sep=";")
  head(mat)
  #Compute mean and stdev maps over the series
  mmap=aggregate(mat[,c('Zvalue','Nsample')],list(Xgd=mat$Xgd,Ygd=mat$Ygd,I=mat$I,J=mat$J),mean,na.rm=TRUE)
  names(mmap)[5:6]=c("Zvalue",'Nsample')
  sdmap=aggregate(mat$Zvalue,list(Xgd=mat$Xgd,Ygd=mat$Ygd,I=mat$I,J=mat$J),sd,na.rm=TRUE)
  names(sdmap)[5]=c("Zstdev")
  msdmap=merge(mmap,sdmap,by.x=c('Xgd','Ygd','I','J'),by.y=c('Xgd','Ygd','I','J'))
  head(msdmap)
  msdmap$Survey=lfs$pat[i]
  msdmap$Year=paste(min(mat$Year),max(mat$Year),sep='-')
  msdmap=msdmap[order(msdmap$J,msdmap$I),]
  head(msdmap)
  
  write.table(msdmap,paste(path,'meanSDfiles/',lfs$pat[i],'_MeanSD.csv',sep=''),sep=';',row.names=FALSE)
  
  #Plot mean maps over the series
  pat=paste(path,'meanSDfiles/',lfs$pat[i],'_MeanSD.csv',sep='')
  pat2=path
  namz='average'
  nams='standard deviation'
  namn='average'
  
  resgp=grid.plot(pat=pat,pat2=pat2,a=0.5,ux11=TRUE,splot=c(.15,.19, .3,.7),namz=namz,namn=namn,nams=nams,
                  i=paste(min(mat$Year),max(mat$Year),sep='-'),
                  lon1=NULL,lon2=NULL,lat1=NULL,lat2=NULL,resolution=1,newbathy=FALSE,add=TRUE,default.grid=FALSE,shelf=shelf,
                  deep=-450,shallow=-50,bstep=450,bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3)

}

# Create image 2x3 mosaics using imagemagick
#-----------------------------------------

#!!!!! image names must have the same number of elements!!!!!!!!
#!!!!! e.g. AC_JUVENA_IB_SA_ANE_age0 and not AC_JUVENA_IB_SA_ANE
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Tidy up folders and move files
setwd(paste(path,"WGACEGG_CRR_GridMaps_mosaics2x3/",sep=''))
system("rm *.png") #unix
#system("del *.png") #windows
setwd(paste(path,"WGACEGG_CRR_GridMaps_mosaics2x3/Mosaics/",sep=''))
system("rm *.png") #unix
#system("del *.png") #windows
setwd(paste(path,"meanMaps/",sep=''))
system("cp *.png ../WGACEGG_CRR_GridMaps_mosaics2x3")
setwd(paste(path,"stdevMaps/",sep=''))
system("cp *MeanSD*stdev.png ../WGACEGG_CRR_GridMaps_mosaics2x3")

# List grid maps
path2=paste(path,'WGACEGG_CRR_GridMaps_mosaics2x3',sep='')
lf2=list.files(path2,pattern='*.png')
lf2s=data.frame(t(data.frame(strsplit(lf2,split='[.]',))))
row.names(lf2s)=seq(length(lf2s[,1]))
names(lf2s)=c('pat','ext')
lf2s$path=lf2
lf2s$pat=as.character(lf2s$pat)
# Extract ids
lids=rep(NA,dim(lf2s)[1])
for (i in 1:dim(lf2s)[1]){
  if (regexpr('MeanSD',lf2s$pat[i][1])==-1){
    di=regexpr('20',lf2s$pat[i])[1]
    if (di==-1) di=regexpr('19',lf2s$pat[i])[1]
    lids[i]=substr(lf2s$pat[i],1,(di-1))
  }
}
ulids=sort(unique(lids[!is.na(lids)]))
# ulids[ulids=="AC_SPRING_IBBB_Bird_"]="AC_SPRING_IBBB_Bird_2"
# ulids[ulids=="AC_SPRING_IBBB_Mam_"]="AC_SPRING_IBBB_Mam_2"
# ulids[ulids=="AC_JUVENA_IB_SA_"]="AC_JUVENA_IB_SA_2"

#setwd(paste(path,"WGACEGG_CRR_GridMaps/",sep=''))
#system("rm *mosaic*.png")

# Create and export mosaics
setwd(path2)
N=length(ulids)
for (i in 1:N){
  idi=ulids[i]
  ci=paste("montage -geometry 800x800 -tile 2x3 ","'",idi,"'","*.png"," ",idi,"2x3mosaic.png",sep='')
  system(ci)
}

setwd(paste(path,"WGACEGG_CRR_GridMaps_mosaics2x3",sep=''))
system("cp *mosaic*.png ./Mosaics")

# Export mosaics files list
path.mos='/home/mathieubuntu/Documents/Data/WGACEGG_CRR/WGACEGG_CRR_GridMaps/Mosaics/'
lfm=list.files(path.mos,pattern='*.png')
write.table(lfm,paste(path.mos,'MosaicsList.csv',sep=''),sep=';',row.names=FALSE)

# Create image 3x4 mosaics using imagemagick
#-----------------------------------------
# Tidy up folders and move files
setwd(paste(path,"WGACEGG_CRR_GridMaps_mosaics3x4/",sep=''))
system("rm *.png")
setwd(paste(path,"WGACEGG_CRR_GridMaps_mosaics3x4/Mosaics/",sep=''))
system("rm *.png")
setwd(paste(path,"meanMaps/",sep=''))
system("cp *.png ../WGACEGG_CRR_GridMaps_mosaics3x4")
setwd(paste(path,"stdevMaps/",sep=''))
system("cp *MeanSD*stdev.png ../WGACEGG_CRR_GridMaps_mosaics3x4")

# List grid maps
path2=paste(path,'WGACEGG_CRR_GridMaps_mosaics3x4',sep='')
lf2=list.files(path2,pattern='*.png')
lf2s=data.frame(t(data.frame(strsplit(lf2,split='[.]',))))
row.names(lf2s)=seq(length(lf2s[,1]))
names(lf2s)=c('pat','ext')
lf2s$path=lf2
lf2s$pat=as.character(lf2s$pat)
# Extract ids
lids=rep(NA,dim(lf2s)[1])
for (i in 1:dim(lf2s)[1]){
  if (regexpr('MeanSD',lf2s$pat[i][1])==-1){
    di=regexpr('20',lf2s$pat[i])[1]
    if (di==-1) di=regexpr('19',lf2s$pat[i])[1]
    lids[i]=substr(lf2s$pat[i],1,(di-1))
  }
}
ulids=sort(unique(lids[!is.na(lids)]))
# ulids[ulids=="AC_SPRING_IBBB_Bird_"]="AC_SPRING_IBBB_Bird_2"
# ulids[ulids=="AC_SPRING_IBBB_Mam_"]="AC_SPRING_IBBB_Mam_2"
# ulids[ulids=="AC_JUVENA_IB_SA_"]="AC_JUVENA_IB_SA_2"

#setwd(paste(path,"WGACEGG_CRR_GridMaps/",sep=''))
#system("rm *mosaic*.png")

# Create and export mosaics
setwd(path2)
N=length(ulids)
for (i in 1:N){
  idi=ulids[i]
  ci=paste("montage -geometry 800x800 -tile 3x4 ","'",idi,"'","*.png"," ",idi,"3x4mosaic.png",sep='')
  system(ci)
}

setwd(paste(path,"WGACEGG_CRR_GridMaps_mosaics3x4",sep=''))
system("cp *mosaic*.png ./Mosaics")

# Export mosaics files list
path.mos='/home/mathieubuntu/Documents/Data/WGACEGG_CRR/WGACEGG_CRR_GridMaps/Mosaics/'
lfm=list.files(path.mos,pattern='*.png')
write.table(lfm,paste(path.mos,'MosaicsList.csv',sep=''),sep=';',row.names=FALSE)
