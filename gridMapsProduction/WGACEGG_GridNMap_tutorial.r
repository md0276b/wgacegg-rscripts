#*******************************************************************************************************
# TUTORIAL FOR MAKING WGAECGG GRIDMAPS WITH ECHOR

# mathieu.doray@ifremer.fr, 01/12/2021

#*******************************************************************************************************
# 0. Setup ----------
#**********************************************************************
# 1.1. Load packages
library(EchoR)
library(ggplot2)

# 1.2. Set paths 
prefix='C:/Users/mdoray/Documents/'
# Path with polygon files
path.polygrid=paste(prefix,"Code/wgacegg-rscripts/data/polygons/",sep='')
# Output path for gridmaps:
path.grids=paste(prefix,"Temp/grids/",sep='')

#**********************************************************************
# 1. Make gridmaps----------------
#****************************************************
# 1.1. Import polygons from csv file -------------
# (csv file available on WGACEGG sharepoint in 'Data/Polygons' folder)
#********************************
# Polygon for "classic" spring acoustic surveys
WGACEGGspringPolygon.df=
read.table(paste(path.polygrid,'WGACEGGspringPolygonOK.csv',sep=''),
            sep=';',header=TRUE)
# Polygon for "classic" autumn acoustic surveys
WGACEGGautumnPolygon.df=
  read.table(paste(path.polygrid,'WGACEGGautumnPolygonOK.csv',sep=''),
             sep=';',header=TRUE)
# Polygon for "extended" autumn acoustic surveys (with Cardigan Bay)
WGACEGGautumnPolygonCarBay.df=
  read.table(paste(path.polygrid,'WGACEGGautumnPol_CarBay.csv',sep=''),
             sep=';',header=TRUE)

# 1.2. Import raw data ------------
#****************************************************
# Required columns: 
# years in 'year' column
# longitudes in 'x' column
# latitudes in 'y' column
# (unique) survey name in 'survey',column, e.g. "AC_SPRING_IBBB"
# species names in 'sp' columns

# Code below will generate fake raw data for testing:
dfs=NULL
for (i in 1:4){
  x=runif(100,-3,-1)
  y=runif(100,44,46)
  z1=rnorm(100,45,10)
  z2=rnorm(100,45,10)
  # Format input dataframe
  dfi=rbind(data.frame(x=x,y=y,NASC=z1,sp='sptest1',
                 year=2000+i,survey='ctest'),
            data.frame(x=x,y=y,NASC=z2,sp='sptest2',
                       year=2000+i,survey='ctest'))
  dfs=rbind(dfs,dfi) 
}
# use your raw data instead

# NASC mosaic bubble plots
lsp=unique(dfs$sp)
for (i in 1:length(lsp)){
  dfsi=dfs[dfs$sp==lsp[i],]
  x11()
  par(bg='white')
  p1<-ggplot(data = dfsi, 
             aes(x = x, y = y, size = log(NASC+1))) +
    geom_point(col=2,alpha=0.7,pch=1)+
    labs(title = 
           paste(lsp[i],'AC SPRING IBBB NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    facet_wrap(.~factor(year),nrow = 3)
  print(p1)
}

# 1.3. Define grid, polygon and make gridmaps ---------
#****************************************************
# 1.3.1. Select variable of interest
vart='NASC'
varname='NASC'

# 1.3.2. Check species/years in dataset
table(dfs$year,dfs$sp)

# 1.3.3. Define grid, smoothing parameters and polygon ------
# Define grid limits
x1=-10;x2=-1;y1=36;y2=49   # grid for "classic" spring acoustic surveys
x1=-15;x2=-1;y1=35;y2=60   # grid for "extended" spring acoustic surveys (incl. WESPAS)
x1=-10;x2=-1;y1=36;y2=53   # grid for autumn acoustic surveys 
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius (in no. of cells)
u=1
# Define iteration number
ni=200
# Define grid
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni)

# Define polygon:
# 'Classic' Spring surveys:
xpol=WGACEGGspringPolygon.df$long
ypol=WGACEGGspringPolygon.df$lat
# 'Extended' Spring surveys:
xpol=poly.IBBBN[,1]
ypol=poly.IBBBN[,2]
# 'Classic' autumn surveys:
xpol=WGACEGGautumnPolygon.df[,1]
ypol=WGACEGGautumnPolygon.df[,2]
# 'Extended' autumn surveys (with Cardigan Bay):
xpol=WGACEGGautumnPolygonCarBay.df[,1]
ypol=WGACEGGautumnPolygonCarBay.df[,2]

# Plot grid and polygon:
grid0=expand.grid(x = xg,y = yg)
plot(grid0$x,grid0$y,main=paste('Res',ax),xlab='',ylab='')
lines(xpol,ypol,type='l')
coast()

# 1.3.4. Grid and plot per species and year ----
#******************************************
# Define output path (change survey code):
path.grids1=paste(path.grids,"AC_SPRING_IBBB/",sep='')
dir.create(path.grids1,recursive=TRUE)

res1=gridNplot(df=dfs,vart=vart,varname=varname,
          tname='year',xname='x',yname='y',sname='survey',
          path.grids=path.grids1,p=0.98,spname='sp',
          zfilter="quantile",newbathy=FALSE,
          default.grid=FALSE,deep=-450,shallow=-50,bstep=450,
          bcol='grey50',drawlabels=TRUE,cex.labs=2,tcls=-0.3,
          lon1=x1,lon2=x2,lat1=y1,lat2=y2,
          xycor=FALSE,mini=-1,maxi=1,centerit=FALSE)

# 1.3.4.1. Get gridmaps as dataframe ----
# to produce mosaic plots with ggplot package
gmdf=res1$gridDataframe

# Plot grid maps mosaic plots per species.

# 1.3.4.2. Add mean and SD maps ------
gmdf.am=aggregate(
  gmdf[,c('Xsample','Ysample','DateAver','DateStdev',
  'TimeAver','TimeStdev','Nsample','Zvalue','Zstdev')],
  list(Survey=gmdf$Survey,sp=gmdf$sp,I=gmdf$I,J=gmdf$J,
       Xgd=gmdf$Xgd,Ygd=gmdf$Ygd),mean,na.rm=TRUE)
gmdf.m.name=paste(
  'mean.',min(gmdf$Year),'-',
  max(gmdf$Year),sep='')
gmdf.am$Year=gmdf.m.name
names(gmdf.am)
gmdf.asd=aggregate(
  gmdf[,c('Xsample','Ysample','DateAver','DateStdev',
                             'TimeAver','TimeStdev','Nsample','Zvalue','Zstdev')],
  list(Survey=gmdf$Survey,sp=gmdf$sp,I=gmdf$I,
       J=gmdf$J,Xgd=gmdf$Xgd,Ygd=gmdf$Ygd),sd,na.rm=TRUE)
gmdf.sd.name=paste(
  'SD.',min(gmdf$Year),'-',
  max(gmdf$Year),sep='')
gmdf.asd$Year=gmdf.sd.name                                      
gmdf.msd=rbind(gmdf,gmdf.am[,names(gmdf)],gmdf.asd[,names(gmdf)])
unique(gmdf.msd$Year)
gmdf.msd$Xgd=as.numeric(gmdf.msd$Xgd)
gmdf.msd$Ygd=as.numeric(gmdf.msd$Ygd)

gmdf.lsp=unique(gmdf.msd$sp)
gmdf.lyears=unique(gmdf$Year)

# 1.4. Mosaic plots, spring surveys ------------
# 1.4.1. Mean maps mosaic plot -----------
# Mosaic plot with all years (plots saved in path.grids1)
for (i in 1:length(gmdf.lsp)){
  gmdfi=gmdf.msd[
    gmdf.msd$sp==gmdf.lsp[i],]
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = gmdfi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(gmdf.lsp[i],'Survey code')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    scale_fill_gradientn(colours=gradient.colors)+
    facet_wrap(.~factor(Year),nrow = 3)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids1,'gridmapMosaic-',
                            gmdf.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}

# Mosaic plot with last N years
Nlast.years=8
for (i in 1:length(gmdf.lsp)){
  gmdfi=gmdf.msd[
    gmdf.msd$sp==gmdf.lsp[i],]
  gmdfi=gmdfi[
    gmdfi$Year%in%
      c(sort(rev(gmdf.lyears)[1:Nlast.years]),
        gmdf.m.name,
        gmdf.sd.name),]
  unique(gmdfi$Year)
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = gmdfi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(gmdf.lsp[i],'Survey code')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    scale_fill_gradientn(colours=gradient.colors)+
    facet_wrap(.~factor(Year),nrow = 2)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids1,'gridmapMosaicLastYears-',
                            gmdf.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}
# 1.4.2. Anomaly maps ------------
# calculate anomaly maps by substrating mean map:
gmdf.ano=NULL
for (i in 1:length(gmdf.lyears)){
  for (j in 1:length(gmdf.lsp)){
    dfiz=gmdf.msd[
      gmdf.msd$sp==gmdf.lsp[j]&
        gmdf.msd$Year==gmdf.lyears[i],]  
    dfim=gmdf.msd[
      gmdf.msd$sp==gmdf.lsp[j]&
        gmdf.msd$Year%in%
        gmdf.msd$Year[grep(
        'mean',gmdf.msd$Year)],]
    names(dfim)[names(dfim)=='Zvalue']='mZvalue'
    dfizm=merge(dfiz,dfim[,c('I','J','mZvalue')],
                by.x=c('I','J'),by.y=c('I','J'))
    dfizm$Zvalue=dfizm$Zvalue-dfizm$mZvalue
    gmdf.ano=rbind(gmdf.ano,
                                      dfizm)
  }  
}

# Mosaic of anomaly plots with all years:
for (i in 1:length(gmdf.lsp)){
  gmdf.anoi=gmdf.ano[
    gmdf.ano$sp==gmdf.lsp[i],]
  summary(gmdf.anoi$Zvalue)
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = gmdf.anoi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(gmdf.lsp[i],'Survey code')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    #scale_fill_distiller(palette="RdBu")+
    scale_fill_gradient2(low = "blue",
                         mid = "white",
                         high = "red",midpoint = 0)+
    facet_wrap(.~factor(Year),nrow = 3)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids1,'gridmapMosaicAnomaly-',
                            gmdf.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}

# Mosaic of anomaly plots with recent years
Nlast.years.ano=8
for (i in 1:length(gmdf.lsp)){
  gmdf.anoi=gmdf.ano[
    gmdf.ano$sp==gmdf.lsp[i],]
  gmdf.anoi=gmdf.anoi[
    gmdf.anoi$Year%in%
      c(sort(rev(gmdf.lyears)[1:Nlast.years.ano]),
        gmdf.m.name,
        gmdf.sd.name),]
  summary(gmdf.anoi$Zvalue)
  if (dim(gmdf.anoi)[1]>0){
    x11()
    par(bg='white')
    p1<-ggplot() +
      geom_raster(data = gmdf.anoi, 
                  aes(x = Xgd, y = Ygd, fill = Zvalue))+
      labs(title = 
             paste(gmdf.lsp[i],'Survey code')) +
      theme(axis.title.x = element_blank(),
            axis.title.y = element_blank())+
      annotation_map(map_data("world"))+ #Add the map as a base layer before the points
      coord_quickmap()+  #Sets aspect ratio
      #scale_fill_distiller(palette="RdBu")+
      scale_fill_gradient2(low = "blue",
                           mid = "white",
                           high = "red",midpoint = 0)+
      facet_wrap(.~factor(Year),nrow = 2)
    print(p1)
    # Save figure
    dev.print(png, file=paste(path.grids1,
                              'gridmapMosaicAnomalyLastYears-',
                              gmdf.lsp[i],
                              ".png",sep=""), 
              units='cm',width=40, height=30,res=300)
    dev.off()
  }else{
    cat('No data available for',gmdf.lsp[i],'\n')
  }
}
