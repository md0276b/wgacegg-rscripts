---
format: docx
reference-doc: ICES Scientific Reports_chairs template.docx
editor: visual
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE,warning = FALSE,message = FALSE)
prefix='/media/mathieu/EchoSonde/Campagnes/'
#prefix='/home/mathieu/Documents/Campagnes/'
prefix='D:/Campagnes/'
prefix='C:/Users/mdoray/Documents/Campagnes/'
cyear=2024
# Path to results
path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
path.grids.IBBB=paste(prefix,"WGACEGG/Data/grids/AC_SPRING_IBBB/",cyear,"/",sep='')
path.grids.IBBBN=paste(prefix,"WGACEGG/Data/grids/AC_SPRING_IBBBN/",cyear,"/",sep='')
path.grids1=paste(path.grids.IBBB,"Mosaics/",sep='')
path.grids1m=paste(path.grids.IBBB,"AverageMaps/meanMaps/",sep='')
path.grids1sd=paste(path.grids.IBBB,"SDmaps/meanMaps/",sep='')
path.grids1.1=paste(path.grids.IBBB,"LastYears/",sep='')
path.grids2=paste(path.grids.IBBBN,"Mosaics/",sep='')
path.grids2m=paste(path.grids.IBBBN,"AverageMaps/meanMaps/",sep='')
path.grids2sd=paste(path.grids.IBBBN,"SDmaps/meanMaps/",sep='')

```

#### 4.2.1.3. SPF NASC distribution from Acoustic Surveys

NB: PELACUS and PELGAS surveys have been canceled in 2020 due to the COVID pandemic.

##### Anchovy

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 1. Adult anchovy mean acoustic density (NASC, m².NM-²) average map derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}
nfile=list.files(path.grids1m)[grep('ENGR-ENC',list.files(path.grids1m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids1m,nfile,sep=''))

```

Adult anchovy core distribution areas in springtime were, by decreasing order of importance: coastal areas in Southern Bay of Biscay (BoB, in Gironde and Landes coast, \~46°N), the Gulf of Cadiz (GoC, \~37°N), and in a small area North of Cape Mondego on the Western coast of Portugal (\~40°N) (Figure 1).

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 2. Adult anchovy mean acoustic density (NASC, m².NM-²) maps derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids1,'gridmapMosaicLastYears-ENGR-ENC.png',sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 3. Adult anchovy mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids1,'gridmapMosaicAnomalyLastYears-ENGR-ENC.png',sep=''))

```

In `r cyear`, anchovy density was above average in NW BoB and NW Iberia, and below average in the Gironde river mouth and GoC core distribution areas (Figure 2).

##### Sardine

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 4. Adult sardine mean acoustic density (NASC, m².NM-²) average map derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}
nfile=list.files(path.grids1m)[grep('SARD-PIL',list.files(path.grids1m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids1m,nfile,sep=''))

```

Sardine core distribution areas in springtime were, by decreasing order of importance: the coastal areas of the Bay of Biscay, the Gulf of Cadiz (\~37°N), the South Western Portuguese coast, and Biscay shelf-break areas. (Figure 4).

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 5. Adult sardine mean acoustic density (NASC, m².NM-²) maps derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids1,'gridmapMosaicLastYears-SARD-PIL.png',sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 6. Adult sardine mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids1,'gridmapMosaicAnomalyLastYears-SARD-PIL.png',sep=''))

```

In `r cyear`, sardine abundance was above-average on Galician coasts and in the Gironde area, and average elsewhere (Figure 6).

##### Chub mackerel

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 7. Adult chub mackerel mean acoustic density (NASC, m².NM-²) average map derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}
nfile=list.files(path.grids1m)[grep('SCOM-COL',list.files(path.grids1m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids1m,nfile,sep=''))

```

Chub mackerel core distribution area in springtime was located in the Cantabrian Sea and Southern BoB (Figure 6).

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 8. Adult chub mackerel mean acoustic density (NASC, m².NM-²) maps derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids1,'gridmapMosaicLastYears-SCOM-COL.png',sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 9. Adult chub mackerel mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELAGO, PELACUS and PELGAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids1,'gridmapMosaicAnomalyLastYears-SCOM-COL.png',sep=''))

```

In `r cyear`, chub mackerel distribution was above average in a hotspot in NE Cantabrian Sea, close to average in the Cantabrian Sea and BoB, below average in Southern Iberia, and absent in the Bay of Biscay (Figure 9).

##### Boarfish

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 10. Adult boarfish mean acoustic density (NASC, m².NM-²) average map derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}
nfile=list.files(path.grids2m)[grep('CAPR-APE',list.files(path.grids2m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2m,nfile,sep=''))

```

Boarfish core distribution areas were located along the continental shelf edge from 45 to 57°N, with higher concentrations in Northern Bay of Biscay and the Celtic Sea, between 46 and 48°N (Figure 10).

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 11. Adult boarfish mean acoustic density (NASC, m².NM-²) maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}
nfile=list.files(path.grids2)[grep('Zvalue_CAPR-APE',list.files(path.grids2))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2,nfile,sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 12. Adult boarfish mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids2,'gridmapMosaicAnomaly-CAPR-APE.png',sep=''))

```

Boarfish global density has increased since 2020. Higher than average boarfish concentrations were observed in `r cyear` from 45° to 58°N.

##### Herring

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 13. Adult herring mean acoustic density (NASC, m².NM-²) average map derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}
nfile=list.files(path.grids2m)[grep('CLUP-HAR',list.files(path.grids2m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2m,nfile,sep=''))

```

Herring core distribution areas were in the North and South of Ireland, from 50° to 58°N.

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 14. Adult herring mean acoustic density (NASC, m².NM-²) maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}
nfile=list.files(path.grids2)[grep('Zvalue_CLUP-HAR',list.files(path.grids2))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2,nfile,sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 15. Adult herring mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids2,'gridmapMosaicAnomaly-CLUP-HAR.png',sep=''))

```

Herring densities were average in `r cyear`, with low and high density patches in the North and South of Ireland (Figure 15).

##### Blue whiting

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 13. Adult blue whiting  mean acoustic density (NASC, m².NM-²) average map derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}
nfile=list.files(path.grids2m)[grep('MICR-POU',list.files(path.grids2m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2m,nfile,sep=''))

```

Blue whiting was scattered in the Celtic Sea, with higher density areas near the shelf edge in the North West of Spain and in the West of Scotland (57-58°N).

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 14. Adult blue whiting mean acoustic density (NASC, m².NM-²) maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

nfile=list.files(path.grids2)[grep('Zvalue_MICR-POU',list.files(path.grids2))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2,nfile,sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 15. Adult blue whiting mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids2,'gridmapMosaicAnomaly-MICR-POU.png',sep=''))

```

Blue whiting densities were average in `r cyear`, except for high density patches South to Ireland and in the North-Western BoB.

##### Atlantic mackerel

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 16. Adult Atlantic mackerel mean acoustic density (NASC, m².NM-²) average map derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

nfile=list.files(path.grids2m)[grep('SCOM-SCO',list.files(path.grids2m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2m,nfile,sep=''))

```

Atlantic mackerel core distribution areas were in the Cantabrian Sea and in the Center of the Bay of Biscay, up to 48°N.

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 17. Adult Atlantic mackerel mean acoustic density (NASC, m².NM-²) maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

nfile=list.files(path.grids2)[grep('Zvalue_SCOM-SCO',list.files(path.grids2))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2,nfile,sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 18. Adult Atlantic mackerel mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids2,'gridmapMosaicAnomaly-SCOM-SCO.png',sep=''))

```

Atlantic mackerel densities were average in all areas in `r cyear`, but in Southern BoB where it was below average and above average in Western Cantabrian Sea.

##### Sprat

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 19. Adult sprat mean acoustic density (NASC, m².NM-²) average map derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

nfile=list.files(path.grids2m)[grep('SPRA-SPR',list.files(path.grids2m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2m,nfile,sep=''))

```

Sprat core distribution areas were onshore in the Bay of Biscay (45-47°N) and in Scotland (57-58°N).

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 20. Adult sprat mean acoustic density (NASC, m².NM-²) maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

nfile=list.files(path.grids2)[grep('Zvalue_SPRA-SPR',list.files(path.grids2))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2,nfile,sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 21. Adult sprat mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids2,'gridmapMosaicAnomaly-SPRA-SPR.png',sep=''))

```

In `r cyear`, sprat densities were below average in core distribution areas in the Bay of Biscay, and above average North to Ireland and in Northern Scotland.

##### Horse mackerel

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 22. Adult horse mackerel mean acoustic density (NASC, m².NM-²) average map derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

nfile=list.files(path.grids2m)[grep('TRAC-TRU',list.files(path.grids2m))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2m,nfile,sep=''))

```

Horse mackerel was scattered on the continental shelf fro 43 to 55°N, with higher density patches in the Celtic Sea.

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 23. Adult horse mackerel mean acoustic density (NASC, m².NM-²) maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

nfile=list.files(path.grids2)[grep('Zvalue_TRAC-TRU',list.files(path.grids2))]
nfile=nfile[grep(cyear,nfile)]

knitr::include_graphics(paste(path.grids2,nfile,sep=''))

```

```{r, echo=FALSE, out.width="100%",fig.cap = "Figure 24. Adult horse mackerel mean acoustic density (NASC, m².NM-²) anomaly maps derived from the PELACUS, PELGAS and WESPAS surveys, 0.25° map cell."}

knitr::include_graphics(paste(path.grids2,'gridmapMosaicAnomaly-TRAC-TRU.png',sep=''))

```

In `r cyear`, horse mackerel distribution was close to average, with a higher density patches in the Celtic Sea and Northern Scotland.
