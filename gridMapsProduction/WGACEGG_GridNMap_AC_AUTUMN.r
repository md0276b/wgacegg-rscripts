# 8.1. Load packages
#****************************************************
library(EchoR)
library(raster)
library(ggplot2)
library(reshape2)
library(sf)

#**************************************************************
# 2. Autumn surveys----
#**************************************************************
# Create paths
prefix='/media/mathieu/EchoSonde/Campagnes/'
prefix='D:/Campagnes/'

path.raw=paste(prefix,"WGACEGG/Data/rawData/SA/",sep='')
path.grids=paste(prefix,"WGACEGG/Data/grids/",sep='')
path.grids3=paste(path.grids,"AC_AUTUMN_IBCE/",sep='')
path.grids3.1=paste(path.grids,"AC_AUTUMN_IBCE/LastYears/",sep='')
dir.create(path.grids3.1)

#****************************************************
# 0.1. Species table ---------------
#****************************************************
PT.sp=data.frame(country='PT',lsp=c("PIL","ANE","MAS","HOM","BOG"))
PT.sp$sp=c('SARD-PIL','ENGR-ENC','SCOM-COL','TRAC-TRU','BOOP-BOO')
FR.sp=data.frame(country='FR',lsp=c('CAPR-APE','CLUP-HAR','ENGR-ENC','MERL-MCC','MERL-MNG',
                                    'MICR-POU','SARD-PIL','SCOM-COL','SCOM-SCO','SPRA-SPR',
                                    'TRAC-MED','TRAC-TRU'))
FR.sp$sp=FR.sp$lsp
SP.sp=data.frame(country='SP',lsp=c("MICRO-POU","MICR-POU","SCOM-SCO","MERL-MER","TRAC-TRA","SARD-PIL","TRAC-PIC",
                                    "BOOP-BOO","SCOM-COL","CAPR-APE","MAUR-MUE","ENGR-ENC","MEGA-NOR",
                                    "TRAC-MED"),
                 sp=c("MICR-POU","MICR-POU","SCOM-SCO","MERL-MCC","TRAC-TRU","SARD-PIL","TRAC-PIC",
                      "BOOP-BOO","SCOM-COL","CAPR-APE","MAUR-MUE","ENGR-ENC","MEGA-NOR",
                      "TRAC-MED"))                 
sp.acegg=rbind(PT.sp,FR.sp,SP.sp)

# 2.1. Basque autumn fish NASC----
#**************************************************************
#path.EU='/home/mathieu/Documents/WGACEGG/grids/rawData/'
path.EU=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/old/',sep='')

feu=list.files(paste(path.EU,sep=''),pattern='.txt')
#feu=list.files(path.EU,pattern='*_corr.txt')
feu.df=data.frame(t(data.frame(strsplit(feu,split='_'))))
years=substr(feu.df[,2],7,10)
for (i in 1:length(feu)){
  dfi=read.table(paste(paste(path.EU,'',sep=''),feu[i],sep=''),
                 sep='\t',header=TRUE)
  dfi$year=years[i]
  if (i==1) {
    sa.eu=dfi
  }else{
    sa.eu=rbind(sa.eu,dfi)
  }  
}
names(sa.eu)
names(dfi)
unique(sa.eu$sp)
sa.eu$sp=as.character(sa.eu$sp)
unique(sa.eu$sp)
table(sa.eu$sp,sa.eu$year)
AC_JUVENA_IB=sa.eu

#sa.eu=read.table(path.EU,sep='\t',header=TRUE)
head(sa.eu)
head(dfi)
unique(sa.eu$year)

# Adds 2015, 2016
path.EU.2015.1=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2015_ane_nodup.txt',sep='')
path.EU.2015.2=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2015_ane_nodup.txt',sep='')
path.EU.2015.3=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2015_pil_nodup.txt',sep='')
df.eu15.1=read.table(path.EU.2015.1,sep='\t',header=TRUE)
head(df.eu15.1)
plot(df.eu15.1$long,df.eu15.1$lat,
     cex=0.1+log(df.eu15.1$sa.ane.juv+1)/10,
     main='JUV')
plot(df.eu15.1$long,df.eu15.1$lat,
     cex=0.1+log(df.eu15.1$sa.ane.adul+1)/10,col=2,
     main='ADUL')
coast()
df.eu15.1=df.eu15.1[order(df.eu15.1$lat,df.eu15.1$long),]
tot=df.eu15.1$sa.ane.juv+df.eu15.1$sa.ane.adul
plot(tot,df.eu15.1$sa.tot)
summary(tot-df.eu15.1$sa.tot)

df.eu15.1=df.eu15.1[,c(1:6,9),]
df.eu15.1$sp='ENGR-ENC'
names(df.eu15.1)[7]='NASC'

df.eu15.2=read.table(path.EU.2015.2,sep='\t',header=TRUE)
head(df.eu15.2)
df.eu15.2=df.eu15.2[,1:7]
df.eu15.2$sp='ENGR-JUV'
names(df.eu15.2)[7]='NASC'
df.eu15.3=read.table(path.EU.2015.3,sep='\t',header=TRUE)
head(df.eu15.3)
df.eu15.3=df.eu15.3[,1:7]
df.eu15.3$sp='SARD-PIL'
names(df.eu15.3)[7]='NASC'
df.eu15=rbind(df.eu15.1,df.eu15.2,df.eu15.3)
head(df.eu15)
boxplot(NASC~sp,df.eu15)
aggregate(df.eu15$NASC,list(df.eu15$sp),summary)
unique(df.eu15$sp)
unique(df.eu15$Survey)
unique(df.eu15$year)
unique(df.eu15$NASC)
df.eu15$date.time=paste(df.eu15$date,df.eu15$hour)
unique(df.eu15$date.time)
df.eu15$date.time=as.character(df.eu15$date.time)
names(df.eu15)[1]='survey'
names(sa.eu)
names(df.eu15)
# checks
unique(nchar(df.eu15$date.time))
df.eu15$date.time[nchar(df.eu15$date.time)<11]
df.eu15[is.na(df.eu15$date),]
df.eu15$id=paste(df.eu15$year,df.eu15$date.time,df.eu15$sp,
                 df.eu15$long,df.eu15$lat)
dupid15=df.eu15[duplicated(df.eu15$id),'id']
dup15=df.eu15[df.eu15$id%in%dupid15,]
dup15=dup15[order(dup15$id),]
dim(dup15)

path.EU.2016.1=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2016_ane_nodup.txt',sep='')
path.EU.2016.2=paste(prefix,'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2016_pil_nodup.txt',sep='')
df.eu16.1=read.table(path.EU.2016.1,sep='\t',header=TRUE)
head(df.eu16.1)
df.eu16.2=read.table(path.EU.2016.2,sep='\t',header=TRUE)
head(df.eu16.2)
df.eu16=rbind(df.eu16.1,df.eu16.2)
df.eu16$date.time=paste(df.eu16$date,df.eu16$hour)
df.eu16$date.time=as.character(df.eu16$date.time)
names(df.eu16)
# checks
unique(nchar(df.eu16$date.time))
df.eu16$date.time[nchar(df.eu16$date.time)<11]
df.eu16[is.na(df.eu16$date),]
df.eu16$id=paste(df.eu16$year,df.eu16$date.time,df.eu16$sp,
                 df.eu16$long,df.eu16$lat)
dupid17=df.eu16[duplicated(df.eu16$id),'id']
dup16=df.eu16[df.eu16$id%in%dupid17,]
dup16=dup16[order(dup16$id),]
dim(dup16)
#write.table(dup17,paste(prefix,
#                      'WGACEGG/grids/rawData/JUVENA/pb/','JUVENA17duplicated.txt',sep=''))

# add 2017
path.EU.2017.1=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2017_ANEADUL_nodup.txt',sep='')
path.EU.2017.2=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2017_ANEJUV_nodup.txt',sep='')
path.EU.2017.3=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2017_PIL_nodup.txt',sep='')
df.eu17.1=read.table(path.EU.2017.1,sep='\t',header=TRUE)
head(df.eu17.1)
df.eu17.1=df.eu17.1[,1:7]
df.eu17.1$sp='ENGR-ENC'
names(df.eu17.1)[7]='NASC'
df.eu17.2=read.table(path.EU.2017.2,sep='\t',header=TRUE)
head(df.eu17.2)
df.eu17.2=df.eu17.2[,1:7]
df.eu17.2$sp='ENGR-JUV'
names(df.eu17.2)[7]='NASC'
df.eu17.3=read.table(path.EU.2017.3,sep='\t',header=TRUE)
head(df.eu17.3)
df.eu17.3=df.eu17.3[,1:7]
df.eu17.3$sp='SARD-PIL'
names(df.eu17.3)[7]='NASC'
df.eu17=rbind(df.eu17.1,df.eu17.2,df.eu17.3)
df.eu17$date.time=paste(df.eu17$date,df.eu17$hour)
df.eu17$date.time=as.character(df.eu17$date.time)
names(df.eu17)=c("survey","year","long","lat","date","hour",
                 "NASC","sp","date.time")
# checks
unique(nchar(df.eu17$date.time))
df.eu17$date.time[nchar(df.eu17$date.time)<11]
df.eu17[is.na(df.eu17$date),]
df.eu17$id=paste(df.eu17$year,df.eu17$date.time,df.eu17$sp,
                 df.eu17$long,df.eu17$lat)
dupid17=df.eu17[duplicated(df.eu17$id),'id']
dup17=df.eu17[df.eu17$id%in%dupid17,]
dup17=dup17[order(dup17$id),]
dim(dup17)

# add 2018
path.EU.2018.1=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2018_ANE_PIL_long.txt',sep='')
df.eu18.1=read.table(path.EU.2018.1,sep='\t',header=TRUE)
head(df.eu18.1)
names(df.eu18.1)[6]='hour'
unique(df.eu18.1$sp)
df.eu18.1.juv=df.eu18.1[df.eu18.1$sp=='ENGR_ENC_JUV',]
df.eu18.1.all=df.eu18.1[df.eu18.1$sp=='ENGR_ENC',]
df.eu18.1.adu=df.eu18.1[df.eu18.1$sp=='ENGR_ENC_ADULT',]
plot(df.eu18.1.juv$long,df.eu18.1.juv$lat,cex=df.eu18.1.juv$NASC/10000)
coast()
plot(df.eu18.1.all$long,df.eu18.1.all$lat,cex=df.eu18.1.all$NASC/10000)
coast()
plot(df.eu18.1.adu$long,df.eu18.1.adu$lat,cex=df.eu18.1.adu$NASC/10000)
coast()
df.eu18.1$date=paste(substr(df.eu18.1$date,7,8),'/',
                     substr(df.eu18.1$date,5,6),'/',
                     substr(df.eu18.1$date,1,4),sep='')
df.eu18.1$date.time=paste(df.eu18.1$date,df.eu18.1$hour)
unique(df.eu18.1$sp)
df.eu18=df.eu18.1[df.eu18.1$sp%in%c('ENGR_ENC_JUV','ENGR_ENC_ADULT','SARD_PIL'),]
head(df.eu18)
df.eu18$date.time

# add 2019
path.EU.2019.1=paste(prefix,
                     'WGACEGG/Data/rawData/SA/JUVENA/NASC_JUVENA2019_ANE_PIL_long.txt',sep='')
df.eu19.1=read.table(path.EU.2019.1,sep='\t',header=TRUE)
head(df.eu19.1)
names(df.eu19.1)[6]='hour'
unique(df.eu19.1$sp)
df.eu19.1.juv=df.eu19.1[df.eu19.1$sp=='ENGR_ENC_JUV',]
df.eu19.1.all=df.eu19.1[df.eu19.1$sp=='ENGR_ENC',]
df.eu19.1.adu=df.eu19.1[df.eu19.1$sp=='ENGR_ENC_ADULT',]
plot(df.eu19.1.juv$long,df.eu19.1.juv$lat,cex=df.eu19.1.juv$NASC/10000)
coast()
plot(df.eu19.1.all$long,df.eu19.1.all$lat,cex=df.eu19.1.all$NASC/10000)
coast()
plot(df.eu19.1.adu$long,df.eu19.1.adu$lat,cex=df.eu19.1.adu$NASC/10000)
coast()
df.eu19.1$date=paste(substr(df.eu19.1$date,9,10),'/',
                     substr(df.eu19.1$date,6,7),'/',
                     substr(df.eu19.1$date,1,4),sep='')
df.eu19.1$date.time=paste(df.eu19.1$date,df.eu19.1$hour)
unique(df.eu19.1$sp)
df.eu19=df.eu19.1[df.eu19.1$sp%in%c('ENGR_ENC_JUV','ENGR_ENC_ADULT','SARD_PIL'),]
head(df.eu19)
df.eu19$date.time

#write.table(dup17,paste(prefix,
#                      'WGACEGG/grids/rawData/JUVENA/pb/','JUVENA17duplicated.txt',sep=''))

# binds all JUVENA data
sa.eu$date.time=as.character(sa.eu$date.time)
df.eu17$date=as.character(df.eu17$date)
sa.eu=rbind(df.eu15[,names(sa.eu)],df.eu16[,names(sa.eu)],
            df.eu17[,names(sa.eu)],df.eu18[,names(sa.eu)],df.eu19[,names(sa.eu)])
# sa.eu=rbind(df.eu16[,names(sa.eu)],
#             df.eu17[,names(sa.eu)])
unique(nchar(sa.eu$date.time))
head(sa.eu)
# correct species names
sa.eu[sa.eu$sp=='ENGR_ENC_ADULT','sp']='ENGR-ENC'
sa.eu[sa.eu$sp=='ENGR_ENC_JUV','sp']='ENGR-JUV'
sa.eu[sa.eu$sp=='SARD_PIL','sp']='SARD-PIL'

#Select Basque data 
head(sa.eu)
df.EU=sa.eu[,c('survey','year','date.time','long','lat','NASC','sp')]
head(df.EU)
df.EU[is.na(df.EU$year),]
unique(df.EU$year)
unique(df.EU$sp)
unique(df.EU$long)
unique(df.EU$lat)
AC_JUVENA_IB=df.EU

head(df.EU)
AC_JUVENA_IBs=df.EU[df.EU$year%in%c(2015:2019),]
unique(AC_JUVENA_IBs$year)
unique(AC_JUVENA_IBs$sp)
unique(AC_JUVENA_IBs$long)
unique(AC_JUVENA_IBs$lat)
dim(AC_JUVENA_IBs)
dim(unique(AC_JUVENA_IBs))
# check for duplicated rows
head(AC_JUVENA_IBs)
AC_JUVENA_IBs$id=paste(AC_JUVENA_IBs$year,AC_JUVENA_IBs$date.time,AC_JUVENA_IBs$sp,
                       AC_JUVENA_IBs$long,AC_JUVENA_IBs$lat)
dupideu=AC_JUVENA_IBs[duplicated(AC_JUVENA_IBs$id),'id']
dupeu=AC_JUVENA_IBs[AC_JUVENA_IBs$id%in%dupideu,]
dupeu=dupeu[order(dupeu$id),]
dim(dupeu)
#write.table(dup17,paste(prefix,
#                      'WGACEGG/grids/rawData/JUVENA/pb/','JUVENA17duplicated.txt',sep=''))
table(dupeu$year)
# checks
unique(nchar(AC_JUVENA_IBs$date.time))
AC_JUVENA_IBs$date.time[nchar(AC_JUVENA_IBs$date.time)<12]
AC_JUVENA_IBs[is.na(AC_JUVENA_IBs$date),]
AC_JUVENA_IBs[is.na(AC_JUVENA_IBs$date),]

names(AC_JUVENA_IBs)=c("survey","year","time","x","y","NASC","sp","id")

# check for duplicates 
# 
# sa.eus=sa.eu[,c('Survey','year','Nesdu','time','long','lat','sa.ane.juv','sa.tot','sa.ane.adul')]
# names(sa.eus)=c('survey','year','Nesdu','time','x','y','ENGR-JUV','ENGR-TOT','ENGR-ENC')
# years=unique(sa.eus$year)
# head(sa.eus)
# length(sa.eus$time);length(unique(sa.eus$time))
# #sa.eus[duplicated(sa.eus$time),'time']
# sa.eus$id=paste(sa.eus$time,sa.eus$x,sa.eus$y)
# dus=sa.eus[sa.eus$time%in%sa.eus$time[duplicated(sa.eus$time)],]
# table(dus$year)
# dus=dus[order(dus$time),]
# dus11=dus[dus$year==2011,]
# dus12=dus[dus$year==2012,]
# plot(dus$x,dus$y)
# table(sa.eus$year)
# years2=years[5:10]
# dus2=sa.eus[sa.eus$id%in%sa.eus$id[duplicated(sa.eus$id)],]
# dim(sa.eus)
# dim(dus2)
# adus2=aggregate(dus2[,c('ENGR-JUV','ENGR-TOT','ENGR-ENC')],list(dus2$id),sum)
# names(adus2)=c('id','ENGR-JUV','ENGR-TOT','ENGR-ENC')
# dim(adus2)
# sa.eus2=rbind(sa.eus[!sa.eus$id%in%dus2$id,c('id','ENGR-JUV','ENGR-TOT','ENGR-ENC')],adus2)
# dim(sa.eus2)
# dus3=sa.eus2[sa.eus2$id%in%sa.eus2$id[duplicated(sa.eus2$id)],]
# # no more duplicated ESDUs
# df.EU=reshape(sa.eus2,varying=list(2:4),v.names = "NASC",
#                idvar = c('id'),
#                direction = "long",times=names(sa.eus2)[2:4],
#                timevar='sp')
# row.names(df.EU)=seq(dim(df.EU)[1])
# head(df.EU)
# dim(df.EU)
# AC_JUVENA_IB=merge(df.EU,unique(sa.eus[,c('id','survey','year','time','x','y')]),by.x='id',by.y='id')
# dim(AC_JUVENA_IB)
# head(AC_JUVENA_IB)
# AC_JUVENA_IB$survey='AC_JUVENA_IB'

# 2.2. UK autumn fish NASC: PELTIC---------------
#***********************************************
path.UK=paste(prefix,'WGACEGG/Data/rawData/SA/PELTIC/',sep='')

fuk=list.files(path.UK,pattern='peltic*')
fuk.df=data.frame(t(data.frame(strsplit(fuk,split='_'))))
years=as.numeric(as.character(paste('20',substr(fuk.df[,1],7,10),sep='')))
for (i in 1:length(fuk)){
  dfi=read.table(paste(path.UK,fuk[i],sep=''),sep=',',header=TRUE)
  dfi$year=years[i]
  if (i==1) {
    sa.uk=dfi
  }else{
    sa.uk=rbind(sa.uk,dfi)
  }  
}
unique(sa.uk$sp)
sa.uk$sp=as.character(sa.uk$sp)
head(sa.uk)
unique(sa.uk$year)
sa.uk$date.time=paste(sa.uk$date,sa.uk$time)
names(sa.uk)[names(sa.uk)=='x']='long'
names(sa.uk)[names(sa.uk)=='y']='lat'
names(sa.uk)[names(sa.uk)=='time']='hour'
df.UK=sa.uk[,names(df.EU)]
df.UK$survey=substr(df.UK$survey,1,6)
names(df.UK)
# Correct species codes
df.UK[df.UK$sp=='ANE','sp']='ENGR-ENC'
df.UK[df.UK$sp=='PIL','sp']='SARD-PIL'
df.UK[df.UK$sp=='SPR','sp']='SPRA-SPR'
df.UK[df.UK$sp=='HER','sp']='CLUP-HAR'
df.UK[df.UK$sp=='HOM','sp']='TRAC-TRU'
df.UK[df.UK$sp=='BOF','sp']='CAPR-APE'
df.UK[df.UK$sp=='WHB','sp']='MICR-POU'
df.UK[df.UK$sp=='HOM','sp']='TRAC-TRU'
df.UK[df.UK$sp=='TRAC-TRA','sp']='TRAC-TRU'

names(df.UK)=c("survey","year","time","x","y","NASC","sp")

# 2.3. Irish herring survey data -------------
#***********************************
path.IRL2=paste(prefix,'WGACEGG/Data/rawData/SA/CSHAS/',sep='')

lfirl2=list.files(path.IRL2)
firl2.df=data.frame(t(data.frame(strsplit(lfirl2,split='_'))))
names(firl2.df)=c('survey','year','var','ext')

for (i in 1:length(lfirl2)){
  df.IRL2=read.table(paste(path.IRL2,lfirl2[i],sep=''),sep=',',header=TRUE)
  head(df.IRL2)
  unique(df.IRL2$Species)
  df0=df.IRL2[df.IRL2$Species==0,]
  df0.1=df0;df0.2=df0;df0.3=df0;df0.4=df0;df0.5=df0
  df0.1$Species='SARD-PIL'
  df0.2$Species='CLUP-HAR'
  df0.3$Species='SPRA-SPR'
  df0.4$Species='ENGR-ENC'
  df0.5$Species='TRAC-TRA'
  df.IRL2.alli=rbind(df.IRL2[df.IRL2$Species=='SARD-PIL',],
                     df.IRL2[df.IRL2$Species=='CLUP-HAR',],
                     df.IRL2[df.IRL2$Species=='SPRA-SPR',],
                     df.IRL2[df.IRL2$Species=='ENGR-ENC',],
                     df.IRL2[df.IRL2$Species=='TRAC-TRA',],
                     df0.1,df0.2,df0.3,df0.4,df0.5)
  df.IRL2.alli$Species=as.character(df.IRL2.alli$Species)
  df.IRL2.alli[df.IRL2.alli$Species=='TRAC-TRA','Species']='TRAC-TRU'
  df.IRL2.alli$date.time=paste(df.IRL2.alli$date,df.IRL2.alli$time)
  names(df.IRL2.alli)[names(df.IRL2.alli)=='time']='hour'
  names(df.IRL2.alli)[names(df.IRL2.alli)=='Species']='sp'
  names(df.IRL2.alli)[names(df.IRL2.alli)=='date.time']='time'
  if (i==1){
    df.IRL2.all=df.IRL2.alli
  }else{
    df.IRL2.all=rbind(df.IRL2.all=df.IRL2.all,df.IRL2.alli)
  }
}  

lsp=unique(df.IRL2.all$sp)
unique(df.IRL2.all$year)
for (i in 1:length(lsp)){
  dfi=df.IRL2.all[df.IRL2.all$sp==lsp[i],]
  plot(dfi$x,dfi$y,cex=0.1+log(dfi$NASC+1)/10,main=lsp[i])
  coast()
}

head(df.IRL2.all)

#****************************************************
# 2.4. Spanish autumn fish NASC ----------
#****************************************************
path.SP2=paste(prefix,'WGACEGG/Data/rawData/SA/IBERAS/IBERASrawNASCgrid2019.csv',
               sep='')

sa.sp2=read.table(path.SP2,sep=',',header=TRUE)
head(sa.sp2)
unique(sa.sp2$sp)
unique(sa.sp2$year)
table(sa.sp2$year)
sa.sp2$sp=as.character(sa.sp2$sp)
#sa.sp2[sa.sp2$sp=='MICRO-POU','sp']='MICR-POU'
#sa.sp2[sa.sp2$sp=='TRAC-TRA','sp']='TRAC-TRU'
sa.sp2=sa.sp2[sa.sp2$sp!="MERL-MER_1",]
unique(sa.sp2$survey)
unique(sa.sp2$sp)
sa.sp2$time=paste(substr(sa.sp2$time,7,8),'/',substr(sa.sp2$time,5,6),'/',substr(sa.sp2$time,1,4),
                  substr(sa.sp2$time,10,18),sep='')

df.SP.autumn=sa.sp2
head(df.SP.autumn)
unique(df.SP.autumn$year)
unique(df.SP.autumn$sp)

# Replace local species name by ACEGG species names
unique(df.SP.autumn$sp)
names(df.SP.autumn)[names(df.SP.autumn)=='sp']='sp0'
df.SP.autumn=merge(df.SP.autumn,sp.acegg[sp.acegg$country=='SP',
                                         c('lsp','sp')],by.x='sp0',by.y='lsp')
unique(df.SP.autumn$sp)
df.SP.autumn=df.SP.autumn[,-1]
head(df.SP.autumn)
df.SP.autumn$survey=paste('IBERAS',df.SP.autumn$year,sep='')
#invert x and y
df.SP.autumn$x0=df.SP.autumn$x
df.SP.autumn$x=df.SP.autumn$y
df.SP.autumn$y=df.SP.autumn$x0

plot(df.SP.autumn$x,df.SP.autumn$y)
coast()

df.SP.autumn[df.SP.autumn$sp=='SCOM-COL',]
head(df.SP.autumn)

# 2.4. Bind autumn surveys data ----
#*****************************************
AC_AUTUMN_IBCE=rbind(AC_JUVENA_IBs[,names(df.UK)],df.UK,df.IRL2.all[,names(df.UK)],
                     df.SP.autumn[,names(df.UK)])

table(AC_AUTUMN_IBCE$year,AC_AUTUMN_IBCE$sp)

AC_AUTUMN_IBCE$surveyComponent=AC_AUTUMN_IBCE$survey
AC_AUTUMN_IBCE$survey='AC_AUTUMN_IBCE'
head(AC_AUTUMN_IBCE)

# Unique sampling points
head(AC_AUTUMN_IBCE)
uxya=unique(AC_AUTUMN_IBCE[,c('year','x','y','surveyComponent')])
plot(uxya$x,uxya$y,asp=1/cos((mean(uxya$x)*pi)/180))
coast()
lines(xpol-0.3,ypol-0.3,col=2)
# Unique sampling points per year
ggplot() +
  geom_point(data = uxya, 
             aes(x = x, y = y),col='blue')+
  theme(axis.title.x = element_blank(),
        axis.title.y = element_blank())+
  #geom_sf(data=sfPoly, fill="transparent",color="red", size=0.25)+
  #scale_fill_continuous(na.value="transparent",type = "viridis")+
  annotation_map(map_data("world"))+ #Add the map as a base layer before the points
  coord_quickmap()+  #Sets aspect ratio
  facet_wrap(.~year,nrow=4)

# 2.4.1. Map ESUs position for current year -------

cyear=2023
uxyas=uxya[uxya$year==cyear,]

x11()
ggplot() +
  geom_point(data = uxyas, 
             aes(x = x, y = y,colour=surveyComponent),size=.1)+
  theme(axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        legend.title=element_blank())+
  ggtitle(paste('WGACEGG autumn joint acoustic survey',cyear))+
  annotation_map(map_data("world"))+ #Add the map as a base layer before the points
  coord_quickmap()+ guides(colour = guide_legend(override.aes = list(size=1)))
# Save figure
dev.print(png, file=paste(path.grids3,
                          'AC-AUTUMN-IBCE_ESUpositions-',
                          cyear,
                          ".png",sep=""), 
          units='cm',width=15, height=20,res=300)

# 2.4.2. Create autumn polygon ------------ 
#x11()
# plot(uxy2a$x,uxy2a$y,asp=1/cos((mean(uxy2a$y)*pi)/180))
# coast()
# #Draw polygon
# #p=locator(type='l')
# #poly.autumn=data.frame(x=p$x,y=p$y)
# #plot(poly.autumn$x,poly.autumn$y,type='l')
# #coast()
# 
# #OR alphahull style
# mcosa=cos(uxy2a$y*pi/180)
# xxa=uxy2a$x*mcosa
# xxa=uxy2a$x
# yya=uxy2a$y
# df=data.frame(xxa,yya)
# plot(xxa,yya)
# coast()
# 
# library(alphahull)
# valpha=.3
# apolya=ahull(xxa,yya,alpha=valpha)
# plot(apolya,main=paste('alpha =',valpha))
# coast()
# 
# # with ahull_track
# apolya.gg <- ahull_track(xxa,yya, alpha=valpha, nps = 3,sc=1)
# 
# dim(apolya.gg[[i]]$data)
# plot(apolya.gg[[i]]$data$x,apolya.gg[[i]]$data$y)
# 
# # get unique tracks
# for(i in 1:length(apolya.gg)) {
#   apolya.gg[[i]]$aes_params$colour <- "green3"
#   tii=apolya.gg[[i]]$data
#   tiis=tii[c(1,dim(tii)[1]),]
#   tiis
#   if (i==1){
#     tiis.db=tiis
#   }else{
#     tiis.db=rbind(tiis.db,tiis)
#   }
# }
# tiis.db=unique(tiis.db)
# plot(tiis.db$x,tiis.db$y)
# coast()
# 
# # plot tracks
# library(ggplot2)
# ggplot(aes(x = xxa, y = yya), data = df)+ 
#   geom_point(aes(x = xxa, y = yya), data = df, alpha = .5, color = "darkred") +
#   apolya.gg
# 
# # identify disjunct polygons
# tiis.db$polygon.id=NA
# tiis.db$di=NA
# pid=1
# dthr=100
# for(i in 1:dim(tiis.db)[1]) {
#   if (i>1){
#     tiis.db[i,'di']=60*sqrt((tiis.db[i-1,'x']-tiis.db[i,'x'])^2+(tiis.db[i-1,'y']-tiis.db[i,'y'])^2)
#     if(tiis.db[i,'di']>dthr){
#       pid=pid+1
#     }
#   }else{
#     tiis.db[i,'di']=NA
#   }
#   tiis.db[i,'polygon.id']=pid
# }  
# unique(tiis.db$polygon.id)  
# summary(tiis.db$di)
# hist(tiis.db$di)
# 
# #tiis.db=rbind(tiis.db[1,],tiis.db,tiis.db[dim(tiis.db)[1],])
# plot(tiis.db$x,tiis.db$y,type='l',col=tiis.db$polygon.id,
#      lty=tiis.db$polygon.id)
# text(tiis.db$x,tiis.db$y,tiis.db$polygon.id)
# coast()

# Create autumn polygons and shapefile 
#***********************************
# library(rgdal)
# lpolys=unique(tiis.db$polygon.id)
# for (i in 1:length(lpolys)){
#   polyixy=tiis.db[tiis.db$polygon.id==lpolys[i],]
#   #polyi=Polygon(polyixy[,c('x','y')])
#   Srsi = Polygons(list(Polygon(polyixy[,c('x','y')])), paste("s",i,sep=''))
#   #assign(paste('poly',i,sep='.'),Srsi,envir=.GlobalEnv)
#   if (i==1){
#     lSrsi=list(Srsi)
#   }else{
#     lSrsi=c(lSrsi,Srsi)
#   }
# }
# 
# SpP = SpatialPolygons(lSrsi, seq(length(lSrsi)))
# SpP.df <- SpatialPolygonsDataFrame(SpP, 
#                                    data.frame(id=seq(length(lSrsi)), 
#                                               row.names=row.names(SpP)))
# plot(SpP)
# plot(SpP.df)
# coast()
# 
# # union polygons
# # Generate IDs for grouping
# upoly.id=rep(1,length(lSrsi))
# 
# # Merge polygons by ID
# SpP.union <- unionSpatialPolygons(SpP, upoly.id)
# 
# # Plotting
# plot(oregon)
# plot(SpP.union, add = FALSE, border = "red", lwd = 2)
# 
# # Export file as shape file
# #***********
# path.polygrid=paste(path.grids,'polygons/',sep='')
# dir.create(path.polygrid)
# path.polygrida=paste(path.grids,'polygons/WGACEGGautumnPolygon.shp',sep='')
# proj4string(SpP.df)="+proj=longlat +datum=WGS84"
# writeOGR(obj=SpP.df, dsn=path.polygrida,layer='WGACEGGautumnPolygon', 
#          driver='ESRI Shapefile',overwrite_layer=TRUE)
# 
# # Import dissolved polygon as shapefile and convert it to dataframe
# #********************************
# path.polygrida2=paste(path.grids,'polygons/WGACEGGautumnPolygonOK.shp',sep='')
# WGACEGGautumnPolygon=readOGR(dsn=path.polygrida2,layer='WGACEGGautumnPolygonOK')
# plot(WGACEGGautumnPolygon)
# coast()
# # export as text file
# write.table(WGACEGGautumnPolygon.df,paste(path.polygrid,'WGACEGGautumnPolygonOK.csv',sep=''),
#             sep=';',row.names=FALSE)
# 
# library(ggplot2)
# WGACEGGautumnPolygon.df=fortify(WGACEGGautumnPolygon)
# head(WGACEGGautumnPolygon.df)
# 
# plot(WGACEGGautumnPolygon.df$long,WGACEGGautumnPolygon.df$lat,type='l')
# coast()

# OR 2.4.2. Import new autumn polygon -----
#********************************
path.polygrida2.1=paste(path.grids,'polygons/WGACEGGautumnPol_CarBay.csv',
                        sep='')
WGACEGGautumnPolygon.df2=read.table(path.polygrida2.1,sep=';',
                                    header=TRUE)
head(WGACEGGautumnPolygon.df2)

path.polygrida2.2=paste(path.grids,'polygons/WGACEGGautumnPol_GC_CarBay.shp',
                        sep='')
WGACEGGautumnPolygon.shp3=read_sf(path.polygrida2.2)
plot(WGACEGGautumnPolygon.shp3)
WGACEGGautumnPolygon.df3=as.data.frame(st_coordinates(WGACEGGautumnPolygon.shp3))

#****************************************
# 2.5. Gridmaps of autumn surveys fish NASC------
#*******************************************
# Optionnaly, import formatted raw data
AC_AUTUMN_IBCE=read.csv2(paste(path.raw,'AC_AUTUMN_IBCE_NASC.csv',sep=''),
                          sep=';',header=TRUE)
head(AC_AUTUMN_IBCE)
summary(AC_AUTUMN_IBCE$x)
summary(AC_AUTUMN_IBCE$NASC)
unique(AC_AUTUMN_IBCE$sp)

### load data
# Select variable of interest
vart='NASC'
varname='NASC'

# 8.3.1. Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
#*************************************
# Define grid limits
# x1=-10.2;x2=0;y1=35.8;y2=53  # old grid < 2021
x1=-10;x2=-1;y1=36;y2=53      # new grid > 2021
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
#u=2                             # old smoothing parameter < 2021
u=1                             # new smoothing parameter > 2021
# Define iteration number
ni=200
# Define grid
define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=FALSE,
                      poligonit=FALSE)
# Set autumn polygon
xpol=WGACEGGautumnPolygon.df3$X
ypol=WGACEGGautumnPolygon.df3$Y
# Check grid and polygon
grid0=expand.grid(x = xg,y = yg)
plot(grid0$x,grid0$y,main=paste('Res',ax))
lines(xpol,ypol,type='l')
coast()

# Grid and plot 
#*********************************
#
AC_AUTUMN_IBCEs=AC_AUTUMN_IBCE[AC_AUTUMN_IBCE$sp%in%c('ENGR-ENC','SARD-PIL'),]

res.IBCE=gridNplot(df=AC_AUTUMN_IBCE,vart=vart,varname=varname,
                   tname='year',xname='x',yname='y',sname='survey',
                   path.grids=path.grids3,p=0.98,spname='sp',
                   zfilter="quantile",newbathy=FALSE,
                   deep=-450,shallow=-50,bstep=450,
                   bcol='grey50',drawlabels=TRUE,
                   lon1=x1,lon2=x2,lat1=y1,lat2=y2,
                   xycor=FALSE,mini=-1,maxi=1,
                   centerit=FALSE)

names(res.IBCE)
AC.AUTUMN.IBCE.gridmaps=res.IBCE$gridDataframe
head(AC.AUTUMN.IBCE.gridmaps)
sort(unique(AC.AUTUMN.IBCE.gridmaps$Year))
table(AC.AUTUMN.IBCE.gridmaps$Year,AC.AUTUMN.IBCE.gridmaps$sp)
AC.AUTUMN.IBCE.lsp=unique(AC.AUTUMN.IBCE.gridmaps$sp)

# Save gridmaps data frame ----
write.csv2(
  AC.AUTUMN.IBCE.gridmaps,
  paste(path.grids3,'AC_AUTUMN_IBCE_gridmaps.csv',sep=''))

# 1.7.6. Make mean and SD maps and mosaic plots ------------
path.mosaics3=paste(path.grids3,'/Mosaics/',sep='')
dir.create(path.mosaics3,recursive = TRUE)
path.mosaics3m=paste(path.grids3,
                     '/AverageMaps/',sep='')
dir.create(path.mosaics3m,recursive = TRUE)
path.mosaics3sd=paste(path.grids3,
                      '/SDmaps/',sep='')
dir.create(path.mosaics3sd,recursive = TRUE)

head(AC.AUTUMN.IBCE.gridmaps)

AC.AUTUMN.IBCE.lsp=unique(AC.AUTUMN.IBCE.gridmaps$sp)
AC.AUTUMN.IBCE.lsp=c(
  "ENGR-ENC","ENGR-JUV","SARD-PIL","SPRA-SPR","TRAC-TRU","SCOM-SCO",
  "MAUR-MUE","MICR-POU","CAPR-APE",
  "BOOP-BOO","SCOM-COL","TRAC-PIC","TRAC-MED")

for (i in 1:length(AC.AUTUMN.IBCE.lsp)){
  AC.AUTUMN.IBCE.gridmapsi=
    AC.AUTUMN.IBCE.gridmaps[
      AC.AUTUMN.IBCE.gridmaps$sp==AC.AUTUMN.IBCE.lsp[i],]
  print(AC.AUTUMN.IBCE.lsp[i])
  mat=grid.plot(pat=AC.AUTUMN.IBCE.gridmapsi,
                input='gridDataframe',ux11=TRUE,
                pat2=path.mosaics3,
                id=AC.AUTUMN.IBCE.lsp[i],
                namz='',nams='',namn='',
                deep=-450,shallow=-50,bstep=450,
                bcol='grey50',drawlabels=TRUE,
                default.grid=FALSE,bathy.plot=FALSE,
                plotit=list(zmean=TRUE,zstd=TRUE,
                            nz=TRUE,
                            plot1=FALSE,mosaic=TRUE),
                ggnrow = 2)
  # Plot mean and SD maps
  gmdf.msd=mat$gmdf.msd.meanZ$gmdf.msd
  unique(gmdf.msd$Year)
  gmdf.msd.mean=gmdf.msd[grep('mean',gmdf.msd$Year),]
  gmdf.msd.sd=gmdf.msd[grep('SD',gmdf.msd$Year),]
  # Mean map
  resi=grid.plot(pat=gmdf.msd.mean,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='Average NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics3m,
                 id=paste(AC.AUTUMN.IBCE.lsp[i],'meanZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE),xlim=NULL,ylim=NULL)
  # SD map
  resi=grid.plot(pat=gmdf.msd.sd,input='gridDataframe',ux11=TRUE,
                 bathy.plot=TRUE,namz='SD NASC',nams='SD NASC',
                 namn='No of samples',pat2=path.mosaics3sd,
                 id=paste(AC.AUTUMN.IBCE.lsp[i],'sdZ',sep="_"),
                 plotit=list(zmean=TRUE,zstd=FALSE,nz=FALSE,plot1=TRUE,
                             mosaic=FALSE))
  if (i==1){
    AC.AUTUMN.IBCE.gridmaps.zmsd=
      mat$gmdf.msd.meanZ$gmdf.msd
  }else{
    AC.AUTUMN.IBCE.gridmaps.zmsd=rbind(
      AC.AUTUMN.IBCE.gridmaps.zmsd,
      mat$gmdf.msd.meanZ$gmdf.msd)
  }
}

head(AC.AUTUMN.IBCE.gridmaps.zmsd)
# Save gridmaps data frame
write.table(
  AC.AUTUMN.IBCE.gridmaps.zmsd,
  paste(path.grids3,'AC_SPRING_IBBBN_gridmapsZmeanSD.csv',
        sep=''),sep=';',row.names = FALSE)

unique(AC.AUTUMN.IBCE.gridmaps$Year)
unique(AC.AUTUMN.IBCE.gridmaps.zmsd$Year)

# 1.7.7. Anomaly maps ------------
AC.AUTUMN.IBCE.lyears=unique(AC.AUTUMN.IBCE.gridmaps$Year)
AC.AUTUMN.IBCE.gridmaps.ano=NULL
for (i in 1:length(AC.AUTUMN.IBCE.lyears)){
  for (j in 1:length(AC.AUTUMN.IBCE.lsp)){
    dfiz=AC.AUTUMN.IBCE.gridmaps.zmsd[
      AC.AUTUMN.IBCE.gridmaps.zmsd$sp==AC.AUTUMN.IBCE.lsp[j]&
        AC.AUTUMN.IBCE.gridmaps.zmsd$Year==AC.AUTUMN.IBCE.lyears[i],]  
    dfim=AC.AUTUMN.IBCE.gridmaps.zmsd[
      AC.AUTUMN.IBCE.gridmaps.zmsd$sp==AC.AUTUMN.IBCE.lsp[j]&
        AC.AUTUMN.IBCE.gridmaps.zmsd$Year%in%
        AC.AUTUMN.IBCE.gridmaps.zmsd$Year[grep(
          'mean',AC.AUTUMN.IBCE.gridmaps.zmsd$Year)],]
    names(dfim)[names(dfim)=='Zvalue']='mZvalue'
    dfizm=merge(dfiz,dfim[,c('I','J','mZvalue')],
                by.x=c('I','J'),by.y=c('I','J'))
    dfizm$Zvalue=dfizm$Zvalue-dfizm$mZvalue
    AC.AUTUMN.IBCE.gridmaps.ano=rbind(
      AC.AUTUMN.IBCE.gridmaps.ano,dfizm)
  }  
}
dim(AC.AUTUMN.IBCE.gridmaps.ano)
dim(AC.AUTUMN.IBCE.gridmaps)

for (i in 1:length(AC.AUTUMN.IBCE.lsp)){
  AC.AUTUMN.IBCE.gridmaps.anoi=AC.AUTUMN.IBCE.gridmaps.ano[
    AC.AUTUMN.IBCE.gridmaps.ano$sp==AC.AUTUMN.IBCE.lsp[i],]
  summary(AC.AUTUMN.IBCE.gridmaps.anoi$Zvalue)
  x11()
  par(bg='white')
  p1<-ggplot() +
    geom_raster(data = AC.AUTUMN.IBCE.gridmaps.anoi, 
                aes(x = Xgd, y = Ygd, fill = Zvalue))+
    labs(title = 
           paste(AC.AUTUMN.IBCE.lsp[i],'AC SPRING IBBBN NASC')) +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_blank())+
    annotation_map(map_data("world"))+ #Add the map as a base layer before the points
    coord_quickmap()+  #Sets aspect ratio
    #scale_fill_distiller(palette="RdBu")+
    scale_fill_gradient2(low = "blue",
                         mid = "white",
                         high = "red",midpoint = 0)+
    facet_wrap(.~factor(Year),ncol = 5)
  print(p1)
  # Save figure
  dev.print(png, file=paste(path.grids3,'Mosaics/gridmapMosaicAnomaly-',
                            AC.AUTUMN.IBCE.lsp[i],
                            ".png",sep=""), 
            units='cm',width=40, height=30,res=300)
  dev.off()
}




# Optionally, list and bind grid files and convert to rasters objects-----------
#*********************************************************
lffssda=list.files(path.gridsa,pattern='*.txt')
#lfs=strsplit(lf,split='[.]',)
lffssdas=data.frame(t(data.frame(strsplit(lffssda,split='[.]'))))
lp=as.character(lffssdas[,1])
lffssdas2=data.frame(t(data.frame(strsplit(lp,split='_'))))
row.names(lffssdas2)=seq(length(lffssdas2[,1]))
lffssdas=cbind(lffssdas,lffssdas2)
head(lffssdas)
names(lffssdas)=c('pat','ext','method','sp','var','filter')
lffssdas$path=lffssda
row.names(lffssdas)=seq(length(lffssdas[,1]))
#lffssdas=lffssdas[lffssdas$sp!='CLUP-HAR',]
#lffssdas=lffssdas[lffssdas$sp=='SCOM-COL',]
lffssdas=lffssdas[!lffssdas$sp%in%c('MICR-POU','Other','BOOP-BOO',
                                    'MEGA-NOR','MERL-MCC','TRAC-PIC',
                                    'SCOM-COL'),]

# Import files and bind maps
#************************************
lyears=2015:2019

for (i in 1:dim(lffssdas)[1]){
  pat=paste(path.gridsa,lffssdas$path[i],sep='')
  mati=read.table(file=pat,header=T,sep=";")
  mati$sp=lffssdas$sp[i]
  #select surveys in lyears
  mati=mati[mati$Year%in%lyears,]
  head(mati)
  mati$Survey='AC_AUTUMN_IBCE'
  if (i==1){
    FishNASCSp.AC_AUTUMN_IBCE=mati
  }else{
    FishNASCSp.AC_AUTUMN_IBCE=rbind(FishNASCSp.AC_AUTUMN_IBCE,mati)
  }
  # convert gridfiles to raster stack format, compute mean and SD maps and plot everything:
  resras=grid2rasterStack(pat=mati,path1=path.gridsa,
                          varid=paste(lffssdas$sp[i],
                                      lffssdas$var[i],sep=' '),
                          anomaly=TRUE)
}  

# Autumn surveys CTD data ------
#***********************************
path.ctd.eu=paste(prefix,
                  '/WGACEGG/grids/rawData/JUVENA/CTD/CTD_Peltic_Juvena_2017.txt',sep='')
ctd.eu=read.table(path.ctd.eu,header=TRUE)
head(ctd.eu)
names(ctd.eu)=c('survey','lat','long','SST','SSS')
ctd.eu$year=2017
ctd.eu$type='CTD'
path.ctd.uk='Q:/Meetings/WGACEGG/WGACEGG2017/Data/Cend 19_17_SAIV.csv'  
ctd.uk=read.table(path.ctd.uk,header=TRUE,sep=',')
head(ctd.uk)
ctd.uks=ctd.uk[,c('Long_dec','Lat_dec','Surface_T','Surface_S')]
ctd.uks$Survey='PELTIC2017'
names(ctd.uks)=c('long','lat','SST','SSS','survey')
ctd.uks$year=2017
ctd.uks$type='CTD'
ctd.au=rbind(ctd.eu,ctd.uks[,names(ctd.eu)])
head(ctd.au)

plot(ctd.au$long,ctd.au$lat)
coast()

#****************************************
# Gridmaps of autumn surveys SST&SSS------
#*******************************************
# 8.3.1. Define grid and mask (default: WGACEGG08 GRID and MASK: generic, no change values for xg,yg,ni,u)
#*************************************
# Define grid limits
x1=-10.2;x2=-1;y1=35.8;y2=52
# Define cell dimensions
ax=0.25;ay=0.25
# Define smoothing radius
u=2
# Define iteration number
ni=200

define.grid.poly.mask(x1,x2,y1,y2,ax,ay,u,ni,shelf=FALSE,
                      poligonit=FALSE)
# Set autumn polygon
xpol=poly.autumn$x
ypol=poly.autumn$y
plot(xpol,ypol,type='l')
coast()

# Grid and plot --------------
#*********************************
# Select variable of interest
vart='SST'
varname='SST'
path.gridsasst=paste(prefix,"WGACEGG/Data/grids/SST_AUTUMN_IBCE/",sep='')

res.SST.IBCE=gridNplot(df=ctd.au,vart=vart,varname=varname,
                       tname='year',xname='long',yname='lat',
                       sname='survey',path.grids=path.gridsasst,
                       p=0.98,spname='type',
                       zfilter="none",newbathy=FALSE,
                       deep=-450,shallow=-50,bstep=450,
                       bcol='grey50',drawlabels=TRUE,
                       lon1=x1,lon2=x2,lat1=42.5,lat2=52)

# Select variable of interest
vart='SSS'
varname='SSS'
path.gridsasss=paste(prefix,"WGACEGG/grids/SSS_AUTUMN_IBCE/",sep='')

res.SSS.IBCE=gridNplot(df=ctd.au,vart=vart,varname=varname,
                       tname='year',xname='long',yname='lat',
                       sname='survey',path.grids=path.gridsasss,
                       p=0.98,spname='type',
                       zfilter="none",newbathy=FALSE,
                       deep=-450,shallow=-50,bstep=450,
                       bcol='grey50',drawlabels=TRUE,
                       lon1=x1,lon2=x2,lat1=42.5,lat2=52)

